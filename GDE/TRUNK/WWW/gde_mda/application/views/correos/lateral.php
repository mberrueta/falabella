<body>

	<div id="contenedor" class="width_full">
	<header id="header">
		<hgroup>
			<h1 class="site_title">
			</h1>
			<h2 class="section_title">
			Gestor de Eventos
			<span><?php echo anchor('supervisor/c_menu_supervisor','Menú Supervisor'); ?></span>
			</h2>
			<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
		</hgroup>
	</header>
	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $this->session->userdata('nombre'); ?></p>
		</div>
	</section>

	<aside id="sidebar" class="column">
			<h3>GESTIÓN DE EVENTOS</h3>
			<ul class="toggle">
					<!--<li><?php echo anchor('supervisor/c_bitacora','Bitácora de eventos'); ?></li>
					<li><?php echo anchor('supervisor/c_resumen_estadistico','Resumen estadístico'); ?></li>-->
			</ul>
			
			<h3>ADMINISTRACIÓN</h3>
			<ul class="toggle">
					<li><?php echo anchor('supervisor/c_crud_cliente','Mantenedor de Relaciones'); ?></li>
					<li><?php echo anchor('supervisor/c_crud_operador','Mantenedor de Operadores'); ?></li>
					<!--<li><?php echo anchor('supervisor/c_crud_analista','Mantenedor de Analistas'); ?></li>
					<li><?php echo anchor('supervisor/c_crud_boleta','Mantenedor de Escal. Boletas'); ?></li>
					<li><?php echo anchor('supervisor/c_crud_event_seg','Mantenedor de Escal. Evento'); ?></li>-->
			</ul>
			
			<footer>
				<hr />
				<p><strong>2015 &copy; Kudaw</strong></p>
			</footer>
	</aside>
