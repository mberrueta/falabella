<?php $this->load->view("/analista/header");?>
<?php $this->load->view("/analista/lateral");?>
		<section id="main" class="column" style="width:75% !important">
			<article class="module width_full">
				<header>
					<h3>Mantenedor de Nodos</h3>
					<label id="count" style="float: right;margin-top: 10px;font-weight: bold;">0 nodos modificados</label>
				</header>
				<div id="cont" style="margin-top:20px; text-align: left !important;">
					<div id="menu">
						Buscar: <input id="search_field" type="text" ></input>
						<input id="exportar" type="image" src="../../images/mantenedor/exportar.png" style="margin-left: 50px; margin-top: -15px;" >
						<input id="validar" type="image" src="../../images/mantenedor/guardar.png" style="float: right; margin-right: 10px; margin-top: -15px;" >
						<input id="deshacer" type="image" src="../../images/mantenedor/deshacer.png" style="float: right; margin-right: 20px; margin-top: -15px;" >
						<input id="reemplazar" type="image" src="../../images/mantenedor/reemplazar.png" style="float: right; margin-right: 20px; margin-top: -15px;" >
					</div>
					<div class="row">
						<div id="tablaNodos" class="col-md-11" style="height:550px; overflow:scroll; top:10px;"></div>
					</div>
				</div>
			</article>
		</section>
		<section id="main" class="column" style="width:10% !important">
			<article class="module width_full">
				<header>
					<h3>Clientes</h3>
				</header>
				<div id="cont" style="margin-top:20px; text-align: left !important;">
					<div class="row">
						<div id="filtros" class="col-md-1">
							<div id="lstClientes" style="height:580px; overflow:scroll">
								
							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
		<div style="clear:both;"></div>
	</div>
	
	
	<div class="modal fade" id="impedimentoOperacional" title="Impedimento Operacional">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p><b>No pueden guardarse cambios con nodos repetidos</b></p>
					<p>Favor revisar el listado de nodos e intentar guardar nuevamente</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="avisoOperacional" title="Aviso Operacional">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p><b id="avisoText"></b></p>
					<p>¿Desea guardar de todas maneras?</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="modal_exportar" title="Aviso para exportar">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>¿Desea exportar los nodos asociados a su usuario?</p><!--<p>(No sera aplicado el filtro para exportar)</p>-->
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="impedimentoCliente" title="Impedimento Operacional">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p><b>No pueden guardarse cambios con Clientes que no esten asociados al Analista o se encuentran vacios</b></p>
					<p id="detalle_cliente"></p>
					<p>Favor revisar el listado de nodos e intentar guardar nuevamente</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="modal_loading" title="Realizando cambios">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>Los cambios estan siendo realizados, por favor espere.</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="modal_reemplazar" title="Reemplazar en columna">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<table>
						<tr>
							<td>Buscar:</td><td><input align="right" id="texto_a"></input></td>
						</tr>
						<tr>
							<td>Reemplazar:</td><td><input align="right" id="texto_b"></input></td>
						</tr>
						<tr>
							<td>Columna:</td>
							<td><select id="columna_remp" >
								<option value="2">CODIGO DE SERVICIO</option>
								<option value="3">IP_PRINCIPAL</option>
								<option value="4">C/S_PRINCIPAL</option>
								<option value="5">CODIGO ADMIN</option>
								<option value="6">REGION</option>
								<option value="7">PROVINCIA</option>
								<option value="8">COMUNA</option>
								<option value="9">SUCURSAL</option>
								<option value="10">FW</option>
								<option value="11">PE</option>
								<option value="12">CRITICIDAD</option>
								<option value="13">SLA</option>
								<option value="14">TELNET_USER</option>
								<option value="15">TELNET_PASS</option>
								<option value="16">TELNET_ENAB_PASS</option>
								<option value="17">SNMP</option>
								<option value="18">Contacto_Habil</option>
								<option value="19">Telefono_Habil</option>
								<option value="20">Celular_Habil</option>
								<option value="21">Correo_Habil</option>
								<option value="22">Contacto_No_Habil</option>
								<option value="23">Telefono_No_Habil</option>
								<option value="24">Celular_No_Habil</option>
								<option value="25">Correo_No_Habil</option>
								<option value="26">Horario_Habil</option>
							</select></td>
						</tr>
					</table>
					<progress id="progress" style="width: 200px;" value="0" max="550"></progress>
					<p id="resultado"></p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/mantenedor/jquery.handsontable.full.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/mantenedor/mantenedor.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/mantenedor/jquery.fileDownload.js"></script>
</body>
</html>