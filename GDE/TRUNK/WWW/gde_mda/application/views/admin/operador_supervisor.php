<?php $this->load->view("/admin/header");?>
<?php $this->load->view("/admin/lateral");?>

	<section id="main" class="column">

		<article class="module width_full">
			<header>
				<h3>Asignacion operador supervisor</h3>
			</header>
			
				<table width="100%">
					<tr>
						<td>
							<span class="multipleSelectBoxControl multipleSelectBoxDiv">Supervisores</span>
							<br><?php 
							$js = 'id="supervisores" style="width:200px;"';
							echo form_dropdown('supervisores', $supervisores, '',$js);
							?>
						</td>
						<td><br>
							<select name="myselect[]" id="OpeAsignados" class="multiselect" size="6" multiple="true" style="width:200px">
							</select>
						</td>
						<td><br>
							<?php 
							$js = 'id="OpSinSup" multiple="true" style="width:33%" class="multiselect"';
							echo form_dropdown('OpSinSup', $OpSinSup, '',$js);
							?>
						</td>
					</tr>
					<tr>
						<td colspan="3"><input type="button" value="Guardar" id="guarda_aso"></td>
					</tr>
				</table>
		
		</article>
	</section>

<div id="msgDialog"><p></p></div>
	
	<div style="clear:both;"></div>
	</div> <!-- contenedor -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/operador_supervisor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/multi_select.js"></script>
<style type="text/css">
.multipleSelectBoxControl span{	/* Labels above select boxes*/
	font-family:arial;
	font-size:11px;
	font-weight:bold;
}
.multipleSelectBoxControl div select{	/* Select box layout */
	font-family:arial;
	height:100%;
}
.multipleSelectBoxControl input{	/* Small butons */
	width:25px;	
}

.multipleSelectBoxControl div{
	float:left;
}
	
.multipleSelectBoxDiv
</style>
<script type="text/javascript">
	createMovableOptions("OpeAsignados","OpSinSup",500,200,'Operadores asignados','Operadores sin supervisor');
</script>
</body>
</html>