<?php

class Usuario extends CI_Model {

	function login($user, $pass){
		
		$results = Array();
		$results['tip'] = NULL;
		$retults['nom'] = NULL;
		
		$this->db->trans_start();
		
			$this->db->select('user_category');
			$this->db->from('usuario_gde');
			$this->db->where('user',$user);
			$this->db->where('user_pass',$pass);
			$result = $this->db->get();
		
			if($result->num_rows() == 1){
				$tipo = $result->row()->user_category;
				$result->free_result();
				
				if( $tipo == 1 || $tipo == 4){
					$this->db->select('name');
					$this->db->select('last_name_a');
					$this->db->from('operador_gde');
					$this->db->where('user',$user);
					
					$result = $this->db->get();

					$results['nom'] = $result->row()->name.' '.$result->row()->last_name_a;
					$results['tip'] = $tipo;
				}
				elseif( $tipo == 2 ) {
					
					if ($user == "admin"){
						$results['nom'] = 'Administrador';
						$results['tip'] = $tipo;
					} else {
						$this->db->select('name');
						$this->db->select('last_name_a');
						$this->db->from('supervisor_gde');
						$this->db->where('user',$user);
						
						$result = $this->db->get();

						$results['nom'] = $result->row()->name.' '.$result->row()->last_name_a;
						$results['tip'] = $tipo;
					}
				} else {
					$this->db->select('name');
					$this->db->select('last_name_a');
					$this->db->from('analista_gde');
					$this->db->where('user',$user);
					
					$result = $this->db->get();

					$results['nom'] = $result->row()->name.' '.$result->row()->last_name_a;
					$results['tip'] = $tipo;
					
				}
				// elseif( $tipo == 2){
					// $results['nom'] = 'Administrador';
					// $results['tip'] = $tipo;
				// }
				// elseif( $tipo == 4){
					// $this->db->select('name');
					// $this->db->select('last_name_a');
					// $this->db->from('operador_gde');
					// $this->db->where('user',$user);
					
					// $result = $this->db->get();

					// $results['nom'] = $result->row()->name.' '.$result->row()->last_name_a;
					// $results['tip'] = $tipo;
				// }
				// else{
					// $results['nom'] = 'Estadisticas';
					// $results['tip'] = $tipo;
				// }
			}
			
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
			$results['nom'] = 'ERROR';
			$results['tip'] = 'ERROR';
			return $results;
		}
		else{
			return $results;
		}
	}
	
	// ADAPTACION CIM GDE
	public function getTypeOfUser( $username ){
	
		$result = array( 'stt' => 'OK', 'msg' => '' );
	
		$this->db->select('user_category');
		$this->db->from('gestor_eventos_rsp.usuario_gde');
		$this->db->where('user', $username);
		$responseObject = $this->db->get();
		
		if( $this->db->_error_number() > 0 ){
			
			log_message('error', sprintf('USER="%s", FUNCTION="%s"', $username, __FUNCTION__) );
			log_message('error', $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());
			
			$result['stt']		= 'ERR';
			$result['msg']	= 'Se presentado un problema en la consulta, favor intente nuevamente. Si el problema persiste contactar con administrador.';
		}
		
		else{
			
			$result['stt']		= 'OK';
			$result['msg']	= $responseObject->row()->user_category;
		}
		
		return $result;
	}
	
	function getAll(){
	
		$sql = "SELECT operador_gde.user as USU_USER,name AS OPE_NOM,last_name_a AS OPE_APP,last_name_b AS OPE_APM,annexed AS OPE_ANE,phone AS OPE_TEL,usuario_gde.user_mail AS USU_EMAIL,usuario_gde.user_category FROM operador_gde LEFT JOIN usuario_gde ON operador_gde.user = usuario_gde.user UNION SELECT supervisor_gde.user,name,last_name_a,last_name_b,annexed,phone,usuario_gde.user_mail,usuario_gde.user_category FROM supervisor_gde LEFT JOIN usuario_gde ON supervisor_gde.user = usuario_gde.user UNION SELECT analista_gde.user,name,last_name_a,last_name_b,annexed,phone,usuario_gde.user_mail,usuario_gde.user_category FROM analista_gde LEFT JOIN usuario_gde ON analista_gde.user = usuario_gde.user";
		
		$query = $this->db->query($sql);
		if( $query->num_rows() > 0 ) {
			return $query->result();
		} 
		else {
			return array();
		}
	}
}

/* End of file usuario.php */
/* Location: ./application/models/usuario.php */