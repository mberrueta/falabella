<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>GDE -> Operador</title>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="Sat, 26 Jul 1997 05:00:00 GMT">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/gde_entel.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/operador.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ie.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/cambiar_estado.js" type="text/javascript"></script>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="twelve columns">
			<div class="row">
				<div class="twelve columns">
					<header id="header">
						<hgroup>
							<h1 class="site_title">
							</h1>
							<h2 class="section_title">
								Gestor de Eventos ENTEL v3.3
								<div id="estado_actual">
									<span id="state-now"></span>
								</div>
							</h2>
							<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
						</hgroup>
					</header>
				</div>
			</div>
			<div class="row">
				<section id="secondary_bar">
						<!--<div class="four columns ">
							<div class="user">
								<p><?php echo $this->session->userdata('nombre'); ?> <button id="changeSTT">Cambiar estado</button></p>
							</div>
						</div>-->
						<div class="four columns">
							<div id="title_menu_header" class="menu_header row" style="width:230px">
								<div id="sem_menu_header" class="sem_menu_header_green two columns" >
									0
								</div>
								<div id="usr_menu_header" class="eight columns" >
									<?php echo $this->session->userdata('nombre'); ?>
								</div>
								<div id="sel_menu_header" class="two columns" ></div>
							</div>
							
							<div id="options_menu_header" class="menu_header row" style="width:215px">
								<div class="twelve columns">
									<div id="displayCambiarStt" class="option_menu_header row" >Cambiar Estado</div>
									<div id="displayCorreoSeg" class="option_menu_header row" >Seguimiento Correo</div>
								</div>
							</div>
						</div>
						<div class="eight columns ">
							<dl class="sub-nav" style="margin-top: 5px; margin-left: 15px;">
								<dt>Navegación:</dt>
								<dd><a href="<?php echo base_url(); ?>index.php/operador/c_principal"> Eventos </a></dd>
								<dd class="active"><a href="<?php echo base_url(); ?>index.php/operador/c_secundario"> Estadísticas </a></dd>
								<dd><a href="<?php echo base_url(); ?>index.php/operador/c_ticket"> Ticket </a></dd>
							</dl>
						</div>
				</section>
			</div>
		</div>
	</div>
	
	
	<!-- Cuerpo de gestor-->
	<div class="row">
		<div class="twelve columns">
			<div id="contenedor_tablaMasivo">

			</div>
			
			<div id="tabs">
				<ul>
					<li><a href="#tabs-0">Falla Masiva - Geográfico</a></li>
					<li><a href="#tabs-1">Falla Masiva - Cliente por Nodo</a></li>
					<li><a href="#tabs-2">Falla Masiva - Cliente por Sucursal</a></li>
					<li><a href="#tabs-3">Estado GENERAL nodos GDE</a></li>
				</ul>
				<div id="tabs-0">
					<iframe id="iframe-0" src="<?php echo $url2;?>" frameborder="0" width="100%" height="600px"></iframe>
				</div>
				<div id="tabs-1">
					<iframe id="iframe-1" src="" frameborder="0" width="100%" height="600px"></iframe>
				</div>
				<div id="tabs-2">
					<iframe id="iframe-2" src="" frameborder="0" width="100%" height="600px"></iframe>
				</div>
				<div id="tabs-3">
					<iframe id="iframe-3" src="" frameborder="0" width="100%" height="960px"></iframe>
				</div>
			</div>
			
		</div>
	</div>

	<div id="cambiar_estado">
		<form id="datos_cambio_turno">
			<fieldset>
				<table>
					<tbody>
						<tr><td><input type="radio" name="estado" value="0"/></td><td>Trabajando</td></tr>
						<tr><td><input type="radio" name="estado" value="1"/></td><td>Receso</td></tr>
						<!-- <tr><td><input type="radio" name="estado" value="2"/></td><td>Cambio de turno</td></tr> -->
					</tbody>
				</table>
			</fieldset>
		</form>
	</div>
	
	<div id="dialog-confirm">
		<span style="float:left; margin:0 7px 20px 0;">Esta a punto de cambiar a estado:</span><span id="state" style="color: red"></span>
	</div>
	
	<div id="dialog-confirmarProcesado">
		<p>¿Está seguro de realizar la siguiente acción?</p>
	</div>
	
	<div id="dialog-procesado">
	</div>
	
	<div id="dialog-cturno">
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
	
		setInterval(function () {
			$.ajax({
				url: "../c_seguimientoEmail/cantidadSeguimientosActivos",
				success: function(data){
					$('#sem_menu_header').html(data);
					if (data == 0){
						$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_green');
					}
					else{
						$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_red');
					}
				}
			});
		},90000);
	});

	oTable = $('#caida').dataTable({
						"sPaginationType": "full_numbers",
						"bJQueryUI": true,
						"sScrollY": "400px",
						"iDisplayLength" : 25,
						"oLanguage": {
							"sUrl": "../../js/spanish.txt"  // Idioma de la tabla
						},
					});
	
		// Despliegue de menu en header
	$('#sel_menu_header').live('click', function(){
		$('#options_menu_header').slideToggle("fast");
	});
	
	$('#displayCorreoSeg').click(function(){
		window.open('../c_seguimientoEmail', '', 'width=1300, height=600, location=no');
	});
	
	$(function() {
		var aIFrames = ['<?php echo $url2; ?>','<?php echo $url3; ?>','<?php echo $url4; ?>','<?php echo $url5; ?>'];
		$( "#tabs" ).tabs({
			selected: 0,
			spinner: 'Retrieving data...',
			select: function(e, ui){
				var index=ui.index;
				// if(index > 0){
					$('iframe').attr('src','');
					$('#iframe-'+index).attr('src',aIFrames[index]);
				// }
			}
		});
	});

</script>
</body>
</html>