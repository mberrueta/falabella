<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_supervisor extends CI_Controller {

	private $form_rules = array();

	public function __construct()
	{
		parent::__construct();
		
		// Set form rules in one place so we can re-use them in regular and ajax validation
		$this->new_sup = array(
				array(
					'field'   => 'nom_sup',
					'label'   => 'Nombre',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'app_sup',
					'label'   => 'Apellido',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'apm_sup',
					'label'   => 'Apellido',
					'rules'   => 'min_length[3]'
				),
				array(
					'field' => 'nom_usu_sup',
					'label' => 'Nombre de usuario',
					'rules' => 'required|min_length[6]|is_unique[usuario.USU_USER]'
				),
				array(
					'field' => 'pass_usu_sup',
					'label' => 'Password supervisor',
					'rules' => 'required'
				),
				array(
					'field' => 'email_usu_sup',
					'label' => 'E-mail',
					'rules' => 'required|valid_email'
				),
				array(
					'field' => 'anexo_sup',
					'label' => 'Anexo',
					'rules' => 'required|min_length[4]|max_length[4]|numeric'
				),
				array(
					'field' => 'tel_sup',
					'label' => 'Telefono',
					'rules' => 'required'
				)
				
		);
		
		$this->update_sup = array(
				array(
					'field'   => 'edit_nom_sup',
					'label'   => 'Nombre supervisor',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'edit_app_sup',
					'label'   => 'Apellido supervisor',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field' => 'edit_nom_usu_sup',
					'label' => 'Nombre de usuario',
					'rules' => 'required|min_length[6]|is_unique[usuario.USU_USER]'
				),
				array(
					'field' => 'edit_pass_usu_sup',
					'label' => 'Password supervisor',
					'rules' => ''
				),
				array(
					'field' => 'edit_pass_usu_sup2',
					'label' => 'Password supervisor',
					'rules' => 'matches[edit_pass_usu_sup]'
				),
				array(
					'field' => 'edit_email_usu_sup',
					'label' => 'E-mail',
					'rules' => 'required|valid_email'
				),
				array(
					'field' => 'edit_anexo_sup',
					'label' => 'Anexo',
					'rules' => 'required|min_length[4]|max_length[4]|numeric'
				),
				array(
					'field' => 'edit_tel_sup',
					'label' => 'Telefono',
					'rules' => 'required'
				)
		);

		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model( 'supervisor' );
	}

	public function index()
	{	
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$data['titulo_pagina'] = 'Funciones supervisor';
		$json_new_sup = $this->form_validation->jquery_options($this->new_sup, site_url('admin/C_crud_supervisor/valida_new_sup'));
		$data['validacion_nuevo_sup'] = $json_new_sup;
		$json_update_sup = $this->form_validation->jquery_options($this->update_sup, site_url('admin/C_crud_supervisor/valida_update_sup'));
		$data['validacion_actualizar_sup'] = $json_update_sup;
		$this->load->view('admin/v_crud_supervisor',$data);
	}
	
	public function valida_new_sup()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		// Set JSON headers, no cache
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');

		$field_name = key($_POST);
		
		// Load validation rules set in constructor
		$this->form_validation->set_rules($this->new_sup);

		// For JSON response, we don't want error delimiters
		$this->form_validation->set_error_delimiters('', '');

		// run_single() is a method extended from core validation library
		if ($this->form_validation->run_single($field_name) == FALSE)
		{
			$response = validation_errors();
		}
		else
		{
			$response = TRUE;
		}

		echo json_encode($response);
	}
	
	public function valida_update_sup()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		// Set JSON headers, no cache
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');

		$field_name = key($_POST);
		
		// Load validation rules set in constructor
		$this->form_validation->set_rules($this->update_sup);

		// For JSON response, we don't want error delimiters
		$this->form_validation->set_error_delimiters('', '');

		// run_single() is a method extended from core validation library
		if ($this->form_validation->run_single($field_name) == FALSE)
		{
			$response = validation_errors();
		}
		else
		{
			$response = TRUE;
		}

		echo json_encode($response);
	}
	
	public function read()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		echo json_encode( $this->supervisor->getAll() );
	}
	
	public function edit(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		/* Codigo de estado:
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el supervisor
		*/
		if( !empty( $_POST ) ) {
			$variables = $_POST;
			if(($variables['edit_pass_usu_sup'] != "")  && ($variables['edit_pass_usu_sup2'] != "") && ($variables['edit_pass_usu_sup'] == $variables['edit_pass_usu_sup2'])){
				$respuesta = $this->supervisor->edit($variables,true);
			}
			else{
				$respuesta = $this->supervisor->edit($variables);
			}
			echo $respuesta;
		}
		else{
			echo "1";
		}
	}
	
	public function getByUser($user)
	{	
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		if( isset( $user ) )
			echo json_encode( $this->supervisor->getByuser( $user ) );
	}
	
	public function delete(){
		/* Codigo de estado:
			0 = OK
			1 = Error borrando el usuario
			2 = Tiene operadores asignados
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if(isset($_POST['usuarios'])){
			$respuesta = "";
			foreach($_POST['usuarios'] as $user){
				$respuesta = $respuesta."".$this->supervisor->delete( $user );
			}
			echo $respuesta;
		}
	}
	
	public function create(){
		/* Codigo de estado:
		   -1 = Error inesperado
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el supervisor
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if( !empty( $_POST ) ) {
			$variables = $_POST;
			$respuesta = $this->supervisor->create($variables);
			echo $respuesta;
		}
		else{
			echo "1";
		}
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2){
			return true;
		}
		else{
			return false;
		}
	}
}