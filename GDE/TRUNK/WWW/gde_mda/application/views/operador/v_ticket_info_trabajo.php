<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Informacion de Trabajo</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/gen_validatorv4.js" type="text/javascript"></script>
</head>
<body>
	
	<div class="row">
		<div class="twelve columns">
			<fieldset>
				<legend>Agregar informacion de Trabajo</legend>
				<div class="container">
					<div class="six columns">
						<label for="estado">Estado: </label>
						<input type="text" class="unselectable" name="estado" readonly="readonly" 
							value="<?php echo $_POST["estado"]?>"
						/>
						<label for="estado">Codigo de Servicio: </label>
						<input type="text" class="unselectable" name="cod_servicio" readonly="readonly"
							value="<?php echo $_POST["cod_servicio"]?>"
						/>
						<label for="estado">Motivo de Estado: </label>
						<input type="text" class="unselectable" name="mot_estado" readonly="readonly"
							value="<?php echo $_POST["motivo"]?>"
						/>
						<label for="estado">Fecha Creacion: </label>
						<input type="text" class="unselectable" name="fecha_crea" readonly="readonly"
							value="<?php echo $_POST["fecha_creacion"]?>"
						/>
						<label for="estado">Fecha Modificacion de Estado: </label>
						<input type="text" class="unselectable" name="fecha_mod" readonly="readonly"
							value="<?php echo $_POST["fecha_modificacion"]?>"
						/>
						<label for="estado">Resumen: </label>
						<input type="text" class="unselectable" name="resumen" readonly="readonly"
							value="<?php echo $_POST["resumen"]?>"
						/>
						<label for="estado">Fuente Registrada: </label>
						<input type="text" class="unselectable" name="fuente" readonly="readonly"
							value="<?php echo $_POST["fuente_registrada"]?>" 
						/>
						<label for="estado">Causa: </label>
						<input type="text" class="unselectable" name="causa" readonly="readonly"
							value="<?php echo $_POST["causa"]?>" 
						/>
					</div>
					<div class="six columns">
						<label for="estado">Login: </label>
						<input type="text" class="unselectable" name="login" readonly="readonly"
							value="<?php echo $_POST["login"]?>"
						/>
						<label for="estado">Grupo Asignado: </label>
						<input type="text" class="unselectable" name="grp_asignado" readonly="readonly"
							value="<?php echo $_POST["grupo_asignado"]?>"
						/>
						<label for="estado">Usuario Asignado: </label>
						<input type="text" class="unselectable" name="usr_asignado" readonly="readonly"
							value="<?php echo $_POST["usuario_asignado"]?>"
						/>
						<label for="estado">Extremo A: </label>
						<input type="text" class="unselectable" name="extremo_a" readonly="readonly"
							value="<?php echo $_POST["extremo_a"]?>"
						/>
						<label for="estado">Extremo B: </label>
						<input type="text" class="unselectable" name="extremo_b" readonly="readonly"
							value="<?php echo $_POST["extremo_b"]?>"
						/>
						<label for="estado">Origen Ticket: </label>
						<input type="text" class="unselectable" name="org_ticket" readonly="readonly"
							value="<?php echo $_POST["origen_ticket"]?>"
						/>
						<label for="estado">Correo Operador: </label>
						<input type="text" class="unselectable" name="correo" readonly="readonly" 
							value="<?php echo $_POST["correo"]?>"
						/>
						<label for="estado">Responsabilidad Falla: </label>
						<input type="text" class="unselectable" name="responsabilidad_falla" readonly="readonly" 
							value="<?php echo $_POST["responsabilidad_falla"]?>"
						/>
					</div>
				</div>
				<div class="container">
					<div class="twelve columns">
						<label for="estado">Resolucion: </label>
						<input type="text" class="unselectable" name="resolucion" readonly="readonly" 
							value="<?php echo $_POST["resolucion"]?>"
						/>
					</div>
				</div>
				<hr>
				<div class="container">
					<div class="ten centered columns">
						<table id="info_trabajo" cellspacing="0">
							<thead>
								<tr>
									<th>FECHA</th>
									<th>ORIGEN</th>
									<th>RESUMEN</th>
									<th>NOTA</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($_POST["info_trabajo"] as $info){ ?>
								<tr>
									<td style="max-width:150px;"><?php echo $info['fecha_ingreso'];?></td>
									<td style="max-width:150px;"><?php echo $info['origen'];?></td>
									<td style="max-width:150px;"><?php echo $info['resumen'];?></td>
									<td style="max-width:150px;"><?php echo $info['notas'];?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<?php echo form_open('/operador/c_ticket/putInformacionTrabajo', array('id' => 'form'));?>
					<div id="agregar_info" style="display:none;" class="container">
						<div class="ten centered columns">
							<label id="msj_error" ></label>
							<label for="estado">Resumen: </label>
							<input type="text" id="add_resumen" name="add_resumen"></input>
							<label for="estado">Nota: </label>
							<textarea id="add_nota" name="add_nota" rows="4" cols="50"></textarea>
							<input type="hidden" id="ticket_id" name="ticket_id" value="<?php echo $_POST["ticket_id"]?>"/>
							<input type="button" id="btnEnviar"  class="button secondary" value="Enviar" style="margin-left:33%;"/>
							<input type="button" id="btncancelar" class="button secondary" value="Cerrar">
						</div>
					</div>
				<?php echo form_close();?>
			</fieldset>
		
		<script  type="text/javascript">
			var frmvalidator = new Validator("form");
			frmvalidator.addValidation("add_resumen","req","Favor llenar campo RESUMEN ");
			frmvalidator.addValidation("add_nota","req","Favor llenar campo NOTA");
		</script>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		var variable = $('input[name="estado"]').val();
		if (variable != 'Cerrado'){
			 $('#agregar_info').show();
		}
	});
	
	$('#btnEnviar').click(function(){
		var objToday = new Date(),
		curMonth = objToday.getMonth() < 10 ? '0' + (objToday.getMonth()+1) : (objToday.getMonth()+1),
		curDay = objToday.getDate() < 10 ? '0' + objToday.getDate() : objToday.getDate(),
		curYear = objToday.getFullYear(),
		curHour = objToday.getHours() < 10 ? "0" + objToday.getHours() : objToday.getHours(),
		curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
		curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds();
		var today = curYear + "-" + curMonth + "-" + curDay + " " +curHour + ":" + curMinute + ":" + curSeconds;
		$('#btnEnviar').attr('disabled','disabled');
		
		$.ajax({
			url: "c_ticket/putInformacionTrabajo",
			type: "POST",
			data: {'ticket_id': $('#ticket_id').val(), 'add_resumen': $('#add_resumen').val(), 'add_nota': $('#add_nota').val()},
			success: function(data){
				if (data == 'OK'){
					$('#msj_error').removeAttr('style');
					$('#msj_error').attr('style','visibility:hidden');
					$('#info_trabajo').append('<tr><td>'+today+'</td><td>Monitor</td><td>'+$('#add_resumen').val()+'</td><td>'+$('#add_nota').val()+'</td></tr>');
					$('#add_nota').val("");
					$('#add_resumen').val("");
					$('#btnEnviar').removeAttr('disabled');
				}
				else{
					$('#msj_error').empty();
					$('#msj_error').attr('style','color:red');
					$('#msj_error').append(data);
					$('#btnEnviar').removeAttr('disabled');
				}
			}
		});
	});
	
	$('#btncancelar').click(function(event){
		window.close();
	});
		
	</script>
</body>
</html>