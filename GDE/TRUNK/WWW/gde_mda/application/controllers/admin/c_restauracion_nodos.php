<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_restauracion_nodos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model( 'nodos' );
		$this->load->helper('download');
	}
	
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$data['titulo_pagina'] = 'Restauracion de Nodos';
		$this->load->view('admin/v_restauracion_nodos',$data);
	}
	
	function listarRespaldos(){
		$result["respaldos"] = $this->nodos->getRespaldos();
		$result["fechas"] = $this->nodos->getFechas();
		
		echo json_encode($result);
	}
	
	function descargarRespaldos($url){
		$data = file_get_contents("/var/www/html/fase_1/backup_nodos/".$url);
		force_download($url, $data);
	}
	
	function restaurarRespaldos(){
		$ruta = $_POST['ruta'];
		// $result = $this->nodos->runRestore($ruta);
		
		// echo json_encode($result);
		$command = '/usr/bin/python /var/www/html/fase_1/application/models/restaurar_nodos.py '.$ruta;
		$output = shell_exec($command);
		echo json_encode($output);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2){
			return true;
		}
		else{
			return false;
		}
	}
}