<body>
	<div id="contenedor" class="width_full">
	<header id="header">
		<hgroup>
			<!--<h1 class="site_title">
			</h1>
			<h2 class="section_title">
			Gestor de Eventos
			<span><?php echo anchor('admin/c_menu_admin','Menú Administrador'); ?></span>
			</h2>-->
			<span class="section_title"> Adessa - Gestor de Eventos</span>


			<div class="btn_view_site" style="margin-top:5px !important;"><?php echo anchor('c_login/salir','Salir'); ?></div>
		</hgroup>
	</header>
	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $this->session->userdata('nombre'); ?></p>
		</div>
	</section>

	<aside id="sidebar" class="column">
			<h3>Administración</h3>
			<ul class="toggle">
					
					<li><?php echo anchor('supervisor/c_crud_operador','Mantenedor de Usuarios'); ?></li>
					<!--<li><?php echo anchor('supervisor/c_crud_analista','Mantenedor de Analista'); ?></li>
					<li><?php echo anchor('admin/c_crud_supervisor','Mantenedor de Supervisores'); ?></li>-->
					<li><?php echo anchor('admin/c_crud_correos','Mantenedor de Correos'); ?></li>
					<li><?php echo anchor('admin/c_crud_categorias','Mantenedor de Categorias'); ?></li>
					<li><?php echo anchor('supervisor/c_crud_cliente','Mantenedor de Perfiles'); ?></li>
			</ul>
			
			<!--<h3>Asociación</h3>
			<ul class="toggle">
					<li><?php echo anchor('admin/c_operador_supervisor','Operador a Supervisor'); ?></li>
			</ul>-->
			
			<h3>Operación</h3>
			<ul class="toggle">
					<li><?php echo anchor('operador/c_principal','Consola de Eventos'); ?></li>
			</ul>
			
			<!--<h3>Utilidades</h3>
			<ul class="toggle">
					<li><?php echo anchor('admin/c_bitacora_nodos','Bitacora de Nodos'); ?></li>
					<li><?php echo anchor('admin/c_restauracion_nodos','Restauracion de Nodos'); ?></li>
			</ul>-->
			<footer>
				<hr />
				<p><strong>2016 &copy; Kudaw</strong></p>
			</footer>
	</aside>
