<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>

	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Mantenedor de Operadores</h3>
			</header>
			
				<!-- <div id="operadores"> -->
					<table id="registros" cellspacing="0" cellpadding="0" border="0" width="100%">
						<thead>
							<tr>
								<!-- <th><input type="checkbox" id="checkall" name="checkall" value=""></th> -->
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Apellido</th>
								<th>Usuario</th>
								<th>E-Mail</th>
								<th>Anexo</th>
								<th>Telefono</th>
								<th>Supervisor</th>
								<th></th><!-- Editar -->
								<th></th><!-- Eliminar -->
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<!-- <td></td> -->
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><!-- <button id="buttonDel"></button> --></td>
								<td><button id="buttonNuevo"></button></td>
							</tr>
						</tfoot>
					</table>
				<!-- </div> -->
	
		</article>
	</section>

	<!-- Animacion de loading -->
	<div id="ajaxLoading">
		<img src="../../images/ajax-loader.gif" alt="Loading Animation" />
		<span>Loading...</span>
	</div>

	<!-- message dialog box -->
	<div id="msgDialog"><p></p></div>
	
	<!-- Dialog Box de confirmacion para eliminacion -->
	<div id="delConfDialog" title="Confirmar">
		<p>¿Seguro que desea eliminar estos datos?</p>
	</div>

	<div id="crear_operador">
		<?php echo validation_errors(); ?>
		<form  action="" method="post" id="datos_nuevo_operador">
			<table width="70%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>Nombre:</td>
					<td><input type="text" id="nom_ope" name="nom_ope" /></td>
				</tr>
				<tr>
					<td>Apellido paterno:</td>
					<td><input type="text" id="app_ope" name="app_ope" /></td>
				</tr>
				<tr>
					<td>Apellido materno:</td>
					<td><input type="text" id="apm_ope" name="apm_ope" /></td>
				</tr>
				<tr>
					<td>Nombre usuario:</td>
					<td><input type="text" id="nom_usu_ope" name="nom_usu_ope"/></td>
				</tr>
				<tr>
					<td>Contraseña:</td>
					<td><input type="password" id="pass_usu_ope" name="pass_usu_ope" /></td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td><input type="text" id="email_usu_ope" name="email_usu_ope" /></td>
				</tr>
				<tr>
					<td>Anexo:</td>
					<td><input type="text" id="anexo_usu_ope" name="anexo_ope" /></td>
				</tr>
				<tr>
					<td>Telefono:</td>
					<td><input type="text" id="tel_usu_ope" name="tel_ope" /></td>
				</tr>
			</table>
		</form>
	</div>
	
	
		<div id="actualizar_operador">
			<form  action="" method="post" id="datos_actualizar_operador">
			<table width="70%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>Nombre:</td>
					<td><input type="text" id="edit_nom_ope" name="edit_nom_ope" /></td>
				</tr>
				<tr>
					<td>Apellido paterno:</td>
					<td><input type="text" id="edit_app_ope" name="edit_app_ope" /></td>
				</tr>
				<tr>
					<td>Apellido materno:</td>
					<td><input type="text" id="edit_apm_ope" name="edit_apm_ope" /></td>
				</tr>
				<tr>
					<td>Nombre usuario:</td>
					<td><input type="text" id="edit_nom_usu_ope" name="edit_nom_usu_ope" disabled="disabled"/></td>
				</tr>
				<tr>
					<td>Contraseña:</td>
					<td><input type="password" id="edit_pass_usu_ope" name="edit_pass_usu_ope" value="" /></td>
				</tr>
				<tr>
					<td>Confirmar contraseña:</td>
					<td><input type="password" id="edit_pass_usu_ope2" name="edit_pass_usu_ope2" value="" /></td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td><input type="text" id="edit_email_usu_ope" name="edit_email_usu_ope" /></td>
				</tr>
				<tr>
					<td>Anexo:</td>
					<td><input type="text" id="edit_anexo_usu_ope" name="edit_anexo_usu_ope" /></td>
				</tr>
				<tr>
					<td>Telefono:</td>
					<td><input type="text" id="edit_tel_usu_ope" name="edit_tel_usu_ope" /></td>
				</tr>
			</table>
		</div>
	
		<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/crud_operador.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#datos_nuevo_operador').validate(<?php echo $validacion_nuevo_op ?>);
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#datos_actualizar_operador').validate(<?php echo $validacion_actualizar_op ?>);
		});
	</script>
</body>
</html>