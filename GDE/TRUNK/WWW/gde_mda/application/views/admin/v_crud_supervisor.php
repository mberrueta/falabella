<?php $this->load->view('/admin/header');?>
<?php $this->load->view('/admin/lateral');?>
	<section id="main" class="column">
		<article class="module width_full">
			<header style="position:relative;">
				<h3>Mantenedor de Supervisores</h3>
				<button id="buttonNuevo" style="position:absolute;right:40px;top:10px;background-color:none;"></button>

			</header>
				<!-- <div id="supervisores"> -->
					<table id="registros" cellspacing="0" cellpadding="0" border="0" width="100%">
						<thead>
							<tr>
								<!-- <th><input type="checkbox" id="checkall" name="checkall" value=""></th>  -->
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Apellido</th>
								<th>Usuario</th>
								<th>E-Mail</th>
								<th>Anexo</th>
								<th>Telefono</th>
								<th></th><!-- Editar -->
								<th></th><!-- Eliminar -->
							</tr>
						</thead>
						<tbody>
						</tbody>
						
					</table>
				<!-- </div> -->
			
		</article>
	</section>


	<!-- Animacion de loading -->
	<div id="ajaxLoading">
		<img src="<?php echo base_url(); ?>images/ajax-loader.gif" alt="Loading Animation" />
		<span>Loading...</span>
	</div>

	<!-- message dialog box -->
	<div id="msgDialog"><p></p></div>
	
	<!-- Dialog Box de confirmacion para eliminacion -->
	<div id="delConfDialog" title="Confirmar">
		<p>¿Seguro que desea eliminar estos datos?</p>
	</div>

	<div id="crear_supervisor" class="crud">
		<?php echo validation_errors(); ?>
		<form  action="" method="post" id="datos_nuevo_supervisor">
			<table width="70%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>Nombre:</td>
					<td><input type="text" id="nom_sup" name="nom_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>Apellido paterno:</td>
					<td><input type="text" id="app_sup" name="app_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>Apellido materno:</td>
					<td><input type="text" id="apm_sup" name="apm_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>Nombre usuario:</td>
					<td><input type="text" id="nom_usu_sup" name="nom_usu_sup"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Contraseña:</td>
					<td><input type="password" id="pass_usu_sup" name="pass_usu_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td><input type="text" id="email_usu_sup" name="email_usu_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>Anexo:</td>
					<td><input type="text" id="anexo_sup" name="anexo_sup" /></td>
					<td></td>
				</tr>
				<tr>
					<td>Telefono:</td>
					<td><input type="text" id="tel_sup" name="tel_sup" /></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
	
	
		<div id="actualizar_supervisor">
			<form  action="" method="post" id="datos_actualizar_supervisor">
			<table width="70%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>Nombre:</td>
					<td><input type="text" id="edit_nom_sup" name="edit_nom_sup" /></td>
				</tr>
				<tr>
					<td>Apellido paterno:</td>
					<td><input type="text" id="edit_app_sup" name="edit_app_sup" /></td>
				</tr>
				<tr>
					<td>Apellido materno:</td>
					<td><input type="text" id="edit_apm_sup" name="edit_apm_sup" /></td>
				</tr>
				<tr>
					<td>Nombre usuario:</td>
					<td><input type="text" id="edit_nom_usu_sup" name="edit_nom_usu_sup" disabled="disabled"/></td>
				</tr>
				<tr>
					<td>Contraseña:</td>
					<td><input type="password" id="edit_pass_usu_sup" name="edit_pass_usu_sup" value="" /></td>
				</tr>
				<tr>
					<td>Confirmar contraseña:</td>
					<td><input type="password" id="edit_pass_usu_sup2" name="edit_pass_usu_sup2" value="" /></td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td><input type="text" id="edit_email_usu_sup" name="edit_email_usu_sup" /></td>
				</tr>
				<tr>
					<td>Anexo:</td>
					<td><input type="text" id="edit_anexo_sup" name="edit_anexo_sup" /></td>
				</tr>
				<tr>
					<td>Telefono:</td>
					<td><input type="text" id="edit_tel_sup" name="edit_tel_sup" /></td>
				</tr>
			</table>
		</div>
	<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/crud_supervisor.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#datos_nuevo_supervisor').validate(<?php echo $validacion_nuevo_sup ?>);
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#datos_actualizar_supervisor').validate(<?php echo $validacion_actualizar_sup ?>);
		});
	</script>
</body>
</html>