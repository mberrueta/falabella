#!/usr/bin/python
import MySQLdb
import MySQLdb.cursors
import csv
import logging
from datetime import datetime


#Se declara instancia de log
logger    = logging.getLogger('Respaldo_de_Nodos')
hdlr      = logging.FileHandler('/var/tmp/respaldo_nodos.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)

#MySQL params
host        = "localhost"
user        = "root"
password    = "gdekudaw"
db          = "gestor_eventos_rsp"
fecha       = datetime.now().strftime("%Y%m%d_%H%M")
ruta        = '/var/www/html/fase_1/backup_nodos/'+fecha+'.txt'

myDb = None

try:
	#Coneccion a BD
	logger.info("Conectando con la Base de Datos")
	myDb = MySQLdb.connect(host, user, password, db, cursorclass=MySQLdb.cursors.DictCursor)
	myCurs = myDb.cursor()
	
	#Se obtienen los ID de cliente
	myCurs.execute("SELECT cliente.CLI_NOM, dispositivo.DIS_NOM, dispositivo.DIS_CODSERV, dispositivo.DIS_IPPRINC, dispositivo.DIS_CSPRINC, dispositivo.DIS_CODADMIN, dispositivo.DIS_CRIT, dispositivo.DIS_SLA, geografia_dispositivo.GEOD_REGION, geografia_dispositivo.GEOD_PROVINCIA ,geografia_dispositivo.GEOD_COMUNA ,geografia_dispositivo.GEOD_SUCURSAL ,arquitectura_dispositivo.ARQ_FW ,arquitectura_dispositivo.ARQ_PE ,arquitectura_dispositivo.ARQ_TUSER ,arquitectura_dispositivo.ARQ_TPASS ,arquitectura_dispositivo.ARQ_TEPASS ,arquitectura_dispositivo.ARQ_SNPM ,contacto_dispositivo.CNT_CNTHABIL ,contacto_dispositivo.CNT_TELHABIL ,contacto_dispositivo.CNT_CELHABIL ,contacto_dispositivo.CNT_CORHABIL ,contacto_dispositivo.CNT_CNTNOHABIL ,contacto_dispositivo.CNT_TELNOHABIL ,contacto_dispositivo.CNT_CELNOHABIL ,contacto_dispositivo.CNT_CORNOHABIL, dispositivo.DIS_ATEN FROM dispositivo LEFT JOIN cliente ON dispositivo.CLI_ID = cliente.CLI_ID LEFT JOIN geografia_dispositivo ON dispositivo.DIS_NOM = geografia_dispositivo.DIS_NOM LEFT JOIN arquitectura_dispositivo ON dispositivo.DIS_NOM = arquitectura_dispositivo.DIS_NOM LEFT JOIN contacto_dispositivo ON dispositivo.DIS_NOM = contacto_dispositivo.DIS_NOM;")
	myDb.commit()
	
	with open(ruta, 'wb') as csvfile:
		rows = myCurs.fetchall()
		spamwriter = csv.writer(csvfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		spamwriter.writerow(["CLIENTE", "NODO", "CODIGO DE SERVICIO", "IP_PRINCIPAL", "C/S_PRINCIPAL", "CODIGO ADMIN", "REGION", "PROVINCIA", "COMUNA", "SUCURSAL", "FW", "PE", "CRITICIDAD", "SLA", "TELNET_USER", "TELNET_PASS", "TELNET_ENAB_PASS", "SNMP", "Contacto_Habil", "Telefono_Habil", "Celular_Habil", "Correo_Habil", "Contacto_No_Habil", "Telefono_No_Habil", "Celular_No_Habil", "Correo_No_Habil", "Horario_Habil"])
		for nodo in rows:
			spamwriter.writerow([nodo['CLI_NOM'], nodo['DIS_NOM'], nodo['DIS_CODSERV'], nodo['DIS_IPPRINC'], nodo['DIS_CSPRINC'], nodo['DIS_CODADMIN'], nodo['GEOD_REGION'], nodo['GEOD_PROVINCIA'], nodo['GEOD_COMUNA'], nodo['GEOD_SUCURSAL'], nodo['ARQ_FW'], nodo['ARQ_PE'], nodo['DIS_CRIT'], nodo['DIS_SLA'], nodo['ARQ_TUSER'], nodo['ARQ_TPASS'], nodo['ARQ_TEPASS'], nodo['ARQ_SNPM'], nodo['CNT_CNTHABIL'], nodo['CNT_TELHABIL'], nodo['CNT_CELHABIL'], nodo['CNT_CORHABIL'], nodo['CNT_CNTNOHABIL'], nodo['CNT_TELNOHABIL'], nodo['CNT_CELNOHABIL'], nodo['CNT_CORNOHABIL'], nodo['DIS_ATEN']])
	logger.info("Se escribe respaldo de nodos en /var/tmp/backup_nodos/")
	
	myCurs.execute("INSERT INTO respaldo_nodos ( RESPALDO_RUTA, RESPALDO_ESTADO ) VALUES ('"+fecha+".txt', 1);")
	myDb.commit()
	logger.info("Se inserta valor del respaldo en Base de Datos")
	
	myDb.close() #Cierre coneccion con bd

	logger.info("Se cierra coneccion con Base de Datos")
	
except:
	import traceback
	if myDb:
		myDb.close()
		
	stack = traceback.format_exc()
	print "Salida forzada: "+str(stack)
	logger.error("Salida forzada")
	logger.error("Problema: "+ str(stack))
