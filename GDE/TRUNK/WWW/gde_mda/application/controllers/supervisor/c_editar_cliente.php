<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_editar_cliente extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cliente');
		$this->load->model('operador');
	}
 
	public function index($mensaje='') {
		$data['men']   = $mensaje;
		$data['final'] = $this->obtenerAsociaciones();
		$data['titulo_pagina'] = 'GDE -> Mantenedor de clientes';
		if($this->session->userdata('tipo') == 0){
			$data['dir_usuario'] = 'supervisor';
		}
		else if($this->session->userdata('tipo') == 2){
			$data['dir_usuario'] = 'admin';
		}
		$this->load->view('supervisor/v_editar_cliente',$data);
	}

	private function obtenerAsociaciones() {
		$offset = 0;
		
		if($this->session->userdata('tipo') == 0){
			$datos = $this->cliente->listar_cliente($this->session->userdata('username'));
		}
		else if($this->session->userdata('tipo') == 2){
			$datos = $this->cliente->listar_cliente($this->session->userdata('username'), true);
		}
		
		$ddmenu = array();
		$data = array();
		
		$j = 0 + $offset;
		foreach ($datos['cli']->result() as $row)
		{
			$Id = $row->CLI_ID;
			$query = $this->cliente->listar_nombre($Id);
			foreach ($query->result() as $row)
			{
				$ddmenu['Cliente']  = $row->CLI_NOM;
				$ddmenu['Criti']    = $row->CLI_CRIT;
			}
			
			$data2 = $this->cliente->listar_asociaciones($Id);
			foreach ($data2['cli']->result() as $row)
			{
				if($row->GRU_TIPO == 'T'){
					$nombre = $this->operador->getByuser($row->USU_USER);
					$ddmenu['Operador']  = $nombre->OPE_NOM." ".$nombre->OPE_APP;
				}
				if($row->GRU_TIPO == 'B'){
					$nombre = $this->operador->getByuser($row->USU_USER);
					$ddmenu['Backup']  = $nombre->OPE_NOM." ".$nombre->OPE_APP;
				}
				if($row->GRU_TIPO == 'T1'){
					$nombre = $this->operador->getByuser($row->USU_USER);
					$ddmenu['Noche']  = $nombre->OPE_NOM." ".$nombre->OPE_APP;
				}
				if($row->GRU_TIPO == 'T2'){
					$nombre = $this->operador->getByuser($row->USU_USER);
					$ddmenu['FDS']  = $nombre->OPE_NOM." ".$nombre->OPE_APP;
				}
			}
			$data[++$j] = $ddmenu;
		}
		return $data;
	}
}