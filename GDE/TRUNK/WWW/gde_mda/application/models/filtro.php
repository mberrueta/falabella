<?php

class Filtro extends CI_Model {
	
	/**
	*TODO
	* comentar
	*/
	protected $_DB2				= null;
	
	/**
	*TODO
	* comentar
	*/
	function __construct() {
		parent::__construct();
		$this->_DB2 = $this->load->database('estadistica', TRUE);
	}
	
	/**
	*Funcionalidad reemplazo de filtros bases ETC (Estado, Tipo de Evento, Cliente)
	*/
	function listarFiltroBase($usuario, $filterNeed, $isOperador = true, $tipoActivo){
		// Filtros Base
		$estadoArray       = array();
		$paisArray         = array();
		$negocioArray      = array();
		$servicioArray     = array();
		$organizationArray = array();
		$tagArray          = array();

		if($tipoActivo){
			$estado   	  = "(status IN (";
			$pais     	  = "(dest_country IN (";
			$negocio 	  = "(dest_bunit IN (";
			// $servicio     = "(dest_service IN (";
			$servicio     = "(";
			$organization = "(filtro_base.src_organization IN (";
			$tag          = "(src_subcategory IN (";
 
			foreach ($tipoActivo as $valor) {
				if (strpos($valor, 'estado') !== false) {
						$temp = str_replace("estado", "", $valor);
					    array_push($estadoArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'pais') !== false) {
						$temp = str_replace("pais", "", $valor);
					    array_push($paisArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'negocio') !== false) {
						$temp = str_replace("negocio", "", $valor);
					    array_push($negocioArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'servicio') !== false) {
						$temp = str_replace("servicio", "", $valor);
					    array_push($servicioArray, "'%%".$temp."%%'");
				}
				elseif (strpos($valor, 'organization') !== false) {
						$temp = str_replace("organization", "", $valor);
					    array_push($organizationArray, "'".$temp."'");
				} 
				elseif (strpos($valor, 'tag') !== false) {
						$temp = str_replace("tag", "", $valor);
					    array_push($tagArray, "'".$temp."'");
				}
			}
			
			//log_message('debug', "Prueba: ".implode(",", $estadoArray));
			$estado   	  .= implode(",", $estadoArray)."))";
			$pais     	  .= implode(",", $paisArray)."))";
			$negocio  	  .= implode(",", $negocioArray)."))";
			//$servicio 	  .= implode(",", $servicioArray)."))";
			$organization .= implode(",", $organizationArray)."))";
			$tag          .= implode(",", $tagArray)."))";

			if (empty($estadoArray))       { $estado       = "(status IN ('Activo','En Proceso'))";}
			if (empty($paisArray))         { $pais         = "1";}
			if (empty($negocioArray))      { $negocio      = "1";}
			if (empty($servicioArray))     { 
				$servicio     = "1";
			} else {
				foreach ($servicioArray as $key=>$value){
					if($key == (count($servicioArray)-1)){
						$servicio .= "dest_service LIKE ".$value;
					} else {
						$servicio .= "dest_service LIKE ".$value." OR ";
					}
				}
				$servicio .= ")";
			}
			if (empty($organizationArray)) { $organization = "1";}
			if (empty($tagArray))          { $tag = "1";}

		}
		else {
			$estado       = "(status IN ('Activo','En Proceso'))";
			$pais         = "1";
			$negocio      = "1";
			$servicio     = "1";
			$organization = "1";
			$tag          = "1";
		}

		//echo ( $estado." AND ".$pais." AND ".$negocio." AND ".$servicio." AND ".$organization);

		$ope	= $this->session->userdata('username');
		$columnaFiltro = array(
			'estado' => array(
				'column'	=> "status",
				'join'		=> array("SELECT status FROM gestor_eventos_rsp.estado_gde")
			),
			'pais' => array(
				'column'	=> "dest_country",
				// 'where'		=> $estado
				'where'		=> $estado." AND ".$negocio." AND ".$servicio." AND ".$organization." AND ".$tag
			),
			'negocio' => array(
				'column'	=> "dest_bunit",
				// 'where'		=> $estado." AND ".$pais
				'where'		=> $estado." AND ".$pais." AND ".$servicio." AND ".$organization." AND ".$tag
			),
			/*'servicio' => array(
				'column'	=> "dest_service",
				// 'where'		=> $estado." AND ".$pais." AND ".$negocio
				'where'		=> $estado." AND ".$pais." AND ".$negocio." AND ".$organization
			),*/
			'organization' => array(
				'column'	=> "src_organization",
				// 'where'		=> $estado." AND ".$pais." AND ".$negocio." AND ".$servicio
				'where'		=> $estado." AND ".$pais." AND ".$negocio." AND ".$servicio." AND ".$tag
			),
			'tag' => array(
				'column'	=> "src_subcategory",
				'where'		=> $estado." AND ".$pais." AND ".$negocio." AND ".$servicio." AND ".$organization
			)
		);
		
		$response = array();
		if( !array_key_exists( $filterNeed, $columnaFiltro ) ){
			$response['stt']		= 'error';
			$response['msg']	= 'El filtro solicitado "'.$filterNeed.'" no se encuentra registrado en el sistema, favor informar a administrador.';
			
		}
		else {
			$result = $this->listFiltersETC($usuario, $filterNeed, $columnaFiltro[$filterNeed]['column'] , $columnaFiltro[$filterNeed], $isOperador);
			$response['stt']	= $result['stt'];
			$response['msg']	= $result['msg'];
		}
		
		return $response;
	}
	
		/**
	*Funcionalidad reemplazo de filtros bases ETC (Estado, Tipo de Evento, Cliente)
	*/
	function listarFiltroBaseServicio($usuario, $filterNeed, $isOperador = true, $tipoActivo){
		// Filtros Base
		$estadoArray       = array();
		$paisArray         = array();
		$negocioArray      = array();
		$servicioArray     = array();
		$organizationArray = array();
		$tagArray          = array();

		if($tipoActivo){
			$estado   	  = "(status IN (";
			$pais     	  = "(dest_country IN (";
			$negocio 	  = "(dest_bunit IN (";
			// $servicio     = "(dest_service IN (";
			$servicio     = "(";
			$organization = "(filtro_base.src_organization IN (";
			$tag          = "(src_subcategory IN (";
 
			foreach ($tipoActivo as $valor) {
				if (strpos($valor, 'estado') !== false) {
						$temp = str_replace("estado", "", $valor);
					    array_push($estadoArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'pais') !== false) {
						$temp = str_replace("pais", "", $valor);
					    array_push($paisArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'negocio') !== false) {
						$temp = str_replace("negocio", "", $valor);
					    array_push($negocioArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'servicio') !== false) {
						$temp = str_replace("servicio", "", $valor);
					    array_push($servicioArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'organization') !== false) {
						$temp = str_replace("organization", "", $valor);
					    array_push($organizationArray, "'".$temp."'");
				}
				elseif (strpos($valor, 'tag') !== false) {
						$temp = str_replace("tag", "", $valor);
					    array_push($tagArray, "'".$temp."'");
				}
			}
			
			//log_message('debug', "Prueba: ".implode(",", $estadoArray));
			$estado   	  .= implode(",", $estadoArray)."))";
			$pais     	  .= implode(",", $paisArray)."))";
			$negocio  	  .= implode(",", $negocioArray)."))";
			// $servicio 	  .= implode(",", $servicioArray)."))";
			$organization .= implode(",", $organizationArray)."))";
			$tag          .= implode(",", $tagArray)."))";

			if (empty($estadoArray))       { $estado       = "(status IN ('Activo','En Proceso'))";}
			if (empty($paisArray))         { $pais         = "1";}
			if (empty($negocioArray))      { $negocio      = "1";}
			if (empty($servicioArray))     { 
				$servicio     = "1";
			} else {
				foreach ($servicioArray as $key=>$value){
					if($key == (count($servicioArray)-1)){
						$servicio .= "dest_service LIKE ".$value;
					} else {
						$servicio .= "dest_service LIKE ".$value." OR ";
					}
				}
				$servicio .= ")";
			}
			if (empty($organizationArray)) { $organization = "1";}
			if (empty($tagArray))          { $tag = "1";}

		}
		else {
			$estado       = "(status IN ('Activo','En Proceso'))";
			$pais         = "1";
			$negocio      = "1";
			$servicio     = "1";
			$organization = "1";
			$tag          = "1";
		}
		
		$where = $estado." AND ".$pais." AND ".$negocio." AND ".$organization." AND ".$tag;
		
		$resultsHTML		= array();
		$additionalClasses	= array();
		$ope				= $this->session->userdata('username');
		
		$sql = "SELECT src_organization FROM gestor_eventos_rsp.grupo_gde WHERE user = '".$ope."' AND src_organization = 'All'";
		$query = $this->_DB2->query($sql);
		// log_message('error', $this->_DB2->last_query());
		
		$sql_2 = "SELECT src_tool FROM gestor_eventos_rsp.grupo_gde WHERE user = '".$ope."' AND src_tool = 'All'";
		$query_2 = $this->_DB2->query($sql_2);
		// log_message('error', $this->_DB2->last_query());
		
		if($query->row() && $query_2->row()){
			$results = $this->_DB2->query("SELECT TRIM('\n' FROM substring_index(substring_index(dest_service, '|', n),'|',-1)) as dest_service,sum(ACUM) as TTL FROM filtro_base JOIN filtro_numbers ON (char_length(dest_service) - char_length(replace(dest_service, '|', '')) >= n) WHERE ".$where." GROUP BY TRIM('\n' FROM substring_index(substring_index(dest_service, '|', n),'|',-1))");
			// log_message('error', $this->_DB2->last_query());
		} else {
			if(!$query->row() && $query_2->row()){
				$results = $this->_DB2->query("SELECT TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1)) as dest_service,sum(ACUM) as TTL FROM estadistica.filtro_base JOIN estadistica.filtro_numbers ON (char_length(dest_service) - char_length(replace(filtro_base.dest_service, '|', '')) >= n) LEFT JOIN gestor_eventos_rsp.grupo_gde ON (gestor_eventos_rsp.grupo_gde.src_organization = filtro_base.src_organization) WHERE ".$where." AND gestor_eventos_rsp.grupo_gde.user ='".$ope."' GROUP BY TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1))");
				// log_message('error', $this->_DB2->last_query());
			} elseif($query->row() && !$query_2->row()){
				$results = $this->_DB2->query("SELECT TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1)) as dest_service,sum(ACUM) as TTL FROM estadistica.filtro_base JOIN estadistica.filtro_numbers ON (char_length(dest_service) - char_length(replace(filtro_base.dest_service, '|', '')) >= n) LEFT JOIN gestor_eventos_rsp.grupo_gde ON (gestor_eventos_rsp.grupo_gde.src_tool = filtro_base.src_tool) WHERE ".$where." AND gestor_eventos_rsp.grupo_gde.user ='".$ope."' GROUP BY TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1))");
				// log_message('error', $this->_DB2->last_query());
			} else {
				$results = $this->_DB2->query("SELECT TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1)) as dest_service,sum(ACUM) as TTL FROM estadistica.filtro_base JOIN estadistica.filtro_numbers ON (char_length(dest_service) - char_length(replace(filtro_base.dest_service, '|', '')) >= n) LEFT JOIN gestor_eventos_rsp.grupo_gde ON (gestor_eventos_rsp.grupo_gde.src_tool = filtro_base.src_tool) LEFT JOIN gestor_eventos_rsp.grupo_gde as b ON (b.src_organization = filtro_base.src_organization) WHERE ".$where." AND gestor_eventos_rsp.grupo_gde.user ='".$ope."' AND b.user ='".$ope."' GROUP BY TRIM('\n' FROM substring_index(substring_index(filtro_base.dest_service, '|', n),'|',-1))");
				// log_message('error', $this->_DB2->last_query());
				
			}
			
			 
		}
		
		foreach($results->result() as $results){
			$resultsHTML[ltrim($results->dest_service)] = $results->TTL;
		}
		
		$html = $this->generarFiltroHTML("servicio", $resultsHTML, $additionalClasses);
		return array('stt' => 'ok', 'msg' => $html);
	}
	
	/**
	*TODO
	* comentar
	*/
	private function listFiltersETC( $usuario, $type, $filter, $additional, $isOperador ){
		
		$OrganizationAll = FALSE;
		$SourceAll 		 = FALSE;
		
		// Validate src_organization
		$sql_orga = "SELECT src_organization FROM gestor_eventos_rsp.grupo_gde WHERE user = '".$usuario."' AND src_organization = 'All'";
		$query = $this->_DB2->query($sql_orga);
		
		$sql_src = "SELECT src_tool FROM gestor_eventos_rsp.grupo_gde WHERE user = '".$usuario."' AND src_tool = 'All'";
		$query_2 = $this->_DB2->query($sql_src);
		
		if($query->row()){
			$OrganizationAll = TRUE;
		}
		
		if($query_2->row()){
			$SourceAll = TRUE;
		}
		
		
		// Create select query
		$this->_DB2->select('filtro_base.'.$filter);
		$this->_DB2->select('sum(ACUM) as TTL', false);
		
		// If exists customs selects
		if( array_key_exists('select',$additional) ){
			foreach( $additional['select'] as $select ){
				$this->_DB2->select($select, false);
			}
		}
		$this->_DB2->from('estadistica.filtro_base');
		
		if(!$OrganizationAll){
			$this->_DB2->join('gestor_eventos_rsp.grupo_gde as a','filtro_base.src_organization = a.src_organization', 'LEFT JOIN');
			$this->_DB2->where('a.user',$usuario);
		}
		
		if(!$SourceAll){
			$this->_DB2->join('gestor_eventos_rsp.grupo_gde as b','filtro_base.src_tool = b.src_tool', 'LEFT JOIN');
			$this->_DB2->where('b.user',$usuario);
		}
		
		// If exists customs where
		if( array_key_exists('where',$additional) ){
			//log_message('debug', "Prueba: ".$additional['where']);
			$this->_DB2->where($additional['where']);
		}
		$this->_DB2->group_by($filter);
		// $this->_DB2->order_by('CLI_CRIT','ASC');
		$this->_DB2->order_by($filter,'ASC');
		$resultsFiltros = $this->_DB2->get();

		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$type);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$type);
		}
		
		// echo  $this->_DB2->last_query();
		
		
		// Se genera tabla HTML con filtros
		$resultsHTML			= array();
		$additionalClasses	= array();
		
		// Si se debe realizar un cruze, es porque el listado de filtros se encuentra completo en otra tabla de la BD
		if( array_key_exists('join',$additional) ){ 
		
			// Se carga el listado de datos para realizar el cruze
			$resultsJoin = null;
			if( sizeof($additional['join']) > 1){
				$resultsJoin = $this->_DB2->query($additional['join'][0], $additional['join'][1]); 
			}
			else{
				$resultsJoin = $this->_DB2->query($additional['join'][0]); 
			}
			
			foreach($resultsJoin->result() as $resultJoin){
				
				if( $resultsFiltros->num_rows() > 0){
					foreach($resultsFiltros->result() as $resultFiltro){
						
						// Si es que se le debe agregar una clase extra al filtro
						
						if( $resultJoin->$filter === $resultFiltro->$filter ){

							$resultsHTML[$resultJoin->$filter] = $resultFiltro->TTL;
							if( array_key_exists('conditionalClass',$additional) ){
							
								if( $resultFiltro->$additional['conditionalClass'][0] == $additional['conditionalClass'][1] ){

									$additionalClasses[$resultJoin->$filter] = $additional['conditionalClass'][2];
								}
							}
							break;
						}
						else{
							$resultsHTML[$resultJoin->$filter] = 0;
						}
					}
				}
				else{
					$resultsHTML[$resultJoin->$filter] = 0;
				}
			}
		}
		else{
			foreach($resultsFiltros->result() as $resultFiltro){
				
				// Si es que se le debe agregar una clase extra al filtro
				if( array_key_exists('conditionalClass',$additional) ){
					
					if( $resultFiltro->$additional['conditionalClass'][0] == $additional['conditionalClass'][1] ){
						
						$additionalClasses[$resultJoin->$filter] = $additional['conditionalClass'][2];
					}
				}
				$resultsHTML[$resultFiltro->$filter] = $resultFiltro->TTL;
			}
		}
		
		$html = $this->generarFiltroHTML($type, $resultsHTML, $additionalClasses);
		return array('stt' => 'ok', 'msg' => $html);
	}

	/**
	* Funcion auxiliar, genera el html necesario para devolver una tabla con los filtros
	*/
	private function generarFiltroHTML($tipo, $arrayFiltro, $additionalClasses){
		
		// echo print_r($additionalClasses);
		/*$resultClientes = $this->_DB2->get('gestor_eventos_rsp.cliente');
		$arrayClientes = array();
		foreach( $resultClientes->result() as $cliente ){
			$arrayClientes[ $cliente->CLI_NOM ] = $cliente->CLI_ID;
		}*/
		
		$this->load->library('table');
        $this->load->helper('form');
		
		$data = array();
		
		foreach($arrayFiltro as $k => $v){
			$checkboxData = array(
				'name'        => $tipo . $k,
				'checked'     => false,
				'value'       => $k,
				'class'       => 'filtro filtro_' . $tipo
			);

			if( array_key_exists($k,$additionalClasses)  ){
				$checkboxData['class'] .= " " . $additionalClasses[$k];
			}
			
			/*if( $tipo=='cliente'  ){
				$checkboxData['value'] = $arrayClientes[$k];
			}*/
			
			array_push($data, array(form_checkbox($checkboxData), $k, $v)); // se carga fila de tabla con checkbox para filtrar
		}

		$tmpl = array ( // Se elimina encabezado de tabla ya que no es ocupado
			'table_open' 	=> '<table class="filterContainer" border="0" cellpadding="0" cellspacing="0">',
			'thead_open'	=> '',
			'thead_close'	=> '',
			'heading_row_start'		=> '',
			'heading_row_end'		=> '',
			'heading_cell_start'	=> '',
			'heading_cell_end'		=> '',
		);
		
		$this->table->set_heading(array());
		$this->table->set_template($tmpl);
		
		return $this->table->generate($data);
	}
	
	/**
	*TODO
	* comentar
	*/
	public function listarFiltroOnDemand($usuario, $filterNeed, $isOperador = true){
		
		$relationFilterFunction = array(
											'fw' => array(
													'table'		=> 'filtro_masivo_fw',
													'column'	=> 'ARQ_FW',
													'title'			=> 'Firewalls'
											),
											'pe' => array(
													'table'		=> 'filtro_masivo_pe',
													'column'	=> 'ARQ_PE',
													'title'			=> 'PEs'
											),
											'region' => array(
													'table'		=> 'filtro_masivo_region',
													'column'	=> 'GEOD_REGION',
													'title'			=> 'Regional'
											),
											'sucursal' => array(
													'table'		=> 'filtro_masivo_sucursal',
													'column'	=> 'GEOD_SUCURSAL',
													'title'			=> 'Sucursales'
											)
										);
										
		$response = array();
		if( !array_key_exists($filterNeed,$relationFilterFunction) ){
			$response['stt']		= 'error';
			$response['msg']	= 'El filtro solicitado "'.$filterNeed.'" no se encuentra registrado en el sistema, favor informar a administrador.';
			
		}
		else {
			$result = $this->listMasiveFilter($usuario, $relationFilterFunction[$filterNeed]['table'] , $relationFilterFunction[$filterNeed]['column'], $relationFilterFunction[$filterNeed]['title'], $isOperador);
			$response['stt']		= $result['stt'];
			$response['msg']	= $result['msg'];
		}
		
		return $response;
	}

	/**
	*TODO
	* comentar
	*/
	private function listMasiveFilter($usuario, $typeFilterTable,$typeFilterColumn, $typeFilterTitle, $isOperador){
		
		$arrayClientes	= array();
		$sqlCliente		= "select gestor_eventos_rsp.cliente.CLI_ID,gestor_eventos_rsp.cliente.CLI_NOM from gestor_eventos_rsp.cliente join gestor_eventos_rsp.grupo on gestor_eventos_rsp.cliente.CLI_ID=gestor_eventos_rsp.grupo.CLI_ID where gestor_eventos_rsp.grupo.GRU_STT=1 and gestor_eventos_rsp.grupo.USU_USER=?";
		$resultClientes	= $this->_DB2->query($sqlCliente, array($usuario)); 
		
		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$typeFilterTable);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$typeFilterTable);
		}
		
		foreach( $resultClientes->result() as $cliente ){
			$arrayClientes[ $cliente->CLI_ID ] = $cliente->CLI_NOM;
		}
		
		$this->_DB2->from($typeFilterTable);
		if(sizeof($arrayClientes) != 0){
			$this->_DB2->where_in('CLI_ID',array_keys($arrayClientes));
		}
		
		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$typeFilterTable);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$typeFilterTable);
		}
		
		$queryObj		= $this->_DB2->get();
		$html			= null;
		
		if( $typeFilterTable != 'filtro_masivo_sucursal' ){
		
			$html = $this->treeShapeA($queryObj, $typeFilterTable, $typeFilterColumn, $typeFilterTitle, $arrayClientes);
		}
		else{
		
			$html = $this->treeShapeB($queryObj, $typeFilterTable, $arrayClientes);
		}

		return array('stt' => 'ok', 'msg' => $html);
	}

	/**
	*TODO
	* comentar
	*/
	private function treeShapeA($queryObj, $typeFilterTable, $typeFilterColumn, $typeFilterTitle, $arrayClientes){
	
		$this->load->helper('html');
		$list = array();
		
		if( $queryObj->num_rows() > 0 ){
		
			$padresDeSucursales = $this->obtenerPadres( $queryObj, $typeFilterColumn ) ;
			foreach( $padresDeSucursales as $nodoPadre ){
			
				$checkboxData = array(
					'name'	=> substr($typeFilterTable,7) . $nodoPadre
					,'value'	=> $nodoPadre
					,'class'	=> implode(' ',array('masivo',substr($typeFilterTable,7)))
				);
				$nameList = form_checkbox($checkboxData) . '<span class="padre_arbol" ><span class="white radius label">-</span></span>' . nbs(2) . $nodoPadre;
				
				$subList = array();
				foreach( $queryObj->result() as $queryRow  ){
				
					if($queryRow->$typeFilterColumn == $nodoPadre && array_key_exists($queryRow->CLI_ID, $arrayClientes )){
					
						$subList[ $arrayClientes[$queryRow->CLI_ID] ] = '&bull; ' . $arrayClientes[$queryRow->CLI_ID];
					}
					
				}
				
				$list[ $nameList  ] = $subList;
			}
		}
		
		$titleFilter = '<span class="padre_arbol"><span class="white radius label">-</span></span>';
		$titleFilter .= nbs(2);
		$titleFilter .= $typeFilterTitle;

		return ul( array( $titleFilter =>$list ) );
	}
	
	/**
	*TODO
	* comentar
	*/
	private function treeShapeB($queryObj, $typeFilterTable, $arrayClientes){

		$this->load->helper('html');
		$list = array();
		
		if( $queryObj->num_rows() > 0 ){
		
			$padresDeSucursales = $this->obtenerPadres( $queryObj, 'CLI_ID' ) ;
			foreach( $padresDeSucursales as $nodoPadre ){
				
				if(!array_key_exists($nodoPadre, $arrayClientes ))
					continue;
					
				$checkboxData = array(
					'name'	=> 'masivo_cliente' . $arrayClientes[$nodoPadre]
					,'value'	=> $arrayClientes[$nodoPadre]
					,'class'	=> 'masivo masivo_cliente'
				);
				$nameList = form_checkbox($checkboxData) . '<span class="padre_arbol" ><span class="white radius label">-</span></span>' .  $arrayClientes[$nodoPadre] . nbs(2);
				
				$subList = array();
				foreach( $queryObj->result() as $queryRow  ){
				
					if($queryRow->CLI_ID == $nodoPadre){
				
						$input = preg_replace("/[^a-zA-Z0-9]+/", "", $queryRow->GEOD_SUCURSAL);
						$checkboxSubData = array(
							'name'			=> 'sucursal' . $input . $nodoPadre
							,'value'			=> $queryRow->GEOD_SUCURSAL
							,'class'			=> implode(' ',array('masivo',substr($typeFilterTable,7)))
							,'data_region' => $queryRow->GEOD_REGION
						);
						$subList[ $queryRow->GEOD_SUCURSAL ] = form_checkbox($checkboxSubData) . $queryRow->GEOD_SUCURSAL;
					}
				}
				
				$list[ $nameList  ] = $subList;
			}
		}
		
		return '<div id="arbol_masivo" >' .  ul($list) . '</div>';
	}
	
	/**
	*Funcion auxiliar: Entrega las raices de un arbol, segun el campo que se le solicita
	*
	*@param String $queryObj    Objeto query el cual se recorrera para encontrar las raices 
	*@param String   $tipoPadre  Campo de objeto que se debe considerar como raiz
	*
	*@return Array Reices del arbol consultado
	*/
	function obtenerPadres($queryObj, $tipoPadre){
		$listaPadres = array();
		
		foreach($queryObj->result() as $queryRow){
			array_push($listaPadres, $queryRow->$tipoPadre);
		}
		return array_unique($listaPadres);
	}
}
?>
