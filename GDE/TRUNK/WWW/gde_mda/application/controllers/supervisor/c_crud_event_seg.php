<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_event_seg extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');
	}
 
	public function index() {
		
		$this->obtenerCorreoSeguimientos();
	}
	
	function loadView($output = null)
	{
		$output->dir_usuario   = 'supervisor';
		$output->titulo_pagina = 'GDE -> Mantenedor de alarmas por evento no gestionado';
		$this->load->view('supervisor/v_crud_evento_seg',$output);
	}

	private function obtenerCorreoSeguimientos() {
	
		$crud = new grocery_CRUD();
		
		$crud->set_table('evento_seguimiento');
		$crud->set_subject('Seguimiento de Eventos');
		$crud->required_fields('EVENTO_CRIT','EVENTO_CLI_CRIT','EVENTO_TESP','EVENTO_CORSEG');
		$crud->columns('EVENTO_CRIT','EVENTO_CLI_CRIT','EVENTO_TESP','EVENTO_CORSEG');
		$crud->order_by('EVENTO_CRIT','desc');
		$crud->order_by('EVENTO_CLI_CRIT','desc');
		
		$crud->set_primary_key('EVENTO_SEG_ID');
		
		$crud->display_as('EVENTO_CRIT','Evento');
		$crud->display_as('EVENTO_CLI_CRIT','Criticidad cliente');
		$crud->display_as('EVENTO_TESP','Tiempo espera (minutos)');
		$crud->display_as('EVENTO_CORSEG','Correo escalamiento');
		
		$crud->unset_print();
		$crud->unset_export();
		
		$crud->add_fields('EVENTO_SEG_ID','EVENTO_CRIT','EVENTO_CLI_CRIT','EVENTO_TESP','EVENTO_CORSEG');
		$crud->edit_fields('EVENTO_CORSEG');
		$crud->set_rules('EVENTO_CORSEG', 'Correo escalamiento', 'callback_validateEmail');
		$crud->change_field_type('EVENTO_SEG_ID','invisible');
		$crud->callback_before_insert(array($this,'insertID'));
		
		$crud->set_lang_string('insert_error', 'Error. Ya existe esta relacion de criticidad entre Nodo-Cliente');
		
		$output = $crud->render();
		
		$this->loadView($output);
	}
	
	public function validateEmail($arrayMail){
	
		$arrayMail = str_replace("\r\n", "\n", $arrayMail);
		if ($arrayMail != '') {
			$csv_array= explode(",",$arrayMail);
			$count = count($csv_array);
			for ($n=0;$n<$count;$n++){
				trim($csv_array[$n]);
				$emails = $csv_array[$n];
				if(!filter_var($emails, FILTER_VALIDATE_EMAIL)){
					$this->form_validation->set_message('validateEmail', 'Favor verificar los correos');
					return FALSE;
				}
			}
			return TRUE;
		}
		return TRUE;
	}
	
	function insertID($post_array) {
		
		$post_array['EVENTO_SEG_ID'] = $post_array['EVENTO_CRIT'] . $post_array['EVENTO_CLI_CRIT'] . $post_array['EVENTO_TESP'];
		return $post_array;
	}
	
}