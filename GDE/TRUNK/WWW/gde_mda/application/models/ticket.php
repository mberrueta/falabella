<?php

class Ticket extends CI_Model {

/*	--------------------------------------------------
	:: Variables Globales
	-------------------------------------------------- */
	/* Array con filtros a listar en seccion de "Filtros" */
	/*
		'NOMBRE DE COLUMNA' => array( 'Frase a desplegar' , 'TABLA CONTRA LA QUE SE DEBE HACER JOIN (devolver columna con el mismo nombre)' ) 
	*/
	protected $filtros = array(
		'TICKET_EST' => array(
			'Filtro Estado de Tickets', 
			'SELECT DISTINCT (`TCKESTADO_INI`) as TICKET_EST FROM `ticket_estado` ORDER BY TICKET_EST ASC'
		), 
		'CLIENTE' => array(
			'Filtro Clientes', 
			'SELECT CLI_NOM as CLIENTE FROM cliente WHERE CLI_ID in %s ORDER BY CLIENTE ASC'
		)
	);
	
	

/*	--------------------------------------------------
	:: Manipulación de ticket
	-------------------------------------------------- */
	
	/**
	* Crea el registro del ticket creado en la base de datos del gestor
	*/
	public function creaTicket($wsResponse){
		$ticket_id = $wsResponse[1];
		$gevs = array();
		$gevs = explode(',',$wsResponse[0][6]);
		
		$ticket = array(
			'TICKET_ID'			=> $ticket_id,
			'TICKET_EST'		=> 'ASSIGNED',
			'TICKET_EMAIL'		=> $wsResponse[0][1],
			'TICKET_RESUMEN'	=> $wsResponse[0][2],
			'TICKET_COD_SERV'	=> $wsResponse[0][0],
			'TICKET_CLIID'		=> $wsResponse[0][5],
			'TICKET_CREADOR'	=> $this->session->userdata('username')
		);
		$this->db->set('TICKET_TSINI', 'NOW()', FALSE);
		$this->db->set('TICKET_SEG_TSINI', 'NOW()', FALSE);
		$this->db->set('TICKET_SEG_TSFIN', '(NOW() + INTERVAL 1 HOUR)', FALSE);
		
		$this->db->insert('ticket', $ticket);
		
		$disnoms = array();
		$this->db->select('DIS_NOM');
		$this->db->from('grupo_evento');
		$this->db->where_in('GEV_ID', $gevs);
		$result = $this->db->get();
		foreach($result->result() as $disnom){
			array_push($disnoms, $disnom->DIS_NOM);
		}
		foreach($gevs as $gev){
			$this->db->where('GEV_ID', $gev);
			$this->db->where('TICKET_ID', 'ERROR');
			$this->db->delete('ticket_gev');
		}
		
		$buffArray = array();
		
		$this->db->select('GEV_ID');
		$this->db->from('grupo_evento');
		$this->db->where_in('EST_NOM', array('Activo','En Proceso'));
		$this->db->where('grupo_evento.GEV_TSINI > DATE_SUB(CURDATE(),INTERVAL 7 DAY)',NULL,FALSE);
		$this->db->where_in('DIS_NOM', $disnoms);
		$result2 = $this->db->get();
		foreach($result2->result() as $gev){
			array_push($buffArray, array('TICKET_ID' => $ticket_id, 'GEV_ID' => $gev->GEV_ID, 'TIPO_TICKET' => $wsResponse[0][8]));
		}
		
		$this->db->insert_batch('ticket_gev',$buffArray);
	}
	
	public function creaErrorTicket($gevs){
		$gevs = explode(',',$gevs);
		
		foreach($gevs as $gev){
			$this->db->from('ticket_gev');
			$this->db->where('GEV_ID', $gev);
			$this->db->where('TICKET_ID', 'ERROR');
			if($this->db->get()->num_rows() == 0){
				$ticgev = array(
					'TICKET_ID'	=> 'ERROR',
					'GEV_ID'	=> $gev,
					'TIPO_TICKET' => 'ERROR'
				);
				$this->db->insert('ticket_gev', $ticgev);
			}
			else{
				
			}
		}
	}
	
	function listarClientes(){
		$this->db->select('grupo.CLI_ID');
		$this->db->from('grupo');
		$this->db->join('cliente','grupo.CLI_ID = cliente.CLI_ID');
		$this->db->where('grupo.USU_USER',$this->session->userdata('username'));
		$this->db->where('grupo.GRU_STT',1);
		$this->db->distinct();
		$result = $this->db->get();
		
		$listadoClientes = array();
		foreach($result->result() as $row){
			array_push($listadoClientes, $row->CLI_ID);
		}
		return $listadoClientes;
	}
	
	/**
	* Entrega el o los estados que siguen al consultado (maquina de estado)
	*/
	public function listarEstados($estadoInicial, $json = TRUE){
		$this->db->select('TCKESTADO_FIN');
		$this->db->from('ticket_estado');
		$this->db->where('TCKESTADO_INI', $estadoInicial);
		$result = $this->db->get();
		
		$options = array();
		
		foreach($result->result() as $option){
			array_push($options, $option->TCKESTADO_FIN);
		}

		return ($json) ? json_encode($options) : $options;
	}
	
	/**
	* 
	*/
	public function cerrarTicket($idsTicket, $accion, $estadoInicial ,$siguienteEstado){
		$this->load->model('evento');
		$cierre = '';
		$cierre2 = array();
		
		$this->db->from('ticket_gev');
		$this->db->where('TICKET_ID', $idsTicket);
		$result = $this->db->get();
		
		if($accion == '0'){
			$idsEventos = array();
			foreach( $result->result() as $row){
				array_push($idsEventos, $row->GEV_ID);
			}
			
			$cierre = $this->evento->cerrar_gevs_ticket( implode(',', $idsEventos), $this->session->userdata('username'));
			$cierre2 = explode('|',$cierre);
			if($cierre2[0] == '0'){
				return $cierre2[1];
			}
			else if($cierre2[0] == '1'){
				$this->db->where('TICKET_ID', $idsTicket);
				$this->db->update('ticket', array( 'TICKET_EST' => $siguienteEstado ) );
				$gevsactivos = array();
				foreach($idsEventos as $gev){
					$this->db->select('EST_NOM');
					$this->db->from('grupo_evento');
					$this->db->where('GEV_ID', $gev);
					
					if($this->db->get()->row()->EST_NOM == 'Activo'){
						array_push($gevsactivos, $gev);
					}
				}
				
				$this->db->where('TICKET_ID', $idsTicket);
				$this->db->where('TCKGEV_EST', 0);
				$this->db->where_in('GEV_ID', $gevsactivos);
				$this->db->update('ticket_gev', array( 'TCKGEV_EST' => 1 ));
				
				return $cierre2[1];
			}
		}
		else if($accion == '1'){
			return 'Ticket reasignado exitosamente';
		}
		else if($accion == '2'){
			return 'Ticket actualizó su motivo de estado';
		}
	}
	
/*	--------------------------------------------------
	:: Filtros de busqueda
	-------------------------------------------------- */	
	
	/**
	* Entrega el listado de filtros registrados
	*/
	public function getFiltros($public = FALSE){
	
		if($public){
			$buffArray = array();
			foreach($this->filtros as $k => $v){
				array_push($buffArray, $k );
			}
			
			return $buffArray;
		}

		return $this->filtros;
	}
	
	/**
	* Devuelve array o json con el listado de filtros con su respectiva tabla generada
	*/
	public function listarFiltros($creador, $json = FALSE){
		$listadoFiltros = array();
		$filtros = $this->getFiltros();
		$clientes = $this->listarClientes();
		
		foreach( $filtros as $filtro => $data ){
			$htmlFiltro = $this->listarFiltro($creador, $filtro, $data[1], $clientes);
			$listadoFiltros[$filtro] = $htmlFiltro;
		}
		return ($json) ? json_encode($listadoFiltros) : $listadoFiltros; // Devuelve un string json si es solicitado
	}
	
	/**
	* Entrega una tabla debidamente formada con los filtros asociados a una columna de la tabla
	*/
	public function listarFiltro($creador, $filtro, $join, $clientes){
		if ($filtro == 'CLIENTE'){
			$this->db->select('CLI_NOM as CLIENTE');
		}
		else {
			$this->db->select($filtro);
		}
		$this->db->select('count(*) as cnt', false);
		$this->db->from('ticket');
		$this->db->where('TICKET_CREADOR', $creador);
		$this->db->join('cliente', 'cliente.CLI_ID = ticket.TICKET_CLIID');
		$this->db->where_in('cliente.CLI_ID',$clientes);
		$this->db->group_by($filtro);
		$resultsFiltros = $this->db->get();
		
		$resultsJoin = null;
		$resultsHTML = array();
		
		if($join){ // Si se debe realizar un cruze, es porque el listado de filtros se encuentra completo en otra tabla de la BD
			$resultsJoin = $this->db->query(sprintf($join,"(".implode(",",$clientes).")")); // Se carga el listado de datos para realizar el cruze
			foreach($resultsJoin->result() as $resultJoin){
				foreach($resultsFiltros->result() as $resultFiltro){
					if( $resultJoin->$filtro === $resultFiltro->$filtro ){
						$resultsHTML[$resultJoin->$filtro] = $resultFiltro->cnt;
						break;
					}
					else{
						$resultsHTML[$resultJoin->$filtro] = 0;
					}
				}
			}
		}
		else{
			foreach($resultsFiltros->result() as $resultFiltro){
				$resultsHTML[$resultFiltro->$filtro] = $resultFiltro->cnt;
			}
		}
		
		return $this->generarFiltroHTML($filtro, $resultsHTML);
	}
	
	/**
	* Funcion auxiliar, genera el html necesario para devolver una tabla con los filtros
	*/
	private function generarFiltroHTML($tipo, $arrayFiltro){
	
		$this->load->library('table');
        $this->load->helper('form');
		
		$data = array();
		
		foreach($arrayFiltro as $k => $v){
			$checkboxData = array(
				'name'        => $tipo,
				'checked'     => false,
				'value'       => $k,
				'class'       => 'filtro ' . $tipo
			);
			array_push($data, array(form_checkbox($checkboxData), $k, $v)); // se carga fila de tabla con checkbox para filtrar
		}

		$tmpl = array ( // Se elimina encabezado de tabla ya que no es ocupado
			'table_open' 	=> '<table border="0" cellpadding="5" cellspacing="5">',
			'thead_open'	=> '',
			'thead_close'	=> '',
			'heading_row_start'		=> '',
			'heading_row_end'		=> '',
			'heading_cell_start'	=> '',
			'heading_cell_end'		=> '',
		);
		
		$this->table->set_heading(array());
		$this->table->set_template($tmpl);
		
        return $this->table->generate($data);
	}
	
	
/*	--------------------------------------------------
	:: TABLA DE TICKETS
	-------------------------------------------------- */	
	
	/**
	* Se genera la tabla para plugin datatables_2
	*/
	public function obtener_tickets($creador){
		
		$this->load->library('datatables');
		
		$this->datatables->select('res.TICKET_ID');			// ID del ticket
		$this->datatables->select('ticket.TICKET_ID as ID');			// ID del ticket
		$this->datatables->select('ticket.TICKET_EST');		// Estado
		$this->datatables->select('ticket.TICKET_TSINI');		// Fecha creacion de ticket
		$this->datatables->select('IF(ticket.TICKET_EST="CLOSED",NULL,TIMESTAMPDIFF(MINUTE,NOW(),ticket.TICKET_SEG_TSFIN)) as REVISION', FALSE);
		$this->datatables->select('CLIENTE');
		$this->datatables->select('res.CANTIDAD');
		$this->datatables->select('IFNULL(nombre.DIS_NOM,res.DIS_NOM) as DISPO', FALSE);
		
		$this->datatables->from('ticket');

		$this->datatables->join('(SELECT TICKET_ID,COUNT(DISTINCT DIS_NOM) AS CANTIDAD,DIS_NOM FROM grupo_evento JOIN ticket_gev WHERE (grupo_evento.GEV_ID=ticket_gev.GEV_ID) GROUP BY TICKET_ID) as res','ticket.TICKET_ID=res.TICKET_ID','left outer');
		$this->datatables->join('(SELECT TICKET_ID, grupo_evento.DIS_NOM, DIS_CODSERV  FROM grupo_evento JOIN ticket_gev ON grupo_evento.GEV_ID=ticket_gev.GEV_ID LEFT OUTER JOIN dispositivo ON dispositivo.DIS_NOM = grupo_evento.DIS_NOM GROUP BY TICKET_ID, grupo_evento.DIS_NOM) as nombre','nombre.TICKET_ID=ticket.TICKET_ID AND nombre.DIS_CODSERV=ticket.TICKET_COD_SERV','left outer');
		$this->datatables->join('(SELECT CLI_ID,CLI_NOM as CLIENTE FROM cliente) as cli','cli.CLI_ID=ticket.TICKET_CLIID');

		$this->datatables->where('ticket.TICKET_CREADOR', $creador);
		
		$this->datatables->add_column('Opciones','<div class="menu_acciones"><img src="'.base_url().'/images/ticket_mail.png" class="mail_boton"><img src="'.base_url().'/images/ticket_info.png" class="info_boton"><span id="spin" class="spinner"></span></div>');
		$this->datatables->edit_column('res.TICKET_ID','<input type="checkbox" name="check" value="$1" class="check" />','res.TICKET_ID');
		
		$result = $this->datatables->generate(); 
		echo $result;
	}
	
	// Resumen:     Obtencion de datos para el despliegue de Datos Ticket (Doble Click en Ticket)
	// Utilizacion: Informacion de Trabajo.
	// Entrada:     ID del TICKET.
	// Salida:      MAIL.
	public function obtenerDatos($ticketId, $tipo = true){
		
		$this->db->select('TICKET_ID');
		$this->db->select('TICKET_EST');
		$this->db->select('TICKET_TSINI');
		$this->db->select('CLI_NOM');
		$this->db->select('TICKET_EMAIL');
		$this->db->select('TICKET_RESUMEN');
		$this->db->from('ticket');
		$this->db->join('cliente', 'cliente.CLI_ID=ticket.TICKET_CLIID');
		$this->db->where('TICKET_ID', $ticketId);
		$result = $this->db->get();
		
		if($tipo){
			$html = "<table>";
			
			foreach($result->result() as $row){
				$html .= "<tr><td>ID Ticket</td><td><b id='selectedID'>".$row->TICKET_ID."</b></td></tr>";
				$html .= "<tr><td>Estado</td><td><b>".$row->TICKET_EST."</b></td></tr>";
				$html .= "<tr><td>Fecha creación</td><td><b>".$row->TICKET_TSINI."</b></td></tr>";
				$html .= "<tr><td>Cliente</td><td><b>".$row->CLI_NOM."</b></td></tr>";
				$html .= "<tr><td>Correo del operador</td><td><b>".$row->TICKET_EMAIL."</b></td></tr>";
				$html .= "<tr><td>Resumen</td><td><b>".$row->TICKET_RESUMEN."</b></td></tr>";
			}
			$html .= "</table>";
			return "<div class=\"display\">".$html."</div>";
		}
	}
	
	// Resumen:     Obtencion de correo asociados a un ticket.
	// Utilizacion: Informacion de Trabajo.
	// Entrada:     ID del TICKET.
	// Salida:      MAIL.
	public function getCorreoTicket($ticketId){
		$this->db->select('TICKET_EMAIL');
		$this->db->from('ticket');
		$this->db->like('TICKET_ID', $ticketId);
		$result = $this->db->get();
		
		foreach($result -> result() as $row){
			return $row->TICKET_EMAIL;
		}
	}
	
	// Resumen:     Obtencion de dispositivos asociados a un ticket.
	// Utilizacion: Informacion para el correo y Detalle de Ticket.
	// Entrada:     ID del TICKET.
	// Salida:      ARRAY de DISPOSITIVOS UNICOS.
	// Errores:     En caso de error se devuelve un string vacio.
	public function obtenerNodosAsociados($ticketId, $tipo = true){
		
		$this->db->select('res.GEV_ID');
		$this->db->select('grupo_evento.DIS_NOM');
		$this->db->select('GEV_TSINI');
		$this->db->select('DIS_CODSERV');
		$this->db->select('TEV_NOM');
		$this->db->select('IFNULL(GEV_SEM,IFNULL(GEV_LOCK,5)) as LOCK_CONT', FALSE);
		$this->db->select('GEV_IP');
		$this->db->select('GEV_CONCNOM');
		$this->db->from('grupo_evento');
		$this->db->join('(SELECT GEV_ID,TICKET_ID FROM ticket_gev) as res','grupo_evento.GEV_ID=res.GEV_ID');
		$this->db->join('(SELECT DIS_NOM,DIS_CODSERV FROM dispositivo) as disp','grupo_evento.DIS_NOM=disp.DIS_NOM','left outer');
		$this->db->like('res.TICKET_ID', $ticketId);
		$this->db->group_by('grupo_evento.DIS_NOM');
		$result = $this->db->get();
		
		if($tipo){
			$html = "<thead><tr><th>Nodo</th><th></th><th>IP</th><th>Servidor</th></tr></thead>";
			$html .= "<tbody>";
			
			foreach($result -> result() as $row){
				$html .= "<tr>";
				$html .= "<td style = \"display:none\">".$row->GEV_ID."</td>";
				$html .= "<td>".$row->DIS_NOM."</td>";
				$html .= "<td><img src=".base_url()."images/s".$row->LOCK_CONT.".png /></td>";
				$html .= "<td>".$row->GEV_IP."</td>";
				$html .= "<td>".$row->GEV_CONCNOM."</td>";
				$html .= "</tr>";
			}
			
			$html .= "</tbody>";
			return "<table id=\"nodos_asociados\" >".$html."</table>";
		} 
		else{
			// Este bloque se utiliza para llenar los datos solicitados para el envio del correo.
			$disp   = array();
			$tipo   = array();
			$codigo = array();
			$fecha  = array();
			
			// Se guardan los resultado en distintos arrays.
			foreach($result->result() as $row){
				array_push($disp,$row->DIS_NOM);
				array_push($codigo,$row->DIS_CODSERV);
				array_push($tipo,$row->TEV_NOM);
				array_push($fecha,$row->GEV_TSINI);
			}
			
			// Se obtiene la fecha del evento mas antiguo
			$min = min(array_map('strtotime', $fecha));
			$fecha = date('d-m-Y H:i', $min);
			
			// Parseador de los tipos
			$tipo_string = '';
			$temp = array_unique($tipo);
			foreach($temp as $valor){
				$tipo_string = $tipo_string.$valor."\n";
			}
			
			// Parseador de los dispositivos
			$disp_string = '';
			$temp = array_unique($disp);
			foreach($temp as $valor){
				$disp_string = $disp_string.$valor."\n";
			}
			
			// Parseador de los codigos
			$codigo_string = '';
			$temp = array_unique($codigo);
			foreach($temp as $valor){
				$codigo_string = $codigo_string.$valor."\n";
			}
			
			// Se eliminan lineas vacias
			$disp = preg_replace('/^[ \t]*[\r\n]+/m', '', $disp_string);
			$tipo_string = preg_replace('/^[ \t]*[\r\n]+/m', '', $tipo_string);
			$codigo = preg_replace('/^[ \t]*[\r\n]+/m', '', $codigo_string);
			
			// Se construye array final con respuestas
			$final = array();
			$final['info_dispo']  = rtrim ($disp_string, "\n");
			$final['info_tipo']   = rtrim ($tipo_string, "\n");
			$final['info_fecha']  = $fecha." Hrs";
			$final['info_codigo'] = rtrim ($codigo_string, "\n");
			
			// Fin
			return $final;
		}
	}
	
	public function obtenerNodosAsociados2($ticketId){
		
		$this->db->select('res.GEV_ID');
		$this->db->select('grupo_evento.DIS_NOM');
		$this->db->from('grupo_evento');
		$this->db->join('(SELECT GEV_ID,TICKET_ID FROM ticket_gev) as res','grupo_evento.GEV_ID=res.GEV_ID');
		$this->db->like('res.TICKET_ID', $ticketId);
		$result = $this->db->get();
		
		$disp   = array();
		foreach($result->result() as $row){
			array_push($disp,$row->DIS_NOM.','.$row->GEV_ID);
		}
		return $disp;
	}
	
	// Resumen:     Obtencion de dispositivos asociados a un ticket.
	// Utilizacion: Envio de Correo a Clientes.
	// Entrada:     ID del TICKET.
	// Salida:      ARRAY de DISPOSITIVOS UNICOS.
	// Errores:     En caso de error se devuelve un string vacio.
	public function obtenerNodosAsociados_Correo($ticketId){
		
		$this->db->select('DIS_NOM');
		$this->db->from('grupo_evento');
		$this->db->join('(SELECT GEV_ID,TICKET_ID FROM ticket_gev) as res','grupo_evento.GEV_ID=res.GEV_ID');
		$this->db->like('res.TICKET_ID', $ticketId);
		$result = $this->db->get();
		
		$nodosAsoc = array();
		
		foreach($result -> result() as $row){
			array_push($nodosAsoc,$row->DIS_NOM);
		}
		
		return array_unique($nodosAsoc);
	}
	
	// Resumen:     Obtencion de codigos de servicios para un ticket en especifico.
	// Utilizacion: Envio de Correo Boleta no Gestionada.
	// Entrada:     ID del TICKET.
	// Salida:      MAIL.
	public function obtenerCodigosServicio($ticketId){
		
		$sql = 'SELECT IFNULL(dispositivo.DIS_CODSERV,dispositivo.DIS_CSPRINC) as DISPOS
				FROM ticket_gev
					LEFT OUTER JOIN grupo_evento ON grupo_evento.GEV_ID = ticket_gev.GEV_ID
					LEFT OUTER JOIN dispositivo ON grupo_evento.DIS_NOM = dispositivo.DIS_NOM
				WHERE TICKET_ID = ?';
		$result = $this->db->query($sql, array($ticketId));
		
		$codigos = array();
		
		foreach($result -> result() as $row){
			array_push($codigos,$row->DISPOS);
		}
		
		return array_unique($codigos);
	}
	
	// Resumen:     Obtencion de los eventos asociados a un grupo ID.
	// Utilizacion: Detalle de Ticket.
	// Entrada:     ID del GRUPO EVENTO.
	// Salida:      HTML con los eventos formados para su visualizacion.
	// Errores:     En caso de error se devuelve el html vacio.
	public function obtenerEventosDatos($grupoId, $nomNodo, $selectedID){
		
		// Rescatamos ticket ID
		// $this->db->select('ticket_gev.TICKET_ID');
		// $this->db->from('ticket_gev');
		// $this->db->where('GEV_ID', $grupoId);
		// $ticketID = $this->db->get()->row()->TICKET_ID;
		// $this->db->flush_cache();
		
		// Rescatamos grupos de eventos asociados
		$this->db->select('ticket_gev.GEV_ID');
		$this->db->from('ticket_gev');
		$this->db->where('TICKET_ID', $selectedID);
		$objGruposID = $this->db->get();
		$this->db->flush_cache();
		
		// Se genera listado de grupos de eventos
		$arrayGruposID = array();
		foreach( $objGruposID->result() as $row ){
			
			array_push($arrayGruposID, $row->GEV_ID );
		}
		
		// Se consultan grupos de eventos
		$this->db->select('EVE_TS');
		$this->db->select('TEV_NOM');
		$this->db->select('EVE_MEN');
		$this->db->from('evento');
		$this->db->where_in('GEV_ID', $arrayGruposID);
		$this->db->where('DIS_NOM', $nomNodo);
		$this->db->order_by('EVE_TS', 'desc');
		$result = $this->db->get();
		$data = $result->row_array(1);
		
		$html = "<p style=\"text-align: left; font-weight: bold;\">Detalle ".$nomNodo.":</p>";
		$html .= "<thead><tr><th>Fecha</th><th>Evento</th><th>Detalle</th></tr></thead>";
		$html .= "<tbody>";
		
		foreach($result -> result() as $row){
			$html .= "<tr>";
			$html .= "<td>".$row->EVE_TS."</td>";
			$html .= "<td>".$row->TEV_NOM."</td>";
			$html .= "<td>".$row->EVE_MEN."</td>";
			$html .= "</tr>";
		}
		
		$html .= "</tbody>";
		return "<table id=\"nodos_eventos_ticket\" >".$html."</table>";
	}
	
	// Resumen:     Setea el valor de la columna TICKET_SEG_TSFIN para calcular el tiempo de desface en el cual no se ha enviado un correo al cliente
	// Utilizacion: Correo de seguimiento de boleta no escalada.
	// Entrada:     TICKET ID.
	// Salida:      Nula.
	public function uptadeRevision($ticketId){
	
		
		$this->db->set('TICKET_SEG_TSINI', 'NOW()', FALSE);
		$this->db->set('TICKET_SEG_TSFIN', '(NOW() + INTERVAL 1 HOUR)', FALSE);
		$this->db->where('TICKET_ID', $ticketId);
		$this->db->update('ticket'); 
		
		/*$sql = 'UPDATE ticket 
				SET TICKET_SEG_TSINI = NOW(), TICKET_SEG_TSFIN = (NOW() + INTERVAL 1 HOUR) 
				WHERE TICKET_ID='.$ticketId;
				
		$this->db->query($sql);*/
	}

	// Resumen:     Update del estado del ticket y fecha de modificacion.
	// Utilizacion: Se genera en caso de respuesta ok en generar info de trabajo.
	// Entrada:     Ticket ID, ESTADO y fecha de modificacion.
	// Salida:      Nula.
	// Errores:     Nula.
	public function updateEstadoTicket($ticketId, $estado, $fecha_modificacion){
		
		// Se guarda el estado anterior en columna TICKET_LAST_EST
		$this->db->select('TICKET_EST');
		$this->db->from('ticket');
		$this->db->where('TICKET_ID', $ticketId);
		$result = $this->db->get();
		
		foreach($result -> result() as $row){
			$old_estado = $row->TICKET_EST;
		}
		
		$old = array(
			'TICKET_LAST_EST' => $old_estado
		);
		
		$this->db->where('TICKET_ID', $ticketId);
		$this->db->update('ticket', $old); 
		// Fin ultimo estado
		
		// Se inserta estado actual
		$nuevo_estado = strtoupper($estado);
		$data = array(
			'TICKET_EST' => $nuevo_estado,
			'TICKET_TSFIN' => $fecha_modificacion
		);
		
		$this->db->where('TICKET_ID', $ticketId);
		$this->db->update('ticket', $data); 
		
	}
	
	// Resumen:     Obtencion de ticket que no han cambiado su estado en su ultimo ciclo dependiendo de la criticidad del cliente y la criticidad del dispostivo. Condicion que el estado no se encuentre cerrado
	// Utilizacion: Correo de seguimiento de boleta no escalada.
	// Entrada:     Nula.
	// Salida:      Respuesta SQL con las comprobaciones.
	// Errores:     Respuesta Nula.
	public function obtenerNodoEstados(){
		
		$sql = 'SELECT ticket.TICKET_ID, ticket.TICKET_TSINI, ticket.TICKET_EST, ticket.TICKET_LAST_EST, ticket.TICKET_CONT, ticket.TICKET_COD_SERV, ticket_gev.GEV_ID, grupo_evento.CLI_ID, grupo_evento.DIS_NOM, cliente.CLI_NOM , cliente.CLI_CRIT, min(dispositivo.DIS_CRIT) as DIS_CRIT, ticket_seguimiento.TICKET_TESP, ticket_seguimiento.TICKET_CORSEG
				FROM ticket
					LEFT OUTER JOIN ticket_gev 
						ON ticket_gev.TICKET_ID = ticket.TICKET_ID
					LEFT OUTER JOIN grupo_evento 
						ON grupo_evento.GEV_ID = ticket_gev.GEV_ID
					LEFT OUTER JOIN cliente 
						ON cliente.CLI_ID = grupo_evento.CLI_ID
					LEFT OUTER JOIN dispositivo 
						ON dispositivo.DIS_NOM = grupo_evento.DIS_NOM
					RIGHT OUTER JOIN ticket_seguimiento 
						ON ticket_seguimiento.TICKET_DIS_CRIT = dispositivo.DIS_CRIT 
							AND ticket_seguimiento.TICKET_CLI_CRIT = cliente.CLI_CRIT
					WHERE ticket.TICKET_EST="PENDING" OR ticket.TICKET_EST="IN PROGRESS" OR ticket.TICKET_EST="ASSIGNED"
					GROUP BY ticket.TICKET_ID';
				
		$result = $this->db->query($sql);
		return $result;
	}
	
	// Resumen:     Aumenta el contador de minutos de la tabla ticket
	// Utilizacion: Escalamiento de boleta no gestionada.
	// Entrada:     TICKET ID y VALOR NUMERICO.
	// Salida:      Nula.
	public function uptadeContadorSeguimiento($ticketId, $valor){
	
		$data = array(
			'TICKET_CONT' => $valor
		);
		
		$this->db->like('TICKET_ID', $ticketId);
		$this->db->update('ticket', $data); 
	}
	
	public function getCriticidadClientes(){
		$sql = 'SELECT DISTINCT (CLI_CRIT)
				FROM  `cliente`
				WHERE (`cliente`.`CLI_CRIT` >= 0 AND `cliente`.`CLI_CRIT` <=9)
				ORDER BY  `cliente`.`CLI_CRIT` ASC ';
		$result = $this->db->query($sql);
		
		$stack = array();
		
		foreach($result -> result() as $row){
			$stack[$row->CLI_CRIT] = $row->CLI_CRIT;
			// array_push($stack, $row->CLI_CRIT);
		}
		
		return $stack;
		}
		
	public function getCriticidadDispo(){
		$sql = 'SELECT DISTINCT (DIS_CRIT)
				FROM  `dispositivo`
				where DIS_CRIT <> "" 
				ORDER BY `dispositivo`.`DIS_CRIT` ASC';

		$result = $this->db->query($sql);
		
		$stack = array();
		
		foreach($result -> result() as $row){
			// array_push($stack, $row->DIS_CRIT);
			$stack[$row->DIS_CRIT] = $row->DIS_CRIT;
		}
		
		return $stack;
	}
	
	// Resumen:     Registro de Bitacora de accion en el ticket realizada.
	// Utilizacion: Envio de Correos.
	// Entrada:     TICKET ID, MENSAJE, ACCION y OPERADOR.
	// Salida:      Nula.
	public function comentaTicket($ticketID,$mensaje,$accion,$operador){
		
		$comentario = array(
			'TICKET_ID'       => $ticketID,
			'TICKET_COM_COM'  => $mensaje,
			'TICKET_COM_ACC'  => $accion,
			'TICKET_USU_USER' => $operador
		);
		$this->db->set('TICKET_COM_TS', 'NOW()', FALSE);
		
		if($this->db->insert('ticket_comentario', $comentario)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function registrarCorreos($postArray)
	{
		$nodos = explode(";",$postArray['table3']);
		for($i=0;$i<count($nodos)-1;$i++){
			$filas[$i] = $nodos[$i];
		}
		$data = array(
			'CORSC_TSINI' => date('Y-m-d H:i:s'),
			'CORSC_TSSEG_FIN' => date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'CORSC_EST' => 1,
			'CORSC_LSTCOM' => $postArray['obs']
			);
		
		$this->db->trans_start();
			$this->db->insert('correo_seguimiento', $data);
		$this->db->trans_complete();
		
		$this->db->trans_start();
			$this->db->select_max('CORSC_ID');
			$this->db->from('correo_seguimiento');
			$result1 = $this->db->get();
		$this->db->trans_complete();
		
		$this->db->trans_start();
			$this->db->select('CLI_ID');
			$this->db->from('cliente');
			$this->db->where('CLI_NOM', $postArray['clientehid']);
			$result2 = $this->db->get();
		$this->db->trans_complete();
		
		foreach($result1->result() as $row){
			$res1["CORSC_ID"] = $row->CORSC_ID;
		}
		foreach($result2->result() as $row){
			$res2["CLI_ID"] = $row->CLI_ID;
		}
		
		$i = 0;
		foreach ($filas as $row) {
			$cel = explode(",",$row);
			if($i == 0){
				$gevid = $cel[0];
				$i++;
			}
			else{
				$gevid = $gevid.','.$cel[0];
			}
		}
		foreach ($filas as $row) {
			$cel = explode(",",$row);
			$data2 = array(
				'CORSC_ID' => $res1["CORSC_ID"],
				'CLI_ID' => $res2["CLI_ID"],
				'DIS_NOM' => $cel[3],
				'GEV_ID' => $cel[0]
			);
			$this->db->trans_start();
				$this->db->insert('correo_nodo', $data2);
			$this->db->trans_complete();
			
			$data3 = array(
				'GEV_ID' => $cel[0],
				'COM_COM' => 'Se envia correo a '.$postArray['para'].' con copia a '.$postArray['copia'].' ++ Con asunto: '.$postArray['asunto'].' ++ Asociado a los ids: '.$gevid,
				'EST_NOM' => 'correo',
				'COM_TS' => date("Y-m-d H:i:s"),
				'USU_USER' => $this->session->userdata('username')
			);
			$this->db->trans_start();
				$this->db->insert('comentario', $data3);
			$this->db->trans_complete();
		}
		$respaldocorreo = array(
			'CORSC_ID' => $res1["CORSC_ID"],
			'CORRES_TO' => $postArray['para'],
			'CORRES_CC' => $postArray['copia'],
			'CORRES_ASU' => $postArray['asunto'],
			'CORRES_CLI' => $postArray['cliente'],
			'CORRES_NUMIN' => $postArray['numin'],
			'CORRES_ESTACT' => $postArray['estactividad'],
			'CORRES_DESCEV' => $postArray['desevento'],
			'CORRES_ACCREAL' => $postArray['accrealiz'],
			'CORRES_OBS' => $postArray['obs'],
			'CORRES_TSINI' => $postArray['fecini'],
			'CORRES_TSFIN' => $postArray['fecter'],
			'CORRES_TIPAL' => $postArray['tipoevento'],
			'CORRES_ALEQ' => $postArray['alaeq'],
			'CORRES_CODSERV' => $postArray['codservicio'],
			'CORRES_SUCUR' => $postArray['sucursales'],
			'CORRES_NOM' => $postArray['nombre'],
			'CORRES_GLOSA' => $postArray['firma'],
			'CORRES_EMAIL' => $postArray['email'],
			'CORRES_TEL' => $postArray['fono']
			);
		$this->db->insert('correo_respaldo', $respaldocorreo);
	}
}
?>