<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_bitacora_nodos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model( 'nodos' );
	}
	
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$data['titulo_pagina'] = 'Bitacora de Nodos';
		$this->load->view('admin/v_bitacora_nodos',$data);
	}
	
	function listarNodos(){
		$result = $this->nodos->listadoNodos();
		
		echo json_encode($result);
	}
	
	function getNodo(){
		$nodo = $_POST['nodo'];
		$result = $this->nodos->getNodo($nodo);
		
		echo json_encode($result);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2){
			return true;
		}
		else{
			return false;
		}
	}
}