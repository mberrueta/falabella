<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" type="text/css" media="screen" />
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>css/layout.css" type="text/css" media="screen" /> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/gde_entel.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-sliderAccess.js"></script>

	<script type="text/javascript" >
	$(document).ready(function(){
$('#tiempo_inicio').datetimepicker({
    dateFormat: 'yy-mm-dd',
    onClose: function(dateText, inst) {
        var endDateTextBox = $('#tiempo_fin');
        if (endDateTextBox.val() != '') {
            var testStartDate = new Date(dateText);
            var testEndDate = new Date(endDateTextBox.val());
            if (testStartDate > testEndDate)
                endDateTextBox.val(dateText);
        }
        else {
            endDateTextBox.val(dateText);
        }
    },
    onSelect: function (selectedDateTime){
        var start = $(this).datetimepicker('getDate');
        $('#tiempo_fin').datetimepicker('option', 'minDate', new Date(start.getTime()));
    }
});
$('#tiempo_fin').datetimepicker({
    dateFormat: 'yy-mm-dd',
    onClose: function(dateText, inst) {
        var startDateTextBox = $('#tiempo_inicio');
        if (startDateTextBox.val() != '') {
            var testStartDate = new Date(startDateTextBox.val());
            var testEndDate = new Date(dateText);
            if (testStartDate > testEndDate)
                startDateTextBox.val(dateText);
        }
        else {
            startDateTextBox.val(dateText);
        }
    },
    onSelect: function (selectedDateTime){
        var end = $(this).datetimepicker('getDate');
        $('#tiempo_inicio').datetimepicker('option', 'maxDate', new Date(end.getTime()) );
    }
});

$('#export_result').click(function(e){
    e.preventDefault();
	
	var t_inicio = $('#tiempo_fin').val();
	var t_fin     = $('#tiempo_inicio').val();
	
	
	
	$('#mensaje').attr('style','').text("Cargando...");
	
	if( t_inicio || t_fin){
		 $('#mensaje').attr('style', 'color: green;').text("Espere un momento, en breve comenzará su descarga...");
		 window.location.href = "c_exportar_resultados/exportar/"+t_inicio.replace(/\:/gi, "<dospuntos>")+"/"+t_fin.replace(/\:/gi, "<dospuntos>");
	}
	else{
		 $('#mensaje').attr('style', 'color: red;').text("Favor seleccione fechas validas");
	}
	
	return 0;
});

});
	</script>
</head>
<body style="background-color: #FFFFFF;">

		<p>Exportación de resultados a CSV</p>
		<p id="mensaje"></p>
		<div>
			<form id="solicitud_csv">
			    <input type="text" name="tiempo_fin" id="tiempo_inicio" value="" /> 
			    <input type="text" name="tiempo_inicio" id="tiempo_fin" value="" />

			    <input type="submit" name="export_result" id="export_result" value="Exportar Resultados" />
			</form>
		</div>

</body>
</html>