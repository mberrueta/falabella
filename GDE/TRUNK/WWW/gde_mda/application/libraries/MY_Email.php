<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email
{
	public function my_print_debugger(){
		$msg = '';
		
		if (count($this->_debug_msg) > 0)
		{
			foreach ($this->_debug_msg as $val)
			{
				$msg .= $val;
			}
		}
		
		$msg .= "<pre>".$this->_header_str."\n".htmlspecialchars($this->_subject)."\n".'</pre>';
		return $msg;
	}
}