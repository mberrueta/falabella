<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_login extends CI_Controller {

	public function index($mensaje='')
	{
		#return $this->load->view('paso_produccion');
		$data = array();
		$mensajes = explode('|', $mensaje);
		if(isset($mensajes[0]) && isset($mensajes[1])){
			if($mensajes[0] == 'E'){
				$data['clase_mensaje'] = "error_login";
			}
			else{
				$data['clase_mensaje'] = "ok_login";
			}
			$data['mensaje'] = $mensajes[1];
		}
		else{
			$data['clase_mensaje'] = "";
			$data['mensaje'] = $mensaje;
		}
		$this->session->sess_destroy();
		$this->load->view('v_login',$data);
	}
	
	public function autenticar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Nombre de usuario', 'trim|required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[4]|max_length[15]');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->index('E|');
		}
		else{
			$this->load->model('usuario');
			$data = $this->usuario->login($this->input->post('username'),md5($this->input->post('password')));
			
			#Si no se encontro coincidencia en BD
			if($data['tip'] == NULL){
				$this->index('E|Usuario o Contraseña invalida');
			}
			
			#Si se presenta problemas en la ejecucion de querys a la BD
			elseif($data['tip'] == 'ERROR'){
				$this->index('E|Se ha presentado un problema al intentar acceder al sistema, favor intentar nuevamente<br />Si el problema persiste comunicar al administrador.');
			}
			
			else{
				$inf = array(
					'username'     => $this->input->post('username'),
					'tipo'         => $data['tip'],
					'nombre'       => $data['nom'],
					'is_logged_in' => true
				);

				$this->session->set_userdata($inf);

				$this->load->model('auditoria');
				$this->auditoria->regAccionAuditoria('LogIn');
				
				if($data['tip'] == 1 || $data['tip'] == 3 || $data['tip'] == 4){ //1 Operador
					return redirect('operador/c_principal');
				}
				if($data['tip'] == 2){ //2 Admin
					return redirect('admin/c_menu_admin');
				} 
				// elseif($data['tip'] == 3){ //4 Visua
					// return redirect('operador/c_principal');
				// }
				/*elseif($data['tip'] == 0){ //0 Supervisor
					return redirect('supervisor/c_menu_supervisor');
				}
				else{ //3 Supervisor turno Vistas Estadisticas
					return redirect('estadisticas/c_menu_estadisticas');
				}*/
			}
		}
	}
	
	public function salir()
	{
		$this->load->model('auditoria');
		$this->auditoria->regAccionAuditoria('LogOut');
		
		$this->index('S|Ha cerrado sesión exitosamente.');
	}
	
	public function acceso_denegado()
	{
		$this->index('E|Usted no tiene los permisos necesarios para acceder a la sección.');
	}
}

/* End of file c_login.php */
/* Location: ./application/controllers/c_login.php */
