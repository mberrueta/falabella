<html >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Email Template Gestor de Eventos</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body style="">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body" style="padding: 0px 20px 0px 20px !important;">
						<span style="font-size: 50px; font-family: Gisha; font-weight: bold; color: #0068A1;">e</span>
						<span style="font-size: 25px; font-family: Arial Black; font-weight: bolder; color: #ef8e01; margin-left: -4px; vertical-align: 6px;">)</span>
						<span style="font-size: 35px; font-family: Gisha; font-weight: bold; color: #0068A1; margin-left: 4px; vertical-align: 3px;">entel</span>
						<span style="font-size: 20px; font-family: Gisha; font-weight: bold; color: #ef8e01; margin-left: 55px; vertical-align: 7px; font-style: oblique;">Centro de Monitoreo de Servicios a Clientes</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-heading">Resumen</div>
							<div class="panel-body">
							</div>
							<!-- Table -->
							<table class="table">
							  <tbody>
								 <tr style="background-color: red;">
									<td colspan=2 >Estado Servicio</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Cliente</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Establecimiento</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Tipo de Falla</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td rowspan=2 >Enlaces</td>
											
									</td>
									<td>principal</td><td>------------</td>
								 </tr>
								 <tr>
									<td>respaldo</td><td>------------</td>
								 </tr>
								 <tr style="background-color: green;">
									<td colspan=2 >Estado Incidente</td>
									<td>------------</td>
								 </tr>
							  </tbody>
							</table>
						 </div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-heading">Detalles</div>
							<div class="panel-body">
							</div>
							<!-- Table -->
							<table class="table">
							  <tbody>
								 <tr style="background-color: white;">
									<td colspan=2 >N° Incidente</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Tipo de Alarma</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Alarma de Equipo</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td colspan=2 >Tipo de Falla</td>
									<td>------------</td>
								 </tr>
								 <tr>
									<td rowspan=2 >Fecha/Hora Incidente</td>
											
									</td>
									<td>inicio</td><td>------------</td>
								 </tr>
								 <tr>
									<td>termino</td><td>------------</td>
								 </tr>
								 <tr style="background-color: white;">
									<td colspan=2 >Dirección del Servicio</td>
									<td>------------</td>
								 </tr>
							  </tbody>
							</table>
						 </div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-heading">Historial</div>
							<div class="panel-body">
							</div>
							<!-- Table -->
							<table class="table">
							  <tbody>
								 <tr>
									<td>21/10/2014 15:55:03</td>
									<td>Se revisan los eventos</td>
								 </tr>
								 <tr>
									<td>21/10/2014 16:32:34</td>
									<td>Segunda revision</td>
								 </tr>
								 <tr>
									<td>21/10/2014 18:02:56</td>
									<td>Cierre de Problema</td>
								 </tr>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-heading">Observaciones</div>
							<div class="panel-body">
								<p>Muchas Observaciones del ultimo correo<p/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body" style="margin-top: -60px; font-size: large;">
						<div class="row">
						  <div class="col-md-1"><span style="font-size: 200px; font-family: Arial Black; font-weight: bolder; color: #ef8e01; margin-left: -4px; vertical-align: 6px;">(</span></div>
						  <div class="col-md-6" style="margin-top: 100px;">
							<div class="row">
							  <div class="col-md-6">Sebastian Quezada</div>
							  <div class="col-md-6">Ingeniero de Sistemas Küdaw S.A.</div>
							</div>
							<div class="row">
							  <div class="col-md-12">Area de Desarrollo</div>
							</div>
							<div class="row">
							  <div class="col-xs-12">El Bosque Sur 65 Of. 3, Las Condes</div>
							</div>
							<div class="row">
							  <div class="col-xs-12">squezada@kudaw.com</div>
							</div>
						  </div>
						  <div class="col-md-1"><span style="font-size: 200px; font-family: Arial Black; font-weight: bolder; color: #ef8e01; margin-left: -4px; vertical-align: 6px;">)</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>