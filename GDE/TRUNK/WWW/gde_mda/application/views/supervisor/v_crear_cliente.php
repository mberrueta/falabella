<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Crear cliente</h3>
			</header>
			<div style="margin:15px;"> 
			<?php
			if($men != ''){
				$mensaje = explode('|',$men);
				echo $mensaje[1];
			}
			?>
			
			
			<?php echo form_open('supervisor/c_crear_cliente/crear_cliente');?>
			<div id="crear_cliente" style="width: 100%;">
				<table border="0" cellspacing="0" cellpadding="0" style="width: 25%; float: left; margin-right: 5%;">
					<caption>Información básica</caption>
					<thead style="background-color: #0073AE; color: white;">
						<tr>
							<th>Atributo</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Nombre:</td>
							<td><input type="text" id="cNombre" size="20" name="cNombre" value="<?php echo set_value('cNombre'); ?>" /></td>
							<td><span id="nameInfo"></span></td>
						</tr>
						<tr>
							<td>Criticidad:</td>
							<td>
								<select name="cCriticidad" style="width: 12em;">
								<?php
									for($i = 0; $i<=9; $i++){
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
								?>
								</select>
							</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<?php 
			if($operadores['stt']){
				echo '<table id="operadores" border="0" cellspacing="0" cellpadding="0" width="100%">';
				echo '<thead>';
					echo '<tr>';
						echo '<th>Operadores</th>';
					echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				echo '<tr>';
				echo '<td><select class="usuarios" id="selectCT1" name="otros[]" size="10" multiple="multiple">';
					foreach($operadores['cli']->result() as $operador) :
						echo '<option value="'.$operador->user.'">'.$operador->name.' '.$operador->last_name_a.'</option>';
					endforeach;
				echo '</select></td>';
				echo '</tr>';
				echo '</tbody>';
				echo '</table>';
			}else{
				echo '<p>Problema</p>'; #Verificar mensaje
			}
			
			echo form_submit('crear','Aceptar');
			echo form_close();?>
	 			</div>
		</article>
	</section><input type="hidden" id="url_js" value="<?php echo site_url(); ?>">
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/asociar_cliente.js"></script>

</body>
</html>