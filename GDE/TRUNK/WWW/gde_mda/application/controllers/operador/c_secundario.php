<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_secundario extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if(!$this->_logeado()){
			return redirect('c_login/acceso_denegado');
		}
		
		$this->load->model('evento');
	}

	public function index(){

		$operador = $this->session->userdata('username');
		$data = array();
		$data['url2'] = "http://192.168.207.90:8000/en-US/account/insecurelogin?username=admin&password=gdekudaw&return_to=/app/gestor_eventos/masivo_DD?USUARIO=".$operador;
		$data['url3'] = "http://192.168.207.90:8000/en-US/account/insecurelogin?username=admin&password=gdekudaw&return_to=/app/gestor_eventos/masivo_DE?USUARIO=".$operador;
		$data['url4'] = "http://192.168.207.90:8000/en-US/account/insecurelogin?username=admin&password=gdekudaw&return_to=/app/gestor_eventos/masivo_DF?USUARIO=".$operador;
		$data['url5'] = "http://192.168.207.90:8000/en-US/account/insecurelogin?username=admin&password=gdekudaw&return_to=/app/gestor_eventos/fcabello_salud_nodos_general";
		
		
		$this->load->view('operador/v_secundaria',$data);
	}
	
	/** Validacion del usuario que solicita controlador
	*/
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 1 || $this->session->userdata('tipo') == 2 || $this->session->userdata('tipo') == 3 || $this->session->userdata('tipo') == 4)){
			return true;
		}
		else{
			return false;
		}
	}

}