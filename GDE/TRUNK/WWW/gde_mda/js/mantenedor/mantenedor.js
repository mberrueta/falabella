/**
Mantenedor de Nodos
mantenedor.js

Autor:
Sebastian Quezada (squezada@kudaw.com)
Kudaw S.A. 2014
*/

/*	--------------------------------------------------
	:: Declaracion de variables globales
	-------------------------------------------------- */
	var handsontable;								// Manejador de tabla tipo Excel
	var data;										// Data completa con modificaciones
	var dataInicial         = new Array();								// Data completa sin modificaciones
	var dataInicial2		= new Array(); //copia de seguridad
	var nodosInicial		= new Array();
	var text					= "";					// Texto a filtrar
	
	var newNodos			= new Array();		// Se almacenan los nodos que seran agregados
	var deletedNodos		= new Array();		// Se almacenan los nodos que seran borrados
	var editedNodos		    = new Array();		// Se almacenan los nodos que seran editados
	
	var nombreClientes	= new Array();		// Listado de clientes
	var clientes_rep	= new Array();		// Listado de clientes no permitidos
	var filtroClientes	= new Array();		// Listado de clientes seleccionados
	var filtredData		= new Array();		// Data filtrada por cliente o texto libre
	var repetidos		= new Array();		// Nodos repetidos
	
	var cod_servicio	= new Array();
	var contacto_habil	= new Array();
	var telefono_habil	= new Array();
	var celular_habil		= new Array();
	var correo_habil		= new Array();
	var validar				= new Array();
	var mostrar				= new Array();
	
	var dataNew 		= new Array();
	var dataIndexNew    = new Array();
	var numCambios = 0; //sirve para restaurar la tabla a la vista original 
	var varRestore = 0;
	var varRestore1 = 0;
	var tablaModNodos = new Array();
	
	var yellowRenderer	= function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		if(value == ""){$(td).css({background: 'yellow'});}
	};
	
	var redRenderer	= function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		if(repetidos.indexOf(value) > -1){$(td).css({background: 'red'});}
	};
	
	var columns				= [					// Definicion de tipos de datos por columna
										{data:0, editor: 'select', selectOptions: nombreClientes},
										{data:1, renderer: redRenderer},
										{data:2, renderer: yellowRenderer},
										{data:3},
										{data:4},
										{data:5},
										{data:6},
										{data:7},
										{data:8},
										{data:9},
										{data:10},
										{data:11},
										{data:12},
										{data:13},
										{data:14},
										{data:15},
										{data:16},
										{data:17},
										{data:18, renderer: yellowRenderer},
										{data:19, renderer: yellowRenderer},
										{data:20, renderer: yellowRenderer},
										{data:21, renderer: yellowRenderer},
										{data:22},
										{data:23},
										{data:24},
										{data:25},
										{data:26, editor: 'select', selectOptions: ["24_7","8_5"]},
										{data:27,editor: 'select', selectOptions: ["Router", "Switch", "Internet", "Seguridad", "Telefonía", "Otros"]},
										{data:28,editor: 'select', selectOptions: ["Principal","Respaldo", "Serial", "N/A"]},
										{data:29}										
									];
	
	var colHeaders			= [					// Nombres de columnas a mostrar
										'CLIENTE',
										'NODO',
										'CODIGO DE SERVICIO',
										'IP_PRINCIPAL',
										'C/S_PRINCIPAL',
										'CODIGO ADMIN',
										'REGION',
										'PROVINCIA',
										'COMUNA',
										'SUCURSAL',
										'FW',
										'PE',
										'CRITICIDAD',
										'SLA',
										'TELNET_USER',
										'TELNET_PASS',
										'TELNET_ENAB_PASS',
										'SNMP',
										'Contacto_Habil',
										'Telefono_Habil',
										'Celular_Habil',
										'Correo_Habil',
										'Contacto_No_Habil',
										'Telefono_No_Habil',
										'Celular_No_Habil',
										'Correo_No_Habil',
										'Horario_Habil',
										'Tipo de Dispositivo',
										'Rol en HA',
										'Par en HA'
									];
	
	var contextMenu		= {
										callback: function (key, options) {
											if (key === 'about') {
												setTimeout(function () {
													alert("This is a context menu with default and custom options mixed");
												}, 100);
											}
										},
										items: {
											"row_below": {
												name: 'Agregar nodo',
											},
											"hsep1": "---------",
											"remove_row": {
												name: 'Eliminar nodo',
											}
										}
									};
	
/*	--------------------------------------------------
	:: Funciones de soporte para el funcionamiento
	-------------------------------------------------- */
	
	/*-------Funcion de filtrado----------------------
	*Filtra dependiendo de 3 factores:
	* - Si existen solo filtros por cliente
	* - Si existen filtros por cliente y texto libre
	* - Si existe solo texto libre
	* Se indica por consola el tipo de filtro realizado
	*------------------------------------------------*/
	function filtrar(){
		var filtro, row, i, col;
		var example = $('#tablaNodos');
		filtredData = new Array();
		text = ('' + $('#search_field').val()).toLowerCase();
		
		if( filtroClientes.length > 0 ){
			console.log("----Filtro de Cliente :"+filtroClientes);
			if(text != "") console.log("----Filtro de Texto   :"+text);
			
			for( i=0; i < filtroClientes.length; i++ ){
				filtro = filtroClientes[i];
				for( row=0; row < data.length; row++ ){
					if(data[row][0] === filtro){
						if( text == "" ){
							filtredData.push(data[row]);
						}
						else{
							for( col=0 ;col < data[row].length; col++ ){
								if(data[row][col] == null){
									continue;
								}
								if(('' + data[row][col]).toLowerCase().indexOf(text) > -1){
									filtredData.push(data[row]);
									break;
								}
							}
						}
					}
				}
			}
			example.handsontable('loadData',filtredData);
		}
		else if( filtroClientes.length == 0 && text != "" ){
			console.log("----Filtro de Texto   :"+text);
			for( row=0; row < data.length; row++ ){
				for( col=0 ;col < data[row].length; col++ ){
					if(data[row][col] == null){
						continue;
					}
					if(('' + data[row][col]).toLowerCase().indexOf(text) > -1){
						filtredData.push(data[row]);
						break;
					}
				}
			}
			example.handsontable('loadData',filtredData);
		}
		else{
			console.log("----Filtro de Cliente (TODOS) :"+nombreClientes);
			example.handsontable('loadData', data);
		}
	}
	
	
	/*-------Funcion de obtener nombre de nodo--------
	*     Entrada : int, numero de fila
	*     Salida  : string, nombre de nodo perteneciente a la fila
	*------------------------------------------------*/
	function getNodo(fila){
		var example = $('#tablaNodos');
		return example.handsontable('getDataAtCell', fila, 1);
	}
	
	
	/*-------Funcion de obtener nombre de nodo--------
	*     Entrada : int, numero de fila
	*     Salida  : string, nombre de nodo perteneciente a la fila
	*------------------------------------------------*/
	function getNodofromData(nodo){
		for( var i=0; i<data.length; i++ ){
			if( data[i][1] == nodo ){
				return i;
			}
		}
		return -1;
	}
	
	//funcion para buscar el nodo en el handsontable solo para nuevos
	function getDataNodofronHandsontable(){
		for (var i=0; i<dataNew.length;i++){
			var act = handsontable.getDataAtRow(dataNew[i]);
			data[dataIndexNew[i]] = act;
		}
	}
	
	function isEmpty(obj) {

		// null and undefined are "empty"
		if (obj == null) return true;

		// Assume if it has a length property with a non-zero value
		// that that property is correct.
		if (obj.length > 0)    return false;
		if (obj.length === 0)  return true;

		// Otherwise, does it have any properties of its own?
		// Note that this doesn't handle
		// toString and valueOf enumeration bugs in IE < 9
		for (var key in obj) {
			if (hasOwnProperty.call(obj, key)) return false;
		}

		return true;
	}
	/*-------Funcion de acciones antes de modificar--------
	*     Entradas : array, arreglo de celdas modificadas (int "fila", int "columna", string "antes", string "despues")
	*                string, tipo de modificacion
	*     Salida   : null
	*------------------------------------------------*/
	var beforeChange = function (change, source){
		//alert("Ingresa aqui!!! y carga la información!!!"); //WPH
		var nodos	= new Array();
		//console.log(change);
		var sacaNuevo=0;
			if( change ){
				numCambios = numCambios + 1;
				for( var i=0; i<change.length; i++ ){
					sacaNuevo=0;
					// Si se modifica un nombre (celda no vacia)
					if( change[i][1] == 1 && change[i][2] != "" && change[i][2] != null ){
						var elim =0, modi=0;nodo1=change[i][3],sacaN=0,ind=change[i][0];
						if(change[i][2] == change[i][3]){
							console.log("nada:" + change[i][2]);
						}
						else{
							//se agrega a borrados si no esta
							if( deletedNodos.indexOf(change[i][2]) == -1 ) {
								deletedNodos.push(change[i][2]);
								
							}
							
							//se saca de nuevos
							if( newNodos.indexOf(change[i][2]) > -1 ) {
								newNodos.splice(newNodos.indexOf(change[i][2]), 1);
								sacaNuevo=1;
							}
							
							//se saca de editados
							if( editedNodos.indexOf(change[i][2]) > -1 ) {
								editedNodos.splice(editedNodos.indexOf(change[i][2]), 1);
							}

							//se agrega a nuevo si no esta 
							if( change[i][3] != "" && newNodos.indexOf(change[i][3]) == -1){
								
								newNodos.push(change[i][3]);
								//cambio WPH
								if (sacaNuevo ==0){
									var act2 = filtredData[change[i][0]];
									//var act2 = handsontable.getDataAtRow(change[i][0]);
									if (isEmpty(act2)){
										filtredData.push([ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ]);
										act2=[ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ];
									}
									
									if (dataNew.indexOf(change[i][0])==-1){
										var act = act2.slice(0);
										act[1] = change[i][3];
										var temp = data.pop();
										data.push(act);
										data.push(temp);
										filtredData.push([ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ]);
									}
								}
								
							}
						}
					}
					// Si se modifica un nombre (celda vacia)
					else if( change[i][1] == 1 && change[i][3] != "" ){
						//si es data filtrada
						if( filtroClientes.length > 0 || text != "" ){
							//if( newNodos.indexOf(change[i][3]) == -1 ) {
							newNodos.push(change[i][3]);
							var act2 = filtredData[change[i][0]];
							//var act2 = handsontable.getDataAtRow(change[i][0]);
							if (isEmpty(act2)){
								filtredData.push([ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ]);
								act2=[ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ];
							}
							var act = act2.slice(0);
							act[1] = change[i][3];
							var temp = data.pop();
							data.push(act);
							dataNew.push(change[i][0]);
							dataIndexNew.push(data.length-1);
							data.push(temp);
							filtredData.push([ null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null ]);
							//}
						}
						else{
							//if( newNodos.indexOf(change[i][3]) == -1 ) {
								newNodos.push(change[i][3]);
							//}
						}
					}
					else{
						// console.log(change);
						if(change[i][2] == change[i][3]){
							console.log("nada" + change[i]);
						}
						else{
							var nodo = getNodo(change[i][0]);
							if( nodo ){
								if( newNodos.indexOf(nodo) == -1 ){
									if( editedNodos.indexOf(nodo) == -1 ){
										editedNodos.push(nodo);
									}
								}
							}
						}
					}
				}
				$("#validar").removeAttr('disabled');
				$("#validar").css('-webkit-filter',' grayscale(0%)');
				$("#deshacer").removeAttr('disabled');
				$("#deshacer").css('-webkit-filter',' grayscale(0%)');
			}
		
	};
	
	/*-------Funcion de acciones antes de borrar filas--------
	*     Entradas : int, numero de fila
	*                int, numero de columna
	*     Salida   : null
	*------------------------------------------------*/
	var beforeRemoveRow = function (fila, cantidad) {
		
		//numCambios=numCambios+1;
		for( var i=fila; i<cantidad+fila; i++ ){
			var nodo = getNodo(i);
			if( nodo ){
				numCambios=numCambios+1;
				if( filtroClientes.length > 0 || text != "" ){
					deletedNodos.push(nodo);
					var nodoData = getNodofromData(nodo);
					data.splice(nodoData,1);
				}
				else{
					deletedNodos.push(nodo);
				}
			}
		}
		$("#validar").removeAttr('disabled');
		$("#validar").css('-webkit-filter',' grayscale(0%)');
		$("#deshacer").removeAttr('disabled');
		$("#deshacer").css('-webkit-filter',' grayscale(0%)');
	};
	
	/*-------Funcion de cambio de check cliente--------
	*     Entrada : null
	*     Salida  : null
	* Si se checkea un cliente es agregado al arreglo "filtroClientes",
	* si se deselecciona es sacado de dicho arreglo
	*------------------------------------------------*/
	$('.checkCliente').live('change',function(){
		if($(this).is(':checked')){
			filtroClientes.push($(this).attr("name"));
		}
		else{
			var valorPorRetirar = $(this).attr("name");
			filtroClientes.splice(filtroClientes.indexOf(valorPorRetirar), 1);
		}
		filtrar();
	});
	
	
	/*-------Funcion de obtener nombre de nodo--------
	*     Entrada : int, numero de fila
	*     Salida  : string, nombre de nodo perteneciente a la fila
	*------------------------------------------------*/
	$('#search_field').on('keyup',function(event){
		if(event.keyCode == 13) {
			filtrar();
		}
	});
	
	/* inicio modificacion validacion */
	var validarNodosbd = function() {
		console.log(newNodos);
		console.log(newNodos.length);		
		if (newNodos.length != 0){
			$.ajax({
				url			: "c_mantenedor/validarNodosNuevos",
				async		: false,
				dataType	: 'json',
				data		: { 'nombreNodosNuevos':newNodos },
				type		: 'POST',
				success	: function (response) {
							if (0 < response.length-1){
								alert("Se esta intentando ingresar nodos que pertenecen a otro cliente");
								for(var i = 0; i<response.length;i++){
									repetidos.push(newNodos[i]);
								}
								return 1;
							} 
							else{
								return 0;
							}
				} 
			});
		}
		else{
			return 0;
		}
	
	}
	/* termino validacion */
	
	var guardarNodos = function() {
		for( var i=0; i<deletedNodos.length; i++ ){
			if( nodosInicial.indexOf(deletedNodos[i]) == -1 ){
				deletedNodos.splice(i, 1);
				i--;
			}
			else if( getNodofromData(deletedNodos[i]) > -1){
				deletedNodos.splice(i, 1);
				i--;
			}
		}
		
		for( var i=0; i<newNodos.length; i++ ){
			if( nodosInicial.indexOf(newNodos[i]) > -1 ){
				editedNodos.push(newNodos[i]);
				newNodos.splice(i, 1);
				i--;
			}
		}
		
		for( var i=0; i<editedNodos.length; i++ ){
			if( nodosInicial.indexOf(editedNodos[i]) == -1 ){
				newNodos.push(editedNodos[i]);
				editedNodos.splice(i, 1);
				i--;
			}
		}
		
		
		deletedNodos		= unique(deletedNodos);
		newNodos				= unique(newNodos);
		editedNodos			= unique(editedNodos);
		
		
		var nuevosNodos	= new Array();
		var editadosNodos	= new Array();
		//console.log(data);
		var nuevosNodosAux	= new Array();
		for( var i=0; i<data.length; i++ ){
			if( newNodos.indexOf(data[i][1]) > -1 && nuevosNodosAux.indexOf(data[i][1])==-1){
				nuevosNodosAux.push(data[i][1]);
				nuevosNodos.push(data[i]);
			}else{
				if( editedNodos.indexOf(data[i][1]) > -1 ){
					editadosNodos.push(data[i]);
				}
			}
		}
		//Modificando en caso de que sean nuevos ya que la data en newNodos ya viene filtrada
		
		
		if( deletedNodos.length == 0 ){deletedNodos = [""];}
		if( nuevosNodos.length == 0 ){nuevosNodos = [""];}
		if( editadosNodos.length == 0 ){editadosNodos = [""];}
		
		for( var i=0; i<nuevosNodos.length; i++ ){
			for( var j=0; j<nuevosNodos[i].length; j++ ){
				if( nuevosNodos[i][j] == null ){
					nuevosNodos[i][j] = "";
				}
			}
		}
		for( var i=0; i<editadosNodos.length; i++ ){
			for( var j=0; j<editadosNodos[i].length; j++ ){
				if( editadosNodos[i][j] == null ){
					editadosNodos[i][j] = "";
				}
			}
		}
		
		var cntDel = Math.floor(deletedNodos.length/500);
		console.log(cntDel);
		for( var i=0; i<=cntDel; i++ ){
			console.log("iteracion ----------"+i);
			console.log((i*500));
			console.log((i+1)*500);
			var tempDeleted = deletedNodos.slice((i*500),(i+1)*500);
			console.log(tempDeleted);
			
			$.ajax({
				url		: "c_mantenedor/eliminarNodos",
				async		: false,
				dataType	: 'json',
				data		: { 'nodosEliminados':tempDeleted },
				type		: 'POST',
				success	: function (response) {
					console.log(response);
					//alert("elimine el nodo!!!!\n"+tempDeleted);
				}
			});
		}
		
		var cntNew = Math.floor(nuevosNodos.length/500);
		console.log(cntNew);
		for( var i=0; i<=cntNew; i++ ){
			console.log("iteracion ----------"+i);
			console.log((i*500));
			console.log((i+1)*500);
			var tempNew = nuevosNodos.slice((i*500),(i+1)*500);
			console.log(tempNew);
			
			$.ajax({
				url		: "c_mantenedor/crearNodos",
				async		: false,
				dataType	: 'json',
				data		: { 'nodosNuevos':tempNew },
				type		: 'POST',
				success	: function (response) {
					console.log(response);
				}
			});
		}
		
		var cntEdit = Math.floor(editadosNodos.length/500);
		console.log(cntEdit);
		for( var i=0; i<=cntEdit; i++ ){
			console.log("iteracion ----------"+i);
			console.log((i*500));
			console.log((i+1)*500);
			var tempEdited = editadosNodos.slice((i*500),(i+1)*500);
			console.log(tempEdited);
			
			$.ajax({
				url		: "c_mantenedor/editarNodos",
				async		: false,
				dataType	: 'json',
				data		: { 'nodosModificados':tempEdited },
				type		: 'POST',
				success	: function (response) {
					console.log(response);
				}
			});
		}
		editedNodos = new Array();
		newNodos = new Array();
		deletedNodos = new Array();
		alert("Los cambios fueron guardados exitosamente");
		location.reload();
	};
	
	var unique = function(origArr) {
		var newArr = [],  
			origLen = origArr.length,  
			found,  
			x, y;  
		
		for ( x = 0; x < origLen; x++ ) {  
			found = undefined;  
			for ( y = 0; y < newArr.length; y++ ) {  
				if ( origArr[x] === newArr[y] ) {  
				  found = true;  
				  break;  
				}  
			}  
			if ( !found) newArr.push( origArr[x] );  
		}  
	   return newArr;  
	};
	
	$('#impedimentoOperacional').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
				handsontable.render();
				$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#avisoOperacional').dialog({
		nameUser: '',
		autoOpen: false,

		buttons: [ {
		 id: "infoIncompleta",
		 text: "Indicar informacion incompleta",
		 click: function() { 
			
			handsontable.render();
			handsontable.selectCell ( mostrar[0][0], mostrar[0][1], mostrar[0][0], mostrar[0][1]);
			$( this ).dialog( 'close' ); 
			}
		},
		{
		 id: "btnGuardar",
		 text: "Guardar",
		 click: function() { 
			var prueba = validarNodosbd();
				if (prueba == 1){
					$('#impedimentoOperacional').dialog('open');
				}
				else {
					$("#validar").attr('disabled','disabled');
					$("#validar").css('-webkit-filter',' grayscale(100%)');
					$("#btnGuardar").button("option", "disabled", true);
					guardarNodos();
				}
				$( this).dialog('close');
		}
		}]
	});

	
	$('#impedimentoCliente').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
				handsontable.render();
				handsontable.selectCell ( cli_null[0], 0, cli_null[0], 0);
				$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#modal_loading').dialog({
		width:800,
		autoOpen: false,
		resizable: false,
		draggable: false,
		modal: true,
		closeText: "hide"
	});
	
	$('#modal_exportar').dialog({
		width:500,
		autoOpen: false,
		resizable: false,
		draggable: false,
		modal: true,
		buttons: {
			'Cancelar': function() {
				$( this ).dialog( 'close' );
			},
			'Exportar': function() {
				var cli;
				if( filtroClientes.length > 0 ){
					cli = filtroClientes;
				}
				else{
					cli = nombreClientes;
				}
				$.ajax({
					url		: "c_mantenedor/createExtract",
					dataType	: 'json',
					data		: {'clientes':cli},
					type		: 'POST',
					success	: function (results) {
						window.open('/fase_1/index.php/analista/c_mantenedor/backup_download/'+results, '', 'width=600,height=500');
					},
					error: function (res) {
						console.log(res);
					}
				});
				$( this ).dialog( 'close' );
			},
		},
	});
	
	$('#modal_reemplazar').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Cancelar': function() {
				$( this ).dialog( 'close' );
			},
			'Reemplazar': function() {
				console.log('INGREASA A REEMPLAZAR');
				var texto_a			= $("#texto_a").val();
				var texto_b			= $("#texto_b").val();
				
				var columna_remp	= $("#columna_remp").val();
				var dataNow			= handsontable.getData();
				
				if( texto_a != "" ){
					console.log(dataNow.length);
					var count = 0;
					var rows = new Array();
					for( var i=0; i<dataNow.length; i++ ){
						if(('' + dataNow[i][columna_remp]).indexOf(texto_a) > -1){
							count++;
							rows.push(i);
						}
					}
					console.log(count);
					$( "progress" ).attr("max",count);
					for( var i=0; i<count; i++ ){
						if( filtroClientes.length > 0 || text != "" ){
							filtredData[rows[i]][columna_remp] = filtredData[rows[i]][columna_remp].replace( texto_a, texto_b );
							if( newNodos.indexOf(filtredData[rows[i]][1]) == -1 ){
								if( editedNodos.indexOf(filtredData[rows[i]][1]) == -1 ){
									editedNodos.push(filtredData[rows[i]][1]);
								}
							}
						}
						else{
							data[rows[i]][columna_remp] = data[rows[i]][columna_remp].replace( texto_a, texto_b );
							if( newNodos.indexOf(data[rows[i]][1]) == -1 ){
								if( editedNodos.indexOf(data[rows[i]][1]) == -1 ){
									editedNodos.push(data[rows[i]][1]);
								}
							}
						}
						$( "progress" ).val(i+1);
						$( "#resultado" ).html("Reemplazo realizado exitosamente. Se modificaron "+count+" nodos");
					}
				}
				else{
					$( this ).dialog( 'close' );
					alert("El texto a buscar no debe ser vacio");
				}
			}
		}
	});
	
	
	
	/*-------Funcion de Validacion de campos--------
	*     Entrada : null
	*     Salida  : null
	*------------------------------------------------*/
	$("#validar").click(function(evt){
		var nodos				= new Array();
		var nodos_base			= new Array();
		var results				= new Array();
		var clientes			= new Array();
		
		cod_servicio			= new Array();
		contacto_habil			= new Array();
		telefono_habil			= new Array();
		celular_habil			= new Array();
		correo_habil			= new Array();
		cli_null				= new Array();
		repetidos				= new Array();
		validar					= new Array();
		mostrar					= new Array();
		clientes_rep			= new Array();
		
		//Aqui se deben ingresar los datos del handsontable
		getDataNodofronHandsontable();
		//mostrar data modificadas
		
		for (var i=dataInicial.length-1; i<data.length; i++){
			console.log(data[i][1]);
		}
		for( var i=0; i<nodosInicial.length-1; i++ ){
			nodos.push(nodosInicial[i]);
		}
		for (var i=dataInicial.length-1; i<data.length-1; i++){
			nodos.push(data[i][1]);
		}
		for( var i=0; i<data.length-1; i++ ){
				//nodos.push(data[i][1]);
				//nodos_base.push(data[i][1]);
				clientes.push(data[i][0]);
				if( data[i][0] == "" || data[i][0] == null ){
					console.log(data[i]);
					cli_null.push(i);
				}
				if( data[i][2] == "" ){
					cod_servicio.push(i);
				}
				if( data[i][18] == "" ){
					contacto_habil.push(i);
				}
				if( data[i][19] == "" ){
					telefono_habil.push(i);
				}
				if( data[i][20] == "" ){
					celular_habil.push(i);
				}
				if( data[i][21] == "" ){
					correo_habil.push(i);
				}
		}
		
		clientes = unique(clientes);
		for ( var i=0; i<clientes.length; i++ ) {
			if( nombreClientes.indexOf(clientes[i]) == -1 && clientes[i] != null ){
				clientes_rep.push(clientes[i]);
			}
		}
		
		nodos.sort();
		//console.log(nodos);
		for ( var i = 0; i < nodos.length - 1; i++ ) {
			if( nodos[i] != "" && nodos[i + 1] == nodos[i] ){
				repetidos.push(nodos[i]);
			}
		}
		if( cli_null.length > 0 ){
			console.log("Clientes no permitidos: "+clientes_rep);
			$('#detalle_cliente').html("Clientes vacios");
			$('#impedimentoCliente').dialog('open');
		}
		else if( clientes_rep.length > 0 ){
			console.log("Clientes no permitidos: "+clientes_rep);
			$('#detalle_cliente').html("Clientes no permitidos: "+clientes_rep);
			$('#impedimentoCliente').dialog('open');
		}
		else if( repetidos.length > 0 ){
			console.log("Nodos repetidos: "+repetidos);
			$('#impedimentoOperacional').dialog('open');
		}
		else{
			if( cod_servicio.length > 0 ){
				validar.push("CODIGO SERVICIO");
				mostrar.push([cod_servicio[0],2]);
			}
			if( contacto_habil.length > 0 ){
				validar.push("CONTACTO HABIL");
				mostrar.push([contacto_habil[0],18]);
			}
			if( telefono_habil.length > 0 ){
				validar.push("TELEFONO HABIL");
				mostrar.push([telefono_habil[0],19]);
			}
			if( celular_habil.length > 0 ){
				validar.push("CELULAR HABIL");
				mostrar.push([celular_habil[0],20]);
			}
			if( correo_habil.length > 0 ){
				validar.push("CORREO HABIL");
				mostrar.push([correo_habil[0],21]);
			}
			if(validar.length > 0){
				$('#avisoText').html("Existe informacion incompleta en la(s) columna(s) "+validar);
				$('#avisoOperacional').dialog('open');
			}
			else{
				//alert('Guarda nodos 2');
				guardarNodos();
			}
			
		}
	});
	
	

	/*-------Funcion para controlar salida------------
	*     Entrada : 
	*     Salida  : 
	*------------------------------------------------*/
	window.onbeforeunload = function() {
		if( deletedNodos.length > 0 || newNodos.length > 0 || editedNodos.length > 0 ){
			return 'Existen nodos sin guardar';
		}
	}
	
/*	-----------------------------------------------------------
	:: Declaracion carga iniciar y de asociacion accion-dom
	----------------------------------------------------------- */
	function llamarCargaDatos(){
		console.log("llama a cargar nodos");
		$.ajax({
			url		: "c_mantenedor/listarClientes",
			dataType	: 'json',
			type		: 'GET',
			success	: function (results) {
				var res = '<table class="filterContainer" border="0" cellpadding="0" cellspacing="0"><tbody>';
				
				for(var i=0; i<results.length; i++){
					res += '<tr><td><input type="checkbox" class="checkCliente" id="'+results[i][1]+'" name="'+results[i][1]+'" value="'+results[i][0]+'" >'+results[i][1]+'</td></tr>';
					nombreClientes.push(results[i][1]);
				}
				res += '</tbody></table>';
				
				$.ajax({
					url		: "c_mantenedor/listarNodos",
					dataType	: 'json',
					data		: {'clientes':nombreClientes},
					type		: 'POST',
					success	: function (res) {
						handsontable.loadData(res);
						data = res.slice(0,res.length);
						//dataInicial = $.extend(true, [], data);
						//for (var i=0;i<data.length;i++){
							//dataInicial.push(data[i]);
						dataInicial=data.slice(0,data.length);
						//}
						for( var i=0; i<dataInicial.length; i++ ){
							nodosInicial.push(dataInicial[i][1]);
							//dataInicial2.push(dataInicial[i]); 
						}
						$("#validar").attr('disabled','disabled');
						$("#validar").css('-webkit-filter',' grayscale(100%)');
						$("#deshacer").attr('disabled','disabled');
						$("#deshacer").css('-webkit-filter',' grayscale(100%)');
					}
				});
				
				$("#lstClientes").html(res);
			},
			error: function (res) {
				console.log(res);
			}
		});
	}
	$(document).ready(function() {
		$('#tablaNodos').handsontable({
			minSpareRows			: 1,
			colHeaders				: ['CLIENTE','NODO','CODIGO DE SERVICIO','IP_PRINCIPAL','C/S_PRINCIPAL','CODIGO ADMIN','REGION','PROVINCIA','COMUNA','SUCURSAL','FW','PE','CRITICIDAD','SLA','TELNET_USER','TELNET_PASS','TELNET_ENAB_PASS','SNMP','Contacto_Habil','Telefono_Habil','Celular_Habil','Correo_Habil','Contacto_No_Habil','Telefono_No_Habil','Celular_No_Habil','Correo_No_Habil','Horario_Habil','Tipo de Dispositivo','Rol en HA','Par en HA'],
			columns					: columns,
			rowHeaders				: true,
			contextMenu				: contextMenu,
			columnSorting			: true,
			manualColumnResize	: false,
			fixedColumnsLeft		: 0,
			Controller				: true,
			beforeChange			: beforeChange,
			beforeRemoveRow		: beforeRemoveRow
		});
		numCambios=0;
		handsontable = $('#tablaNodos').data('handsontable');
		llamarCargaDatos();
		
	});
	//restaurar
	function resetVar(){
		//recuperar el valoar de dataInicial
		/*if (varRestore=1){
			dataInicial2 = new Array();
			dataInicial2=dataInicial.slice(0,dataInicial.length);
		}*/
		//data = dataInicial2.slice(0,dataInicial.length);
		//dataInicial= dataInicial2.slice(0,dataInicial.length);
		/*dataInicial=new Array();
		data ={};
		handsontable={};
		newNodos = new Array();
		deletedNodos = new Array();
		editedNodos = new Array();*/
		
		
		handsontable={};								// Manejador de tabla tipo Excel
		data={};										// Data completa con modificaciones
		dataInicial         = new Array();								// Data completa sin modificaciones
		dataInicial2		= new Array(); //copia de seguridad
		nodosInicial		= new Array();
		text					= "";					// Texto a filtrar
	
		newNodos			= new Array();		// Se almacenan los nodos que seran agregados
		deletedNodos		= new Array();		// Se almacenan los nodos que seran borrados
		editedNodos		    = new Array();		// Se almacenan los nodos que seran editados
	
		nombreClientes	= new Array();		// Listado de clientes
		clientes_rep	= new Array();		// Listado de clientes no permitidos
		//filtroClientes	= new Array();		// Listado de clientes seleccionados
		filtredData		= new Array();		// Data filtrada por cliente o texto libre
		repetidos		= new Array();		// Nodos repetidos
	
		cod_servicio	= new Array();
		contacto_habil	= new Array();
		celular_habil		= new Array();
		telefono_habil	= new Array();
		correo_habil		= new Array();
		validar				= new Array();
		mostrar				= new Array();
	
		dataNew 		= new Array();
		dataIndexNew    = new Array();
		tablaModNodos = new Array();
		
		
		
		
	}
	function cargaDatos(){
		$('#tablaNodos').handsontable({
			minSpareRows			: 1,
			colHeaders				: ['CLIENTE','NODO','CODIGO DE SERVICIO','IP_PRINCIPAL','C/S_PRINCIPAL','CODIGO ADMIN','REGION','PROVINCIA','COMUNA','SUCURSAL','FW','PE','CRITICIDAD','SLA','TELNET_USER','TELNET_PASS','TELNET_ENAB_PASS','SNMP','Contacto_Habil','Telefono_Habil','Celular_Habil','Correo_Habil','Contacto_No_Habil','Telefono_No_Habil','Celular_No_Habil','Correo_No_Habil','Horario_Habil','Tipo de Dispositivo','Rol en HA','Par en HA'],
			columns					: columns,
			rowHeaders				: true,
			contextMenu				: contextMenu,
			columnSorting			: true,
			manualColumnResize	: false,
			fixedColumnsLeft		: 0,
			Controller				: true,
			beforeChange			: beforeChange,
			beforeRemoveRow		: beforeRemoveRow
		});
		numCambios=0;
		handsontable = $('#tablaNodos').data('handsontable');
		llamarCargaDatos();
	}
	$('#deshacer').click(function(evt){
		console.log('deshacer!!!, creo que algunas cosas deberian volver a su posición original');
		varRestore =1;
		varRestore1 =1;
		//numero de cambios
		console.log("numCambios---aaaaa-:"+numCambios);
		//handsontable = $('#tablaNodos').data('handsontable');
		resetVar();
		cargaDatos();
		
		//handsontable.loadData(data);
		$("#deshacer").attr('disabled','disabled');
		$("#deshacer").css('-webkit-filter',' grayscale(100%)');
		$("#validar").attr('disabled','disabled');
		$("#validar").css('-webkit-filter',' grayscale(100%)');
		/*if( handsontable.isUndoAvailable() ){
			console.log('retornar...')
			handsontable.undo();
			$("#deshacer").removeAttr('disabled');
			$("#deshacer").css('-webkit-filter',' grayscale(0%)');
			console.log('aun hay cosas que deshacer--')
		}
		else{
			$("#deshacer").attr('disabled','disabled');
			$("#deshacer").css('-webkit-filter',' grayscale(100%)');
			//WPH en caso de que se deshaga todo deberia de volver al estado originla
			resetVar()
			$("#validar").attr('disabled','disabled');
			$("#validar").css('-webkit-filter',' grayscale(100%)');
		}*/
	});
	
	$('#exportar').click(function(evt){
		$('#modal_exportar').dialog('open');
	});
	
	$('#reemplazar').click(function(evt){
		$( "#resultado" ).html("");
		$( "progress" ).val(0);
		$('#modal_reemplazar').dialog('open');
	});
	
	setInterval(function () {
		var a= newNodos.length+deletedNodos.length+editedNodos.length;
		$("#count").html(a+" nodos modificados");
		if (varRestore1 >0 && varRestore1<10){
			varRestore1++;
			//var filtroClientes	= new Array();
			for (var i=0;i<filtroClientes.length;i++){
				filtro='#'+filtroClientes[i];
				//document.getElementsByName(filtro).checked="checked"
				$(filtro).attr('checked','checked');
			}
			filtrar();
		}
		if( a > 1 ){
			$("#validar").removeAttr('disabled');
			$("#validar").css('-webkit-filter',' grayscale(0%)');
		}
	},1000);
	