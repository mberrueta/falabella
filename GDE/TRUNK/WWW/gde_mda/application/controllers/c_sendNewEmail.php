<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_sendNewEmail extends CI_Controller {
	public function index(){}
	
	public function loadView(){
		$data = array();
		$this->load->view('v_sendNewEmail', $data);
	}
	
	public function fullData(){
		$this->load->model('correo');
		$res['filas'] = $this->codServicio($this->input->post('filas'));
		$res['nodos'] = $this->arrayDispip($this->input->post('filas'));
		$res['direc'] = $this->dirServicio($this->input->post('filas'));
		$res['firma'] = $this->correo->copyEmail2();
		
		echo json_encode($res);
	}
	
	public function toEmail(){
		$this->load->model('correo');
		$listaCorreos = $this->correo->toEmails($_POST['Area']);
		echo json_encode($listaCorreos);
	}

	public function getMensaje(){
		$this->load->model('evento');
		$mensaje = $this->evento->obtenerMensajeCorreo($_POST['Id']);
		echo json_encode($mensaje);
	}
	
	public function copyEmail(){
		$this->load->model('correo');
		$correoUS = $this->correo->copyEmail();
		echo json_encode($correoUS);
	}
	
	public function validarPost($postArray){
		$valid = true;
		if( empty($postArray['para']) ){ $valid = false; }
		return $valid;
	}
	
	public function sendEmail(){
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$postArray = $this->input->post();
		
		//$postArray['table3']		= $postArray['table'];
		//$postArray['table']		= $this->arrayDispip($postArray['table']);
		$postArray['group_id']	        = $postArray['group_id'];
		$postArray['mail_tipo_aler']	= $postArray['mail_tipo_aler'];
		$postArray['tipomail']	        = 'evento_new';
		$postArray['historial']	        = '<table><tr><td colspan=2 style="padding: 8px; font-size: x-small; font-family: arial, sans-serif;" >Correo de inicio de seguimiento (ver area de "Ultimo comentario")</td></tr></table>';
		
		$valid = $this->validarPost($postArray);
		
		$this->email->from($postArray['desde'], $postArray['desde_nom']);
		$this->email->to($postArray['para']);
		$this->email->cc($postArray['copia']);
		
		$email = $this->load->view('v_templateNewEmail', $postArray, TRUE);
		
		$this->email->subject($postArray['asunto']);
		$this->email->message($email);
		// echo $this->email->print_debugger();
		
		if($valid){
			$send = $this->email->send();
			//var_dump($this->email->print_debugger());
			//print_r($this->email->print_debugger());
			if($send)
			{
				$this->saveEmail($postArray);
				
				// Actualización de Estado (Solo "Activo" -> "En Proceso")
				$this->load->model('evento');
				
				$estado_actual = $this->evento->getEstadoGrupoEvento($postArray['group_id']);
				
				if($estado_actual == "Activo"){
					$this->evento->cambiar_estado($postArray['group_id'], "En Proceso", "Cambio de estado automatico por envío de correo realizado", $this->session->userdata('username'));
				}
				
				//$messageResult = 'Correo enviado exitosamente';
				//echo json_encode($messageResult);
				$data['messageResult'] = 'Correo enviado exitosamente';
				return $this->load->view('v_sendEmailResult', $data);
			}
			else{
				//$messageResult = 'Favor intentelo nuevamente';
				//echo json_encode($messageResult);
				$data['messageResult'] = 'Favor intentelo nuevamente';
				return $this->load->view('v_sendEmailResult', $data);
			}
		}
		else{
			//$messageResult = 'Favor rellene campos incompletos, intentelo nuevamente';
			//echo json_encode($messageResult);
			$data['messageResult'] = 'Favor rellene campos incompletos, intentelo nuevamente';
			return $this->load->view('v_sendEmailResult', $data);
		}
	}
	
	public function saveEmail($postArray){
		$this->load->model('correo');
		$this->correo->registrarCorreos_new($postArray);
	}
	
	public function saveError($postArray,$Error){
		$this->load->model('correo');
		$this->correo->registrarError($postArray,$Error);
	}
	
	public function getSucursal($nodo){
		$this->load->model('correo');
		return $this->correo->getSucursal($nodo);
	}
	
	public function codServicio($postArray){
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		$codserv = array("");
		for($i=0;$i<count($nodos)-1;$i++){
			$cel = explode(",",$nodos[$i]);
			$codserv[$i] = $this->correo->codigoServicio($cel[3]);
		}
		return $codserv;
	}
	
	public function arrayDispip($postArray){
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		$nodosip = array("");
		for($i=0;$i<count($nodos)-1;$i++){
			$cel = explode(",",$nodos[$i]);
			$dispip = $this->correo->arrayDispip($cel[3]); 
			$nodosip[$i] = $cel[3]."\t".$dispip;
		}
		return $nodosip;
	}
	
	public function dirServicio($postArray){
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		$dirserv = array("");
		for($i=0;$i<count($nodos)-1;$i++){
			$cel = explode(",",$nodos[$i]);
			$dirserv[$i] = $this->correo->getSucursal($cel[3]); 
		}
		return $dirserv;
	}
	
	public function get_alias(){
		$this->load->model('cliente');
		$alias = $this->cliente->get_alias_nom($this->input->post('Cliente')); 
		echo json_encode($alias);
	}
}

/* End of file c_sendEmail.php */
/* Location: ./application/controllers/c_sendNewEmail.php */
