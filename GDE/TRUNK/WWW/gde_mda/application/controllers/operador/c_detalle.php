<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_detalle extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		$this->load->model('evento');
	}

	public function obtenerHistoria()
	{
		//$nodo     = $this->input->post('nodo');
		$grupoId  = $this->input->post('grupoId');
		//$nombre   = $this->input->post('nombre');

		$detalles = array();
		//$detalles['hist'] = $this->evento->obtenerHistoria($nodo,$nombre);
		$detalles['sumz'] = $this->evento->obtenerSumarizados($grupoId);
		$detalles['cmnt'] = $this->evento->obtenerComentarios($grupoId);
		echo json_encode($detalles);
	}
	
	public function registrarComentario(){
		$coment      = $this->input->post('coment');
		$grupoId     = $this->input->post('grupoId');
		$comTicketId = $this->input->post('comTicketId');
		$envMDA      = $this->input->post('envMDA');
		
		$usuario = $this->session->userdata('username');
		
		$output = $this->evento->registrarComentario($grupoId, $coment, $usuario);
		$result = explode("|",$output);
		
		if ($result[0] == 1){
			if($envMDA == 1){
				if($comTicketId != "ERROR" && $comTicketId != "Sin Ticket"){
				
					$this->load->model('webservices');
					
					// Obtener estado actual del Ticket
					
					$retorno = $this->webservices->consultaIncidente($comTicketId);
					
					if($retorno){
						$retorno = rtrim($retorno, "\n");
						$output  = explode(',',$retorno);
						
						if($output[0] == 'fail' or $output[0] == 'error'){
							// Problema con el Web Services.
							// $coment = 'ERROR en envio de comentarios (Detalle: Problemas con web services)';
							// $this->evento->registrarComentario($grupoId, $coment, $usuario);
							echo 'ERROR en envio de comentarios (Detalle: Problemas con web services)';
						} else{
							// Envio al WS de actualizacion.
							$coment_firma = $coment . ", desde Gestor de Eventos Corporativo.";
							$status = $output[1];
							
							$retorno = $this->webservices->actualizaEstadoIncidente($comTicketId, $status, $coment_firma);
							$retorno = rtrim($retorno, "\n");
							$output  = explode(',',$retorno);
							
							if($output[0] == 'fail' or $output[0] == 'error'){
								echo 'ERROR en envio de comentarios (Detalle: Problemas con web services)';
							} else {
								echo "Comentario enviado exitosamente";
							}
						}
					}	else{
						echo 'ERROR en envio de comentarios (Detalle: Problemas con web services)';
					}
				}
			}
		}
		echo $this->evento->obtenerComentarios($grupoId);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 1 || $this->session->userdata('tipo') == 2 || $this->session->userdata('tipo') == 3 || $this->session->userdata('tipo') == 4)){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	// Funcionalidad Adessa
	public function obtenerHijos()
	{
		echo $this->evento->obtener_eventos_hijos($this->session->userdata('username'));
	}
}
