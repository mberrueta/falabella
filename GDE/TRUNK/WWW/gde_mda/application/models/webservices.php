<?php

class WebServices extends CI_Model {
	
	protected $realPath		= null;
	protected $pythonPath	= null;
	protected $wsFilePath	= null;
	
	function __construct() {
		parent::__construct();
		
		$this->wsFilePath		= realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "ws" . DIRECTORY_SEPARATOR . "clienteRemedy.py ";
		$this->pythonPath	= "/usr/bin/python ";
	}
	
	/**
	*Crea el Ticket en Remedy
	*
	*@param String	$codserv	Codigo de servicio de el/los nodo(s) del Ticket
	*@param String	$email		Correo del operador que crea el Ticket
	*@param String	$resumen	Resumen del Ticket
	*@param int		$fuente		Fuente del Ticket
	*@param String	$origen		Origen del Ticket
	*
	*@return String[] Arreglo con los resultados de la creacion del Ticket
	*/
	public function crearTicket($ID){
		
		// Obtiene información del ticket
		
		$sql = "SELECT grupo_evento_gde.*,contacto_dispositivo_gde.encargado as gruporesolutor,contacto_dispositivo_gde.support_organization,categoria_gde.pais,categoria_gde.encargado,categoria_gde.app,categoria_gde.detalle FROM `grupo_evento_gde` LEFT JOIN contacto_dispositivo_gde ON grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization LEFT JOIN categoria_gde ON (categoria_gde.pais = ( CASE grupo_evento_gde.dest_country WHEN 'Chile' THEN 'CL' WHEN 'Uruguay' THEN 'UY' WHEN 'Peru' THEN 'PE' WHEN 'Mexico' THEN 'ME' WHEN 'Colombia' THEN 'CO' WHEN 'Brasil' THEN 'BR' WHEN 'Brazil' THEN 'BR' WHEN 'Argentina' THEN 'AR' ELSE 'N/A' END ) AND ( grupo_evento_gde.type = categoria_gde.detalle OR categoria_gde.detalle = CONCAT(grupo_evento_gde.type,'.',grupo_evento_gde.severity)) AND categoria_gde.encargado = contacto_dispositivo_gde.encargado ) WHERE grupo_evento_gde.id = ".$ID;
		$result = $this->db->query($sql);
		
		/*$this->db->select('*');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id = '.$ID);
		$result = $this->db->get();*/
		
		// Datos de eventos del GDE
		$Pais                 = $result->row()->dest_country;
		$Negocio              = $result->row()->dest_bunit;
		$ServicioAfectados    = $result->row()->src_system;
		$AreaResponsable      = $result->row()->src_organization;
		$TipoDispositivo      = $result->row()->src_category;
		$UbicacionDispositivo = $result->row()->src_subcategory;
		$NombreDispositivo    = $result->row()->src_name;
		$IPPuerta             = $result->row()->src_ip;
		$FechaHora            = $result->row()->start_time;
		$Descripcion          = $result->row()->type;
		
		// Mantenedor de categorias
		$Categoria1           = $result->row()->pais.".MONITOREO.".$result->row()->encargado;
		$Categoria2           = $result->row()->app;
		$Categoria3           = $result->row()->detalle;
		
		// Mantenedor de correos
		$GrupoResolutor       = $result->row()->gruporesolutor;
		$SupportOrganization  = $result->row()->support_organization;
		
		if($result->row()->severity == "CRITICAL"){
			$Urgencia = "1-Critical";
			$Impacto  = "1-Extensive/Widespread"; 
		} else {
			$Urgencia = "3-Medium";
			$Impacto  = "3-Moderate/Limited"; 
		}
		
		$command = $this->pythonPath . $this->wsFilePath .'createTicket'.' "'.$Pais.'" "'.$Negocio.'" "'.$ServicioAfectados.'" "'.$AreaResponsable.'" "'.$TipoDispositivo.'" "'.$UbicacionDispositivo.'" "'.$NombreDispositivo.'" "'.$IPPuerta.'" "'.$FechaHora.'" "'.$Descripcion.'" "'.$Urgencia.'" "'.$Categoria1.'" "'.$Categoria2.'" "'.$Categoria3.'" "'.$GrupoResolutor.'" "'.$SupportOrganization.'" "'.$Impacto.'"';
		
		$output = shell_exec($command);
		return $output;
	}
	
	
	/**
	*Consulta el estado del Ticket OK
	*
	*@param String	$idticket	Identificador unico de cada Ticket
	*
	*@return String[] Arreglo con los resultados de la consulta del Ticket
	*/
	public function consultaIncidente($ID){
		
		$command = $this->pythonPath . $this->wsFilePath . 'consultaIncidente'." ".$ID." 2>&1";
		
		exec($command, $output, $status);
		return $output[0];
	}
	
	/**
	*Cierra el Ticket seleccionado
	*
	*@param String	$idticket	Identificador unico de cada Ticket
	*@param Bool	$accion		Identifica mediante que accion fue cerrado el ticket
	*
	*@return String[] Arreglo con los resultados del cierre del Ticket
	*/
	public function actualizaEstadoIncidente($ID, $status, $comentario){
		$command = $this->pythonPath . $this->wsFilePath . 'actualizaEstadoIncidente'.' '.$ID.' "'.$status.'" "'.$comentario.'"';
		
		$output = shell_exec($command);
		return $output;
	}
	
}