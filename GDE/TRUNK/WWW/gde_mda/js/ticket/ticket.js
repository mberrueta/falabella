
var oTable, oTableReg;
var aMarcados	= new Array();	// Checkbox marcados
var aCheck		= new Array();  // Estados de tickets seleccionados
var aTicketId	= new Array();
var sCloseOpc	= '';	//Opcion al cerrar un ticket
var iActiveWS	= 0; 	//Llamadas a ws activas: si es mayor que 0 no se realiza la recarga automatica y si es mayor que 1 no se realiza la recarga al finalizar la consulta

$(document).ready(function(){
	oTable = $('#eventos').dataTable({
		"bJQueryUI": true,
		"sScrollY": "400px",				// Alto de la tabla
		"sScrollX": "100%",					// Ancho de la tabla
		"sPaginationType": "full_numbers",	// Se declara que la paginacion entrege todos los numeros
		"iDisplayLength" : 50,				// Numero de eventos a desplegar por default
		"aoColumnDefs": [
		  { "sName": "res.TICKET_ID"		, "sWidth": "10px", "bSearchable": false, "bSortable": false  , "aTargets": [ 0 ] },
		  { "sName": "ticket.TICKET_EST"	, "sWidth": "40px", "aTargets": [ 1 ] },
		  { "sName": "ticket.TICKET_TSINI"	, "aTargets": [ 2 ] },
		  { "sName": "ID"					, "aTargets": [ 3 ] },
		  { "sName": "CLIENTE"				, "aTargets": [ 4 ] },
		  { "sName": "res.CANTIDAD"			, "sWidth": "20px", "aTargets": [ 5 ] },
		  { "sName": "DISPO"				, "aTargets": [ 6 ] },
		  { "sName": "REVISION"				, "sWidth": "80px", "aTargets": [ 7 ] },
		  { "sName": "Opciones"				, "aTargets": [ 8 ] }
		],
		"oLanguage": {
			"sUrl": "../../js/spanish.txt"  // Idioma de la tabla
		},
		"aaSorting": [[2,'desc']],
		"bProcessing": true,
		"bServerSide": true,
		"bDeferRender": true,
		"sAjaxSource": "c_ticket/listarTickets",
		'fnServerData': function(sSource, aoData, fnCallback){
			$.ajax({
			  'dataType': 'json',
			  'type'    : 'POST',
			  'url'     : sSource,
			  'data'    : aoData,
			  'success' : fnCallback
			});
		 },
		 'fnDrawCallback': function( oSettings ) {
			
			oClase = 'background-color:#DF0101; color:#FFFFFF; font-weight: bold;';
			
			$('#eventos tbody tr').each(function(){
				oValor = $(this).find("td:eq(7)").text().trim();
				valor = parseInt(oValor);
				if(valor){
					if(valor < 0){
						$(this).find("td:eq(7)").attr('style',oClase);
					}
					$(this).find("td:eq(7)").append('m');
				}
			});
			
			$.ajax({
				url: "../c_seguimientoEmail/cantidadSeguimientosActivos",
				success: function(data){
					$('#sem_menu_header').html(data);
					if (data == 0){
						$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_green');
					}
					else{
						$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_red');
					}
				}
			});
			
			$.ajax({
				'url': 'c_ticket/listarFiltros',
				'data': {'json': true},
				'type': 'POST',
				'dataType': 'json',
				'success' : function(data){
					
					for (var filtro in data){
						$('#filtro_'+filtro).html(data[filtro]);
					}
					for(var i=0; i<aMarcados.length; i++){
						$('input[value="' + aMarcados[i] + '"]').prop("checked",true);
					}
					
					// bCambiarEstado();
				}
			});
		 }
	});
	
/*	--------------------------------------------------
	:: Valores iniciales
	-------------------------------------------------- 
	*/
	// Recargar la tabla de eventos
	setInterval(function () {
		// console.log(iActiveWS);
		if(iActiveWS == 0){
			filtrar();
		}
		$.ajax({
			url: "../c_seguimientoEmail/cantidadSeguimientosActivos",
			success: function(data){
				$('#sem_menu_header').html(data);
				if (data == 0){
					$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_green');
				}
				else{
					$('#sem_menu_header').removeClass('sem_menu_header_red sem_menu_header_green').addClass('sem_menu_header_red');
				}
			}
		});
	},1000*60*1.5);  // Numero de milisegundos en los que se repetira la accion (90 segundos)
	
	
	$('#dialog_cerrar').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
				sCloseOpc = $("input[name='opc']:checked").val();
				var sel = '';
				if(sCloseOpc == 'cerrar'){
					sel = 'Cerrar ticket';
				}
				else if(sCloseOpc == 'noconf'){
					sel = 'Cliente no conforme';
				}
				else if(sCloseOpc == 'cambiar'){
					sel = 'Cambiar motivo de estado a observación';
				}
				$('#dialog_cerrarconfirm').empty();
				$('#dialog_cerrarconfirm').append('<p>Esta a punto de realizar la siguiente accion: '+sel+'</p>');
				$('#dialog_cerrarconfirm').dialog('open');
				$( this ).dialog( 'close' );
			},
			'Cancelar': function() {
				$( this ).dialog( 'close' );
			}
		}
	});
	$('#dialog_cerrarconfirm').dialog({
		nameUser: '',
		autoOpen: false,
		modal: true,
		buttons: {
			'Aceptar': function() {
				if(sCloseOpc == 'cerrar'){
					cerrarTicket(0);
				}
				else if(sCloseOpc == 'noconf'){
					cerrarTicket(1);
				}
				else if(sCloseOpc == 'cambiar'){
					cerrarTicket(2);
				}
				$( this ).dialog( 'close' );
			},
			'Cancelar': function() { 
				$("input[value='cerrar']").attr('checked', true);
				$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#dialog_cerrarresult').dialog({
		nameUser: '',
		autoOpen: false,
		modal: true,
		buttons: {
			'Aceptar': function() {
				$( this ).dialog( 'close' );
				filtrar();
			}
		}
	});
});
	
/*	--------------------------------------------------
	:: Funciones de soporte para el funcionamiento
	-------------------------------------------------- */
	
// Funcion que devuelve los valores unicos de un Array
	var unique = function(origArr) {  
		var newArr = [],  
			origLen = origArr.length,  
			found,  
			x, y;  
	  
		for ( x = 0; x < origLen; x++ ) {  
			found = undefined;  
			for ( y = 0; y < newArr.length; y++ ) {  
				if ( origArr[x] === newArr[y] ) {  
				  found = true;  
				  break;  
				}  
			}  
			if ( !found) newArr.push( origArr[x] );  
		}  
	   return newArr;  
	};
	
	function getFiltrosActivos( filtro ){
		v = new Array();

		if(filtro == "texto"){
			b = $('.filtro_'+filtro).val();
			v.push(b);
		}
		else{
			$( '.filtro[name="' + filtro + '"]:checked').each(function(){
				v.push($(this).val());
			});
		}
		// console.log(filtro);
		// console.log(v.join(","));
		return v.join(",");
	}
	
	function replaceAll( text, busca, reemplaza ){
		while (text.toString().indexOf(busca) != -1)
			text = text.toString().replace(busca,reemplaza);
			return text;
	}
	
	function AfterSendEmail(){
		// oTable.fnDraw();
		filtrar();
	}
	
	$.fn.spin = function(opts) {
		this.each(function() {
			var $this = $(this),
				data = $this.data();
			if (data.spinner) {
				data.spinner.stop();
				delete data.spinner;
			}
			if (opts !== false) {
				data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
			}
		});
		return this;
	};

	
// Funcion que llama a fnFilter, con los filtros seleccionados, para filtrar la tabla de eventos
	function filtrar(){

		aMarcados = new Array();
		
		$('input[type="checkbox"]:checked').each(function(){
				aMarcados.push($(this).val());
		});
		
		$.ajax({
			'dataType': 'json',
			'url'     : 'c_ticket/getFiltros',
			'success' : function(data){
			
				var objectParams = {};
				for( var i=0; i<data.length; i++ ){
					
					if(data[i] == 'CLIENTE'){
						objectParams[data[i]] = getFiltrosActivos(data[i]);
					}
					else{
						objectParams["ticket."+data[i]] = getFiltrosActivos(data[i]);
					}
					
					// console.log(data[i]);
				}
				// console.log(objectParams);
				oTable.fnMultiFilter(objectParams);
			}
		});
		
		// console.log(getFiltrosActivos('texto'));
		oTable.fnFilter( getFiltrosActivos('texto') );
	}
	

/*	--------------------------------------------------
	:: Movimiento Header
	-------------------------------------------------- */
	
	$('.mov_contenedor_header').live('click', function(){
		var value = $('#row_header').attr('style').split(':')[1].split('px;')[0];
		if(value == 0){
			$('#row_header').attr('style','margin-top:-120px;');
			$(this).text("vv");
		}else{
			$('#row_header').attr('style','margin-top:0px;');
			$(this).text("^^");
		}
	});
	
	// Despliegue de Correo a Cliente
	$('.mail_boton').live('click',function(){
		// Se obtiene el Id del Ticket y el Nombre del Cliente afectado
		estado   = $(this).closest('#eventos tbody tr').find('td:eq(1)').text().trim();
		console.log(estado);
		if(estado=='CLOSED'){
			alert("Verificar que el estado de los tickets no sea CLOSED");
		}
		else if(estado=='CANCELLED'){
			alert("Verificar que el estado de los tickets no sea CANCELLED");
		}
		else {
			idTicket   = $(this).closest('#eventos tbody tr').find('td:eq(3)').text().trim();
			nmCliente = $(this).closest('#eventos tbody tr').find('td:eq(4)').text().trim();
			
			// Se genera el asunto del correo
			asunto = 'Incidente Cliente ' + nmCliente;
			
			// Se busca la informacion que es cargada en el correo
			$.post('../operador/c_ticket/getInformacionCorreo', {'idRegistro': idTicket} ,function(data){
				obj=JSON.parse(data);
				console.log(obj);
				var correos = '';
				if(obj.Correos){
					correos = replaceAll(obj.Correos, ";", ",");
				}
				
				$.post('../operador/c_ticket/loadViewEmailTicket', {'Correos': correos, 'asunto': asunto, 'idTicket': idTicket, 'copia': obj.copia, 'tipo': obj.info_tipo, 'equipo': obj.info_dispo, 'sucursal': obj.sucursal, 'fecha_inicio': obj.info_fecha, 'codigo': obj.info_codigo, 'telefono': obj.telefono, 'nombre': obj.nombre, 'cliente': nmCliente } ,function(data){
					w = window.open('', '', 'width=800,height=550');
					w.document.write(data);
					w.document.close();
				});
			});
		}
	});
	
	// Despliegue de Informacion de Trabajo
	$('.info_boton').live('click',function(){
		idTicket   = $(this).closest('#eventos tbody tr').find('td:eq(3)').text().trim();
		iActiveWS += 1;
		// Bloque para mostrar carga de web services
		var colIndex = $(this).closest("td").index();
		var rowIndex = $(this).closest("tr").index();
		var info = $(this); 
		
		// Se esconde imagen para no ser consultada nuevamente
		info.hide();
		var target = $('#eventos tbody tr:eq('+rowIndex+')').find('td:eq('+colIndex+')').find('span');
		target.spin({
			lines: 9, // The number of lines to draw
			length: 5, // The length of each line
			width: 3, // The line thickness
			radius: 4, // The radius of the inner circle
			color: '#000', // #rgb or #rrggbb
			speed: 2.2, // Rounds per second
			trail: 50, // Afterglow percentage
			shadow: false // Whether to render a shadow
		});
		// Fin Bloque para mostrar carga de web services
		
		// Consulta a web services Consulta Incidente
		$.post('../operador/c_ticket/getInformacionTrabajo', {'idRegistro': idTicket} ,function(data){
			if(data){
				if(data == 'error'){
					alert("Respuesta de WebService: Ticket no existe en base de datos.")
				} else if(data == 'error_2'){
					alert("Respuesta de WebService: Ticket no existe en base de datos.")
				} else{
					obj=JSON.parse(data);
					if(obj.Codigo_Retorno == 'OK'){
						if(iActiveWS == 1){
							filtrar();
						}
						$.post('../operador/c_ticket/loadViewInfoTicket', {'estado': obj.estado, 'cod_servicio': obj.codigo_servicio, 'motivo': obj.motivo_estado,'origen_ticket': obj.origen_ticket,'fuente_registrada': obj.fuente_registrada, 'fecha_creacion': obj.fecha_creacion, 'fecha_modificacion': obj.fecha_modificacion, 'resumen': obj.resumen, 'usuario_asignado': obj.asignado, 'grupo_asignado': obj.grupo_asignado, 'login': obj.login, 'correo': obj.correo, 'extremo_a': obj.extremo_a, 'extremo_b': obj.extremo_b, 'ticket_id': idTicket, 'info_trabajo': obj.info_trabajo, 'resolucion': obj.resolucion, 'causa': obj.causa, 'responsabilidad_falla': obj.responsabilidad_falla} ,function(data){
							w = window.open('', '', 'width=800,height=550');
							w.document.write(data);
							w.document.close();
						});
					}
					else{
						alert(obj.Codigo_Retorno)
					}
				}
			} else {
				// Error con web services
				alert("Error con Web Services. Favor intentelo nuevamente. \nSi el problema persiste favor contactar al Administrador");
			}
			
			// Se detiene la carga y se vuelve a mostrar info de trabajo
			target.data('spinner').stop();
			iActiveWS -= 1;
			info.show();
		});
	});
	

/*	--------------------------------------------------
	:: Seleccion de registro
	-------------------------------------------------- */
	$('#eventos tbody tr').live('dblclick', function(e){
		e.preventDefault();
		$('#eventos tbody tr').removeClass("row_selected");
		$(this).addClass("row_selected"); // Se agrega clase al row seleccionado
		var idRegistro = $(this).find(">:first-child").find(">:first-child").val();
		// console.log(idRegistro);
		$.ajax({
			url:'c_ticket/getInformacionTicket',
			type:'POST',
			cache: false,
			data: { 'idRegistro': idRegistro },
			success: function( html ) {
				obj = jQuery.parseJSON(html);
				$("#bloqueDatos").html('').html(obj.datos);
				$("#bloqueNodosAsociados").html('').html(obj.nodos);
				$("#bloqueEventosNodos").html('<b>Para ver detalle de los eventos asociados realice doble click en el nodo correspondiente</b>')
			}
		});
	});
	
/*	--------------------------------------------------
	:: Seleccion de registro Eventos Sumarizados
	-------------------------------------------------- */	
	$('#nodos_asociados tbody tr').live('dblclick', function(e){
		e.preventDefault();
		$('#nodos_asociados tbody tr').removeClass("row_selected");
		$(this).addClass("row_selected"); // Se agrega clase al row seleccionado
		
		var idRegistro = $(this).find('td:eq(0)').text().trim();
		var nomNodo = $(this).find('td:eq(1)').text().trim();
		var selectedID = $('#selectedID').text().trim();
		
		$.ajax({
			url:'c_ticket/getInformacionNodo',
			type:'POST',
			cache: false,
			data: { 'idRegistro': idRegistro, 'nomNodo': nomNodo, 'selectedID': selectedID },
			success: function( html ) {
				obj = jQuery.parseJSON(html);
				$("#bloqueEventosNodos").html('').html(obj.eventos);
			}
		});
	});
	
	
/*	--------------------------------------------------
	:: Marcar casillas de eventos
	-------------------------------------------------- */
	$("#checkall").live('change',function(){
		var chstatus = $(this).is(':checked');
		if(chstatus == true){
			$('#eventos tbody tr td input[type=checkbox]:not(:checked)').attr("checked", "checked"); 
			$('#eventos tbody tr').addClass('marcado');
		}
		else{
			$('#eventos tbody tr td input[type=checkbox]:checked').removeAttr("checked");  
			$('#eventos tbody tr').removeClass('marcado');
		}
		 bCambiarEstado();
	});
	
	$('.check').live('change',function(){
		if($(this).is(':checked')){
			$(this).parent().parent().addClass("marcado");
		}
		else{
			$(this).parent().parent().removeClass("marcado");
		}
		bCambiarEstado();
	});
	
/*	--------------------------------------------------
	:: Filtros de busqueda
	-------------------------------------------------- */
	$('.filtro').live('change',function(){
		filtrar();
	});
	
/*	--------------------------------------------------
	:: Cambiar estado de eventos
	-------------------------------------------------- */
	$('#subCerrarTicket').live('click',function(e){
		e.preventDefault();
		$('#dialog_cerrar').dialog('open');
	});
	
	
	function bCambiarEstado(){
		var aCheck = new Array();
		var i = 0;
		$('.check:checked').each(function(){
			i += 1;
			aCheck.push($(this).parent().parent().find("td:eq(1)").text());
		});

		aCheck = unique(aCheck); // Estados de los eventos seleccionados
		if(aCheck.indexOf('CERRADO') == -1 && i == 1){
			$('#subCerrarTicket').attr('disabled', false);
			$('#cant_select').html(i);
			return true;
		}
		else{
			$('#cant_select').html(i);
			$('#subCerrarTicket').attr('disabled', true);
			return false;
		}
	}
	
	
	function cerrarTicket(accion){
		aTicketId = new Array();
		$('.check:checked').each(function(){
			aTicketId.push($(this).attr('value'));
			aCheck.push($(this).parent().parent().find("td:eq(1)").text());
		});
		// console.log(aTicketId);
		
		$.ajax({
			url:'c_ticket/cerrarTicket',
			type:'POST',
			cache: false,
			data: { 'estadoInicial':aCheck , 'idsTicket':aTicketId[0] , 'accion':accion },
			success: function( html ) {
				// console.log(html);
				$('#dialog_cerrarresult').empty();
				$('#dialog_cerrarresult').append('<p>'+html+'</p>');
				$('#dialog_cerrarresult').dialog('open');
			},
			error: function( ) {
				// console.log('error');
			}
		});
	}
	
/*	--------------------------------------------------
	:: Despliegue menu de pruebas y acciones
	-------------------------------------------------- */

	// Despliegue de menu en header
	$('#sel_menu_header').live('click', function(){
		$('#options_menu_header').slideToggle("fast");
	});