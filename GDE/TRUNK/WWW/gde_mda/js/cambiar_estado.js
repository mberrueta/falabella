var getEstado = 'c_cambiar_turno/obtener_estado';

$( function() {
	
	leerDatos();
	
	$('#displayCambiarStt').click(function(){
		$('#cambiar_estado').dialog( 'open' );
	});
		
	$('#cambiar_estado').dialog({
		autoOpen: false,
		height: 180,
		width: 220,
		title: 'Cambiar estado a:',
		modal: true,
		buttons: {
			'Cancelar': function() {
				$(this).dialog( 'close' );
			},
			'Aceptar': function() {
				var query = $('#datos_cambio_turno').serializeArray();
				if (query[0].value == 0){
					document.getElementById('state').innerHTML = 'Trabajando';
				}
				else if (query[0].value == 1){
					document.getElementById('state').innerHTML = 'Receso';
				}
				else{
					document.getElementById('state').innerHTML = 'Cambio de Turno';
				}
				$('#cambiar_estado').dialog( 'close' );
				$('#dialog-confirm').dialog( 'open' );
			}
		}
	});
	
	$('#dialog-confirm').dialog({
		autoOpen: false,
		resizable: false,
		height: 120,
		width: 380,
		modal: true,
		buttons: {
			"Cancelar": function() {
				$( this ).dialog( "close" );
			},
			"Aceptar": function() {
					var query = $('#datos_cambio_turno').serializeArray();
					$.ajax({
					type: 'POST',
					url: 'c_cambiar_turno/cambiar_turno',
					data: query,
					success: function( response ) {
						status  = '';
						mensaje = response.split('|');
						if(mensaje[0] == 0){
							status = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">';
						}
						else{
							status = '<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>';
						}
						$( '#dialog-confirm' ).dialog( 'close' );
						$('#dialog-cturno').html('').append('<p>'+status+''+mensaje[1]+'</p>');
						$('#dialog-cturno').dialog('open');
						
					}
				});
			}
		}
	});
	
	$('#dialog-cturno').dialog({
		nameUser: '',
		autoOpen: false,
		modal: true,
		close: function(event, ui) { 
				window.location.reload(); 
		}
	});
	
});

	


function leerDatos(){

	$.post('c_cambiar_turno/obtener_estado', function(data){
		var obj = jQuery.parseJSON(data);
		//console.log(obj.lbl);
		if(obj.stt === 1 || obj.stt === 2 ){
			$('#state-now').attr('style','font-size:14pt; color:#FF0000;');
		}
		else{
			$('#state-now').attr('style','font-size:14pt; color:#31B404;');
		}
		$('#state-now').html(obj.lbl);
	});
}