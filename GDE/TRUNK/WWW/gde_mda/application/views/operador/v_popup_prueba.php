<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>GDE -> Operador -> Prueba</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/gde_entel.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/popup.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ie.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
</head>
<body>
	<div id="contenedor">
		<header>
			<img class="flotante" src="<?php echo base_url(); ?>images/logo_entel.png" />
			<div class="flotante" >
				<?php
					echo '<table>';
					echo '<tr><td>Prueba: </td><td>&nbsp;</td><td>'.$tipo.'</td></tr>';
					echo '<tr><td>Cliente: </td><td>&nbsp;</td><td>'.$cliente.'</td></tr>';
					echo '<tr><td>Nodo: </td><td>&nbsp;</td><td>'.$nomnodo.'</td></tr>';
					echo '<tr><td>IP Nodo: </td><td>&nbsp;</td><td>'.$nodo.'</td></tr>';
					echo '</table>';
				?>
			</div>
			<div style="clear:both;"></div>
		</header>
		<!--<pre style="word-wrap: break-word; white-space: pre-wrap;">-->
		<div id="panelLoading">
			<img src="<?php echo base_url(); ?>images/loading.gif" />
			<p>Realizando prueba, favor espere mientras se completa...</p>
		</div>
		<iframe height="350" width="600" frameborder="0" id="iframePrueba" src="<?php echo base_url();?>index.php/operador/c_prueba/prueba<?php echo "/".$tipo."/".$cliente."/".$nodo; ?>">
			problema al cargar iframe
		</iframe>
		<!--</pre>-->
		<div style="clear:both;"></div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#iframePrueba').load(function(){
				$('#panelLoading').remove();
			});
		});
	</script>
</body>
</html>