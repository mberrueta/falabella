<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>

	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Eliminar Cliente</h3>
			</header>
			<div class="module_content" >
			
			<?php
				if($men != ''){
					$mensaje = explode('|',$men);
					echo $mensaje[1];
				}
			?>
			
			<?php
				if($lcl['stt']){
					echo form_open('supervisor/c_eliminar_cliente/eliminar_cliente');
					echo '<table border="1">';
					echo '<thead>';
					echo '<tr><th>CLIENTE</th><th></th></tr>';
					echo '</thead>';
					echo '<tbody>';
					foreach($lcl['cli']->result() as $cliente){
						echo '<tr>';
						echo '<td>'.form_label($cliente->CLI_NOM,$cliente->CLI_NOM).'</td>';
						echo '<td>'.form_checkbox($cliente->CLI_NOM,$cliente->CLI_NOM).'</td>';
						echo '<tr>';
					}
					echo '<tbody>';
					echo '<table>';
					echo '<br />';
					echo form_submit('eliminar','Eliminar');
					echo form_close();
				}
				else{
					echo '<p>Problema</p>'; #Verificar mensaje
				}
			?>
			
			</div>
		</article>
	</section>

</body>
</html>
