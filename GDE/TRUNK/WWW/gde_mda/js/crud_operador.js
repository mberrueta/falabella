var readUrl   = 'c_crud_operador/read',
	createUrl = 'c_crud_operador/create',
    updateUrl = 'c_crud_operador/edit',
    delUrl    = 'c_crud_operador/delete',
	getUser   = 'c_crud_operador/getbyUser';
	
$( function() {
	$( '#msgDialog' ).dialog({
        autoOpen: false,    
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });

	/*Al cargar la vista se deben cargar los datos existentes*/
	leerDatos();
	$('#registros').show();
	/* --------------------------------------*/
	/* Inicio de funcion para checkbox */
	$("#checkall").live('change',function(){
		var chstatus = $(this).is(':checked');
		if(chstatus == true){
			$('#registros tbody tr td input[type=checkbox]:not(:checked)').attr("checked", "checked"); 
		}
		else{
			$('#registros tbody tr td input[type=checkbox]:checked').removeAttr("checked");  
		}
	});
	/*Fin de funcion para checkbox*/
	/* --------------------------------------*/
	/* Inicio para generar username y password automaticamente*/
	$('#nom_ope').keyup(function(e){
		var nombre = $(this).val();
		var apellido = $('#app_ope').val();
		var username = nombre.substring(0,1)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_ope").val(username);
		$("#pass_usu_ope").val(username);
	});
	
	$('#app_ope').keyup(function(e){
		var nombre = $('#nom_ope').val();
		var apellido = $(this).val();
		var username = nombre.substring(0,1)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_ope").val(username);
		$("#pass_usu_ope").val(username);
	});
	/* Fin para generar username y password automaticamente */
	/* --------------------------------------*/
	/* Inicio dialogo confirmar borrar operador*/
	$('#delConfDialog').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
			$( this ).dialog( 'close' );
			},
			'Yes': function() {
				var operadores_eliminar = new Array();
				
				if(nameUser == ''){ //Si nameUser es vacio significa que el dialog se abrio desde los checkbox.
					//Recuperar values de los operadores con check
					$('#registros tbody tr td input[type=checkbox]:checked').each(function()
					{
						operadores_eliminar.push($(this).val());
					});
					//alert(operadores_eliminar);
				}
				else{
					operadores_eliminar.push(nameUser);
					//alert(operadores_eliminar);
				}
				var toBeSent = {usuarios: operadores_eliminar}; 
				$( '#ajaxLoading' ).fadeIn( 'slow' );
				$( this ).dialog( 'close' );
				$.ajax({
					type: 'POST',
					url: delUrl,
					data: toBeSent,
					success: function( response ) {
						leerDatos();
						$( '#ajaxLoading' ).fadeOut( 'slow' );
					}
				});
			}
		}
	});
	/* Fin dialogo confirmar borrar operador*/
	/* --------------------------------------*/
	/* Inicio dialogo para crear operador */
	$('#crear_operador').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Crear nuevo operador',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$( '#crear_operador input' ).val( '' );
			$('#datos_nuevo_operador').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Crear': function() {
				if($('#datos_nuevo_operador').valid()){
					$( '#ajaxLoading' ).fadeIn( 'slow' )
					var query = $('#datos_nuevo_operador').serializeArray();
					$.ajax({
						type: 'POST',
						url: createUrl,
						data: query,
						success: function( response ) {
							if(response == 0){
								leerDatos();
								$('#crear_operador').dialog( 'close' );
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								//Limpiar los campos de texto
								$( '#crear_operador input' ).val( '' );
							}
							else{
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
								$('#msgDialog').dialog('open');
							}
						}
					});
					//Limpiar los campos de texto
					$( '#crear_operador input' ).val( '' );
				}
				else{
					alert('Los datos estan con error');
				}
			}
		}
	});
	/* Fin dialogo para crear operador */
	
	/* Inicio dialogo para editar operador */
	$('#actualizar_operador').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Editar operador',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$('#actualizar_operador input').val( '' );
			$('#datos_actualizar_operador').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Guardar': function() {
				if($('#datos_actualizar_operador').valid()){
					$( '#ajaxLoading' ).fadeIn( 'slow' )
					var query = $('#datos_actualizar_operador').serializeArray();
					var nom_usu = {name: "edit_nom_usu_ope", value: $('#edit_nom_usu_ope').val() };
					query.push(nom_usu);
					$.ajax({
						type: 'POST',
						url: updateUrl,
						data: query,
						success: function( response ) {
							if(response == 0){
								leerDatos();
								$('#actualizar_operador').dialog( 'close' );
								//Limpiar los campos de texto
								$( '#actualizar_operador input' ).val( '' );
							}
							else{
								$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
								$('#msgDialog').dialog('open');
							}
							$( '#ajaxLoading' ).fadeOut( 'slow' );
						}
					});
					//Limpiar los campos de texto
				}
				else{
					alert('Los datos estan con error');
				}
			}
		}
	});
	/* Fin dialogo para editar operador */

	/* Inicio Binding para nuevo operador */
	$('#buttonNuevo').click(function(){
		$('#crear_operador').dialog( 'open' );
	});
	
	/* Inicio Binding para editar operador */
    $( '#registros' ).delegate( 'img.EditBtn', 'click', function() {
        nameUser = $( this ).parents( 'tr' ).attr( "id" );
        $( '#ajaxLoading' ).fadeIn( 'slow' );
		$('#datos_actualizar_operador input').val("");
        
        $.ajax({
    //        url: getUser +'/' + nameUser,
            url: readUrl,
            dataType: 'json',  
            success: function( response ) {
				var cont = 0;
				var i = 0;
				response.forEach(function(e){ 
					if(e.USU_USER == nameUser) 
						i=cont;
					cont++;})

                $( '#edit_nom_ope' ).val( response[i].OPE_NOM );
                $( '#edit_app_ope' ).val( response[i].OPE_APP );
				$( '#edit_apm_ope' ).val( response[i].OPE_APM );
				$( '#edit_nom_usu_ope' ).val( response[i].USU_USER );
				$( '#edit_email_usu_ope' ).val( response[i].USU_EMAIL );
				$( '#edit_anexo_usu_ope' ).val( response[i].OPE_ANE );
				$( '#edit_tel_usu_ope' ).val( response[i].OPE_TEL );
				$( '#edit_tipo_usu_ope' ).val( response[i].user_category );
				
				$( '#edit_tipo_usu_ope_hidden' ).val( response[i].user_category );
				
                $( '#ajaxLoading' ).fadeOut( 'slow' );
                $( '#actualizar_operador' ).dialog( 'open' );
            }
        });
        
        return false;
    }); 
	
	
	/* Eliminar Operador desde tarro de basura*/
    $( '#registros' ).delegate( 'img.DeleteBtn', 'click', function() {
		nameUser = $( this ).parents( 'tr' ).attr( "id" );
		var texto = '&iquest;Confirma que desea eliminar al operador '+nameUser+'?';
		$('#delConfDialog').html(texto);
		$('#delConfDialog').dialog('option', 'nameUser', nameUser).dialog('open');
    }); 

	/*Eliminar todos los operadores seleccionados desde checkbox*/
	$('#buttonDel').click(function(){
		nameUser = '';
		$('#delConfDialog').dialog('option', 'nameUser', nameUser).dialog('open');
	});
	
}); //end document ready

function leerDatos(){
    //Mostrar loading
    $( '#ajaxLoading' ).fadeIn( 'slow' );
    
    $.ajax({
        url: readUrl,
        dataType: 'json',
        success: function( response ) {
			var salida = '';
			
			for( var i in response ) {
				var tipo   = '';
				
				salida += "<tr id=\""+ response[ i ].USU_USER+"\">";
				//salida += "<td><input type=\"checkbox\" name=\"checkelimina\" value=\""+response[ i ].USU_USER+"\"></td>";
				salida += "<td>"+ response[ i ].OPE_NOM+"</td>";
				salida += "<td>"+ response[ i ].OPE_APP+"</td>";
				salida += "<td>"+ response[ i ].OPE_APM+"</td> <!-- OPE_APM -->";
				salida += "<td>"+ response[ i ].USU_USER+"</td>";
				salida += "<td>"+ response[ i ].USU_EMAIL+"</td>";
				salida += "<td>"+ response[ i ].OPE_ANE+"</td> <!-- OPE_ANE -->";
				salida += "<td>"+ response[ i ].OPE_TEL+"</td> <!-- OPE_TEL -->";
				
				if(response[ i ].user_category == "1") tipo = "Operador";
				if(response[ i ].user_category == "2") tipo = "Administrador";
				if(response[ i ].user_category == "3") tipo = "Visualizador";
				if(response[ i ].user_category == "4") tipo = "Super Operador";
				
				
				salida += "<td>"+ tipo+"</td> <!-- SUP_USU_USER -->";
				salida += "<td><img src=\"../../images/edit-icon.png\" class=\"EditBtn\"></td>";
				salida += "<td><img src=\"../../images/delete-icon.png\" class=\"DeleteBtn\"></td>";
				salida += "</tr>";
			}
			//Limpiar filas antiguas
            $( '#registros tbody' ).html( '' );
            //Mostrar los registros
			$( "#registros tbody" ).html(salida);
            //Esconder el loading
            $( '#ajaxLoading' ).fadeOut( 'slow' );
        }
    });
}
