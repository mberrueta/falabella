<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_exportar_resultados extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
	}
	
	public function index(){
		
		$this->load->view('supervisor/v_exportar_resultados');
	}
	
	public function exportar($t_inicio, $t_fin){
		$this->load->model('evento');
	    $this->load->helper('download');
		
		$t_inicio = str_replace("%3Cdospuntos%3E",":",$t_inicio);
		$t_inicio = str_replace("%20"," ",$t_inicio);
		
		$t_fin     = str_replace("%3Cdospuntos%3E",":",$t_fin);
		$t_fin     = str_replace("%20"," ",$t_fin);
		
	    $csv_exportacion = $this->evento->exportar($t_inicio, $t_fin);
		$name = "resultados.csv";
		force_download($name, $csv_exportacion);
		return;
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 0){
			return true;
		}
		else{
			return false;
		}
	}
	
}