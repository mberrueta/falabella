<?php

class Analista extends CI_Model {
	
	public function create($usuario){
		echo 
		
		/* Crear el usuario en la BD */
		$this->db->trans_start();
		$user = array(
			'user'  => $usuario['nom_usu_ope'],
			'user_pass'  => md5($usuario['pass_usu_ope']),
			'user_category'  => '3', /* Codigo 0 de supervisor */
			'user_mail' => $usuario['email_usu_ope'] 
		);
		$this->db->insert('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 2;
		}
		else{
			/*Crear el */
			$this->db->trans_start();
			$supervisor = array(
				'user'     => $usuario['nom_usu_ope'],
				'name'      => $usuario['nom_ope'],
				'last_name_a'      => $usuario['app_ope'],
				'last_name_b'      => $usuario['apm_ope'],
				'annexed'      => $usuario['anexo_ope'],
				'phone'      => $usuario['tel_ope'],
				
			);
			$this->db->insert('analista_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 3; /*No se pudo guardar el supervisor en la BD*/
			}
			else{
				return 0; /*Se inserto correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function delete($user){
		$this->db->trans_start();
		
			$this->db->where('user', $user);
			$this->db->delete('grupo_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('analista_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('usuario_gde');
			$this->db->flush_cache();
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; /*No se pudo eliminar el operador en la BD*/
		}
		else{
			return 0; /*Se elimino correctamente el usuario y el operador */
		}
		
	}
	
	public function edit($usuario, $cambia_pass=false){
		/* Actualizar el usuario en la BD */
		$this->db->trans_start();
		if($cambia_pass){
			$user = array(
				'user_pass'  => md5($usuario['edit_pass_usu_ope']),
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		else{
			$user = array(
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		
		$this->db->where('user', $usuario['edit_nom_usu_ope']);
		$this->db->update('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; //Problemas modificando usuario
		}
		else{
			/*Modificar el supervisor*/
			$this->db->trans_start();
			$supervisor = array(
				//'USU_USER'     => $usuario['edit_nom_usu_sup'],
				'name'      => $usuario['edit_nom_ope'],
				'last_name_a'      => $usuario['edit_app_ope'],
				'last_name_b'      => $usuario['edit_apm_ope'],
				'annexed'      => $usuario['edit_anexo_usu_ope'],
				'phone'      => $usuario['edit_tel_usu_ope']
			);
			$this->db->where('user', $usuario['edit_nom_usu_ope']);
			$this->db->update('analista_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 2; /*No se pudo modificar el supervisor en la BD*/
			}
			else{
				return 0; /*Se modifico correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function editCambioTipo($usuario, $cambia_pass=false){
			
			if($cambia_pass){
				$user = array(
					'user_pass'  => md5($usuario['edit_pass_usu_ope']),
					'user_mail' => $usuario['edit_email_usu_ope'],
					'user_category' => $usuario['edit_tipo_usu_ope'] 
				);
			}
			else{
				$user = array(
					'user_mail' => $usuario['edit_email_usu_ope'],
					'user_category' => $usuario['edit_tipo_usu_ope'] 
				);
			}
			
			$this->db->where('user', $usuario['edit_nom_usu_ope']);
			$this->db->update('usuario_gde', $user);
			// log_message('error', $this->db->last_query());
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 1; //Problemas modificando usuario
			} 
			else {
				
				// Borrar registro anterior
				if($usuario['edit_tipo_usu_ope_hidden'] == "1" || $usuario['edit_tipo_usu_ope_hidden'] == "4"){
					$this->db->where('user', $usuario['edit_nom_usu_ope']);
					$this->db->delete('operador_gde');
					$this->db->flush_cache();
				} elseif($usuario['edit_tipo_usu_ope_hidden'] == "2"){
					$this->db->where('user', $usuario['edit_nom_usu_ope']);
					$this->db->delete('supervisor_gde');
					$this->db->flush_cache();
				} else {
					$this->db->where('user', $usuario['edit_nom_usu_ope']);
					$this->db->delete('analista_gde');
					$this->db->flush_cache();
				}
				// log_message('error', $this->db->last_query());
				$this->db->trans_start();
				
				$supervisor = array(
					'user'     => $usuario['edit_nom_usu_ope'],
					'name'      => $usuario['edit_nom_ope'],
					'last_name_a'      => $usuario['edit_app_ope'],
					'last_name_b'      => $usuario['edit_apm_ope'],
					'annexed'      => $usuario['edit_anexo_usu_ope'],
					'phone'      => $usuario['edit_tel_usu_ope'],
					
				);
				
				if($usuario['edit_tipo_usu_ope'] == "1" || $usuario['edit_tipo_usu_ope'] == "4" ){
					$this->db->insert('operador_gde', $supervisor);
				} elseif($usuario['edit_tipo_usu_ope'] == "2"){
					$this->db->insert('supervisor_gde', $supervisor);
				} else {
					$this->db->insert('analista_gde', $supervisor);
				}
				
				// log_message('error', $this->db->last_query());
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return 3; /*No se pudo guardar el supervisor en la BD*/
				}
				else{
					return 0; /*Se inserto correctamente el usuario y el supervisor */
				}
			}
	}
	
}
	
?>