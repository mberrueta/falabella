<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_sendEmail extends CI_Controller {

	public function index()
	{
		
	}
	
	public function loadView()
	{
		$data = array();
		$this->load->view('v_sendEmail', $data);
	}
	
	public function toEmail()
	{
		$this->load->model('correo');
		$listaCorreos = $this->correo->toEmails($_POST['Nodos']);
		echo json_encode($listaCorreos);
	}
	
	public function test()
	{
		$this->load->model('correo');
		$res['filas'] = $this->codServicio($this->input->post('filas'));
		$res['nodos'] = $this->arrayDispip($this->input->post('filas'));
		$res['firma'] = $this->correo->copyEmail2();
		
		echo json_encode($res);
	}
	
	public function copyEmail()
	{
		$this->load->model('correo');
		$correoUS = $this->correo->copyEmail();
		echo json_encode($correoUS);
	}
	
	public function validarPost($postArray)
	{
		$valid = true;
		if(
			empty($postArray['para']) 
			|| empty($postArray['copia']) 
			// || empty($postArray['asunto']) 
			|| empty($postArray['cliente']) 
			// || empty($postArray['breclamo']) 
			// || empty($postArray['bred']) 
			// || empty($postArray['enprin']) 
			// || empty($postArray['enresp']) 
			// || empty($postArray['comentario'])
		){
			$valid = false;
		}
		return $valid;
	}
	
	public function sendEmail()
	{
		
		$this->load->library('email');
		$postArray = $this->input->post();
		
		// log_message('error', print_r($postArray));
		
		
		$this->load->model('correo');
		// $postArray['ope'] = $this->correo->copyEmail2();
		// $postArray['codserv'] = $this->codServicio($postArray['table']);
		
		$postArray['table3'] = $postArray['table'];
		$postArray['table'] = $this->arrayDispip($postArray['table']);
		$postArray['tipomail'] = 'evento';
		
		$valid = $this->validarPost($postArray);
		
		$this->email->from($postArray['desde'], $postArray['desde_nom']);
		$this->email->to($postArray['para']);
		$this->email->cc($postArray['copia']);
		$email = $this->load->view('v_templateEmail', $postArray, TRUE);
		$this->email->subject($postArray['asunto']);
		$this->email->message($email);
		
		if($valid){
			$send = $this->email->send();
			if($send)
			{
				$this->saveEmail($postArray);
				$data['messageResult'] = 'Correo enviado exitosamente';
				return $this->load->view('v_sendEmailResult', $data);
			}
			else{
				$this->saveError($postArray,$this->email->print_debugger());
				$data['messageResult'] = 'Favor intentelo nuevamente';
				return $this->load->view('v_sendEmailResult', $data);
			}
		}
		else{
			$data['messageResult'] = 'Favor rellene campos incompletos, intentelo nuevamente';
			return $this->load->view('v_sendEmailResult', $data);
		}
	}
	
	public function saveEmail($postArray)
	{
		$this->load->model('correo');
		$this->correo->registrarCorreos($postArray);
	}
	
	public function saveError($postArray,$Error)
	{
		$this->load->model('correo');
		$this->correo->registrarError($postArray,$Error);
	}
	
	public function getSucursal($nodo)
	{
		$this->load->model('correo');
		return $this->correo->getSucursal($nodo);
	}
	
	public function codServicio($postArray)
	{
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		$codserv = array("");
		for($i=0;$i<count($nodos)-1;$i++){
			$cel = explode(",",$nodos[$i]);
			$codserv[$i] = $this->correo->codigoServicio($cel[3]);
		}
		return $codserv;
	}
	
	public function arrayDispip($postArray)
	{
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		$nodosip = array("");
		for($i=0;$i<count($nodos)-1;$i++){
			$cel = explode(",",$nodos[$i]);
			$dispip = $this->correo->arrayDispip($cel[3]); 
			$nodosip[$i] = $cel[3]."\t".$dispip;
		}
		// return array_unique($nodosip);
		return $nodosip;
	}
	
	public function get_alias()
	{
		
		$this->load->model('cliente');
		$alias = $this->cliente->get_alias_nom($this->input->post('Cliente')); 
		echo json_encode($alias);
	}
}

/* End of file c_sendEmail.php */
/* Location: ./application/controllers/c_sendEmail.php */
