<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_analista extends CI_Controller {

	private $form_rules = array();

	public function __construct()
	{
		parent::__construct();
		
		// Set form rules in one place so we can re-use them in regular and ajax validation
		$this->new_op = array(
				array(
					'field'   => 'nom_ope',
					'label'   => 'Nombre analista',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'app_ope',
					'label'   => 'Apellido analista',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field' => 'nom_usu_ope',
					'label' => 'Nombre de usuario',
					'rules' => 'required|min_length[6]|is_unique[usuario.USU_USER]'
				),
				array(
					'field' => 'pass_usu_ope',
					'label' => 'Password',
					'rules' => 'required'
				),
				array(
					'field' => 'email_usu_ope',
					'label' => 'E-mail',
					'rules' => 'required|valid_email'
				),
				array(
					'field' => 'anexo_usu_ope',
					'label' => 'Anexo',
					'rules' => 'required|min_length[4]|max_length[4]|numeric'
				),
				array(
					'field' => 'tel_usu_ope',
					'label' => 'Telefono',
					'rules' => 'required'
				)
		);
		
		$this->update_op = array(
				array(
					'field'   => 'edit_nom_ope',
					'label'   => 'Nombre',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'edit_app_ope',
					'label'   => 'Apellido',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field' => 'edit_nom_usu_ope',
					'label' => 'Nombre de usuario',
					'rules' => 'required|min_length[6]|is_unique[usuario.USU_USER]'
				),
				array(
					'field' => 'edit_pass_usu_ope',
					'label' => 'Password',
					'rules' => ''
				),
				array(
					'field' => 'edit_pass_usu_ope2',
					'label' => 'Password',
					'rules' => 'matches[edit_pass_usu_ope]'
				),
				array(
					'field' => 'edit_email_usu_ope',
					'label' => 'E-mail',
					'rules' => 'required'
				),
				array(
					'field' => 'edit_anexo_usu_ope',
					'label' => 'Anexo',
					'rules' => 'required|numeric'
				),
				array(
					'field' => 'edit_tel_usu_ope',
					'label' => 'Telefono',
					'rules' => 'required'
				)
		);

		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model( 'analista' );
	}

	public function index()
	{	$data = array();
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if($this->session->userdata('tipo') == 0){
			$data['dir_usuario'] = 'supervisor';
		}
		else if($this->session->userdata('tipo') == 2){
			$data['dir_usuario'] = 'admin';
		}
		else{
			$data['dir_usuario'] = $this->session->userdata('tipo') == 0;
		}
		
		
		
		$data['titulo_pagina'] = 'Funciones analista';
		$json_new_op = $this->form_validation->jquery_options($this->new_op, site_url('supervisor/c_crud_analista/valida_new_op'));
		$data['validacion_nuevo_op'] = $json_new_op;
		$json_update_op = $this->form_validation->jquery_options($this->update_op, site_url('supervisor/c_crud_analista/valida_update_op'));
		$data['validacion_actualizar_op'] = $json_update_op;
		$this->load->view('supervisor/v_crud_analista',$data);
	}
	
	public function valida_new_op()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		// Set JSON headers, no cache
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');

		$field_name = key($_POST);
		
		// Load validation rules set in constructor
		$this->form_validation->set_rules($this->new_op);

		// For JSON response, we don't want error delimiters
		$this->form_validation->set_error_delimiters('', '');

		// run_single() is a method extended from core validation library
		if ($this->form_validation->run_single($field_name) == FALSE)
		{
			$response = validation_errors();
		}
		else
		{
			$response = TRUE;
		}

		echo json_encode($response);
	}
	
	public function valida_update_op()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		// Set JSON headers, no cache
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');

		$field_name = key($_POST);
		
		// Load validation rules set in constructor
		$this->form_validation->set_rules($this->update_op);

		// For JSON response, we don't want error delimiters
		$this->form_validation->set_error_delimiters('', '');

		// run_single() is a method extended from core validation library
		if ($this->form_validation->run_single($field_name) == FALSE)
		{
			$response = validation_errors();
		}
		else
		{
			$response = TRUE;
		}

		echo json_encode($response);
	}
	
	public function read()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		echo json_encode( $this->analista->getAll() );
	}
	
	public function edit(){
		/* Codigo de estado:
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el analista
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if( !empty( $_POST ) ) {
			$variables = $_POST;
			if(($variables['edit_pass_usu_ope'] != "")  && ($variables['edit_pass_usu_ope2'] != "") && ($variables['edit_pass_usu_ope'] == $variables['edit_pass_usu_ope2'])){
				$respuesta = $this->analista->edit($variables,true);
			}
			else{
				$respuesta = $this->analista->edit($variables);
			}
			echo $respuesta;
		}
		else{
			echo "1";
		}
	}
	
	public function getByUser($user)
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if( isset( $user ) )
			echo json_encode( $this->analista->getByuser( $user ) );
	}
	
	public function delete(){
		/* Codigo de estado:
			0 = OK
			1 = Error borrando el usuario
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if(isset($_POST['usuarios'])){
			$respuesta = "";
			foreach($_POST['usuarios'] as $user){
				$respuesta = $respuesta."".$this->analista->delete( $user );
			}
			echo $respuesta;
		}
	}
	
	public function create(){
		/* Codigo de estado:
			-1 = Error inesperado
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el analista
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if( !empty( $_POST ) ) {
			$variables = $_POST;
			$this->load->model('analista');
			$respuesta = $this->analista->create($variables);
			echo $respuesta;
		}
		else{
			echo "1";
		}
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 0 || $this->session->userdata('tipo') == 2 )){
			return true;
		}
		else{
			return false;
		}
	}
}