<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_ticket extends CI_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->model('ticket');
	}
	
	public function index(){
		$data['filtros'] = $this->listarFiltros();
		$data['titleFiltros'] = $this->ticket->getFiltros();
		
		$this->load->view('operador/v_ticket', $data);
	}
	
	// Resumen:  Obtencion de los tickets para el despliegue en la tabla
	// Entrada:  Nula
	// Salida:   Lista de Tickets.
	public function listarTickets(){
	
		echo $this->ticket->obtener_tickets($this->session->userdata('username'));
	}
	
	public function getFiltros(){
		echo json_encode($this->ticket->getFiltros(true));
	}
	
	public function cerrarTicket(){
		$this->load->model('webservices');
		$estadoInicial		= $this->input->post('estadoInicial');
		$idsTicket			= $this->input->post('idsTicket');
		$accion				= $this->input->post('accion');
		$retorno2			= array();
		
		$retorno = $this->webservices->actualizaEstadoIncidente($accion, $idsTicket);
		$retorno = rtrim($retorno, "\n");
		
		$retorno2 = explode(',',$retorno);
		$estadoActual = strtoupper($retorno2[1]);
		if($retorno2[0] == 'OK'){
			$res = $this->ticket->cerrarTicket($idsTicket, $accion, $estadoInicial ,$estadoActual);
			
			// Se registra en bitacora la accion
			$usuario	= $this->session->userdata('username');
			$mensaje	= $retorno2[1];
			if(!$this->ticket->comentaTicket($idsTicket,$mensaje,"Movio",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			
			echo $res;
		}
		else{
			$usuario	= $this->session->userdata('username');
			$mensaje	= 'Error: '.$retorno2[0];
			if(!$this->ticket->comentaTicket($idsTicket,$mensaje,"Error Mover",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			echo $retorno2[0];
		}
		
	}
	
	function listadoClientes(){
		$usuarioActivo = $this->session->userdata('username');
		$this->load->model('ticket');
		return $this->ticket->listarClientes($usuarioActivo, true);
	}
	
	public function listarFiltros(){
		$json = $this->input->post('json');
		if(!$json){
			return $this->ticket->listarFiltros($this->session->userdata('username'), $json);
		} else{
			echo $this->ticket->listarFiltros($this->session->userdata('username'), $json);
		}
	}
	
	public function listarEstados(){
		$estadoInicial = $this->input->post('estadoInicial');
		echo $this->ticket->listarEstados($estadoInicial);
	}
	
	// Resumen:  Obtencion de datos para el detalle del ticket (doble click en tabla)
	// Entrada:  ID del TICKET (VIA POST)
	// Salida:   JSON con LA INFORMACION= Datos del Ticket y Grupos de Eventos asociados al ticket
	public function getInformacionTicket(){
		$ticketId = $this->input->post('idRegistro');
		$detalles = array();
		$detalles['datos'] = $this->ticket->obtenerDatos($ticketId);
		$detalles['nodos'] = $this->ticket->obtenerNodosAsociados($ticketId);
		echo json_encode($detalles);
	}
	
	// Resumen:  Obtencion de datos para el despliegue del formulario de envio de correo a clientes.
	// Entrada:  ID del TICKET (VIA POST)
	// Salida:   JSON con LA INFORMACION del Correo
	public function getInformacionCorreo(){
		$this->load->model('correo');
		$ticketId = $this->input->post('idRegistro');
		
		// Se obtiene informacion para el despliegue del correo
		$detalles = $this->ticket->obtenerNodosAsociados($ticketId, false);
		
		// Obtencion de nodos asociados al ticket
		$arrayNodos = $this->ticket->obtenerNodosAsociados_Correo($ticketId);
		$detalle = $this->correo->toEmails($arrayNodos);
		if($detalle){
			$detalles['Correos'] = $detalle['Correos'];
		}
		$sucursales = array();
		foreach( $arrayNodos as $nodo ){
			$this->load->model( 'correo' );
			$suc = $this->correo->getSucursal( $nodo );
			array_push( $sucursales, $suc );
		}
		
		
		// Correo de Operador para campo copia
		
		$datos_firma = $this->correo->copyEmail2();
		$detalles['copia'] = $datos_firma[0];
		$detalles['telefono'] = $datos_firma[1];
		$detalles['nombre'] = $datos_firma[2];
		$detalles['sucursal'] = implode("\n",$sucursales);
		
		echo json_encode($detalles);
	}
	
	// Obtencion de datos para el despliegue de informacion de nodos relacionados con grupos de eventos..
	// ENTRADA: ID del Grupo de EVENTO (VIA POST)
	// SALIDA: JSON con los EVENTOS asociados al Grupo de EVENTO
	public function getInformacionNodo(){
		
		$grupoId = $this->input->post('idRegistro');
		$nomNodo = $this->input->post('nomNodo');
		$selectedID = $this->input->post('selectedID');
		$detalles = array();
		$detalles['eventos'] = $this->ticket->obtenerEventosDatos($grupoId, $nomNodo, $selectedID);
		echo json_encode($detalles);
	}
	
	// Obtencion de datos para el despliegue de la informacion de trabajo, informacion del ticket.
	// Ademas de actualizacion del estado del ticket.
	// ENTRADA: ID del TICKET (VIA POST)
	// SALIDA: JSON con LA INFORMACION
	public function getInformacionTrabajo(){
		
		$this->load->model('webservices');
		$ticketId = $this->input->post('idRegistro');
		
		// Consulta Incidentes
		$detalles = $this->webservices->consultaIncidente($ticketId);
		$detalles = rtrim($detalles, "\n");
		
		if($detalles == 'fail'){
			echo false;
		}
		else if($detalles == 'error'){
			echo 'error';
		}
		else {
			// Consulta Info de Trabajo
			$info_trabajo = $this->webservices->consultaInfoTrabajo($ticketId);
			$info_trabajo = rtrim($info_trabajo, "\n");
			
			// Si web services no estan respondiendo
			if($info_trabajo == 'fail'){
				echo false;
			} 
			else if($info_trabajo == 'error'){
				echo 'error_2';
			} 
			else {
				// Correo de Operador
				$this->load->model('ticket');
				$correo = $this->ticket->getCorreoTicket($ticketId);
				
				// Bloque de consulta de Incidencia
				$resultado = array();

				$data = explode("\n", $detalles);
				foreach ($data as $valor){
					$values = explode(",", $valor);
					if($values[0] != "_fin_"){
						$resultado[$values[0]] = $values[1];
					}
				}
				// Fin Bloque de consulta de Incidencia
				if($resultado['Codigo_Retorno'] == 'OK'){
					// Bloque de informacion de trabajo
					$info_trabajo_res = array();
					$info_trabajo_res = explode("_fin_", $info_trabajo);
					
					$final = array();
					foreach ($info_trabajo_res as $valor){
						$resultado_info = array();
						$info_values = explode("\n", $valor, 5);
						
						$temp = array();
						foreach ($info_values as $valor_2){
							$info_data = explode(",,", $valor_2, 2);
							if(isset($info_data[1])){
								$temp[@$info_data[0]] = @$info_data[1];
							}
						}
						array_push($final, $temp);
					}
					$resultado['info_trabajo'] = $final;
					
					// Correo Operador
					$resultado['correo'] = $correo;
					
					// Fin Bloque de informacion de trabajo
					if($resultado['info_trabajo'][0]['Codigo_Retorno'] == 'OK'){
						// Update estado del ticket
						$this->ticket->updateEstadoTicket($ticketId, $resultado['estado'], $resultado['fecha_modificacion']);
						
						// Resultado Final
						echo json_encode($resultado);
					}
					else{
						$usuario	= $this->session->userdata('username');
						$mensaje	= 'Error: '.$resultado['Codigo_Retorno'];
						if(!$this->ticket->comentaTicket($ticketId,$mensaje,"Error Consulta InfoTrabajo",$usuario)){
							// Registro de la no insercion del comentario
							log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
						}
						$resultado['info_trabajo'][0]['fecha_ingreso'] = '-';
						$resultado['info_trabajo'][0]['origen'] = '-';
						$resultado['info_trabajo'][0]['resumen'] = '-';
						$resultado['info_trabajo'][0]['notas'] = '-';
						echo json_encode($resultado);
					}
					
				}
				else{
					$usuario	= $this->session->userdata('username');
					$mensaje	= 'Error: '.$resultado['Codigo_Retorno'];
					if(!$this->ticket->comentaTicket($ticketId,$mensaje,"Error Consulta Incidente",$usuario)){
						// Registro de la no insercion del comentario
						log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
					}
					echo json_encode($resultado);
				}
				
			}
		}
	}
	
	// Agregar informacion de Trabajo.
	// ENTRADA: ID del TICKET, NOTA Y RESUMEN (VIA POST)
	// SALIDA: RESPUESTA OK/NOK
	public function putInformacionTrabajo(){
		$ticketId	= $this->input->post('ticket_id');
		$nota		= $this->input->post('add_nota');
		$resumen	= $this->input->post('add_resumen');
		$tipo		= 2000;//Comunicacion del Cliente
		$origen		= 7000;//Monitor
		
		$order		= array("\r\n", "\n", "\r");
		$replace	= '<br />';
		$nota		= str_replace($order, $replace, $nota);
		$this->load->model('webservices');
		$result = $this->webservices->creaInfoTrabajo($ticketId,$tipo,$origen,$resumen,$nota);
		$result = rtrim($result, "\n");
		if ($result == 'OK'){
			
			// Se registra en bitacora la accion
			$usuario	= $this->session->userdata('username');
			$mensaje	= "Informacion de Trabajo. Resumen: ".$resumen;
			if(!$this->ticket->comentaTicket($ticketId,$mensaje,"Creo",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			$data['messageResult'] = 'Se agrego exitosamente Informacion de Trabajo';
			// return $this->load->view('operador/v_ticket_mail_result', $data);
		} 
		else{
			$usuario	= $this->session->userdata('username');
			$mensaje	= 'Error: '.$result;
			if(!$this->ticket->comentaTicket($ticketId,$mensaje,"Error Crea InfoTrabajo",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			$data['messageResult'] = 'Favor intentelo nuevamente';
			// return $this->load->view('operador/v_ticket_mail_result', $data);
		}
		echo $result;
	}
	
	// Se carga la vista de envio de correo.
	public function loadViewEmailTicket()
	{
		$data = array();
		$this->load->view('operador/v_ticket_mail', $data);
	}
	
	// Se carga la vista de visualizacion de informacion de trabajo.
	public function loadViewInfoTicket()
	{
		$data = array();
		$this->load->view('operador/v_ticket_info_trabajo', $data);
	}
	
	// Se recarga la vista de visualizacion de informacion de trabajo.
	public function reloadViewInfoTicket()
	{
		$data = $this->input->post();
		
		
		$this->load->view('operador/v_ticket_info_trabajo', $data);
	}
	
	// Envio de correo a Cliente generado por el operador.
	// ENTRADA: DATOS del CORREO (via POST)
	// SALIDA: OK/NOK
	public function sendEmailTicket()
	{
		$this->load->library('email');
		$this->load->model('correo');
		
		// Recopilacion de datos basicos.
		$para   = $this->input->post('ticket_para');
		$copia  = $this->input->post('ticket_cc');
		$asunto = $this->input->post('ticket_asunto');
		
		// Obtencion de IP en Nodos
		$equipo = $this->input->post('equipo');
		$nodos = explode("\r\n",$equipo);
		
		$nodosip = '';
		for($i=0;$i<count($nodos);$i++){
			$dispip = $this->correo->arrayDispip($nodos[$i]); 
			$nodosip = $nodosip.$nodos[$i]."\t".$dispip."\n";
		}
		
		// Recopilacion de data para utilizacion en template de correo.
		$detalles = array();
		$respaldo = array();
		
		$detalles['ticket_id']      = $this->input->post('ticket_id');
		$detalles['cliente']        = $this->input->post('cliente');
		$detalles['tipo']           = nl2br($this->input->post('tipo_alarm'));
		$detalles['equipo']         = nl2br($nodosip);
		$detalles['codigos']        = nl2br($this->input->post('codigo'));
		$detalles['estado']         = $this->input->post('actividad');
		$detalles['fecha_inicio']   = $this->input->post('fecha_inicio');
		$detalles['fecha_termino']  = $this->input->post('fecha_termino');
		$detalles['descripcion']    = nl2br($this->input->post('desc_evento'));
		$detalles['acciones']       = nl2br($this->input->post('acc_reali'));
		$detalles['observaciones']  = nl2br($this->input->post('observaciones'));
		$detalles['sucursal']       = nl2br($this->input->post('sucursal'));
		$detalles['nombre']         = $this->input->post('nombre');
		$detalles['firma']          = nl2br($this->input->post('firma'));
		$detalles['email']          = $this->input->post('email');
		$detalles['fono']           = $this->input->post('fono');
		
		$respaldo['tipomail']       = 'ticket';
		$respaldo['para']           = $this->input->post('ticket_para');
		$respaldo['copia']          = $this->input->post('ticket_cc');
		$respaldo['asunto']         = $this->input->post('ticket_asunto');
		$respaldo['numin']          = $this->input->post('ticket_id');
		$respaldo['cliente']        = $this->input->post('cliente');
		$respaldo['clientehid']     = $this->input->post('clientehid');
		$respaldo['tipoevento']     = $this->input->post('tipo_alarm');
		$respaldo['alaeq']          = $nodosip;
		$respaldo['codservicio']    = $this->input->post('codigo');
		$respaldo['estactividad']   = $this->input->post('actividad');
		$respaldo['fecini']         = $this->input->post('fecha_inicio');
		$respaldo['fecter']         = $this->input->post('fecha_termino');
		$respaldo['desevento']      = $this->input->post('desc_evento');
		$respaldo['accrealiz']      = $this->input->post('acc_reali');
		$respaldo['obs']            = $this->input->post('observaciones');
		$respaldo['sucursales']     = $this->input->post('sucursal');
		$respaldo['nombre']         = $this->input->post('nombre');
		$respaldo['firma']          = $this->input->post('firma');
		$respaldo['email']          = $this->input->post('email');
		$respaldo['fono']           = $this->input->post('fono');
		
		$respaldo['nodos']          = $this->ticket->obtenerNodosAsociados2($this->input->post('ticket_id'));
		// Datos para el envio del correo
		$this->email->from('gde@entel.cl', 'Gestor de Eventos CMSC');
		$this->email->to($para);
		$this->email->cc($copia);
		$this->email->subject($asunto);
		
		// Se cargan los datos en el template
		$email = $this->load->view('v_templateEmailBoleta', $detalles, TRUE);
		$this->email->message($email);
		
		// Se envia correo a Cliente
		if ($this->email->send()){
		
			// Se resetea contador de Horas
			$this->ticket->uptadeRevision($this->input->post('ticket_id'));
			$this->saveEmail($respaldo);
			// Se registra la accion en base de datos
			$usuario = $this->session->userdata('username');
			$mensaje = "Correo: ".$this->input->post('ticket_asunto');
			
			if(!$this->ticket->comentaTicket($this->input->post('ticket_id'),$mensaje,"Envio",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			
			// $ticketID,$mensaje,$accion,$operador
			// $data['messageResult'] = 'Correo enviado exitosamente';
			// return $this->load->view('operador/v_ticket_mail_result', $data);
			echo 'OK';
			
		} else{
			// Se registra la accion en base de datos
			$usuario = $this->session->userdata('username');
			$mensaje = "Error: ".$this->email->my_print_debugger();
			
			if(!$this->ticket->comentaTicket($this->input->post('ticket_id'),$mensaje,"Error Envio Correo",$usuario)){
				// Registro de la no insercion del comentario
				log_message('error', "Error al Insertar datos en tabla: ticket_comentario");
			}
			// Debug del mail en archivo de logs
			$debug_mail = $this->email->my_print_debugger();
			log_message('error', $debug_mail);
			 
			// $data['messageResult'] = 'Favor intentelo nuevamente';
			// return $this->load->view('operador/v_ticket_mail_result', $data);
			echo 'ERROR';
		}
	}
	function saveEmail($postArray){
		$this->load->model('correo');
		$this->correo->registrarCorreos2($postArray);
	}
	function listadoClientesAsociados(){
		$usuarioActivo = $this->session->userdata('username');
		// $usuarioActivo = 'jaraneda';
		$this->load->model('cliente');
		echo json_encode($this->cliente->listarClientes($usuarioActivo, true));	// Se obtienen los IDs de clientes para el operador
	}
}
	

/* End of file c_ticket.php */
/* Location: ./application/controllers/c_ticket.php */