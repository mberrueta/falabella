<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Gestor de Eventos</title>
	<!--link rel="shortcut icon" href="<?php echo base_url(); ?>images/entelpcs.ico" /-->
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>css/layout.css" type="text/css" media="screen" /> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/gde_entel.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
	<div id="contenedor" class="width_3_quarter">
		<div class="logo_falabella">
			<a href="<?php echo base_url(); ?>" title="entel" target="_parent"></a>
		</div>
		<div id="form_login" class="caja_login" style="width: 350px;"> 
			<span>Gestor de Eventos</span>
			<!--<span style="color:#FF0000; font-weight:bold;">TRABAJANDO, FAVOR NO ACCEDER AL GESTOR</span>-->
			<div class="<?php echo $clase_mensaje; ?>">
				<p><?php echo $mensaje?></p>
			</div>
			<?php
				
				echo form_open('c_login/autenticar');
				echo '<table><tr><td>';
				echo form_label('Usuario', 'username');
				echo '</td><td>';
				echo form_input('username');
				echo '</td></tr><tr><td>';
				echo "<div class=\"error_login\">".form_error('username')."</div>";
				echo form_label('Contraseña', 'password');
				echo '</td><td>';
				echo form_password('password');
				echo "<div class=\"error_login\">".form_error('password')."</div>";
				echo '</td></tr><table>';
				echo form_submit('submit', 'Entrar');
				echo form_close();
			?>
			<p style="text-align:left; padding: 7px;"><br/><br /></p>
			
			<!--<p style="text-align:left; padding: 7px;">&copy; 2015 ENTEL S.A. Gestor de Eventos es un servicio creado por la Subgerencia de Monitoreo y Disponibilidad de Servicio.
			Todos los derechos son reservados.<br/><br />
			Para soporte comunicarse al teléfono móvil {56 9} 8198 9538.</p>-->
		</div>
	</div>

</body>
</html>