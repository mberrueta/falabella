<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_editar_detalle_cliente extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cliente');
		$this->load->model('operador');
		$this->load->helper('email');
	}
 
	public function index($nombre,$mensaje='') {
		$data2['men'] = $mensaje;
		if($this->cliente->verificar_cliente(urldecode($nombre)))
		{
			$data2['final'] 				= $this->obtener_operadores(urldecode($nombre),0);

			$data2['nombre']     = $nombre;
			$data2['titulo_pagina'] = 'GDE -> Mantenedor de Perfiles -> Editar Perfil';
			
			$data2['dir_usuario']  = 'admin';
			$data2['Organization'] = $this->cliente->listar_organization();
			$data2['Servicio']     = $this->cliente->listar_servicio();
			
			$this->load->view('supervisor/v_editar_detalle_cliente',$data2);
		}
		else{
			redirect('supervisor/c_crud_cliente','S|Cliente inexistente');
		}
	}
	
	
	/** Registra los nuevos operadores de un cliente, segun rango asignado
	*
	*/
	
	public function editar_asociar_cliente() {
		
		
		// Se realiza update de operadores a clientes
		if($this->input->post('operadores') == null OR $this->input->post('backup') == null){
			return $this->index($this->input->post('nombre'),'E|No puede dejar un Usuario sin asignación, favor verifique');
		}
		else{
			$update = $this->cliente->update_cliente($this->input->post('nombre'), $this->input->post('operadores'), $this->input->post('backup'));  
		}
		// update realizado exitosamente
		if ($update){ 
			return $this->index($this->input->post('nombre'),'S|'.$this->input->post('nombre').' editado exitosamente');
		}
		else{
			return $this->index($this->input->post('nombre'),'E|Problema al realizar la actualización, intente nuevamente.<br /> Si el problema persiste favor comunicar al Administrador');
		}
	}
	
	
	/** Lista los operadores asociados a un cliente, segun su rango
	*
	*/
	
	public function obtener_operadores($nombre,$flag) {
			$ddmenu						= array();
			$ddmenu['Organization']		= "";
			$ddmenu['Herramienta']		= "";
			
			
			$data2 = $this->cliente->listar_asociaciones_organizacion($nombre);
			$data3 = $this->cliente->listar_asociaciones_herramienta($nombre);
			
			foreach ($data2['cli']->result() as $row){
					$ddmenu['Organization']  .= $row->src_organization."<br />";
				}
			
			foreach ($data3['cli']->result() as $row){
				$ddmenu['Herramienta']  .= $row->src_tool."<br />";
			}

			
			if ($flag == 0) return $ddmenu;
			else { echo json_encode($ddmenu); }
	}
}