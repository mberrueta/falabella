<?php $this->load->view('admin/header'); ?>
<?php $this->load->view('admin/lateral'); ?>

		<section id="main" class="column" style="width:80%; height:600px;">
			<article class="module width_full" style="margin:auto">
				<div class="module_content" style="height:550px;">
					<h4 align="left">Historial de cambios en Nodos</h4>
					
					<div id="estado" align="left" style="float: left; height:400px; overflow:auto; width: 300px; margin-top: 50px;">
						<table>
						<thead >
							<tr><th style="width: 300px">Nodos</th></tr>
						</thead>
						<tbody id="tabla_nodos">
						</tbody>
						</table>
					</div>
					<div id="tabla" style="margin-left: 320px; margin-top: 58px; overflow:auto; height:400px">
						<label id="nodo" align="left" style="font-weight: bolder;"></label>
						<table>
						<thead >
							<tr>
								<th style="width: 200px">Fecha</th>
								<th style="width: 80px">Usuario</th>
								<th style="width: 80px">Accion</th>
								<th style="width: 600px">Detalle</th>
							</tr>
						</thead>
						<tbody id="detalle_nodo">
						</tbody>
						</table>
					</div>
				</div>
			</article>
			
		</section>
		<div style="clear:both;"></div>
	</div>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
				url		: "c_bitacora_nodos/listarNodos",
				dataType	: 'json',
				type		: 'GET',
				success	: function (results) {
					var tabla_nodos = "";
					for( var i=0; i<results.length; i++ ){
						tabla_nodos += "<tr><td >"+results[i]+"</td></tr>";
					}
					$('#tabla_nodos').html(tabla_nodos);
				},
				error: function (results) {
					console.log(results);
				}
			});
		});
		
		$('#tabla_nodos tr td').live("dblclick", function(evt){
			console.log($(this).text());
			$('#nodo').html("NODO: "+$(this).text());
			$.ajax({
				url		: "c_bitacora_nodos/getNodo",
				dataType	: 'json',
				data		: {'nodo':$(this).text()},
				type		: 'POST',
				success	: function (results) {
					console.log(results);
					var detalle_nodo = "";
					for( var i=0; i<results.length; i++ ){
						detalle_nodo += "<tr><td >"+results[i][0]+"</td><td >"+results[i][1]+"</td><td >"+results[i][2]+"</td><td ><p align=\"justify\">"+results[i][3]+"</p></td></tr>";
					}
					$('#detalle_nodo').html(detalle_nodo);
				},
				error: function (results) {
					console.log(results);
				}
			});
		});
		
	</script>
</body>
</html>
