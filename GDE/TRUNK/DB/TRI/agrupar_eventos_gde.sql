USE `gestor_eventos_rsp`;
--
-- Disparadores `evento`
--
CREATE TRIGGER `agrupar_eventos_gde` BEFORE INSERT ON `evento_gde`
 FOR EACH ROW BEGIN
	DECLARE identificador INT;
	DECLARE severidad varchar(20);
	DECLARE estado varchar(20);
	DECLARE tipo varchar(255);
	DECLARE fecha datetime;
	
	IF NEW.severity='OK' THEN
		SET identificador=(SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		IF identificador IS NOT NULL THEN
			SELECT `status`,`start_time`, `type` INTO estado, fecha, tipo FROM `grupo_evento_gde` WHERE `id`=identificador;
			
			SET NEW.group_id = identificador;
			UPDATE `grupo_evento_gde` SET `status` = 'Cerrado', `count` = `count`+1 WHERE `id`=identificador;
			INSERT INTO `comentario_gde` (`group_id`, `start_time`, `info`, `type`, `user`, `status`) VALUES (identificador, NEW.start_time, 'Alerta normalizada', NULL, 'gestor', 'Cerrado');
			
			IF estado='Activo' THEN
				INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
			ELSE
				SET fecha=(SELECT MAX(`start_time`) FROM `historia_gde` WHERE `group_id`=identificador);
				INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
			END IF;
		END IF;
	ELSE
		SET identificador=(SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		IF identificador IS NOT NULL THEN
			SELECT `status`,`start_time`, `type`, `severity` INTO estado, fecha, tipo, severidad FROM `grupo_evento_gde` WHERE `id`=identificador;
			
			IF NEW.severity=severidad THEN
				SET NEW.group_id = identificador;
				UPDATE `grupo_evento_gde` SET `count` = `count`+1 WHERE `id`=identificador; 
			ELSE
				SET NEW.group_id = identificador;
				UPDATE `grupo_evento_gde` SET `status` = 'Cerrado', `count` = `count`+1 WHERE `id`=identificador;
				INSERT INTO `comentario_gde` (`group_id`, `start_time`, `info`, `type`, `user`, `status`) VALUES (identificador, NEW.start_time, 'Alerta normalizada', NULL, 'gestor', 'Cerrado');
				
				IF estado='Activo' THEN
					INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
				ELSE
					SET fecha=(SELECT MAX(`start_time`) FROM `historia_gde` WHERE `group_id`=identificador);
					INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
				END IF;
				
				INSERT INTO `grupo_evento_gde` (`status`,`src_name`, `src_subname` ,`type`,`count`,`severity`,`start_time`,`body`,`src_ip`,src_organization, src_category,src_subcategory, src_tool, dest_bunit, dest_service,dest_country,src_system,folio_mda) VALUES (NEW.status, NEW.src_name, NEW.src_subname, NEW.type,1, NEW.severity, NEW.start_time,NEW.body,NEW.src_ip,NEW.src_organization, NEW.src_category, NEW.src_subcategory, NEW.src_tool, NEW.dest_bunit, NEW.dest_service,NEW.dest_country,NEW.src_system,0);
			
				SET NEW.group_id = (SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
			END IF;
		ELSE
			INSERT INTO `grupo_evento_gde` (`status`,`src_name`, `src_subname` ,`type`,`count`,`severity`,`start_time`,`body`,`src_ip`,src_organization, src_category,src_subcategory, src_tool, dest_bunit, dest_service,dest_country,src_system,folio_mda) VALUES (NEW.status, NEW.src_name, NEW.src_subname, NEW.type,1, NEW.severity, NEW.start_time,NEW.body,NEW.src_ip,NEW.src_organization, NEW.src_category, NEW.src_subcategory, NEW.src_tool, NEW.dest_bunit, NEW.dest_service,NEW.dest_country,NEW.src_system,0);
			
			SET NEW.group_id = (SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		END IF;
	END IF;
END