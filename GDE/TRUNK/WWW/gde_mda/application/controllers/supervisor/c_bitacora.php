<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_bitacora extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('supervisor');
		$this->load->model('evento');
	}
	
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$data['titulo_pagina'] = 'GDE -> Bitacora de eventos';
		
		$data['eventos_cliente']  = $this->supervisor->resumen_eventos_cliente($this->session->userdata('username'));
		$data['eventos_operador'] = $this->supervisor->resumen_eventos_operador($this->session->userdata('username'));
		
		
		$this->load->view('supervisor/v_bitacora', $data);
	}
	
	public function obtener_eventos(){
		echo $this->supervisor->resumen_listado_eventos($this->session->userdata('username'));
	}
	
	public function obtener_filtros(){

		$filtros = array();
		$filtros['fOperador']	= $this->evento->listar_operador_filtro($this->session->userdata('username'), false);

		echo json_encode($filtros);
	}
	
	public function obtener_historia(){
		$data['tablas'] = $this->supervisor->obtener_historia($this->input->post('idEvento'));
		echo $this->load->view('supervisor/v_popup_bitacora',$data,TRUE);
	}
	
	public function lookupOperador(){
		$listaOperadores = $this->supervisor->lookupOperador();
		echo $listaOperadores;
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 0){
			return true;
		}
		else{
			return false;
		}
	}
}