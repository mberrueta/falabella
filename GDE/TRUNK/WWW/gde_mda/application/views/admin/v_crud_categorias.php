<?php $this->load->view('/admin/header');?>
<?php $this->load->view('/admin/lateral');?>
	<section id="main" class="column">
		<article class="module width_full" style="width: 97% !important; border-bottom:0;">
			<header>
				<h3><?php echo $titulo_pagina; ?></h3>
			</header>
			<div id="principalMail" style="margin:30px 0; position:relative;">
				<div style="    position: absolute;right: 10px;top: 2px;width: 438px;">
					<label style="float: left;width: 80px;margin: 2px;padding: 0;" for="principalMail">Búsqueda:</label>
					<input type="text" id="search" style="float: left;margin-bottom: 30px;" name="search">
				</div>
				<button id="buttonNuevo" style="position:absolute;right:0;"></button>
			</div>
			<div style="    position: absolute;
							top: 181px;
							bottom: 0px;
							overflow-x: auto;
							right: 0;
							left: 250px; border:1px solid #ddd;">	
					<div id="static-header"  style="position:fixed;">
						<div data-header="enc" style="float:left;">Grupo Resolutor</div>
						<div data-header="pais" style="float:left;">País</div>		
						<div data-header="app" style="float:left;">Aplicación</div>
						<div data-header="det" style="float:left;">Detalle</div>
						<div data-header="editar" style="float:left;"></div>
						<div data-header="eliminar" style="float:left;"></div>
					</div>		
					<table id="registros" cellspacing="0" cellpadding="0" border="0" width="100%">
						<thead>
							<tr>
								<!-- <th><input type="checkbox" id="checkall" name="checkall" value=""></th>  -->
								<th id="enc">Grupo Resolutor</th>
								<th id="pais">País</th>
								<th id="app">Aplicación</th>
								<th id="det">Detalle</th>
								<th id="editar"></th><!-- Editar -->
								<th id="eliminar"></th><!-- Eliminar -->
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<!-- <td></td> -->
								<td></td>
								<td></td>
								<td></td>
								<!--<td></td>
								<td></td>
								<td></td>-->
								<td></td>
								<td></td>
								<td><!-- <button id="buttonDel"></button> --></td>

							</tr>
						</tfoot>

					</table>
					
				<!-- </div> --></div>
			
		</article>
	</section>

	<!-- Animacion de loading -->
	<div id="ajaxLoading">
		<img src="<?php echo base_url(); ?>images/ajax-loader.gif" alt="Loading Animation" />
		<span>Loading...</span>
	</div>

	<!-- message dialog box -->
	<div id="msgDialog"><p></p></div>
	
	<!-- Dialog Box de confirmacion para eliminacion -->
	<div id="delConfDialog" data-delete="" title="Confirmar">
		<p>¿Seguro que desea eliminar estos datos?</p>
	</div>

	<div id="crear_categoria" class="crud">
		<?php echo validation_errors(); ?> 
		<form  action="" method="post" id="datos_nuevo_patron_mail">
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>País:</td>
					<td><input type="text" id="pais" size="60" style="float: left;" name="pais"  required/></td>
					<td></td>
				</tr>
				<tr>
					<td>Encargado:</td>
					<td><input type="text" id="encargado" size="60" style="float: left;" name="encargado" required /></td>
					<td></td>
				</tr>
				<tr>
					<td>Aplicación:</td>
					<td><input type="text" id="app"  size="60" style="float: left;" name="app" required /></td>
					<td></td>
				</tr>
				<tr>
					<td>Detalle:</td>
					<td><input type="text" id="det" size="60" name="det" style="float: left;"/></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="actualizar_categoria">
		<form  action="" method="post" id="datos_actualizar_categoria">
		<input type="hidden" id="id" name="id"/>
		<table width="100%" border="0" cellspacing="2" cellpadding="2">
			<tr>
				<td>País:</td>
				<td><input type="text" id="pais" size="60" style="float: left;" name="pais"  required/></td>
				<td></td>
			</tr>
			<tr>
				<td>Encargado:</td>
				<td><input type="text" id="encargado" size="60" style="float: left;" name="encargado" required /></td>
				<td></td>
			</tr>
			<tr>
				<td>Aplicación:</td>
				<td><input type="text" id="app"  size="60" style="float: left;" name="app" required /></td>
				<td></td>
			</tr>
			<tr>
				<td>Detalle:</td>
				<td><input type="text" id="det" size="60" name="det" style="float: left;"/></td>
				<td></td>
			</tr>
		</table>
	</div>

	<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/crud_categorias.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
		});
	</script>
		<script type="text/javascript">
		$(function(){
			
		});
	</script>
</body>
</html>