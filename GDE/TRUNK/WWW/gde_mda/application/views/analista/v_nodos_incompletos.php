<?php $this->load->view("/analista/header");?>
<?php $this->load->view("/analista/lateral");?>

	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Nodos Incompletos</h3>
			</header>
			<div style="margin:15px;"> 
				<div id="tabs">
					<ul>
						<li><a href="#tabs-0">Nodos sin CODIGO DE SERVICIO</a></li>
						<li><a href="#tabs-1">Nodos sin CONTACTO</a></li>
						<li><a href="#tabs-2">Nodos sin CRITICIDAD</a></li>
					</ul>
					<div id="tabs-0">
						<iframe id="iframe-0" src='<?php echo $url1; ?>' frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-1">
						<iframe id="iframe-1" src='' frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-2">
						<iframe id="iframe-2" src='' frameborder="0" width="100%" height="600px"></iframe>
					</div>
				</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script>
	$(function() {
		var aIFrames = ['<?php echo $url1; ?>','<?php echo $url2; ?>','<?php echo $url3; ?>'];
		$( "#tabs" ).tabs({
			selected: 0,
			spinner: 'Retrieving data...',
			select: function(e, ui){
				var index=ui.index;
				$('iframe').attr('src','');
				$('#iframe-'+index).attr('src',aIFrames[index]);
			}
		});
	});
	</script>
</body>
</html>