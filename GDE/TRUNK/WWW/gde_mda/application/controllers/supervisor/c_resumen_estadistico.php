<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_resumen_estadistico extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$usuario = 'icastro';
		
		$data = array();
		$data['titulo_pagina'] = 'GDE -> Resumen estadistico';
		$data['supervisor'] = $this->session->userdata('username');
		$data['url1'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/sonda_Masivo_2";
		$data['url2'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_global?supervisor=".$this->session->userdata('username');
		$data['url3'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_operador?supervisor=".urlencode($this->session->userdata('username'));
		$data['url4'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_global?supervisor=*";
		
		$data['url5'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/masivo_DD?USUARIO=".$usuario;
		$data['url6'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/masivo_DE?USUARIO=".$usuario;
		$data['url7'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/masivo_DF?USUARIO=".$usuario;
		$data['url8'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_cumplimiento_sla?sup=".$this->session->userdata('username');
		$data['url9'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/gde_auditoria_tiempoRespuesta_tipoEvento";
		$data['url10'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/gde_auditoria_tiempoRespuesta_operador?supervisor=".$this->session->userdata('username');
		$data['url11'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/concentradores";
		$data['url12'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/monitoreo_salud_gde";
		$data['url13'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/fcabello_salud_nodos_general";

		$this->load->view('supervisor/v_resumen_estadistico', $data);

	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 0){
			return true;
		}
		else{
			return false;
		}
	}
}
