<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>GDE -> Bitacora -> Detalle</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/gde_entel.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/popup.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ie.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
</head>
<body>
	<div id="contenedor">
		<header>
			<img class="flotante" src="<?php echo base_url(); ?>images/logo_entel.png" />
			<div class="flotante" >
				<?php echo $tablas['tabla_info'];?>
			</div>
			<div style="clear:both;"></div>
		</header>
		<?php
			echo '<br /><br />';
			echo '<div class="tabla_presentacion">';
			echo $tablas['tabla_hist'];
			echo '</div>';
			echo '<br /><br />';
			echo '<div class="tabla_presentacion">';
			echo $tablas['tabla_sumz'];
			echo '</div>';
			// Acciones en tickets
			echo '<br /><br />';
			echo '<div class="tabla_presentacion">';
			echo $tablas['tabla_tick'];
			echo '</div>';
		?>
		<div style="clear:both;"></div>
	</div> 
</body>
</html>