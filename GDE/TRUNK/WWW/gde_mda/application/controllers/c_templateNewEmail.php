<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_templateNewEmail extends CI_Controller {

	public function loadTemplateEmail(){
	
		$data = array();
		$this->load->view('v_templateNewEmail', $data);
	}
}

/* End of file c_templateNewEmail.php */
/* Location: ./application/controllers/c_templateEmail.php */