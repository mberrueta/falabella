<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_boleta extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');
	}
 
	public function index() {
		
		$this->obtenerCorreoSeguimientos();
	}
	
	function loadView($output = null)
	{
		$output->dir_usuario   = 'supervisor';
		$output->titulo_pagina = 'GDE -> Mantenedor de Escalamiento de Boletas';
		$this->load->view('supervisor/v_crud_boleta',$output);
	}

	private function obtenerCorreoSeguimientos() {
	
		$crud = new grocery_CRUD();
		
		$crud->set_table('ticket_seguimiento');
		$crud->set_subject('Correo Seguimiento Boleta');
		$crud->required_fields('TICKET_TESP');
		$crud->columns('TICKET_DIS_CRIT','TICKET_CLI_CRIT','TICKET_TESP','TICKET_CORSEG');
		$crud->order_by('TICKET_DIS_CRIT','desc');
		$crud->order_by('TICKET_CLI_CRIT','desc');
		
		$crud->set_primary_key('TICKET_SEG_ID');
		
		$crud->display_as('TICKET_DIS_CRIT','Criticidad nodo');
		$crud->display_as('TICKET_CLI_CRIT','Criticidad cliente');
		$crud->display_as('TICKET_TESP','Tiempo espera (horas)');
		$crud->display_as('TICKET_CORSEG','Correo escalamiento');
		
		$crud->unset_print();
		$crud->unset_export();
		
		$crud->add_fields('TICKET_SEG_ID','TICKET_DIS_CRIT','TICKET_CLI_CRIT','TICKET_TESP','TICKET_CORSEG');
		$crud->edit_fields('TICKET_CORSEG');
		$crud->set_rules('TICKET_CORSEG', 'Correo escalamiento', 'callback_validateEmail');
		$crud->change_field_type('TICKET_SEG_ID','invisible');
		$crud->callback_before_insert(array($this,'insertID'));
		
		$crud->callback_add_field('TICKET_DIS_CRIT',array($this,'_disp_add'));
		$crud->callback_add_field('TICKET_CLI_CRIT',array($this,'_crit_add'));
		
		$crud->set_lang_string('insert_error', 'Error. Ya existe esta relacion de criticidad entre Nodo-Cliente');
		
		$output = $crud->render();
		
		$this->loadView($output);
	}
	
	public function validateEmail($arrayMail){
	
		$arrayMail = str_replace("\r\n", "\n", $arrayMail);
		if ($arrayMail != '') {
			$csv_array= explode(",",$arrayMail);
			$count = count($csv_array);
			for ($n=0;$n<$count;$n++){
				trim($csv_array[$n]);
				$emails = $csv_array[$n];
				if(!filter_var($emails, FILTER_VALIDATE_EMAIL)){
					$this->form_validation->set_message('validateEmail', 'Favor verificar los correos');
					return FALSE;
				}
			}
			return TRUE;
		}
		return TRUE;
	}
	
	// Generacion de ID para relacion de criticidades.
	function insertID($post_array) {
		
		$post_array['TICKET_SEG_ID'] = $post_array['TICKET_DIS_CRIT'] . $post_array['TICKET_CLI_CRIT'];
		return $post_array;
	}
	
	function _disp_add($value = '') {
		
		$this->load->model('ticket');
		$this->load->helper('form');
		
		$options = $this->ticket->getCriticidadDispo();
		return form_dropdown('TICKET_DIS_CRIT', $options, $value);
	}
	
	function _crit_add($value = '') {
		$this->load->model('ticket');
		$this->load->helper('form');
		
		$options = $this->ticket->getCriticidadClientes();
		return form_dropdown('TICKET_CLI_CRIT', $options, $value);
	}
	
}