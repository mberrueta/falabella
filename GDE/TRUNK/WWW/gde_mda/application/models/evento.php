<?php

class Evento extends CI_Model {
		function __construct(){
		parent::__construct();

	}
	/**
	*Lista los filtros para TIPO DE EVENTO con sus respectivos contadores
	*
	*@param String $usuario    Identificador del usuario que consulta
	*@param Bool   $isOperador Representa si el usuario que consulta es operador o supervisor
	*
	*@return String Html con los filtros solicitados
	*/
	function listar_operador_filtro($usuario, $isOperador = true){
		$sql = "SELECT USU_USER, SUP_USU_USER, OPE_NOM, OPE_APP FROM operador";
		$filtros = $this->db->query($sql);
		
		$html = '<table><tbody>';
		foreach($filtros->result() as $filtro){
			$html .= '<tr><td>';
			$html .= '<input class="filtro filtro_operador" type="checkbox" name="operador" ';
			$html .= 'value="'.$filtro->USU_USER.'"> '.$filtro->OPE_NOM.' '.$filtro->OPE_APP;
			$html .= '</td></tr>';
		}
		$html .= '</tbody></table>';
		
		return $html;
	}
	
	function insertaTicketManual($id, $folio){
		$asocia = array(
			'folio_mda' => $folio
		);
		$this->db->where('id', $id);
		$respuesta = $this->db->update('grupo_evento_gde', $asocia);
		return $respuesta;
	}
	
	/**
	* Funcionalidades solicitadas por Adessa.
	**/
	function obtener_eventos_vista_servidor($usuario){
		
		$this->db->select('src_organization');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_organization IS NOT NULL');
		$resultFiltro_1 = $this->db->get();
		$arrayFiltro_1 = array();
		foreach($resultFiltro_1->result() as $estado){
			array_push($arrayFiltro_1, $estado->src_organization);
		}
		
		$this->db->select('src_tool');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_tool IS NOT NULL');
		$resultFiltro_2 = $this->db->get();
		$arrayFiltro_2 = array();
		foreach($resultFiltro_2->result() as $estado){
			array_push($arrayFiltro_2, $estado->src_tool);
		}
		
		
		$this->load->library('Datatables');
		//$this->datatables->select('MIN(severity) as severity, src_name,src_ip,dest_country,dest_bunit,dest_service,src_organization,src_subcategory');
		$this->datatables->select('MIN(severity) as severity, src_name,src_ip,dest_country,dest_bunit, count(*) as count');
		$this->datatables->from('grupo_evento_gde');
		$this->datatables->group_by('src_name,src_ip');
		
		$v_estado = $this->input->post('v_estado');
		$v_pais = $this->input->post('v_pais');
		$v_negocio = $this->input->post('v_negocio');
		$v_servicio = $this->input->post('v_servicio');
		$v_organization = $this->input->post('v_organization');
		$v_tag = $this->input->post('v_tag');
		$v_search = $this->input->post('v_search');
		$v_criticidad = $this->input->post('v_criticidad');
		
		if($v_estado != ''){
			$arrayEstado = explode("@",$v_estado);
			$this->datatables->where_in('status',$arrayEstado);
		}
		
		if($v_pais != ''){
			$arrayPais = explode("@",$v_pais);
			$this->datatables->where_in('dest_country',$arrayPais);
		}
		
		if($v_negocio != ''){
			$arrayNegocio = explode("@",$v_negocio);
			$this->datatables->where_in('dest_bunit',$arrayNegocio);
		}
		
		if($v_servicio != ''){
			$servicio = "(";
			$arrayServicio = explode("@",$v_servicio);
			
			foreach ($arrayServicio as $key=>$value){
				if($key == (count($arrayServicio)-1)){
					$servicio .= "src_system LIKE '%".$value."%'";
				} else {
					$servicio .= "src_system LIKE '%".$value."%' OR ";
				}
			}
			$servicio .= ")";
			$this->datatables->where($servicio);
		}
		
		if($v_organization != ''){
			$arrayOrg = explode("@",$v_organization);
			$this->datatables->where_in('src_organization',$arrayOrg);
		} else {
			if(!in_array("All", $arrayFiltro_1)){
					$this->datatables->where_in('src_organization',$arrayFiltro_1);
				}
		}
		
		if($v_tag != ''){
			$arrayTag = explode("@",$v_tag);
			$this->datatables->where_in('src_subcategory',$arrayTag);
		}
		
		if($v_search != ''){
			$Custom = '%'.$v_search.'%';
			$value = '(type LIKE \''.$Custom.'\' OR src_name LIKE \''.$Custom.'\' OR src_ip LIKE \''.$Custom.'\' OR dest_country LIKE \''.$Custom.'\' OR dest_bunit LIKE \''.$Custom.'\' OR dest_service LIKE \''.$Custom.'\' OR src_organization LIKE \''.$Custom.'\' OR src_subcategory LIKE \''.$Custom.'\')';
			$this->datatables->where($value);
		}
		
		if($v_criticidad != '1'){
			$this->datatables->where('severity', $v_criticidad);
		}
		
		if(!in_array("All", $arrayFiltro_2)){
			$this->datatables->where_in('src_tool',$arrayFiltro_2);
		}
			
		//log_message('debug', 'Se ha presentado un error en la BD, codigo ' . print_r($this->datatables));
		
		$result = $this->datatables->generate(); 
		//log_message('debug', 'Se ha presentado un error en la BD, codigo ' . $result);
		$result = (object) array_merge( (array)json_decode($result), array( 'query' => $this->datatables->last_query() ) );

		echo json_encode($result);
		return;
	}
	
	function obtener_eventos_vista_pais_negocio($usuario){
		
		$this->db->select('src_organization');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_organization IS NOT NULL');
		$resultFiltro_1 = $this->db->get();
		$arrayFiltro_1 = array();
		foreach($resultFiltro_1->result() as $estado){
			array_push($arrayFiltro_1, $estado->src_organization);
		}
		
		$this->db->select('src_tool');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_tool IS NOT NULL');
		$resultFiltro_2 = $this->db->get();
		$arrayFiltro_2 = array();
		foreach($resultFiltro_2->result() as $estado){
			array_push($arrayFiltro_2, $estado->src_tool);
		}
		
		$this->load->library('Datatables');
		/*$this->datatables->select('min(grupo_evento_gde.severity) as severity,grupo_evento_gde.dest_country,agrupacion_sistema_gde.id,agrupacion_sistema_gde.value');
		$this->datatables->from('grupo_evento_gde');
		$this->datatables->join('agrupacion_sistema_gde','grupo_evento_gde.dest_bunit = agrupacion_sistema_gde.id AND grupo_evento_gde.dest_country = agrupacion_sistema_gde.dest_country AND grupo_evento_gde.status = agrupacion_sistema_gde.status AND grupo_evento_gde.dest_service = agrupacion_sistema_gde.dest_service AND grupo_evento_gde.src_organization = agrupacion_sistema_gde.src_organization AND grupo_evento_gde.src_subcategory = agrupacion_sistema_gde.src_subcategory','INNER');
		$this->datatables->where('grupo_evento_gde.src_system IS NOT NULL');
		$this->datatables->group_by('agrupacion_sistema_gde.id,grupo_evento_gde.dest_country,agrupacion_sistema_gde.value');*/
		
		$this->datatables->select('min(severity) as severity');
		$this->datatables->select('dest_country');
		$this->datatables->select('dest_bunit');
		$this->datatables->select('TRIM("\n" FROM substring_index(substring_index(src_system, "|", n),"|",-1)) as src_system', FALSE);
		$this->datatables->select('count(*) as count');
		$this->datatables->from('grupo_evento_gde');
		$this->datatables->join('numbers','char_length(src_system) - char_length(replace(src_system, \'|\', \'\')) >= n');
		$this->datatables->where('grupo_evento_gde.src_system IS NOT NULL');
		$this->datatables->group_by('dest_country,');
		$this->datatables->group_by('dest_bunit');
		$this->datatables->group_by('src_system', FALSE);
		$this->datatables->group_by('TRIM("\n" FROM substring_index(substring_index(src_system, "|", n),"|",-1))');
		
		$v_estado = $this->input->post('v_estado');
		$v_pais = $this->input->post('v_pais');
		$v_negocio = $this->input->post('v_negocio');
		$v_servicio = $this->input->post('v_servicio');
		$v_organization = $this->input->post('v_organization');
		$v_tag = $this->input->post('v_tag');
		$v_search = $this->input->post('v_search');
		$v_criticidad = $this->input->post('v_criticidad');
		
		if($v_estado != ''){
			$arrayEstado = explode("@",$v_estado);
			$this->datatables->where_in('grupo_evento_gde.status',$arrayEstado);
		}
		
		if($v_pais != ''){
			$arrayPais = explode("@",$v_pais);
			$this->datatables->where_in('grupo_evento_gde.dest_country',$arrayPais);
		}
		
		if($v_negocio != ''){
			$arrayNegocio = explode("@",$v_negocio);
			$this->datatables->where_in('grupo_evento_gde.dest_bunit',$arrayNegocio);
		}
		
		if($v_servicio != ''){
			$servicio = "(";
			$arrayServicio = explode("@",$v_servicio);
			
			foreach ($arrayServicio as $key=>$value){
				if($key == (count($arrayServicio)-1)){
					$servicio .= "grupo_evento_gde.src_system LIKE '%".$value."%'";
				} else {
					$servicio .= "grupo_evento_gde.src_system LIKE '%".$value."%' OR ";
				}
			}
			$servicio .= ")";
			$this->datatables->where($servicio);
		}
		
		if($v_organization != ''){
			$arrayOrg = explode("@",$v_organization);
			$this->datatables->where_in('grupo_evento_gde.src_organization',$arrayOrg);
		} else {
			if(!in_array("All", $arrayFiltro_1)){
					$this->datatables->where_in('grupo_evento_gde.src_organization',$arrayFiltro_1);
				}
		}
		
		if($v_tag != ''){
			$arrayTag = explode("@",$v_tag);
			$this->datatables->where_in('grupo_evento_gde.src_subcategory',$arrayTag);
		}
		
		if($v_search != ''){
			$Custom = '%'.$v_search.'%';
			// $value = '(grupo_evento_gde.type LIKE \''.$Custom.'\' OR grupo_evento_gde.src_name LIKE \''.$Custom.'\' OR grupo_evento_gde.src_ip LIKE \''.$Custom.'\' OR grupo_evento_gde.dest_country LIKE \''.$Custom.'\' OR grupo_evento_gde.dest_bunit LIKE \''.$Custom.'\' OR grupo_evento_gde.dest_service LIKE \''.$Custom.'\' OR grupo_evento_gde.src_organization LIKE \''.$Custom.'\' OR grupo_evento_gde.src_subcategory LIKE \''.$Custom.'\')';
			$value = '(grupo_evento_gde.dest_country LIKE \''.$Custom.'\' OR grupo_evento_gde.dest_bunit LIKE \''.$Custom.'\' OR grupo_evento_gde.src_system LIKE \''.$Custom.'\')';
			$this->datatables->where($value);
		}
		
		if($v_criticidad != '1'){
			$this->datatables->where('grupo_evento_gde.severity', $v_criticidad);
		}
		
		if(!in_array("All", $arrayFiltro_2)){
			$this->datatables->where_in('src_tool',$arrayFiltro_2);
		}
		
		//print_r($this->datatables);
		
		$result = $this->datatables->generate(); 
		
		//log_message('error', 'Se ha presentado un error en la BD, codigo ' . print_r($this->datatables->last_query()));
		//log_message('debug', 'Se ha presentado un error en la BD, codigo ' . $result);
		$result = (object) array_merge( (array)json_decode($result), array( 'query' => $this->datatables->last_query() ) );

		echo json_encode($result);
		return;
	}
	
	function obtener_eventos_hijos($usuario){
		
		// Filtros
		$this->db->select('src_organization');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_organization IS NOT NULL');
		$resultFiltro_1 = $this->db->get();
		$arrayFiltro_1 = array();
		foreach($resultFiltro_1->result() as $estado){
			array_push($arrayFiltro_1, $estado->src_organization);
		}
		
		$this->db->select('src_tool');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_tool IS NOT NULL');
		$resultFiltro_2 = $this->db->get();
		$arrayFiltro_2 = array();
		foreach($resultFiltro_2->result() as $estado){
			array_push($arrayFiltro_2, $estado->src_tool);
		}
		
		$nombre    = $this->input->post('nombre');
		$ip        = $this->input->post('ip');
		$pais      = $this->input->post('pais');
		$negocio   = $this->input->post('negocio');
		//$servicio  = $this->input->post('servicio');
		//$organiza  = $this->input->post('organiza');
		//$tag       = $this->input->post('tag');
		
		$sistema   = $this->input->post('sistema');
		//$aplica    = $this->input->post('aplica');
		
		if($sistema != ''){
			$value = '(g.dest_country = \''.$pais.'\' AND g.dest_bunit LIKE \'%'.$negocio.'%\' AND g.src_system LIKE \'%'.$sistema.'%\')';
		} else {
			//$value = '(g.src_name = \''.$nombre.'\' AND g.src_ip = \''.$ip.'\' AND g.dest_country = \''.$pais.'\' AND g.dest_bunit = \''.$negocio.'\' AND g.dest_service = \''.$servicio.'\' AND g.src_organization = \''.$organiza.'\' AND g.src_subcategory = \''.$tag.'\')';
			$value = '(g.src_name = \''.$nombre.'\' AND g.src_ip = \''.$ip.'\')';
		}
		
				// Se genera array con clientes que puede consultar el operador
		/*$this->db->select('dest_country');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('status',1);
		$resultPais = $this->db->get();
		$arrayPais = array();
		foreach($resultPais->result() as $estado){
			array_push($arrayPais, $estado->dest_country);
		}*/
		
		// Se genera array con estados
		$resultEstado = $this->db->get('estado_gde');
		$arrayEstado = array();
		foreach($resultEstado->result() as $estado){
			if( $estado->status != '' )
				array_push($arrayEstado, $estado->status);
		}

		// Se genera el array con los correos de seguimiento activos
		/*$this->db->select('group_id');
		$this->db->from('correo_seguimiento_gde');
		$this->db->where('status',1);
		$resultCorreosSeguimiento = $this->db->get();
		$arrayCorreosSeguimiento = array();
		foreach( $resultCorreosSeguimiento->result() as $correoSeguimiento ){
			$arrayCorreosSeguimiento[ $correoSeguimiento->group_id ] = '<img src="' . base_url() . 'images/seg.png">';
		}*/
		
		$this->load->library('Datatables');

			// $this->datatables->select('g.id as ID');
			// $this->datatables->select('g.severity as SEVERITY');
			// $this->datatables->select('correo_seguimiento_gde.group_id as EMAIL');
			// $this->datatables->select('g.status as STATUS');
			// $this->datatables->select('g.id as REMEDY');
			// $this->datatables->select('g.start_time as FECHA');
			// $this->datatables->select('g.type as TYPE');
			// $this->datatables->select('g.src_name as SRC_NAME');
			// $this->datatables->select('g.count as COUNT');
			// $this->datatables->select('g.src_ip as IP');
			// $this->datatables->select('g.src_organization as SRC_ORGANIZATION');
			// $this->datatables->select('g.src_subcategory as TAG');
			// $this->datatables->select('g.src_category as SRC_CATEGORY');
			// $this->datatables->select('g.dest_service as DEST_SERVICE');
			// $this->datatables->select('g.dest_bunit as DEST_BUNIT');
			// $this->datatables->select('g.dest_country AS DEST_COUNTRY');
			// $this->datatables->select('g.src_tool as TOOL');
			
			// // Nuevo
			// $this->datatables->where($value);
			
			// $this->datatables->from('grupo_evento_gde g');
			
			// $this->datatables->join('correo_seguimiento_gde','g.id=correo_seguimiento_gde.group_id','LEFT OUTER');


			$this->datatables->select('g.id as ID');
			$this->datatables->select('g.severity as SEVERITY');
			$this->datatables->select('g.send_mail as EMAIL');
			$this->datatables->select('g.status as STATUS');
			$this->datatables->select('g.id as REMEDY');
			$this->datatables->select('g.start_time as FECHA');
			$this->datatables->select('g.type as TYPE');
			$this->datatables->select('g.src_name as SRC_NAME');
			$this->datatables->select('g.count as COUNT');
			$this->datatables->select('g.src_ip as IP');
			$this->datatables->select('g.src_organization as SRC_ORGANIZATION');
			$this->datatables->select('g.src_subcategory as TAG');
			$this->datatables->select('g.src_category as SRC_CATEGORY');
			$this->datatables->select('g.src_system as DEST_SERVICE');
			$this->datatables->select('g.dest_bunit as DEST_BUNIT');
			$this->datatables->select('g.dest_country AS DEST_COUNTRY');
			$this->datatables->select('g.src_tool as TOOL');
			
			// Nuevo
			$this->datatables->where($value);
			
			$this->datatables->from('grupo_evento_gde g');
			
			$estado    = $this->input->post('estado');
			if($estado != ''){
				$arrayEstado = explode("@",$estado);
				$this->datatables->where_in('g.status',$arrayEstado);
			}
			
			// Descomentar para filtro por PAIS
			/*$queryPais = $this->input->post('sSearch_14');
			if($queryPais == ''){
				$this->datatables->where_in('g.dest_country',$arrayPais);
			}*/
			
			if(!in_array("All", $arrayFiltro_1)){
				$this->datatables->where_in('g.src_organization',$arrayFiltro_1);
			}
			
			if(!in_array("All", $arrayFiltro_2)){
				$this->datatables->where_in('g.src_tool',$arrayFiltro_2);
			}
			
			$this->datatables->edit_column('ID','<input type="checkbox" name="$1" value="$1" class="check" />','ID');
			//$this->datatables->edit_column('LOCK_CONT','<img src="'.base_url().'images/s$1.png" />','LOCK_CONT');
			//$this->datatables->edit_column('EMAIL',$arrayCorreosSeguimiento,'EMAIL');
			$this->datatables->edit_column('EMAIL','<img src="'.base_url().'images/seg$1.png" />','EMAIL');
			$this->datatables->edit_column('REMEDY',' ','REMEDY');
			
			$result = $this->datatables->generate(); 
			$result = (object) array_merge( (array)json_decode($result), array( 'query' => $this->datatables->last_query() ) );
			
			

		if( $this->datatables->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de listar filtros para el perador ' . $usuario);
			log_message('error', $this->datatables->_error_number());
			log_message('error', $this->datatables->_error_message());
			log_message('error', $this->datatables->last_query());
			
			$errorMsg = 'Se ha presentado un problema, favor intentar nuevamente. Si el problema persiste informar a Administrador';
			$result = '{"sEcho":0,"iTotalRecords":1,"iTotalDisplayRecords":1,"aaData":[["---","---","---","---","---","'.$errorMsg.'","---","---","---","---","---","---","---","---","---","---","---","---","---","---",]],"sColumns":"g.id,g.severity,correo_nodo_gde.id,g.status,g.id,g.start_time,g.type,g.src_name,g.count,g.src_ip,LOCK_CONT,g.src_organization,g.dest_service,g.dest_bunit,g.dest_country,g.src_category,g.info"}';
			echo $result;
			return;
		}
		
		echo json_encode($result);
		return;
		
	}
	
	/**
	*Lista los eventos que se le deben desplegar al usuario en particular
	*
	*@param String $usuario Identificador del usuario que consulta por sus eventos
	*
	*@return String json con los eventos a ser desplegados en el plugin datatables, más las opciones necesarias para filtrado del plugin
	*/
	function obtener_eventos($usuario){
		
		
		// Filtros
		$this->db->select('src_organization');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_organization IS NOT NULL');
		$resultFiltro_1 = $this->db->get();
		$arrayFiltro_1 = array();
		foreach($resultFiltro_1->result() as $estado){
			array_push($arrayFiltro_1, $estado->src_organization);
		}
		
		$this->db->select('src_tool');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_tool IS NOT NULL');
		$resultFiltro_2 = $this->db->get();
		$arrayFiltro_2 = array();
		foreach($resultFiltro_2->result() as $estado){
			array_push($arrayFiltro_2, $estado->src_tool);
		}
		
		// Se genera array con estados
		$resultEstado = $this->db->get('estado_gde');
		$arrayEstado = array();
		foreach($resultEstado->result() as $estado){
			if( $estado->status != '' )
				array_push($arrayEstado, $estado->status);
		}

		// Se genera el array con los correos de seguimiento activos
		/*$this->db->select('group_id');
		$this->db->from('correo_seguimiento_gde');
		$this->db->where('status',1);
		$resultCorreosSeguimiento = $this->db->get();
		$arrayCorreosSeguimiento = array();
		foreach( $resultCorreosSeguimiento->result() as $correoSeguimiento ){
			$arrayCorreosSeguimiento[ $correoSeguimiento->group_id ] = '<img src="' . base_url() . 'images/seg.png">';
		}*/
		
		//print_r ($arrayCorreosSeguimiento);
		
		$this->load->library('Datatables');

			// $this->datatables->select('g.id as ID');
			// $this->datatables->select('g.severity as SEVERITY');
			// $this->datatables->select('correo_seguimiento_gde.group_id as EMAIL');
			// $this->datatables->select('g.status as STATUS');
			// $this->datatables->select('g.id as REMEDY');
			// $this->datatables->select('g.start_time as FECHA');
			// $this->datatables->select('g.type as TYPE');
			// $this->datatables->select('g.src_name as SRC_NAME');
			// $this->datatables->select('g.count as COUNT');
			// $this->datatables->select('g.src_ip as IP');
			// $this->datatables->select('g.src_organization as SRC_ORGANIZATION');
			// $this->datatables->select('g.src_subcategory as TAG');
			// $this->datatables->select('g.src_category as SRC_CATEGORY');
			// $this->datatables->select('g.dest_service as DEST_SERVICE');
			// $this->datatables->select('g.dest_bunit as DEST_BUNIT');
			// $this->datatables->select('g.dest_country AS DEST_COUNTRY');
			// $this->datatables->select('g.src_tool as TOOL');
			
			// $this->datatables->from('grupo_evento_gde g');
			
			// $this->datatables->join('correo_seguimiento_gde','g.id=correo_seguimiento_gde.group_id','LEFT OUTER');
			$this->datatables->select('g.id as ID');
			$this->datatables->select('g.severity as SEVERITY');
			$this->datatables->select('g.send_mail as EMAIL');
			$this->datatables->select('g.status as STATUS');
			$this->datatables->select("CASE WHEN g.folio_mda = -1 THEN 'ERROR' ELSE CASE WHEN g.folio_mda = 0 THEN 'Sin Ticket' ELSE g.folio_mda END END as REMEDY",false);
			// $this->datatables->select('g.id as REMEDY');
			$this->datatables->select('g.start_time as FECHA');
			$this->datatables->select('g.type as TYPE');
			$this->datatables->select('g.src_name as SRC_NAME');
			$this->datatables->select('g.count as COUNT');
			$this->datatables->select('g.src_ip as IP');
			$this->datatables->select('g.src_organization as SRC_ORGANIZATION');
			$this->datatables->select('g.src_subcategory as TAG');
			$this->datatables->select('g.src_category as SRC_CATEGORY');
			$this->datatables->select('g.src_system as DEST_SERVICE');
			$this->datatables->select('g.dest_bunit as DEST_BUNIT');
			$this->datatables->select('g.dest_country AS DEST_COUNTRY');
			$this->datatables->select('g.src_tool as TOOL');
			
			$this->datatables->from('grupo_evento_gde g');
			/*$queryEstado = $this->input->post('sSearch_3');
			if($queryEstado == ''){
				$this->datatables->where_in('g.status',$arrayEstado);
			}*/
			
			$v_estado = $this->input->post('v_estado');
			$v_pais = $this->input->post('v_pais');
			$v_negocio = $this->input->post('v_negocio');
			$v_servicio = $this->input->post('v_servicio');
			$v_organization = $this->input->post('v_organization');
			$v_tag = $this->input->post('v_tag');
			$v_search = $this->input->post('v_search');
			$v_criticidad = $this->input->post('v_criticidad');
			
			if($v_estado != ''){
				$arrayEstado = explode("@",$v_estado);
				$this->datatables->where_in('g.status',$arrayEstado);
			}
			
			if($v_pais != ''){
				$arrayPais = explode("@",$v_pais);
				$this->datatables->where_in('g.dest_country',$arrayPais);
			}
			
			if($v_negocio != ''){
				$arrayNegocio = explode("@",$v_negocio);
				$this->datatables->where_in('g.dest_bunit',$arrayNegocio);
			}
			
			if($v_servicio != ''){
				$servicio = "(";
				$arrayServicio = explode("@",$v_servicio);
				
				foreach ($arrayServicio as $key=>$value){
					if($key == (count($arrayServicio)-1)){
						$servicio .= "g.src_system LIKE '%".$value."%'";
					} else {
						$servicio .= "g.src_system LIKE '%".$value."%' OR ";
					}
				}
				$servicio .= ")";
				$this->datatables->where($servicio);
			}
			
			if($v_organization != ''){
				$arrayOrg = explode("@",$v_organization);
				$this->datatables->where_in('g.src_organization',$arrayOrg);
			}
			
			if($v_tag != ''){
				$arrayTag = explode("@",$v_tag);
				$this->datatables->where_in('g.src_subcategory',$arrayTag);
			}
			
			if($v_search != ''){
				$Custom = '%'.$v_search.'%';
				$value = '(g.type LIKE \''.$Custom.'\' OR g.src_name LIKE \''.$Custom.'\' OR g.src_ip LIKE \''.$Custom.'\' OR g.dest_country LIKE \''.$Custom.'\' OR g.dest_bunit LIKE \''.$Custom.'\' OR g.dest_service LIKE \''.$Custom.'\' OR g.src_organization LIKE \''.$Custom.'\' OR g.src_subcategory LIKE \''.$Custom.'\')';
				$this->datatables->where($value);
			}
			
			if($v_criticidad != '1'){
				$this->datatables->where('g.severity', $v_criticidad);
			}
			
			
			// Descomentar para filtro por PAIS
			/*$queryPais = $this->input->post('sSearch_14');
			if($queryPais == ''){
				$this->datatables->where_in('g.dest_country',$arrayPais);
			}*/
			
			// FILTRO USUARIO
			if($v_organization == ''){
				if(!in_array("All", $arrayFiltro_1)){
					$this->datatables->where_in('g.src_organization',$arrayFiltro_1);
				}
			}
			
			if(!in_array("All", $arrayFiltro_2)){
				$this->datatables->where_in('g.src_tool',$arrayFiltro_2);
			}
			
			$this->datatables->edit_column('ID','<input type="checkbox" name="$1" value="$1" class="check" />','ID');
			//$this->datatables->edit_column('LOCK_CONT','<img src="'.base_url().'images/s$1.png" />','LOCK_CONT');
			$this->datatables->edit_column('EMAIL','<img src="'.base_url().'images/seg$1.png" />','EMAIL');
			// $this->datatables->edit_column('REMEDY',' ','REMEDY');
			
			$result = $this->datatables->generate(); 
			$result = (object) array_merge( (array)json_decode($result), array( 'query' => $this->datatables->last_query() ) );
			
			

		if( $this->datatables->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de listar filtros para el perador ' . $usuario);
			log_message('error', $this->datatables->_error_number());
			log_message('error', $this->datatables->_error_message());
			log_message('error', $this->datatables->last_query());
			
			$errorMsg = 'Se ha presentado un problema, favor intentar nuevamente. Si el problema persiste informar a Administrador';
			$result = '{"sEcho":0,"iTotalRecords":1,"iTotalDisplayRecords":1,"aaData":[["---","---","---","---","---","'.$errorMsg.'","---","---","---","---","---","---","---","---","---","---","---","---","---","---",]],"sColumns":"g.id,g.severity,correo_nodo_gde.id,g.status,g.id,g.start_time,g.type,g.src_name,g.count,g.src_ip,LOCK_CONT,g.src_organization,g.dest_service,g.dest_bunit,g.dest_country,g.src_category,g.info"}';
			echo $result;
			return;
		}
		//echo $result;
		
		echo json_encode($result);
		return;
	}

	
	
	function obtener_eventos_export($usuario){
		
		// Filtros
		$this->db->select('src_organization');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_organization IS NOT NULL');
		$resultFiltro_1 = $this->db->get();
		$arrayFiltro_1 = array();
		foreach($resultFiltro_1->result() as $estado){
			array_push($arrayFiltro_1, $estado->src_organization);
		}
		
		$this->db->select('src_tool');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('src_tool IS NOT NULL');
		$resultFiltro_2 = $this->db->get();
		$arrayFiltro_2 = array();
		foreach($resultFiltro_2->result() as $estado){
			array_push($arrayFiltro_2, $estado->src_tool);
		}
		
		$this->db->select('g.severity as SEVERITY');
		$this->db->select('g.status as STATUS');
		$this->db->select('g.folio_mda as FOLIO');
		$this->db->select('g.send_mail as CORREO');
		$this->db->select('g.start_time as FECHA');
		$this->db->select('g.type as TYPE');
		$this->db->select('g.src_name as SRC_NAME');
		$this->db->select('g.count as COUNT');
		$this->db->select('g.src_ip as IP');
		$this->db->select('g.src_organization as SRC_ORGANIZATION');
		$this->db->select('g.src_subcategory as TAG');
		$this->db->select('g.src_category as SRC_CATEGORY');
		$this->db->select('g.src_system as DEST_SERVICE');
		$this->db->select('g.dest_bunit as DEST_BUNIT');
		$this->db->select('g.dest_country AS DEST_COUNTRY');
		$this->db->select('g.src_tool as TOOL');
		
		$this->db->from('grupo_evento_gde g');
		
		
		$v_estado = $this->input->post('v_estado');
		$v_pais = $this->input->post('v_pais');
		$v_negocio = $this->input->post('v_negocio');
		$v_servicio = $this->input->post('v_servicio');
		$v_organization = $this->input->post('v_organization');
		$v_tag = $this->input->post('v_tag');
		$v_search = $this->input->post('v_search');
		$v_criticidad = $this->input->post('v_criticidad');
			
		if($v_estado != ''){
			$arrayEstado = explode("@",$v_estado);
			$this->db->where_in('g.status',$arrayEstado);
		}
		
		if($v_pais != ''){
			$arrayPais = explode("@",$v_pais);
			$this->db->where_in('g.dest_country',$arrayPais);
		}
		
		if($v_negocio != ''){
			$arrayNegocio = explode("@",$v_negocio);
			$this->db->where_in('g.dest_bunit',$arrayNegocio);
		}
		
		if($v_servicio != ''){
			$servicio = "(";
			$arrayServicio = explode("@",$v_servicio);
			
			foreach ($arrayServicio as $key=>$value){
				if($key == (count($arrayServicio)-1)){
					$servicio .= "g.src_system LIKE '%".$value."%'";
				} else {
					$servicio .= "g.src_system LIKE '%".$value."%' OR ";
				}
			}
			$servicio .= ")";
			$this->db->where($servicio);
		}
		
		if($v_organization != ''){
			$arrayOrg = explode("@",$v_organization);
			$this->db->where_in('g.src_organization',$arrayOrg);
		} else {
			if(!in_array("All", $arrayFiltro_1)){
				$this->datatables->where_in('g.src_organization',$arrayFiltro_1);
			}
		}
		
		if($v_tag != ''){
			$arrayTag = explode("@",$v_tag);
			$this->db->where_in('g.src_subcategory',$arrayTag);
		}
		
		if($v_search != ''){
			$Custom = '%'.$v_search.'%';
			$value = '(g.src_name LIKE \''.$Custom.'\' OR g.src_ip LIKE \''.$Custom.'\' OR g.dest_country LIKE \''.$Custom.'\' OR g.dest_bunit LIKE \''.$Custom.'\' OR g.dest_service LIKE \''.$Custom.'\' OR g.src_organization LIKE \''.$Custom.'\' OR g.src_subcategory LIKE \''.$Custom.'\')';
			$this->db->where($value);
		}
		
		if($v_criticidad != '1'){
			$this->db->where('g.severity', $v_criticidad);
		}
		
		if(!in_array("All", $arrayFiltro_2)){
			$this->datatables->where_in('g.src_tool',$arrayFiltro_2);
		}
		
		$fields = array( 'SEVERITY', 'STATUS', 'FOLIO', 'CORREO', 'FECHA', 'TYPE', 'SRC_NAME', 'COUNT', 'IP', 'SRC_ORGANIZATION', 'TAG', 'SRC_CATEGORY', 'DEST_SERVICE', 'DEST_BUNIT', 'DEST_COUNTRY', 'TOOL' );
		$result = $this->db->get();
		$this->db->flush_cache();
		
		return array("fields" => $fields, "query" => $result);
		
		
		
		/*$html = "";
		foreach($result->result() as $row){
			$html .= "<tr>";
			$html .= "<td></td><td>".$row->SEVERITY."</td><td></td><td>".$row->STATUS."</td><td></td><td>".$row->FECHA."</td><td>".$row->TYPE."</td><td>".$row->SRC_NAME."</td><td>".$row->COUNT."</td><td>".$row->IP."</td><td>".$row->SRC_ORGANIZATION."</td><td>".$row->TAG."</td><td>".$row->SRC_CATEGORY."</td><td>".$row->DEST_SERVICE."</td><td>".$row->DEST_BUNIT."</td><td>".$row->DEST_COUNTRY."</td><td>".$row->TOOL."</td>";
			$html .= "</tr>";
		}
		return "<thead><tr><th></th><th>SEVERIDAD</th><th>CORREO</th><th>ESTADO</th><th>FOLIO MDA</th><th>FECHA</th><th>TIPO ALERTA</th><th>NOMBRE</th><th>Ac.</th><th>IP</th><th>AREA SOPORTE</th><th>UBICACION DEL ELEMENTO</th><th>DESCRIPCION DEL ELEMENTO</th><th>SERVICIO</th><th>NEGOCIO</th><th>PAIS</th><th>HERRAMIENTA</th></tr></thead><tbody>".$html."</tbody>";*/
	}
	
	/**Entrega el detalle de los eventos seleccionados
	* @param idsGrupos Array $_POST ids de los eventos a consultar
	*
	* @return String detalle (mensaje) de los eventos seleccionados
	*/
	function detalle_eventos($idsGrupos){
		
		//$this->db->select('src_name');
		$this->db->select('body');
		$this->db->from('evento_gde');
		$this->db->where_in('group_id',explode(',' ,$idsGrupos));
		$result = $this->db->get();
		$this->db->flush_cache();
		
		$html = "";
		
		// Se genera string con detalle de eventos solicitados
		foreach($result->result() as $row){
			$html .= "Detalle = ".$row->body."\n";
		}
		return $html;
	}
	
	function getEstadoGrupoEvento($id){
		
		//$this->db->select('src_name');
		$this->db->select('status');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id = '.$id);
		$result = $this->db->get();
		return $result->row()->status;
	}
	
	/**Entrega el detalle de los eventos seleccionados
	* @param idsGrupos Array $_POST ids de los eventos a consultar
	*
	* @return String detalle (mensaje) de los eventos seleccionados
	*/
	function detalle_eventos2($idsGrupos){
		
		$this->db->select('TEV_NOM');
		$this->db->select('EVE_MEN');
		$this->db->from('evento');
		$this->db->where_in('GEV_ID',explode(',' ,$idsGrupos));
		$result = $this->db->get();
		$this->db->flush_cache();
		
		$html = "";
		
		// Se genera string con detalle de eventos solicitados
		foreach($result->result() as $row){
			$html .= $row->TEV_NOM." ".$row->EVE_MEN."\n";
		}
		return $html;
	}
	
	/**
	*Cambia el estado de los grupos de eventos seleccionados
	*
	*@param String $gruposId        Contiene los ids de grupos seleccionados
	*@param String $siguienteEstado Indica el estado al cual cambiaran los grupos de eventos
	*@param String $comentario      Comentario ingresado por Operador al cambio de estado
	*@param String $operadorId      Identificador del Operador
	*
	*@return String Mensaje sobre el resultado del cambio de estado
	*/
	function cambiar_estado($gruposId, $siguienteEstado, $comentario, $operadorId){
		// $this->db->trans_start();
			$this->db->select('DISTINCT(status) as status');
			$this->db->from('grupo_evento_gde');
			$this->db->where_in('id', explode(',',$gruposId));
			$result = $this->db->get();
			
			$this->db->flush_cache();
			
			// Parte de los eventos seleccionados ya fueron gestionados
			if($result->num_rows() != 1){ 
				return '0|Los estados de los eventos seleccionados difieren'; 
			}
			else{
				
				// Se obtiene el estado actual en el que se encuentran los eventos consultados
				$actualEstado = $result->row()->status;
				
				//log_message('error', 'Error en ejecucion de query filtro '.$actualEstado);
				
				// Se valida que la gestion sobre los eventos seleccionados se puede realizar
				if(( ($siguienteEstado == 'Omitido' || $siguienteEstado == 'En Proceso' || $siguienteEstado == 'Deshabilitar') && $actualEstado != 'Activo') || ( ($siguienteEstado == 'Cerrado' || $siguienteEstado == 'Boleta') && ($actualEstado != 'En Proceso' && $actualEstado != 'Deshabilitar'))){
					
					return '0|Los eventos seleccionados, o parte de ellos, ya han sido gestionados'; 
				}
				
				// Se realiza el cambio de estado de los eventos
				$this->db->where_in('id', explode(',',$gruposId));
				$this->db->update('grupo_evento_gde', array('status' => $siguienteEstado));
				$this->db->flush_cache();
				
				// Se cierra LOCK de los eventos a gestionar y que son movidos a Omitido o Cerrado
				if($siguienteEstado == 'Omitido' || $siguienteEstado == 'Cerrado'){ 
				
					foreach(explode(',',$gruposId) as $gev){
						$this->db->select('status_lock');
						$this->db->from('grupo_evento_gde');
						$this->db->where('id', $gev);
						$sem = $this->db->get();
					
						$this->db->where('id',$gev);
						//$this->db->where_in('TEV_NOM', array('DOWN', 'IFDOWN', 'VPN_DOWN', 'UP', 'IFUP', 'VPN_UP'));
						$this->db->update('grupo_evento_gde', array('sem' => '1'));  
						$this->db->flush_cache();
					}
				
					$this->db->where_in('id', explode(',',$gruposId));
					//$this->db->where_in('TEV_NOM', array('DOWN', 'IFDOWN', 'VPN_DOWN', 'UP', 'IFUP', 'VPN_UP'));
					$this->db->update('grupo_evento_gde', array('status' => $siguienteEstado, 'status_lock' => '1'));
					$this->db->flush_cache();
					// Se cierra lock
				}

				$buffArray = array();              //Insercion en la tabla historia
				//$buffArray_update = array(); //Array para realizar update de comentario en tabla grupo_evento
				$buffArrau_coment = array();
				
				$dateTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"),date("d"),date("Y")));
				
				
				foreach(explode(',',$gruposId) as $grupoId){
					if(is_numeric($grupoId)){
						
						
						// Se recupera la información necesaria para completar la auditoria
						if( strtolower($actualEstado) == 'activo' ){
							$this->db->select('type as EVENTO');
							$this->db->select('start_time as FECHA');
							$this->db->from('grupo_evento_gde');
							$this->db->where('id', $grupoId);
							$buff_InfoResultHistoria = $this->db->get();
						}
						else{
							$this->db->select('type as EVENTO');
							$this->db->select('start_time as FECHA');
							$this->db->from('historia_gde');
							$this->db->where('group_id', $grupoId);
							$this->db->order_by('start_time', 'DESC');
							$buff_InfoResultHistoria = $this->db->get();
						}
						
						/*log_message('error', 'Error en ejecucion de query filtro '.$grupoId);
						log_message('error', 'Error en ejecucion de query filtro '.$actualEstado);
						log_message('error', 'Error en ejecucion de query filtro '.$siguienteEstado);
						log_message('error', 'Error en ejecucion de query filtro '.$dateTime);
						log_message('error', 'Error en ejecucion de query filtro '.$operadorId);
						log_message('error', 'Error en ejecucion de query filtro '.$buff_InfoResultHistoria->row()->FECHA);
						log_message('error', 'Error en ejecucion de query filtro '.$buff_InfoResultHistoria->row()->EVENTO);*/
						
						/*echo ($grupoId."\n");
						echo ($actualEstado."\n");
						echo ($siguienteEstado."\n");
						echo ($dateTime."\n");
						echo ($operadorId."\n");
						echo ($buff_InfoResultHistoria->row()->FECHA."\n");
						echo ($buff_InfoResultHistoria->row()->EVENTO."\n");*/

						// Se agrega registro a la tabla historia
						array_push($buffArray, array('group_id' => $grupoId, 'start_status' => $actualEstado, 'end_status' => $siguienteEstado, 'start_time' => $dateTime, 'user' => $operadorId, 'access_time' => $buff_InfoResultHistoria->row()->FECHA, 'type' =>  $buff_InfoResultHistoria->row()->EVENTO));
						
						if($comentario != ''){
							//Agrega comentario a tabla comentario
							array_push($buffArrau_coment, array('group_id' => $grupoId, 'start_time' => $dateTime, 'info' => $comentario, 'status' => $siguienteEstado, 'user' => $operadorId));
							//Agrega a comentario de tabla grupo_evento
							//array_push($buffArray_update, array( 'id' => $grupoId, 'info' => $comentario));
						}
						
						// Se agrega comentario automatico de cambio de estado en eventos
						array_push($buffArrau_coment, array('group_id' => $grupoId, 'start_time' => $dateTime, 'info' => 'Movido a estado "'.$siguienteEstado.'"', 'status' => $siguienteEstado, 'user' => $operadorId));
						//array_push($buffArray_update, array( 'id' => $grupoId, 'info' => 'Movido a estado "'.$siguienteEstado.'"'));
					}
				}
				
				
				
				$this->db->flush_cache();
				$this->db->insert_batch('historia_gde',$buffArray);
				$this->db->flush_cache();
				$this->db->insert_batch('comentario_gde',$buffArrau_coment);              //Registra comentario en tabla comentario
				$this->db->flush_cache();
				
				//$this->db->update_batch('grupo_evento_gde', $buffArray_update, 'id'); // ES REEMPLAZADO POR BUG EN VERSION DE FRAMEWORK
				// foreach( $buffArray_update as $rowUpdate ){
					// $this->db->where('GEV_ID', $rowUpdate['GEV_ID']);
					// $this->db->update('grupo_evento', array('GEV_COM' => $rowUpdate['GEV_COM']));
					// $this->db->flush_cache();
				// }
				
			}
		// $this->db->trans_complete();
		// if ($this->db->trans_status() === FALSE){
			// return '0|Se ha presentado un problema en el sistema, favor intentar nuevamente.<br />Si el problema persiste comunicar al administrador';
		// }
		// else{
			return '1|Cambio de estado realizado exitosamente';
		// }
	}
	
	/***************** DETALLES ********************/
	
	/**
	*Entrega la historia del nodo solicitado
	*@param String $nodo    Nombre del nodo por el cual se consulta
	*
	*@return String Historia de eventos del nodo consultado
	*/
	function obtenerHistoria($nodo, $nombre){
		$this->db->select('start_time');
		//$this->db->select('src_name');
		$this->db->select('severity');
		$this->db->select('body');
		$this->db->select('src_ip');
		$this->db->from('evento_gde');
		$this->db->where('type',$nodo);
		$this->db->where('src_name',$nombre);
		$this->db->where('start_time >',date("Y-m-d H:i:s", mktime(date("H"), date("i"), 0, date("m"),date("d")-7,date("Y"))));
		$this->db->order_by('start_time','desc');
		//$this->db->order_by('src_name','desc');
		// $this->db->limit(100);
		$result = $this->db->get();
		
		$html = "";
		foreach($result->result() as $row){
			$html .= "<tr>";
			$html .= "<td><strong>Fecha</strong> = ".$row->start_time."</br><strong>IP</strong> = ".$row->src_ip."</br><strong>Severidad</strong> = ".$row->severity."</br><strong>Detalle</strong> = ".$row->body."</td>";
			$html .= "</tr>";
		}
		return "<table cellspacing=\"5\" border=\"1\"><tbody>".$html."</tbody></table>";
	}
	
	/**
	*Entrega los eventos sumarizados del nodo solicitado
	*
	*@param String $nodo    Nombre del nodo por el cual se consulta
	*
	*@return String Historia de eventos del nodo consultado
	*/
	function obtenerSumarizados($grupoId){
		$this->db->select('start_time');
		$this->db->select('severity');
		$this->db->select('body');
		$this->db->select('src_ip');
		$this->db->from('evento_gde');
		$this->db->where('group_id',$grupoId);
		$this->db->order_by('start_time','desc');
		//$this->db->order_by('src_name','desc');
		$result = $this->db->get();
		
		$html = "";
		foreach($result->result() as $row){
			$html .= "<tr>";
			$html .= "<td><strong>Fecha</strong> = ".$row->start_time."</br><strong>IP</strong> = ".$row->src_ip."</br><strong>Severidad</strong> = ".$row->severity."</br><strong>Detalle</strong> = ".$row->body."</td>";
			//$html .= "<td>".$row->src_name."</td>";
			//$html .= "<td>".$row->src_ip."</td>"</br>IP ;
			//$html .= "<td>".$row->body."</td>";
			$html .= "</tr>";
		}
		return "<table cellspacing=\"5\" border=\"1\"><tbody>".$html."</tbody></table>";
	}

	function obtenerMensajeCorreo($grupoId){
		$this->db->select('body');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id',$grupoId);
		//$this->db->order_by('src_name','desc');
		$result = $this->db->get();
		
		$value = "";
		foreach($result->result() as $row){
			$value .= $row->body;
		}
		return $value;
	}
	
	//TODO: comentar
	function obtenerComentarios($grupoId){
		$this->db->select('start_time');
		$this->db->select('user');
		$this->db->select('info');
		$this->db->from('comentario_gde');
		$this->db->where('group_id',$grupoId);
		$this->db->order_by('start_time','desc');
		$result = $this->db->get();
		$html = "";
		foreach($result->result() as $row){
			$html .= "<tr>";
			$html .= "<td><strong>Fecha</strong> = ".$row->start_time."</br><strong>Usuario</strong> = ".$row->user."</br><strong>Mensaje</strong> = ".$row->info."</td>";
			$html .= "</tr>";
		}
		return "<table cellspacing=\"5\" border=\"1\"><tbody>".$html."</tbody></table>";
	}
	
	//TODO: comentar
	function registrarComentario($grupoId, $coment, $usuario){
		$this->db->trans_start();
			$this->db->select('status');
			$this->db->from('grupo_evento_gde');
			$this->db->where('id',$grupoId);
			$result = $this->db->get();
			$actualEstado = $result->row()->status;
			$this->db->flush_cache();
			$dateTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"),date("d"),date("Y")));
			
			$this->db->insert('comentario_gde', array('group_id' => $grupoId, 'start_time' => $dateTime, 'info' => $coment, 'status' => $actualEstado, 'user' => $usuario));
			$this->db->flush_cache();
			
			// [28-12-2016] Se elimina por no ser necesario
			//$this->db->update('grupo_evento_gde', array('info' => $coment), array('id' => $grupoId));
			//$this->db->flush_cache();
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return '0|Se ha presentado un problema en el sistema, favor intentar nuevamente.<br />Si el problema persiste comunicar al administrador';
		}
		else{
			return '1|Cambio de estado realizado exitosamente';
		}
	}
	/******* Datos para Inegracion Remedy *********/
	function codServicio($listanodos){
		$sql = "SELECT DIS_CODSERV, DIS_CODADMIN FROM dispositivo WHERE DIS_NOM IN (".$listanodos.")";
		$result = $this->db->query($sql);
		$codigos = array();
		foreach($result->result() as $row){
			array_push($codigos, $row->DIS_CODSERV);
			array_push($codigos, $row->DIS_CODADMIN);
		}
		return $codigos;
	}
	
	function codServ($listanodos){
		$sql = "SELECT DIS_CODSERV FROM dispositivo WHERE DIS_NOM IN (".$listanodos.")";
		$result = $this->db->query($sql);
		$codigos = array();
		foreach($result->result() as $row){
			array_push($codigos, $row->DIS_CODSERV);
		}
		$codigos = array_unique($codigos);
		$cods = '';
		$cods = implode(',', $codigos);
		return $cods;
	}
	
	function Email()
	{
		$user = $this->session->userdata('username');
		
		$this->db->select('USU_EMAIL');
		$this->db->from('usuario');
		$this->db->where('USU_USER', $user);
		$result = $this->db->get();
		
		foreach($result->result() as $row){
			return $row->USU_EMAIL;
		}
	}
	
	function cliId($listaclientes){
		$sql = "SELECT CLI_ID FROM cliente WHERE CLI_NOM IN (".$listaclientes.")";
		$result = $this->db->query($sql);
		foreach($result->result() as $row){
			return $row->CLI_ID;
		}
	}
	
	function validaCodServ($codserv){
		$sql = "SELECT * FROM ticket WHERE TICKET_COD_SERV = ".$codserv;
		$result = $this->db->query($sql);
		
		return $result->num_rows();
	}
	
	/***************** PRUEBAS ********************/
	
	
	/**
	*Cambia el estado de los grupos de eventos seleccionados
	*
	*@param String $nodo    Nombre del nodo al cual se le realizará la prueba
	*@param String $cliente Cliente al cual pertenece el nodo
	*
	*@return String Mensaje sobre el resultado del cambio de estado
	*/
	function prueba_pivote($cmd, $nodo, $cliente){
		
		if (!preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/',$nodo) || $nodo == '0.0.0.0'){
			$html = '<tr>';
			$html .= '<td>La IP del nodo ('.$nodo.') no se ha especificado correctamente<br />Favor comunicar al Administrador.</td>';
			$html .= '</tr>';
			return '<table>'.$html.'</table>';
		}
		
		$this->db->select('CLI_CONCIP');
		$this->db->from('cliente');
		$this->db->where('CLI_NOM',$cliente);
		$result = $this->db->get();
		
		$command = "/usr/bin/python ".dirname(__FILE__).DIRECTORY_SEPARATOR."telnet_pivote.py '".$cmd."' '".$result->row()->CLI_CONCIP."' '".$nodo."'";
		#$command .= " 2>&1";
		#$output = system($command);
		$output = shell_exec($command);

		$html  = '';
		$table = preg_split( '/\r\n|\r|\n/',$output);
		foreach($table as $tr){
			$html .= '<tr>';
			$html .= '<td>'.$tr.'</td>';
			$html .= '</tr>';
		}
		return '<table>'.$html.'</table>';
	}
	
	
	/******************* EXTRAS *********************/
	
	
	/**
	*Exporta los eventos solicitados entre las fechas especificadas, considerando solo aquellos eventos que continuan con "Bolsa Abierta"
	*
	*@param String $t_inicio   Fecha de inicio para la consulta
	*@param String $t_fin       Fecha de fin para la consulta
	*
	*@return String resultado en formato csv
	*/
	function exportar($t_fin, $t_inicio){
		
		$result = $this->db->query("SELECT max(evento.EVE_TS) as FECHA_HORA, cliente.CLI_NOM as CLIENTE, evento.DIS_NOM as NODO, dispositivo.DIS_CRIT as CRITICIDAD, evento.DIS_IP as IP, dispositivo.DIS_CODSERV as CODIGO_SERVICIO, geografia_dispositivo.GEOD_SUCURSAL as SUCURSAL, geografia_dispositivo.GEOD_COMUNA as COMUNA , geografia_dispositivo.GEOD_PROVINCIA as PROVINCIA, geografia_dispositivo.GEOD_REGION as REGION
		FROM evento
		LEFT OUTER JOIN geografia_dispositivo ON evento.DIS_NOM= geografia_dispositivo.DIS_NOM
		LEFT OUTER JOIN dispositivo ON evento.DIS_NOM= dispositivo.DIS_NOM
		JOIN cliente ON evento.CLI_ID=cliente.CLI_ID
		WHERE GEV_ID=ANY(
		  SELECT GEV_ID FROM grupo_evento WHERE GEV_TSINI>='".$t_inicio."' AND GEV_TSINI<='".$t_fin."' AND GEV_LOCK=0
		)
		AND TEV_NOM='DOWN' 
		GROUP BY evento.DIS_NOM
		ORDER BY evento.EVE_TS ASC");
		
		$this->load->dbutil();
		
		$delimiter = ";";
        $newline = "\n";
        return $this->dbutil->csv_from_result($result, $delimiter, $newline);

	}
	
	
	/**
	*Tabla que se despliega a operadores para indicar la cantidad de nodos afectados por cliente
	*
	*@return String html con la tabla formados
	*/
	function tabla_nodos_caidos(){
		$result = $this->db->query("SELECT MASIVO.CLIENTE, MASIVO.NODO as DISP, MASIVO.SUCURSAL AS SUC, TDISP, TSUC , 
ROUND((MASIVO.NODO*100)/ TDISP, 2) as PDISP,  ROUND((MASIVO.SUCURSAL*100)/TSUC, 2) as PSUC
FROM (
	SELECT cliente.CLI_NOM AS CLIENTE, 
	count( DISTINCT case when geografia_dispositivo.GEOD_SUCURSAL != '' OR geografia_dispositivo.GEOD_SUCURSAL IS NULL then geografia_dispositivo.GEOD_SUCURSAL else null end ) AS SUCURSAL, 
	count( grupo_evento.DIS_NOM ) AS NODO
	FROM grupo_evento
	JOIN cliente ON grupo_evento.CLI_ID = cliente.CLI_ID
	LEFT OUTER JOIN geografia_dispositivo ON grupo_evento.DIS_NOM = geografia_dispositivo.DIS_NOM
	WHERE (
	grupo_evento.EST_NOM = 'Activo'
	OR grupo_evento.EST_NOM = 'En Proceso'
	)
	AND grupo_evento.TEV_NOM = 'Down'
	AND grupo_evento.GEV_LOCK =0
	AND grupo_evento.CLI_ID !=1
	GROUP BY cliente.CLI_NOM
) AS MASIVO
LEFT OUTER JOIN (
	SELECT cliente.CLI_NOM as CLIENTE, count(dispositivo.DIS_NOM) as TDISP, IFNULL(T.TSUC, 0) as TSUC 
	FROM dispositivo 
	JOIN cliente ON dispositivo.CLI_ID = cliente.CLI_ID 
	LEFT OUTER JOIN (
	  select CLI_NOM, count(distinct GEOD_SUCURSAL) as TSUC 
	  from dispositivo 
	  join geografia_dispositivo using(DIS_NOM) 
	  join cliente using(CLI_ID) 
	  where GEOD_SUCURSAL IS NOT NULL 
	  AND GEOD_SUCURSAL!='' 
	  group by CLI_NOM) AS T 
	ON T.CLI_NOM  = cliente.CLI_NOM 
	GROUP BY cliente.CLI_NOM
) as T ON MASIVO.CLIENTE = T.CLIENTE
GROUP BY MASIVO.CLIENTE ORDER BY `MASIVO`.`CLIENTE` ASC");
		
		$html = "<thead><tr><th>Cliente</th><th>N° Equipos</th><th>% Equipos</th><th>N° Sucursales</th><th>% Sucursales</th></tr></thead>";
		$html .= "<tbody>";
		
		foreach($result -> result() as $row){
			$html .= "<tr>";
			$html .= "<td>".$row->CLIENTE."</td>";
			$html .= "<td>".$row->DISP."</td>";
			$html .= "<td>".$row->PDISP."</td>";
			$html .= "<td>".$row->SUC."</td>";
			$html .= "<td>".$row->PSUC."</td>";
			$html .= "</tr>";
		}
		
		$html .= "</tbody>";
		return "<table id=\"caida\" >".$html."</table>";
	}
}
?>
