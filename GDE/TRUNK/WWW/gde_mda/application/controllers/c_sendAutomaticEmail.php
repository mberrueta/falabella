<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// class SendMailThread extends Thread {
// 	public function run($mail, $body){
		
// 		//llamado a la libreria email		
// 		$this->load->library('email');
		
// 		$this->email->set_newline("\r\n");
// 		$this->email->from("hugo.gallardo@opentek.cl", "Hugo Gallardo");
// 		$this->email->to(explode(';',$mail->to_mail));	
// 		$this->email->cc(explode(';',$mail->cc_mail));
// 		$this->email->subject("Asunto por definir");
// 		$this->email->message($this->load->view('v_templateNewEmail', $body, TRUE));
// 		$send = $this->email->send();
// 	}
// }


class C_sendAutomaticEmail extends CI_Controller {
	
	
	public function index(){}

	//Cambia el estado de un grupo de eventos de activo En proceso
	private function changeStatusGroup($mail){
		$this->load->model('evento');
		$this->evento->cambiar_estado($mail->id, "En Proceso", "Cambio de estado automatico por envío de correo realizado", "gestor");
		//print_r("Entre a cambiar a ".$mail->id);
	}

	//Retorna una lista de registros que deben ser enviados por mails segun los criterios dados
	private function getMails(){

		//Array que contendra los mails validos
		$mails = array();
		
		//Query que obtiene todos los registros necesarios
		$this->db->select('grupo_evento_gde.*');
		$this->db->select('contacto_dispositivo_gde.*');
		$this->db->from('grupo_evento_gde');
		$this->db->join('contacto_dispositivo_gde', 'grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization');
		$this->db->join('correo_seguimiento_gde', 'grupo_evento_gde.id = correo_seguimiento_gde.group_id','left');
		$this->db->where('correo_seguimiento_gde.id IS NULL', null, false);
		// $this->db->where_in('grupo_evento_gde.severity', array('CRITICAL', 'MINOR', 'MAYOR'));
		$this->db->where('contacto_dispositivo_gde.automatic', 1);
		$this->db->where('grupo_evento_gde.start_time >= NOW() - INTERVAL 2 MINUTE', NULL, FALSE);
		$result = $this->db->get();
		
		//Valida que concuerde la severidad con el flag de envio de mails
		if($result->num_rows > 0)
		foreach($result->result() as $row )
				array_push($mails,$row);

		if(count($mails) == 0) return false;
		return $mails;

	}

	//Función que retorna el mail del soporte
	private function getMailSupport(){
		$this->db->query("valor");
		$this->db->from('parametros');
		$this->db->where('`parametro`','mail_mesa_ayuda');
		$result = $this->db->get();

		return $result->result()[0]->valor;
	}

	private function createCommentGroup($mail, $intentos, $confirmacion){

		$data = array(
			'group_id'   => $mail->id,
			'start_time' => date("Y-m-d H:i:s"),
			'info'       => $confirmacion ? "Se envió mail automatico." : "ERROR: Mail automatico no pudo ser enviado tras ".$intentos." intentos.",
			'type'       => NULL,
			'user'       => 'GDE',
			'status'     => $mail->status
		);
		$this->db->Insert('comentario_gde', $data);

	}

	private function saveSendMail($mail, $intentos, $confirmacion){
		
		//Almacena los datos de envios
		$data = array(
			'start_time'     => date("Y-m-d H:i:s"),
			'end_time'       => date("Y-m-d H:i:s"),
			'seg_start_time' => date("Y-m-d H:i:s"),
			'seg_end_time'   => date("Y-m-d H:i:s"),
			'count'          => $intentos,
			'status'         => $confirmacion ? 1 : 0,
			'info'           => $confirmacion ? "Se envió mail automaticamente con los siguientes parametros: \n Nro. intentos: ".$intentos : "Correo automatico no pudo ser enviado tras ".$intentos." intentos.",
			'type'           => 'automatic',
			'group_id'       => $mail->id
		);

		$this->db->Insert('correo_seguimiento_gde', $data);

		$this->createCommentGroup($mail, $intentos, $confirmacion);

		if($confirmacion){
			
			if($mail->status == "Activo"){
				$this->load->model('evento');
				$this->evento->cambiar_estado($mail->id, "En Proceso", "Cambio de estado automatico por envío de correo realizado", "gestor");
			}
		}
		
		$this->db->where('id',$mail->id);
		$this->db->update('grupo_evento_gde', array('send_mail' => 0));
	}

	//Función recursiva, envios y reintentos de envios de mails.
	private function send($mail, $body){
		
		//Indica la cantidad de reintentos
		$cont = 0;
		
		// echo $mail->critical_mayor; # 1
		// echo $mail->severity;       # MINOR
		// echo $mail->minor;          # 1
		
		if(($mail->critical_mayor == 1 && ($mail->severity == "CRITICAL" OR $mail->severity == "MAJOR")) or ($mail->minor == 1 && $mail->severity == "MINOR")){
			$mailSupport = ";".$this->getMailSupport();
		} else {
			$mailSupport = "";
		}

		//Indica si el mail fue enviado finalmente
		$send_bool = TRUE;
		
		//llamado a la libreria email		
		$this->load->library('email');

		//Set de variables de envio de mails
		$this->email->set_newline("\r\n");
		$this->email->from("cdge@Falabella.cl", "Gestion de Eventos");
		$this->email->to(explode(';',$mail->to_mail.$mailSupport));	
		// $this->email->to(explode(';',$mail->to_mail));	
		$this->email->cc(explode(';',$mail->cc_mail));
		$this->email->subject("Notificación de Incidente - ".$mail->src_name." - ".$mail->type);
		
		if($body['ticket']){
			$this->email->message($this->load->view('v_templateNewEmailFolio', $body, TRUE));
		} else {
			$this->email->message($this->load->view('v_templateNewEmail', $body, TRUE));
		}
		
		
		// echo "CORREO DEBUG";
		// print_r ($mail->to_mail.$mailSupport);
		// echo "--------";
		// echo $send_bool;
		// echo "--------";
		// print_r ($body);
		
		//Envia el mail
		$send = $this->email->send();

		//En caso de problemas al enviar el mail, realiza otros dos reintentos
		if(!$send){
			// Primer reintento
			$send = $this->email->send();
			$cont++;
			// print_r($this->email->print_debugger());

			if(!$send){
				// Segundo reintento
				$send = $this->email->send();
				$cont++;
				// print_r($this->email->print_debugger());

				// Si al segundo reintento no se pudo mandar se indica al flag que no se puede mandar el mail
				if(!$send) $send_bool = FALSE;
			}
		} 

		//Almacena los resultados en la tabla correspondiente
		$this->saveSendMail($mail,$cont,$send_bool);
		
	}

	//Funcioon principal, envio de mail
	public function sendEmail(){

		//Llama los datos a enviar
		$array_mails = $this->getMails();

		echo $array_mails ? date("Y-m-d H:i:s")." : Enviando ".count($array_mails)." Mails \n" : date("Y-m-d H:i:s")." : Sin Mails que enviar \n" ;
		//Bucle envio de mails
		if($array_mails)
			foreach($array_mails as $mail){
				
				//asrray del cuerpo del mensaje
				//$body['mensaje_mda']  = "Señores MDA, favor generar folio y contactar telefonicamente al turno de área responsable, ya que degrada servicio.";
				
				$mail_mensaje_mda = '';
				$mail_severidad   = $mail->severity;
				$mail_servicios   = $mail->src_system;
				$mail_area        = $mail->src_organization;
				
				if($mail_severidad == "CRITICAL"){
					if(preg_match('/No catalogado/i', $mail_servicios)) {
						$mail_mensaje_mda = "Señores MDA, favor generar folio y derivar ticket a: ".$mail_area;
					} else {
						$mail_mensaje_mda = "Señores MDA, favor generar folio y contactar telefónicamente a: ".$mail_area.", ya que degrada a los servicios: ".str_replace("|","",$mail_servicios);
					}
				} else if ($mail_severidad == "MAJOR"){
					$mail_mensaje_mda = "Señores MDA, favor generar folio y derivar ticket a: ".$mail_area;
				} else {
					$mail_mensaje_mda = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
				}
				
				
				$body['mensaje_mda']  = $mail_mensaje_mda;
				$body['mensaje']      = $mail->body;
				$body['severidad']    = $mail->severity;
				$body['pais']      	  = $mail->dest_country;
				$body['negocio']   	  = $mail->dest_bunit;
				$body['servicio']     = $mail->src_system;
				$body['area']         = $mail->src_organization;
				$body['tipo_disp']    = $mail->src_category;
				$body['ubicacion']    = $mail->src_subcategory;
				$body['ip'] 	      = $mail->src_ip;
				$body['nombre']       = $mail->src_name;
				$body['fecha'] 	      = $mail->start_time;
				$body['fechactual']   = $mail->start_time;
				$body['obs'] 	      = "Correo enviado automáticamente, desde Gestor de Eventos Corporativo";
				// $body['to_mail']      = $body['to_mail'].";".$mailSupport;
				
				$this->send($mail,$body,0);
			}
	}
	
	// Adaptaciones por integracion con MDA
	
	private function getInfo($ID)
	{
		//Query que obtiene todos los registros necesarios
		$this->db->select('grupo_evento_gde.*');
		$this->db->select('contacto_dispositivo_gde.*');
		$this->db->from('grupo_evento_gde');
		$this->db->join('contacto_dispositivo_gde', 'grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization');
		$this->db->where('grupo_evento_gde.id = '.$ID);
		$result = $this->db->get();
		return $result;
	}
	
	public function sendEmailbyID($ID){
		
		$response = $this->getInfo($ID);
		$mail     = $response->row();
		
		$mail_mensaje_mda = '';
		$mail_severidad   = $mail->severity;
		$mail_servicios   = $mail->src_system;
		$mail_area        = $mail->src_organization;
		
		$folio = '';
		if($mail->folio_mda == -1){
			$folio = "ERROR";
		} else {
			$folio = $mail->folio_mda;
		}
		
		if($mail_severidad == "CRITICAL"){
			if(preg_match('/Redes/i', $mail_area)) {
				if(preg_match('/Site/i', $mail->src_subcategory)) {
					$mail_mensaje_mda = "FAVOR CONTACTAR TELEFONICAMENTE AL AREA RESOLUTORA Y GESTION DE INCIDENTES \n";
					$mail_mensaje_mda .= "Señores ".$mail_area.", favor revisar la siguiente notificación.";
				} else {
					$mail_mensaje_mda = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
				}
			} else {
				$mail_mensaje_mda = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
			}
		} else {
			$mail_mensaje_mda = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
		}
		// } else if ($mail_severidad == "MAJOR"){
			// $mail_mensaje_mda = "Señores MDA, favor generar folio y derivar ticket a: ".$mail_area;
		// } else {
			// $mail_mensaje_mda = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
		// }
		
		$body['mensaje_mda']  = "Señores ".$mail_area.", favor revisar la siguiente notificación.";
		$body['mensaje']      = $mail->body;
		$body['ticket']       = $folio;
		$body['severidad']    = $mail->severity;
		$body['pais']      	  = $mail->dest_country;
		$body['negocio']   	  = $mail->dest_bunit;
		$body['servicio']     = $mail->src_system;
		$body['area']         = $mail->src_organization;
		$body['tipo_disp']    = $mail->src_category;
		$body['ubicacion']    = $mail->src_subcategory;
		$body['ip'] 	      = $mail->src_ip;
		$body['nombre']       = $mail->src_name;
		$body['fecha'] 	      = $mail->start_time;
		$body['fechactual']   = $mail->start_time;
		$body['obs'] 	      = "Correo enviado automáticamente, desde Gestor de Eventos Corporativo";
		
		$this->send($mail,$body,0);
	}
	
}


/* End of file c_sendEmail.php */
/* Location: ./application/controllers/c_sendNewEmail.php */
