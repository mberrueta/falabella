<body>
	
	<div id="contenedor" class="width_full">
	<header id="header">
		<hgroup>
			<h1 class="site_title">
			</h1>
			<h2 class="section_title">
			Gestor de Eventos ENTEL
			<span><?php echo anchor('analista/c_mantenedor','Menú Analista'); ?></span>
			</h2>
			<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
		</hgroup>
	</header>
	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $this->session->userdata('nombre'); ?></p>
		</div>
	</section>
	
	<aside id="sidebar" class="column">
		<h3>ADMINISTRACIÓN</h3>
		<ul class="toggle">
				<li><?php echo anchor('analista/c_mantenedor','Mantenedor de Nodos'); ?></li>
				<li><?php echo anchor('analista/c_nodos_incompletos','Nodos Incompletos'); ?></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>2015 &copy; Kudaw</strong></p>
		</footer>
	</aside>
