var buffCliente;
var pathname = window.location.pathname.split( '/' );
var hostname = window.location.hostname;
var protocol    = window.location.protocol;

$(document).ready(function(){

/*	--------------------------------------------------
:: Movimiento de clientes
-------------------------------------------------- */

	// Agrega el o los elementos seleccionados a la lista de operador reemplazo
	// clientes seleccionados de izquierda a derecha
	$('#add').live('click',function(e) {
		e.preventDefault();
		return !$('#cliPrincipal option:selected').remove().appendTo('#cliReemplazo');  
	 });
	 
	 // Agrega el o los elementos seleccionados a la lista de operador principal
	 // clientes seleccionados de derecha a izquierda
	 $('#remove').live('click',function(e) {
		e.preventDefault();
		$('#cliReemplazo option:selected').each(function(){
			if($("#cliPrincipal option[value='"+ $(this).val() + "']").html() == null){
				$(this).remove().appendTo('#cliPrincipal'); 
			}
		}); 
	 });
	 
	 // Agrega todos elementos seleccionados a la lista de operador reemplazo
	 // todos los clientes de izquierda a derecha
	 $('#addAll').live('click',function(e) {
		e.preventDefault();
		return !$('#cliPrincipal option:not(:disabled)').remove().appendTo('#cliReemplazo');  
	 });
	 
	 //Agrega todos elementos seleccionados a la lista de operador principal
	 // todos los clientes de derecha a izquieda
	 $('#removeAll').live('click',function(e) {
		e.preventDefault();
		$('#cliReemplazo option').each(function(){
			if($("#cliPrincipal option[value='"+ $(this).val() + "']").html() == null){
				$(this).remove().appendTo('#cliPrincipal'); 
			}
		});  
	 }); 
});

/*	--------------------------------------------------
:: Seleccion de operadores
-------------------------------------------------- */

var path_controler = protocol+"//"+hostname+"/"+pathname[1]+"/index.php/c_operador_operador/listarClientes";

// Seleccion de operador principal
$('#opPrincipal').change(function(){
	var op = $(this).val();
	buffCliente = $('#opPrincipal option:selected').val();
	$('#opReemplazo option').removeAttr('disabled');
	$('#opReemplazo option[value="'+buffCliente+'"]').attr('disabled','disabled');
	
	$.ajax({
		url: path_controler,
		type: "POST",
		data: "usuario="+op,
		cache: false,
		dataType: 'json',
		success: function(result){
			$('#cliPrincipal').html('').append(result.listCli);
			$('#cliPrincipal option').removeAttr('disabled');
			$('#cliReemplazo option').each(function(){
				$("#cliPrincipal option[value='"+ $(this).val() + "']").attr('disabled', true );
			});
			$('#cliPrincipal option').removeAttr('class');
			$('#cliPrincipal option:not(:disabled)').addClass('optPrinc');
		}
	});
});

// Seleccion de operador reemplazo
$('#opReemplazo').change(function(){
	var op = $(this).val();
	buffCliente = $('#opReemplazo option:selected').val();
	$('#opPrincipal option').removeAttr('disabled');
	$('#opPrincipal option[value="'+buffCliente+'"]').attr('disabled','disabled');
	
	$.ajax({
		url: path_controler,
		type: "POST",
		data: "usuario="+op,
		cache: false,
		dataType: 'json',
		success: function(result){
			$('#cliReemplazo').html('').append(result.listCli);
			$('#cliPrincipal option').removeAttr('disabled');
			$('#cliReemplazo option').each(function(){
				$("#cliPrincipal option[value='"+ $(this).val() + "']").attr('disabled', true );
			});
			$('#cliPrincipal option').removeAttr('class');
			$('#cliPrincipal option:not(:disabled)').addClass('optPrinc');
		}
	});
});