-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-05-2017 a las 12:32:24
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestor_eventos_rsp`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_FILTRO` ()  BEGIN
START TRANSACTION;
    CREATE TEMPORARY TABLE temporal
	   SELECT gestor_eventos_rsp.grupo_evento_gde.status,
            gestor_eventos_rsp.grupo_evento_gde.dest_country,
            gestor_eventos_rsp.grupo_evento_gde.dest_bunit,
            gestor_eventos_rsp.grupo_evento_gde.src_organization,
            gestor_eventos_rsp.grupo_evento_gde.src_subcategory,
            gestor_eventos_rsp.grupo_evento_gde.src_system,
            sum(gestor_eventos_rsp.grupo_evento_gde.count) as ACUM,
            'OK'
	   FROM gestor_eventos_rsp.grupo_evento_gde

	   GROUP BY gestor_eventos_rsp.grupo_evento_gde.status,
            gestor_eventos_rsp.grupo_evento_gde.dest_country,
            gestor_eventos_rsp.grupo_evento_gde.src_system,
            gestor_eventos_rsp.grupo_evento_gde.dest_bunit,
            gestor_eventos_rsp.grupo_evento_gde.src_organization,
            gestor_eventos_rsp.grupo_evento_gde.src_subcategory;
	
  UPDATE estadistica.filtro_base SET ESTADO='OLD';
	
  INSERT INTO estadistica.filtro_base
	
  SELECT * FROM temporal;
  
  DELETE FROM estadistica.filtro_base WHERE ESTADO='OLD';
  
  DROP TEMPORARY TABLE temporal;
COMMIT;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agrupacion_sistema_gde`
--

CREATE TABLE `agrupacion_sistema_gde` (
  `id` varchar(255) DEFAULT NULL,
  `dest_country` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `dest_service` varchar(255) DEFAULT NULL,
  `src_organization` varchar(255) DEFAULT NULL,
  `src_subcategory` varchar(255) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analista_gde`
--

CREATE TABLE `analista_gde` (
  `user` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name_a` varchar(255) DEFAULT NULL,
  `last_name_b` varchar(255) DEFAULT NULL,
  `annexed` varchar(255) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `analista_gde`
--

INSERT INTO `analista_gde` (`user`, `name`, `last_name_a`, `last_name_b`, `annexed`, `rut`, `phone`) VALUES
('vsepulveda', 'Victor', 'Sepulveda', '', '2996', NULL, '2996');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arquitectura_dispositivo`
--

CREATE TABLE `arquitectura_dispositivo` (
  `DIS_NOM` varchar(200) NOT NULL,
  `ARQ_FW` varchar(30) DEFAULT NULL,
  `ARQ_PE` varchar(100) DEFAULT NULL,
  `ARQ_TUSER` varchar(30) DEFAULT NULL,
  `ARQ_TPASS` varchar(30) DEFAULT NULL,
  `ARQ_TEPASS` varchar(30) DEFAULT NULL,
  `ARQ_SNPM` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
  `AUD_ID` int(11) UNSIGNED NOT NULL,
  `USU_USER` varchar(20) NOT NULL,
  `AUD_TS` datetime NOT NULL,
  `AUD_IP` varchar(20) DEFAULT NULL,
  `AUD_ACC` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_gde`
--

CREATE TABLE `auditoria_gde` (
  `id` int(11) UNSIGNED NOT NULL,
  `user` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `info` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_gde`
--

CREATE TABLE `categoria_gde` (
  `id` int(11) NOT NULL,
  `pais` varchar(124) NOT NULL,
  `app` varchar(124) NOT NULL,
  `detalle` varchar(124) NOT NULL,
  `encargado` varchar(124) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `CLI_ID` int(11) NOT NULL,
  `CLI_NOM` varchar(30) NOT NULL,
  `CLI_CRIT` int(11) NOT NULL,
  `CLI_CONCIP` varchar(50) DEFAULT NULL,
  `CLI_CONCNOM` varchar(50) DEFAULT NULL,
  `CLI_ALIAS` varchar(50) DEFAULT NULL,
  `CLI_NOM_ALIAS` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_gde`
--

CREATE TABLE `cliente_gde` (
  `id` int(11) NOT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `severity` int(11) DEFAULT NULL,
  `src_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_gde`
--

CREATE TABLE `comentario_gde` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `info` varchar(1024) DEFAULT NULL,
  `type` varchar(1024) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_seguimiento`
--

CREATE TABLE `comentario_seguimiento` (
  `COMSC_ID` int(11) UNSIGNED NOT NULL,
  `CORSC_ID` int(11) UNSIGNED NOT NULL,
  `COMSC_COM` varchar(1023) NOT NULL,
  `COMSC_TS` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_seguimiento_gde`
--

CREATE TABLE `comentario_seguimiento_gde` (
  `id` int(11) UNSIGNED NOT NULL,
  `seg_id` int(11) UNSIGNED NOT NULL,
  `info` varchar(1023) NOT NULL,
  `start_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_seguimiento_interno`
--

CREATE TABLE `comentario_seguimiento_interno` (
  `CORSC_ID` int(11) NOT NULL,
  `COMSC_COM` varchar(1023) NOT NULL,
  `COMSC_TS` datetime NOT NULL,
  `COMSC2_ID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_dispositivo`
--

CREATE TABLE `contacto_dispositivo` (
  `DIS_NOM` varchar(200) NOT NULL,
  `CNT_CNTHABIL` varchar(512) DEFAULT NULL,
  `CNT_TELHABIL` varchar(512) DEFAULT NULL,
  `CNT_CELHABIL` varchar(512) DEFAULT NULL,
  `CNT_CORHABIL` varchar(2048) DEFAULT NULL,
  `CNT_CNTNOHABIL` varchar(4096) DEFAULT NULL,
  `CNT_TELNOHABIL` varchar(512) DEFAULT NULL,
  `CNT_CELNOHABIL` varchar(512) DEFAULT NULL,
  `CNT_CORNOHABIL` varchar(4096) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_dispositivo_gde`
--

CREATE TABLE `contacto_dispositivo_gde` (
  `Id` int(11) NOT NULL,
  `src_organization` varchar(255) NOT NULL,
  `to_mail` varchar(4096) DEFAULT NULL,
  `cc_mail` varchar(4096) DEFAULT NULL,
  `encargado` varchar(120) DEFAULT NULL,
  `critical_mayor` tinyint(1) DEFAULT NULL,
  `minor` tinyint(1) DEFAULT NULL,
  `automatic` tinyint(1) DEFAULT NULL,
  `support_organization` varchar(1024) NOT NULL DEFAULT 'Por definir'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_dispositivo_gde`
--

INSERT INTO `contacto_dispositivo_gde` (`Id`, `src_organization`, `to_mail`, `cc_mail`, `encargado`, `critical_mayor`, `minor`, `automatic`, `support_organization`) VALUES
(1, 'Administracion de Sistemas_BCO', 'administraciondesistemas@bancofalabella.cl', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; sbriones@bancofalabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Sin Servicio', 0, 0, 0, 'Sin Servicio'),
(2, 'Administracion Sistemas', 'AdministracionSistemas@Falabella.cl', 'GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(3, 'Administracion Sistemas - GSC Sistemas Administrativos Soporte', 'AdministracionSistemas@Falabella.cl; ', 'GSCSistemasAdministrativosSoporte@Falabella.cl; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; administradoresmonitoreo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(4, 'Administracion Sistemas - Plataforma Tienda', 'AdministracionSistemas@Falabella.cl', 'PlataformadeTienda@Falabella.cl; SoporteIngenieria-Alarmas@Falabella.cl; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; administradoresmonitoreo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(5, 'Alertas de Seguridad - Peru', '_AlertasdeSeguridad-Peru@Falabella.cl', 'sacordovar@sodimac.com.pe; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Peru.Sodimac Seguridad Informatica y Redes', 1, 0, 0, 'PERU-GERENCIA SISTEMAS - OF.APOYO'),
(6, 'B2C Factory ASLP', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; B2CFactoryASLP@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(7, 'Balanzas y Pos Tottus', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; balanzasups@tottus.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(8, 'Banco Falabella', 'soporteAutoservicios@bancofalabella.cl; redes.banco@bancofalabella.cl;', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; sbriones@bancofalabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(9, 'Banco-SoporteSwitch', 'MesadeAyudaRegional@Falabella.cl; SoporteSwitch@Falabella.cl', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; sbriones@bancofalabella.cl; ; mrodriguezp@falabella.cl; josorio@falabella.com.co; cartperez@falabella.com.co; SolucionesTecnologicasCMR&CO@falabella.com.co; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;', NULL, NULL, NULL, NULL, 'Por definir'),
(10, 'Bco Falabella', 'monitoreoSpectrum@bancofalabella.cl', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; sbriones@bancofalabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(11, 'BTM-SoporteMDW', 'soportemdw@falabella.cl', 'nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(12, 'Cajas ARG', 'mesadeayuda@falabella.com.ar;  administracioncajassistemasadmcajasacs@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(13, 'Cajas ARG_VDP', 'mesadeayuda@falabella.com.ar; administracioncajassistemasadmcajasacs@falabella.com.ar;monitoreo.click@accenture.com', 'Gestion de Servicios al Cliente <SoporteIT@falabella.com.ar>; Jose Maria Lopez [Sistemas AR] <JMLopez@falabella.com.ar>; Administrador Mesa de Ayuda <>; ciglesias@Falabella.com.ar; Monitoreo NeoSecure <Nas_fal@neosecure.com>; Gestion de Eventos <GestiondeEventos@Falabella.cl>; Area de Monitoreo Corporativo <AreadeMonitoreoCorporativo@Falabella.cl>', NULL, NULL, NULL, NULL, 'Por definir'),
(14, 'CCTV ARG', 'mesadeayuda@falabella.com.ar; CCTV@falabella.cl;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(15, 'CCTV ARG_Sod', 'mesadeayuda@falabella.com.ar; CCTV@falabella.cl;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; jlatorre@sodimac.com.ar; fbuldorini@sodimac.com.ar; fcalani@sodimac.com.ar; gubarrios@sodimac.com.ar; ciglesias@Falabella.com.ar; ;;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(16, 'CDGE - SoporteIngenieria', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; gestiondeeventos@falabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(17, 'Comunicaciones ARG', 'mesadeayuda@falabella.com.ar; comunicaciones@falabella.com.ar;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(18, 'Comunicaciones ARG_Sod', 'mesadeayuda@falabella.com.ar; comunicaciones@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; jlatorre@sodimac.com.ar; fbuldorini@sodimac.com.ar; fcalani@sodimac.com.ar; gubarrios@sodimac.com.ar; ciglesias@Falabella.com.ar; ;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; * Soporte On Line SAT <_SoporteOnLineSAT@Falabella.com.ar>', NULL, NULL, NULL, NULL, 'Por definir'),
(19, 'ContactCenter', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdcallcentertelefonia@falabella.cl;', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(20, 'DBA Colombia', 'MesadeAyudaRegional@Falabella.cl; dbacolombia@falabella.cl', 'IngenieriaColombia@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(21, 'Falabella Movil', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; Notificaciones_incidentes@movilfalabela.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(22, 'Gestion_Operativa_Sistemas_Seguros_Falabella', 'Gestion_Operativa_Sistemas_Seguros_Falabella@Falabella.cl; monitoreo.click@accenture.com; helpdesk@falabella.cl', 'SoporteWeb@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(23, 'Gestion_Operativa_Sistemas_Seguros_Falabella_W', 'Gestion_Operativa_Sistemas_Seguros_Falabella@Falabella.cl; monitoreo.click@accenture.com; helpdesk@falabella.cl', 'SoporteWeb@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(24, 'Guided Selling', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; guidedselling@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(25, 'HD B2B', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDB2B@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(26, 'HD Panel Control Remoto', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdpanelcontrolremoto@falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(27, 'HD Panel Control Remoto - SoporteWeb', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdpanelcontrolremoto@falabella.cl', 'SoporteWeb@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(28, 'HD SIF', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDSIF@Falabella.cl', 'soporteweb@falabella.cl;; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(29, 'HD SIR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsir@falabella.cl', 'spwebwindows@falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(30, 'HD SIR - Hd Soporte CEP', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsir@falabella.cl', 'HDSoporteCEP@Falabella.cl;  ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(31, 'HD Soporte al Negocio', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsoportealnegocio@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(32, 'HD Soporte al Negocio - Plataforma Web sobre Windows', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsoportealnegocio@falabella.cl', 'spwebwindows@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(33, 'HD Soporte al Negocio - Soporte Web', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsoportealnegocio@falabella.cl', 'soporteweb@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(34, 'HD Soporte CEO', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDSoporteCEO@Falabella.cl', 'SoporteWeb@Falabella.cl; cvergara@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(35, 'HD Soporte de Tienda', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDSoportedeTienda@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(36, 'HD SRX A Soporte', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDSRXASoporte@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(37, 'HD WMS', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; hdwms@falabella.cl', ' ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(38, 'HD WMS ARKAVIA', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; hdwms@falabella.cl', 'soporteweb@falabella.cl; soporte@arkavia.com; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(39, 'HD WMS_PE', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; hdwms@falabella.cl', ' OPERADORESDEMONITOREO@bancofalabella.com.pe; PE_SIF_PLATAFORMA_WINDOWS@Falabella.com.pe; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;', NULL, NULL, NULL, NULL, 'Por definir'),
(40, 'HD XPC', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;hd_xpc@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(41, 'HDCallCenterTelefonia', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; HDCallCenterTelefonia@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(42, 'Incidencia Seguros', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; Gestion_Operativa_Sistemas_Seguros_Falabella@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(43, 'Incidentes MovilFalabella', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; Incidentes_MovilFalabella@movilfalabella.cl', 'SoporteWeb@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(44, 'Informática Central Tottus', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; informaticacentraltottus@tottus.cl;', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(45, 'Infraestructura ARG', 'mesadeayuda@falabella.com.ar; infraestructura@falabella.com.ar;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(46, 'IngSistemasSodimacPeru', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; IngSistemasSodimacPeru@Falabella.cl', 'jlora@sodimac.com.pe; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(47, 'Instalaciones', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; instalaciones@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(48, 'Instalaciones_Tradis', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportetecnicotradis@falabella.cl', 'soportetecnicotradis@falabella.cl; ; gestiondeeventos@falabella.cl; monitoreocorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(49, 'NAS', 'nas_fal@falabella.cl', 'nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(50, 'Operadores Colombia', 'MesadeAyudaRegional@Falabella.cl; operacol@falabella.com.co', 'Subgerencia Producción Colombia; Alejandra Rangel; Jaime Osorio; soporteseguroscolombia; Soluciones Tecnologicas; Administrador Mesa de Ayuda; Monitoreo NeoSecure; Gestion de Eventos; Area de Monitoreo Corporativo; Operadores Tiendas; Cristian Zambrano; Johan Mogollon', NULL, NULL, NULL, NULL, 'Por definir'),
(51, 'Operadores Colombia - SoporteIngenieria', 'MesadeAyudaRegional@Falabella.cl; operacol@falabella.com.co', 'IngenieriaColombia@Falabella.cl; mrodriguezp@falabella.cl; josorio@falabella.com.co; soporteseguroscolombia@falabella.com.co; SolucionesTecnologicasCMR&CO@falabella.com.co;  ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; OperadoresTiendas@falabella.com.co;cczambrano@Falabella.com.co;jsmogollon@Falabella.com.co', NULL, NULL, NULL, NULL, 'Por definir'),
(52, 'Operadores Colombia-ContactCenter', 'MesadeAyudaRegional@Falabella.cl; operacol@falabella.com.co', 'mrodriguezp@falabella.cl; josorio@falabella.com.co; TelecomunicacionesColombia@Falabella.com.co; cartperez@falabella.com.co; soporteseguroscolombia@falabella.com.co; SolucionesTecnologicasCMR&CO@falabella.com.co;  jpreyes@falabella.cl; hdcallcentertelefonia@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;cczambrano@Falabella.com.co;jsmogollon@Falabella.com.co', NULL, NULL, NULL, NULL, 'Por definir'),
(53, 'Operadores_Avellaneda', 'mesadeayuda@falabella.com.ar; DL-OperadoresAvellaneda@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(54, 'Operadores_CD', 'mesadeayuda@falabella.com.ar; dl-operadorescd@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(55, 'Operadores_Cordoba', 'mesadeayuda@falabella.com.ar; DL-OperadoresCordoba@Falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(56, 'Operadores_DOT', 'mesadeayuda@falabella.com.ar; DL-OperadoresDot@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(57, 'Operadores_Florida', 'mesadeayuda@falabella.com.ar; DL-OperadoresFlorida@falabella.com.ar;monitoreo.click@accenture.com', ' soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(58, 'Operadores_Mendoza', 'mesadeayuda@falabella.com.ar; DL-OperadoresMendoza@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(59, 'Operadores_Rosario', 'mesadeayuda@falabella.com.ar; DL-OperadoresRosario@Falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(60, 'Operadores_Sanjuan', 'mesadeayuda@falabella.com.ar; DL-OperadoresSanJuan@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(61, 'Operadores_SodARG', 'mesadeayuda@falabella.com.ar; opersodarg@sodimac.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; jlatorre@sodimac.com.ar; fcalani@sodimac.com.ar; gubarrios@sodimac.com.ar; ciglesias@Falabella.com.ar; ;sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(62, 'Operadores_TOM', 'mesadeayuda@falabella.com.ar; DL-OperadoresTOM@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(63, 'Operadores_Uni', 'mesadeayuda@falabella.com.ar; DL-OperadoresUni@falabella.com.ar;monitoreo.click@accenture.com', 'soporteit@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(64, 'Oracle Implantaciones_QA', 'Oracle-Soporte@Falabella.cl', 'ymmunoz@Falabella.cl;  nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(65, 'Oracle Mantenciones', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; oracle-mantenciones@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(66, 'Oracle Soporte - Sistemas ARR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; Oracle-Soporte@Falabella.cl', 'sistemasarr@sodimac.cl; SoporteWeb@Falabella.cl; cvergara@falabella.cl; jandiaz@sodimac.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(67, 'Oracle-Soporte', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;Oracle-Soporte@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(68, 'Oracle-Soporte - Soporte DTE', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;Oracle-Soporte@Falabella.cl', 'SoporteDTE@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(69, 'Oracle-Soporte - Soporte OMS', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;Oracle-Soporte@Falabella.cl', 'Soporteoms@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(70, 'Oracle-Soporte - SPR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;Oracle-Soporte@Falabella.cl', 'aseider@Falabella.cl; SoporteSpr@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(71, 'Oracle-Soporte_Satif', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;Oracle-Soporte@Falabella.cl', 'SoporteSatif@Falabella.cl; sspambientes@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(72, 'Oracle_Mantenciones-Oracle_Soporte', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; Oracle-Mantenciones@Falabella.cl', 'Oracle-Soporte@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(73, 'Perú', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; operadoresdemonitoreo@bancofalabella.com.pe', 'PE_SIF_BD_SERVICIOS@falabella.com.pe; jmreyes@falabella.com.pe; RCASAFRANCA@sagafalabella.com.pe; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(74, 'Perú-ContactCenter', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; operadoresdemonitoreo@bancofalabella.com.pe; jpreyes@falabella.cl', 'jmreyes@falabella.com.pe; RPATINOC@bancofalabella.com.pe; jechegaray@falabella.com.pe; harbanil@falabella.com.pe; RCASAFRANCA@sagafalabella.com.pe; hdcallcentertelefonia@falabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(75, 'PeSodimac-telecomunicaciones', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; PeSodimac-telecomunicaciones@sodimac.com.pe', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; UPS-SodimacPeru@Falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(76, 'PE_CCF_HelpDesk', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; PE_CCF_HelpDesk@falabella.com.pe', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(77, 'PE_SIF_BD_SERVICIOS', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; PE_SIF_BD_SERVICIOS@falabella.com.pe', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(78, 'PE_SIF_PLATAFORMA_WINDOWS', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; PE_SIF_PLATAFORMA_WINDOWS@Falabella.com.pe', 'gsanchezg@falabella.com.pe; farar@falabella.com.pe; abustamanted@falabella.com.pe; OPERADORESDEMONITOREO@bancofalabella.com.pe; gestiondeincidentesperu@falabella.com.pe; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(79, 'Plataforma Citrix', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; plataformaCitrix@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(80, 'Plataforma de Tienda', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; plataformadetienda@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(81, 'Plataforma de Tienda - Soporte Web', 'monitoreo.click@accenture.com; plataformadetienda@falabella.cl', 'Soporteweb@falabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(82, 'Plataforma Retail Colombia', 'MesadeAyudaRegional@Falabella.cl; PlataformaRetailColombia@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(91, 'Plataforma Windows', 'PlataformaWindows@falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(92, 'Plataforma Windows - Sistema B2C', 'PlataformaWindows@falabella.cl', 'sistemasb2c@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(93, 'Plataforma Windows - Sistema Riesgo CMR', 'PlataformaWindows@falabella.cl', 'Sistemasriesgocmr@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(94, 'Plataforma Windows - Sodimac', 'PlataformaWindows@falabella.cl', 'mdonoso@sodimac.cl; HDSoportealNegocio@Falabella.cl; Sodimac@inmotion.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(95, 'Plataforma Windows-Citrix', 'plataformawindows@falabella.cl', 'plataformaCitrix@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(96, 'Plataforma Windows-ContactCenter', 'PlataformaWindows@falabella.cl', 'hdcallcentertelefonia@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(97, 'Plataforma Windows-Soporte DTE', 'PlataformaWindows@falabella.cl', 'soportedte@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(98, 'Plataforma Windows-Soporte Redes', 'PlataformaWindows@falabella.cl', 'soporteredes@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(99, 'Plataforma Windows-SoporteEPM', 'PlataformaWindows@falabella.cl', 'SoporteEPM@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(100, 'Plataforma Windows-SoporteSPR', 'PlataformaWindows@falabella.cl', 'SoporteSpr@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(101, 'Plataforma Windows-SqlServer', 'PlataformaWindows@falabella.cl', 'SqlServer-Soporte@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(102, 'Plataforma Windows-SqlServer-Satif', 'PlataformaWindows@falabella.cl', 'SqlServer-Soporte@Falabella.cl; stsatif@Falabella.cl;  administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(103, 'Plataforma Windows-SqlServer-Sodimac', 'PlataformaWindows@falabella.cl', 'SqlServer-Soporte@Falabella.cl; mdonoso@sodimac.cl; HDSoportealNegocio@Falabella.cl; Sodimac@inmotion.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(104, 'Plataformas ARG', 'WindowsAdmin@falabella.com.ar ;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ;ACS@falabella.com.ar;; sgalanes@falabella.com.ar; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Windows Admin', 1, 0, 0, 'Por definir'),
(105, 'Plataformas ARG-ContactCenter', 'WindowsAdmin@falabella.com.ar;monitoreo.click@accenture.com', 'hdcallcentertelefonia@falabella.cl; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Windows Admin', 1, 0, 0, 'Por definir'),
(106, 'Plataformas ARG-Seguridad_Informatica_Argentina', 'mesadeayuda@falabella.com.ar; WindowsAdmin@falabella.com.ar;monitoreo.click@accenture.com', 'SeguridadInformatica@Falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(107, 'Plataformas ARG_CCTV', 'mesadeayuda@falabella.com.ar; WindowsAdmin@falabella.com.ar ;monitoreo.click@accenture.com', 'KPICCTV@Falabella.com.ar; ciglesias@Falabella.com.ar; ACS@falabella.com.ar;; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(108, 'Plataformas ARG_Hyperion', 'mesadeayuda@falabella.com.ar; WindowsAdmin@falabella.com.ar; soporte.falabella@technolabcorp.com;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(109, 'Plataformas ARG_Sharepoint', 'mesadeayuda@falabella.com.ar; WindowsAdmin@falabella.com.ar;monitoreo.click@accenture.com', 'SoporteIT@falabella.com.ar; ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(110, 'Produccion Flexcube FIF', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; ProduccionFlexcubeFIF@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(111, 'Produccion GSC', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; producciongsc@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(112, 'Produccion GSC - Soporte CRM', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; producciongsc@Falabella.cl', 'Soporte_CRM@falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(113, 'Proyectos CDGE', 'ProyectosCDGE@Falabella.cl', 'GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', '', 0, 0, 0, 'Por definir'),
(114, 'Redes Banco ', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; redes.banco@bancofalabella.cl', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; sbriones@bancofalabella.cl; soporteAutoservicios@bancofalabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(115, 'Seguridad Informatica ARG', 'mesadeayuda@falabella.com.ar; seguridadinformatica@falabella.com.ar;monitoreo.click@accenture.com', 'ciglesias@Falabella.com.ar; ; sgalanes@falabella.com.ar; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(116, 'Seguridad Informática Perú', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; SeguridadInformaticaPeru@falabella.com.pe', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(117, 'Seguridad Interna', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; seguridadinterna@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(118, 'Seguridad Perimetral', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SeguridadPerimetral@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(119, 'Seguridad Perimetral Colombia', 'SeguridadPerimetralColombia@Falabella.cl; MesadeAyudaRegional@Falabella.cl', 'seguridadinterna@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(120, 'Seguridad y Telecomunicaciones', 'MesadeAyudaRegional@Falabella.cl; telecomunicacionesColombia@falabella.com.co', 'SeguridadPerimetralColombia@Falabella.cl; mrodriguezp@falabella.cl; josorio@falabella.com.co; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;', NULL, NULL, NULL, NULL, 'Por definir'),
(121, 'Servidores y Pos Tottus', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; servidorespos@tottus.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(122, 'ServidoresCentralesTottus', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; servidorescentrales@tottus.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(123, 'Sistemas B2C', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; sistemasb2c@falabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(124, 'Sistemas B2C - SoporteWeb-Ecommerce ', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; sistemasb2c@falabella.cl', 'SoporteWebEcommerce@Falabella.cl; AdministracionSistemas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(125, 'Sistemas Canales', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; stsatif@Falabella.cl', 'AdministracionSistemas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(126, 'Sistemas Riesgo CMR', 'Sistemasriesgocmr@Falabella.cl; monitoreo.click@accenture.com; helpdesk@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(127, 'SistemasWebCMR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; stsatif@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(128, 'SistemasWebCMR - SoporteWeb', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; stsatif@Falabella.cl', 'soporteweb@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(129, 'SOC Falabella', 'monitoreo.click@accenture.com; SOC@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(130, 'Sodimac-Inmotion', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; sodimac@inmotion.cl; services.desk@inmotion.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(131, 'Sodimac-Inmotion-HdSoporte', 'monitoreo.click@accenture.com; monitoreo.click@accenture.com; helpdesk@falabella.cl; sodimac@inmotion.cl; services.desk@inmotion.cl', 'HDSoportealNegocio@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(132, 'Soporte Autenticacion', 'soporteautenticacion@falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Soporte Autenticación Robusta', 1, 0, 0, 'CHILE - GERENCIA DE OPERACIONES Y SISTEMAS CORP'),
(133, 'Soporte Autenticacion - Soporte Web', 'soporteautenticacion@falabella.cl', 'SoporteWeb@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Soporte Autenticación Robusta', 1, 0, 0, 'CHILE - GERENCIA DE OPERACIONES Y SISTEMAS CORP'),
(134, 'Soporte Autoriza 7', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; SoporteAutoriza7@Falabella.cl', 'SoporteSwitch@Falabella.cl;; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(135, 'Soporte Continuidad Motor de Riesgo', 'SoporteContinuidadMotordeRiesgo@Falabella.cl', 'GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', 'Soporte Continuidad Motor de Riesgo', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES Y SISTEMAS'),
(136, 'Soporte Core', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportecore@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(137, 'Soporte CRM', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporte_crm@falabella.cl', 'Soporte_CRM@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(138, 'Soporte EPM', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteEPM@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(139, 'Soporte EPM - PlataformaWebSobreWindows', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteEPM@falabella.cl', 'spwebwindows@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(140, 'Soporte EPM - Soporte Web', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteEPM@falabella.cl', 'soporteweb@falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(141, 'Soporte ERP', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteERP@Falabella.cl', 'soporteweb@falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(142, 'Soporte Falabella Connect', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportefalabellaconnect@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(143, 'Soporte Fraude', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportefraude@falabella.cl', 'soporteweb@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(144, 'Soporte Incidentes CMR SW', 'monitoreo.click@accenture.com; SoporteIncidentesCMR@Falabella.cl', 'soporteweb@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(145, 'Soporte Intranet', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteIntranet@falabella.cl', 'PlataformaWindows@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(146, 'Soporte Maximo SIF', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportemaximosif@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(147, 'Soporte MDW', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteMDW@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(148, 'Soporte OMS', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteoms@falabella.cl', 'soporteweb@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(149, 'Soporte PIM', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoportePIM@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(150, 'Soporte Plataforma Virtual', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; avmwarefacl@Falabella.cl;', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(151, 'Soporte Pos', 'MesadeAyudaRegional@Falabella.cl; soportepos@falabella.cl', 'operadorestiendas@falabella.cl;; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(152, 'Soporte Produccion Sistemas TxD', 'monitoreo.click@accenture.com; SopProdSistemasTxD@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(153, 'Soporte Redes', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(154, 'Soporte Redes - Ancho Banda', 'soporteredes@falabella.cl;  aaprado@falabella.cl', 'GestiondeEventos@Falabella.cl;  nas_fal@neosecure.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(155, 'Soporte Redes - Ancho Banda Enlace', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl;  aaprado@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(156, 'Soporte Redes - Banco', 'soporteredes@falabella.cl; monitoreo.click@accenture.com; helpdesk@falabella.cl', 'monitoreoSpectrum@bancofalabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(157, 'Soporte Redes - ContactCenter', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', 'jpreyes@falabella.cl; hdcallcentertelefonia@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(158, 'Soporte Redes - PE_CCF_ALERTAS', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', 'PE_CCF_ALERTAS@falabella.com.pe; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(159, 'Soporte Redes - Plataforma Windows', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', 'plataformawindows@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(160, 'Soporte Redes - Prime', 'monitoreo.click@accenture.com; helpdesk@falabella.cl;soporteredes@falabella.cl', 'soportewifi@falabella.cl;; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(161, 'Soporte Redes - Telecomunicaciones Colombia', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', 'telecomunicacionescolombia@falabella.com.co; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(162, 'Soporte Redes Peru', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; soporteredesperu@bancofalabella.com.pe', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; SeguridadyComunicacionesPeru@falabella.com.pe', NULL, NULL, NULL, NULL, 'Por definir'),
(163, 'Soporte Redes-Telefonia', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', 'SoporteTelefonia@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl ', NULL, NULL, NULL, NULL, 'Por definir'),
(164, 'Soporte Satif', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteSatif@Falabella.cl', 'sspambientes@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(165, 'Soporte SCA', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteSCA@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(166, 'Soporte SPR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteSpr@Falabella.cl', 'plataformawindows@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(167, 'Soporte SQL', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SqlServer-Soporte@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(168, 'Soporte Switch', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteSwitch@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(169, 'Soporte Telefonia', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteTelefonia@Falabella.cl', '; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(170, 'Soporte Telefonia_ING', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteTelefonia@Falabella.cl', 'IngenieriaenTelecomunicaciones@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(171, 'Soporte TI Viajes', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; turnoviajes@falabella.cl', 'rmatus@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(172, 'Soporte Unix Mantencion', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteUnixMan@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(173, 'Soporte WEB', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(174, 'Soporte WEB - Core', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'soportecore@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(175, 'Soporte WEB - CyberFinancial', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'soportecyberfinancial@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(176, 'Soporte Web - Hd B2B', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'HDB2B@Falabella.cl; AdministracionSistemas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(177, 'Soporte Web - Hd Soporte al Negocio', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'HDSoportealNegocio@Falabella.cl; IngenieriaSistemas_SodimacChile@Falabella.cl; AdministracionSistemas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(178, 'Soporte WEB - Quickpay', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'soportequickpay@falabella.cl;; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(179, 'Soporte WEB - Sistemas Canales CMR', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'stsatif@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(180, 'Soporte WEB - Soporte Flexcube FIF', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'SoporteFlexcubeFIF@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; ProduccionFlexcubeFIF@Falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(181, 'Soporte WEB - Soporte OMS', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'soporteoms@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(182, 'Soporte Web - SoporteCorpOmnichannel', 'monitoreo.click@accenture.com; SoporteWeb@falabella.cl', 'SoporteCorpOmnichannel@Falabella.cl; gestiondeeventos@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir');
INSERT INTO `contacto_dispositivo_gde` (`Id`, `src_organization`, `to_mail`, `cc_mail`, `encargado`, `critical_mayor`, `minor`, `automatic`, `support_organization`) VALUES
(183, 'Soporte Web Ecommerce ', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportewebecommerce@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(184, 'Soporte Web QA', 'SoporteWeb@falabella.cl', 'ymmunoz@Falabella.cl; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(185, 'Soporte Web-Ecommerce - Ingenieria', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soportewebecommerce@falabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(186, 'Soporte Web-Produccion GSC', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'producciongsc@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(187, 'Soporte WEB-SoporteDWH', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'SoporteDWH@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(188, 'Soporte WEB-SoporteERP', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb@falabella.cl', 'SoporteERP@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(189, 'Soporte WIFI - Prime', 'soportewifi@Falabella.cl', 'GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(190, 'SoporteCyberFinancial', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteCyberfinancial@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(191, 'SoporteDTE', 'SoporteDTE@Falabella.cl; monitoreo.click@accenture.com; helpdesk@falabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; ; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; nas_fal@neosecure.com', NULL, NULL, NULL, NULL, 'Por definir'),
(192, 'SoporteIngenieria', 'SoporteIngenieria-Alarmas@Falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(193, 'SoporteIngenieria - Contact Center', 'SoporteIngenieria-Alarmas@Falabella.cl', 'hdcallcentertelefonia@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(194, 'SoporteIngenieria - Core', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soportecore@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(195, 'SoporteIngenieria - Ingenieria Sistemas_Sodimac Chile', 'SoporteIngenieria-Alarmas@Falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(196, 'SoporteIngenieria - MDW', 'SoporteIngenieria-Alarmas@Falabella.cl', 'SoporteMDW@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(197, 'SoporteIngenieria - Plataforma Tienda', 'SoporteIngenieria-Alarmas@Falabella.cl', 'plataformadetiendas@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(198, 'SoporteIngenieria - Produccion GSC', 'SoporteIngenieria-Alarmas@Falabella.cl', 'producciongsc@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(199, 'SoporteIngenieria - Proyectos CDGE', 'SoporteIngenieria-Alarmas@Falabella.cl', 'ProyectosCDGE@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(200, 'SoporteIngenieria - SistemaWebCMR', 'SoporteIngenieria-Alarmas@Falabella.cl', 'SistemasWebCMR@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(201, 'SoporteIngenieria - Soporte DTE', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soportedte@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(202, 'SoporteIngenieria - Soporte Flexcube FIF', 'SoporteIngenieria-Alarmas@Falabella.cl', 'ProduccionFlexcubeFIF@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(203, 'SoporteIngenieria - SoporteCRM', 'SoporteIngenieria-Alarmas@Falabella.cl', 'Soporte_CRM@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(204, 'SoporteIngenieria - SoporteDWH', 'SoporteIngenieria-Alarmas@Falabella.cl', 'SoporteDWH@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(205, 'SoporteIngenieria - SoporteEPM', 'SoporteIngenieria-Alarmas@Falabella.cl', 'SoporteEPM@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; ', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(206, 'SoporteIngenieria - SoporteERP', 'SoporteIngenieria-Alarmas@Falabella.cl', 'SoporteERP@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(207, 'SoporteIngenieria - SPR', 'SoporteIngenieria-Alarmas@Falabella.cl', 'aseider@Falabella.cl; SoporteSpr@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(208, 'SoporteIngenieria - SW', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soporteweb@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(209, 'SoporteIngenieria QA', 'SoporteIngenieria-Alarmas@Falabella.cl', 'ymmunoz@Falabella.cl;  administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(210, 'SoporteIngenieria_Netapp', 'SoporteIngenieria-Alarmas@Falabella.cl', 'GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(211, 'SoporteIngenieria_SoporteFraude', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteIngenieria-Alarmas@Falabella.cl', 'soportefraude@falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(212, 'SoporteNegocioSodimac', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; hdsoportealnegocio@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(213, 'SoporteRespaldo-Oracle', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteRespaldos@Falabella.cl', 'Oracle-Soporte@Falabella.cl; Oracle-Mantenciones@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(214, 'SoporteWeb-Implantaciones', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SoporteWeb-Implantaciones@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(215, 'Soporte_Redes', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; soporteredes@falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(216, 'SqlServer-Soporte', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; SqlServer-Soporte@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(217, 'Tecnología Banco Falabella Colombia ', 'MesadeAyudaRegional@Falabella.cl; GerenciadeTecnologiaCorporativa@Falabella.cl', 'SubgerenciaProduccionColombia@Falabella.cl; OperadoresTiendas@falabella.com.co;  ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;RequerimientosInfraestructuraColombia@Falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(218, 'Tecnologia Call Center', 'MesadeAyudaRegional@Falabella.cl; GerenciadeTecnologiaCorporativa@Falabella.cl', '; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(219, 'Tecnología Falabella Colombia ', 'MesadeAyudaRegional@Falabella.cl; GerenciadeTecnologiaCorporativa@Falabella.cl', 'OperadoresTiendas@falabella.com.co;  ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;', NULL, NULL, NULL, NULL, 'Por definir'),
(220, 'UPS-SodimacPeru', 'monitoreo.click@accenture.com; mesadeayudaregional@falabella.cl; UPS-SodimacPeru@Falabella.cl', 'PeSodimac-telecomunicaciones@sodimac.com.pe; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl; UPS-SodimacPeru@Falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(221, 'Viajes Falabella', 'monitoreo.click@accenture.com; helpdesk@falabella.cl; turnoportalviajes@Falabella.cl', 'VFAlertas@falabella.cl; rglagos@Falabella.cl; ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', NULL, NULL, NULL, NULL, 'Por definir'),
(226, 'hugo', 'SubgerenciaProduccionColombia@Falabella.cl; OperadoresTiendas@falabella.com.co;  ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;RequerimientosInfraestructuraColombia@Falabella.cl', 'SubgerenciaProduccionColombia@Falabella.cl; OperadoresTiendas@falabella.com.co;  ; nas_fal@neosecure.com; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl;RequerimientosInfraestructuraColombia@Falabella.cl', 'hugo', 0, 0, 0, 'Por definir'),
(228, 'SOPORTE EXAORA', 'soporteexaora@falabella.cl', 'manage-ops-support_mx@oracle.com; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Soporte Exaora', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(229, 'Plataforma Windows - Produccion GSC', 'PlataformaWindows@falabella.cl', 'producciongsc@falabella.cl ; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(230, 'Plataforma Windows_PlataformaTienda', 'PlataformaWindows@falabella.cl', 'PlataformadeTienda@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(231, 'Plataforma Windows-PlataformaTienda', 'PlataformaWindows@falabella.cl', 'PlataformadeTienda@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(232, 'PlataformaWindows', 'PlataformaWindows@falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(233, 'PlataformaWindows-SoporteRedes', 'PlataformaWindows@falabella.cl', 'soporteredes@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; AdessaCommandCenter@Falabella.cl', 'SERVIDORES WINDOWS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(234, 'Ingenieria_TCP', 'SoporteIngenieria-Alarmas@Falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(235, 'Soporte Ingenieria_SoporteCyberfinancial', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soportecyberfinancial@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(236, 'SoporteIngenieria - GOP Seguros', 'SoporteIngenieria-Alarmas@Falabella.cl', 'GOP_Seguros@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(237, 'SoporteIngenieria - Soporte Cyberfinancial', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soportecyberfinancial@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(238, 'SoporteIngenieria - Soporte OMS', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soporteoms@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(239, 'SoporteIngenieria - SoporteFraude', 'SoporteIngenieria-Alarmas@Falabella.cl', 'soportefraude@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(240, 'SoporteIngenieria_QA', 'SoporteIngenieria-Alarmas@Falabella.cl', 'ymmunoz@Falabella.cl;  administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(241, 'SoporteIngenieria-PlataformaTienda', 'SoporteIngenieria-Alarmas@Falabella.cl', 'plataformadetiendas@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'INGENIERIA DE SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(242, 'Soporte Exaora - Produccion Flexcube FIF ', 'soporteexaora@falabella.cl', 'manage-ops-support_mx@oracle.com; ProduccionFlexcubeFIF@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Soporte Exaora', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(243, 'Soporte_Exaora', 'soporteexaora@falabella.cl', 'manage-ops-support_mx@oracle.com; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Soporte Exaora', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(244, 'Soporte Exaora omnichannel', 'soporteexaora@falabella.cl', 'manage-ops-support_mx@oracle.com; SoporteCorpOmnichannel@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Soporte Exaora', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(245, 'Operadores de Monitoreo Peru', 'operadoresdemonitoreo@bancofalabella.com.pe', 'GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Peru.Operadores de Monitoreo', 1, 0, 0, 'PERU - SIFSAC - GERENCIA ADMINISTRACION SERVICIOS TI'),
(246, 'Operadores de Monitoreo Peru - Pe_Sif_PlataformaWindows', 'operadoresdemonitoreo@bancofalabella.com.pe', 'PE_ANALISTAS_CONTROL_PRODUCCION_PD@falabella.com.pe; gestiondeincidentesperu@falabella.com.pe; PE_SIF_PLATAFORMA_WINDOWS@Falabella.com.pe; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Peru.Operadores de Monitoreo', 1, 0, 0, 'PERU - SIFSAC - GERENCIA ADMINISTRACION SERVICIOS TI'),
(247, 'Soporte_Autenticacion', 'soporteautenticacion@falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Soporte Autenticación Robusta', 1, 0, 0, 'CHILE - GERENCIA DE OPERACIONES Y SISTEMAS CORP'),
(248, 'SoporteAutenticacion_SoporteWeb', 'soporteautenticacion@falabella.cl', 'SoporteWeb@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Soporte Autenticación Robusta', 1, 0, 0, 'CHILE - GERENCIA DE OPERACIONES Y SISTEMAS CORP'),
(249, 'Soporte Autenticacion - Monitoreo Banco', 'soporteautenticacion@falabella.cl', 'monitoreobanco@bancofalabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Soporte Autenticación Robusta', 1, 0, 1, 'CHILE - GERENCIA DE OPERACIONES Y SISTEMAS CORP'),
(251, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'oracle-soporte@falabella.cl', 'SopProdSistemasTxD@Falabella.cl; administradoresmonitoreo@falabella.cl; gestiondeeventoscorporativo@falabella.cl', 'BASE DE DATOS ORACLE', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(252, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'oracle-soporte@falabella.cl', 'SopProdSistemasTxD@Falabella.cl; administradoresmonitoreo@falabella.cl; gestiondeeventoscorporativo@falabella.cl', 'BASE DE DATOS ORACLE', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(253, 'Gestion de Eventos', 'gestiondeeventoscorporativo@falabella.cl', 'administradoresmonitoreo@falabella.cl; npsanmartin@falabella.cl', 'GESTION DE EVENTOS', 0, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(254, 'Sistemas B2C - SoporteIngenieria', 'sistemasb2c@falabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'B2C', 1, 0, 1, 'CHILE - GERENCIA SISTEMAS COMERCIO ELECTRÓNICO'),
(255, 'Administracion de Sistemas - Soporte Cyberfinancial', 'AdministracionSistemas@Falabella.cl', 'SoporteCyberfinancial@Falabella.cl ; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(256, 'Administracion de Sistemas_SoporteCyberfinancial', 'AdministracionSistemas@Falabella.cl', 'SoporteCyberfinancial@Falabella.cl ; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl; MonitoreoCorporativo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(257, 'Administracion Sistemas BCO SoporteIngenieriaSistemas', 'administraciondesistemas@bancofalabella.cl', 'efredesz@bancofalabella.cl; soporteeingenieriasistemas@bancofalabella.cl; monitoreobanco@bancofalabella.cl', 'Sin Servicio', 0, 0, 0, 'Sin Servicio'),
(258, 'AdministracionSistemas_PlataformaTienda', 'AdministracionSistemas@Falabella.cl', 'PlataformadeTienda@Falabella.cl; SoporteIngenieria-Alarmas@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES'),
(259, 'Soporte_Continuidad_Motor_de_Riesgo', 'SoporteContinuidadMotordeRiesgo@Falabella.cl', 'GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl', 'Soporte Continuidad Motor de Riesgo', 1, 0, 0, 'CHILE - GERENCIA OPERACIONES Y SISTEMAS'),
(260, 'AlertasdeSeguridad_Peru', '_AlertasdeSeguridad-Peru@Falabella.cl', 'sacordovar@sodimac.com.pe; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'Peru.Sodimac Seguridad Informatica y Redes', 1, 0, 0, 'PERU-GERENCIA SISTEMAS - OF.APOYO'),
(261, 'Administracion Sistemas - SoporteIngenieria_Core', 'Administraciondesistemas@bancofalabella.cl', 'SoporteIngenieria-Alarmas@Falabella.cl; soportecore@falabella.cl; administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', 'ADMINISTRACION SISTEMAS', 1, 0, 1, 'CHILE - GERENCIA OPERACIONES'),
(262, 'Atentus-BancoFalabellaChile', 'Administraciondesistemas@bancofalabella.cl', 'afiorid@bancofalabella.cl; mgodoyp@bancofalabella.cl; GestionIncidentes@Falabella.cl; ProduccionFlexcubeFIF@Falabella.cl', 'Sin Servicio', 0, 0, 0, 'Sin Servicio'),
(263, 'Atentus-ControldeProcesosPAC', 'ControldeProcesosPAC@Falabella.cl', 'OperacionesCMR@Falabella.cl; SistemasWebCMR@Falabella.cl; GestionIncidentes@Falabella.cl; gestiondeeventos@falabella.cl', '', 1, 0, 0, ''),
(264, 'Atentus-Falanet', 'falanet@falabella.cl', 'GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl\n', 'Falta', 1, 0, 0, 'Falta'),
(265, 'Atentus-GestionOperativaSistemasSegurosFalabella', 'Gestion_Operativa_Sistemas_Seguros_Falabella@Falabella.cl\n', 'SoporteWeb@falabella.cl; GestionIncidentes@Falabella.cl; Soporte_B2C_Seguros@Falabella.cl; GestiondeEventos@Falabella.cl; administradoresmonitoreo@falabella.cl\n', 'GESTION OPERACIONAL SEGUROS FALABELLA', 1, 0, 0, 'CHILE - GERENCIA SISTEMAS SEGUROS'),
(266, 'SoporteCorpOmnichannel - Soporte Turno CMR', 'SoporteCorpOmnichannel@Falabella.cl', 'soporte_falabella@technisys.com, GestionIncidentesOmnichannel@Falabella.cl, stsatif@Falabella.cl, gestionincidentes@falabella.cl, gestiondeeventos@falabella.cl', 'Soporte Corp Omnichannel', 0, 0, 0, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(267, 'SoporteCorpOmnichannel - Soporte Sistemas CMR ARG', 'SoporteCorpOmnichannel@Falabella.cl', 'soporte_falabella@technisys.com, GestionIncidentesOmnichannel@Falabella.cl, SoporteSistemasCMRArg@Falabella.cl, gestionincidentes@falabella.cl, gestiondeeventos@falabella.cl', 'Soporte Corp Omnichannel', 0, 0, 0, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(268, 'SoporteCorpOmnichannel - Proyecto Omnichannel', 'SoporteCorpOmnichannel@Falabella.cl', 'soporte_falabella@technisys.com, GestionIncidentesOmnichannel@Falabella.cl, proyectoOmnichannel@bancofalabella.cl, gestionincidentes@falabella.cl, gestiondeeventos@falabella.cl, afiorid@bancofalabella.cl, mgodoyp@bancofalabella.cl, sbriones@bancofalabella.cl, administraciondesistemas@bancofalabella.cl', 'Soporte Corp Omnichannel', 0, 0, 0, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(272, 'Gestion Incidentes Omnichannel - Soporte Web', 'GestionIncidentesOmnichannel@falabella.cl', 'SoporteWeb@Falabella.cl, GestiondeEventos@Falabella.cl, gestionincidentes@falabella.cl, administradoresmonitoreo@falabella.cl', 'Soporte Corp Omnichannel', 1, 0, 1, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(273, 'Gestion Incidentes Omnichannel - SoporteIngenieria', 'GestionIncidentesOmnichannel@falabella.cl', 'SoporteIngenieria@Falabella.cl, GestiondeEventos@Falabella.cl, gestionincidentes@falabella.cl, administradoresmonitoreo@falabella.cl', 'Soporte Corp Omnichannel', 1, 0, 1, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(274, 'Gestion Incidentes Omnichannel - Soporte Exaora', 'GestionIncidentesOmnichannel@falabella.cl', 'soporteexaora@Falabella.cl, GestiondeEventos@Falabella.cl, gestionincidentes@falabella.cl, administradoresmonitoreo@falabella.cl', 'Soporte Corp Omnichannel', 1, 0, 1, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(275, 'Gestion Incidentes Omnichannel', 'GestionIncidentesOmnichannel@falabella.cl', 'GestiondeEventos@Falabella.cl, gestionincidentes@falabella.cl, administradoresmonitoreo@falabella.cl', 'Soporte Corp Omnichannel', 1, 0, 1, 'CHILE - GERENCIA SERVICIOS CORPORATIVOS'),
(276, 'Oracle Soporte', 'Oracle-Soporte@falabella.cl', 'administradoresmonitoreo@falabella.cl; GestiondeEventos@Falabella.cl', '', 0, 0, 0, 'Por definir');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_nodo`
--

CREATE TABLE `correo_nodo` (
  `CORSC_ID` int(11) UNSIGNED NOT NULL,
  `CLI_ID` int(11) UNSIGNED NOT NULL,
  `DIS_NOM` varchar(50) NOT NULL,
  `GEV_ID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_nodo_gde`
--

CREATE TABLE `correo_nodo_gde` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_respaldo`
--

CREATE TABLE `correo_respaldo` (
  `CORRES_FROM_NAME` varchar(50) DEFAULT NULL,
  `CORRES_FROM_MAIL` varchar(50) DEFAULT NULL,
  `CORSC_ID` int(11) NOT NULL,
  `CORRES_TO` varchar(2048) NOT NULL,
  `CORRES_CC` varchar(2048) NOT NULL,
  `CORRES_ASU` varchar(200) DEFAULT NULL,
  `CORRES_CLI` varchar(30) NOT NULL,
  `CORRES_NUMIN` varchar(100) DEFAULT NULL,
  `CORRES_ESTACT` varchar(200) DEFAULT NULL,
  `CORRES_DESCEV` varchar(8192) DEFAULT NULL,
  `CORRES_ACCREAL` varchar(8192) DEFAULT NULL,
  `CORRES_OBS` varchar(8192) DEFAULT NULL,
  `CORRES_TSINI` varchar(100) NOT NULL,
  `CORRES_TSFIN` varchar(100) NOT NULL,
  `CORRES_TIPAL` varchar(8192) NOT NULL,
  `CORRES_ALEQ` varchar(8192) NOT NULL,
  `CORRES_CODSERV` varchar(8192) NOT NULL,
  `CORRES_SUCUR` varchar(8192) NOT NULL,
  `CORRES_NOMESTAB` varchar(500) DEFAULT NULL,
  `CORRES_ESTSERV` varchar(50) DEFAULT NULL,
  `CORRES_ESTSERVLAN` varchar(50) DEFAULT NULL,
  `CORRES_ESTINC` varchar(50) DEFAULT NULL,
  `CORRES_ENLPRINC` varchar(50) DEFAULT NULL,
  `CORRES_ENLRESP` varchar(50) DEFAULT NULL,
  `CORRES_TIPFAIL` varchar(500) DEFAULT NULL,
  `CORRES_NOM` varchar(100) NOT NULL,
  `CORRES_GLOSA` varchar(1000) NOT NULL,
  `CORRES_EMAIL` varchar(100) NOT NULL,
  `CORRES_TEL` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_seguimiento`
--

CREATE TABLE `correo_seguimiento` (
  `CORSC_ID` int(11) UNSIGNED NOT NULL,
  `CORSC_TSINI` datetime NOT NULL,
  `CORSC_TSFIN` datetime DEFAULT NULL,
  `CORSC_TSSEG_INI` datetime NOT NULL,
  `CORSC_TSSEG_FIN` datetime NOT NULL,
  `CORSC_CNT` int(11) NOT NULL,
  `CORSC_EST` int(1) DEFAULT NULL,
  `CORSC_LSTCOM` varchar(1023) DEFAULT NULL,
  `CORSC_TIPO` varchar(10) NOT NULL DEFAULT 'evento'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_seguimiento_gde`
--

CREATE TABLE `correo_seguimiento_gde` (
  `id` int(11) UNSIGNED NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `seg_start_time` datetime DEFAULT NULL,
  `seg_end_time` datetime NOT NULL,
  `count` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `info` varchar(1023) DEFAULT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'evento',
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo`
--

CREATE TABLE `dispositivo` (
  `CLI_ID` int(11) NOT NULL,
  `DIS_NOM` varchar(200) NOT NULL,
  `DIS_CRIT` varchar(50) DEFAULT '0',
  `DIS_CODSERV` varchar(100) DEFAULT '',
  `DIS_IPPRINC` varchar(30) DEFAULT '',
  `DIS_CSPRINC` varchar(30) DEFAULT '',
  `DIS_CODADMIN` varchar(30) DEFAULT '',
  `DIS_SLA` varchar(30) DEFAULT '',
  `DIS_ATEN` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo_completo`
--

CREATE TABLE `dispositivo_completo` (
  `CLI_ID` int(11) NOT NULL,
  `DIS_NOM` varchar(200) NOT NULL,
  `DIS_CRIT` varchar(50) DEFAULT '0',
  `GEOD_SUCURSAL` varchar(200) DEFAULT NULL,
  `GEOD_REGION` varchar(30) DEFAULT NULL,
  `ARQ_FW` varchar(30) DEFAULT NULL,
  `ARQ_PE` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo_gde`
--

CREATE TABLE `dispositivo_gde` (
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `severity` varchar(50) DEFAULT NULL,
  `service_code` varchar(100) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `concentrator` varchar(50) DEFAULT NULL,
  `admin_code` varchar(50) DEFAULT NULL,
  `sla` varchar(50) DEFAULT NULL,
  `duration` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EST_NOM` char(20) NOT NULL,
  `EST_DESC` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_gde`
--

CREATE TABLE `estado_gde` (
  `id` bigint(20) NOT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado_gde`
--

INSERT INTO `estado_gde` (`id`, `status`) VALUES
(1, 'Activo'),
(2, 'Cerrado'),
(3, 'En Proceso'),
(4, 'Omitido'),
(5, 'Deshabilitar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento_gde`
--

CREATE TABLE `evento_gde` (
  `id` bigint(20) NOT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `severity` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `src_name` varchar(255) NOT NULL,
  `src_subname` varchar(255) DEFAULT NULL,
  `src_ip` varchar(255) NOT NULL,
  `src_organization` varchar(255) DEFAULT NULL,
  `src_category` varchar(255) NOT NULL,
  `src_subcategory` varchar(255) NOT NULL,
  `src_tool` varchar(255) DEFAULT NULL,
  `dest_bunit` varchar(255) NOT NULL,
  `dest_service` varchar(255) NOT NULL,
  `dest_country` varchar(255) NOT NULL,
  `body` varchar(1024) NOT NULL,
  `src_system` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `evento_gde`
--
DELIMITER $$
CREATE TRIGGER `agrupar_eventos_gde` BEFORE INSERT ON `evento_gde` FOR EACH ROW BEGIN
	DECLARE identificador INT;
	DECLARE severidad varchar(20);
	DECLARE estado varchar(20);
	DECLARE tipo varchar(255);
	DECLARE fecha datetime;
	
	IF NEW.severity='OK' THEN
		SET identificador=(SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		IF identificador IS NOT NULL THEN
			SELECT `status`,`start_time`, `type` INTO estado, fecha, tipo FROM `grupo_evento_gde` WHERE `id`=identificador;
			
			SET NEW.group_id = identificador;
			UPDATE `grupo_evento_gde` SET `status` = 'Cerrado', `count` = `count`+1 WHERE `id`=identificador;
			INSERT INTO `comentario_gde` (`group_id`, `start_time`, `info`, `type`, `user`, `status`) VALUES (identificador, NEW.start_time, 'Alerta normalizada', NULL, 'gestor', 'Cerrado');
			
			IF estado='Activo' THEN
				INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
			ELSE
				SET fecha=(SELECT MAX(`start_time`) FROM `historia_gde` WHERE `group_id`=identificador);
				INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
			END IF;
		END IF;
	ELSE
		SET identificador=(SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		IF identificador IS NOT NULL THEN
			SELECT `status`,`start_time`, `type`, `severity` INTO estado, fecha, tipo, severidad FROM `grupo_evento_gde` WHERE `id`=identificador;
			
			IF NEW.severity=severidad THEN
				SET NEW.group_id = identificador;
				UPDATE `grupo_evento_gde` SET `count` = `count`+1 WHERE `id`=identificador; 
			ELSE
				SET NEW.group_id = identificador;
				UPDATE `grupo_evento_gde` SET `status` = 'Cerrado', `count` = `count`+1 WHERE `id`=identificador;
				INSERT INTO `comentario_gde` (`group_id`, `start_time`, `info`, `type`, `user`, `status`) VALUES (identificador, NEW.start_time, 'Alerta normalizada', NULL, 'gestor', 'Cerrado');
				
				IF estado='Activo' THEN
					INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
				ELSE
					SET fecha=(SELECT MAX(`start_time`) FROM `historia_gde` WHERE `group_id`=identificador);
					INSERT INTO `historia_gde` (`group_id`, `start_status`, `end_status`, `start_time`, `user`, `access_time`, `type`) VALUES (identificador, estado, 'Cerrado', NOW(), 'gestor', fecha, tipo);
				END IF;
				
				INSERT INTO `grupo_evento_gde` (`status`,`src_name`, `src_subname` ,`type`,`count`,`severity`,`start_time`,`body`,`src_ip`,src_organization, src_category,src_subcategory, src_tool, dest_bunit, dest_service,dest_country,src_system,folio_mda) VALUES (NEW.status, NEW.src_name, NEW.src_subname, NEW.type,1, NEW.severity, NEW.start_time,NEW.body,NEW.src_ip,NEW.src_organization, NEW.src_category, NEW.src_subcategory, NEW.src_tool, NEW.dest_bunit, NEW.dest_service,NEW.dest_country,NEW.src_system,0);
			
				SET NEW.group_id = (SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
			END IF;
		ELSE
			INSERT INTO `grupo_evento_gde` (`status`,`src_name`, `src_subname` ,`type`,`count`,`severity`,`start_time`,`body`,`src_ip`,src_organization, src_category,src_subcategory, src_tool, dest_bunit, dest_service,dest_country,src_system,folio_mda) VALUES (NEW.status, NEW.src_name, NEW.src_subname, NEW.type,1, NEW.severity, NEW.start_time,NEW.body,NEW.src_ip,NEW.src_organization, NEW.src_category, NEW.src_subcategory, NEW.src_tool, NEW.dest_bunit, NEW.dest_service,NEW.dest_country,NEW.src_system,0);
			
			SET NEW.group_id = (SELECT MAX(`id`) FROM `grupo_evento_gde` WHERE `src_name`=NEW.src_name AND `src_subname`=NEW.src_subname AND `type`=NEW.type AND `src_ip`=NEW.src_ip AND `status`!='Cerrado');
		END IF;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento_seguimiento`
--

CREATE TABLE `evento_seguimiento` (
  `EVENTO_SEG_ID` int(11) NOT NULL,
  `EVENTO_CRIT` enum('IFDOWN','DOWN','VPN_DOWN','CPU') NOT NULL,
  `EVENTO_CLI_CRIT` enum('0','1','2','3','4','5','6','7','8','9') NOT NULL,
  `EVENTO_TESP` enum('5','15','30') NOT NULL,
  `EVENTO_CORSEG` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `geografia_dispositivo`
--

CREATE TABLE `geografia_dispositivo` (
  `DIS_NOM` varchar(200) NOT NULL,
  `GEOD_REGION` varchar(30) DEFAULT NULL,
  `GEOD_PROVINCIA` varchar(30) DEFAULT NULL,
  `GEOD_COMUNA` varchar(100) DEFAULT NULL,
  `GEOD_SUCURSAL` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `USU_USER` varchar(20) NOT NULL,
  `CLI_ID` int(11) NOT NULL,
  `GRU_TIPO` char(20) NOT NULL,
  `GRU_STT` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_evento_gde`
--

CREATE TABLE `grupo_evento_gde` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `src_name` varchar(255) NOT NULL,
  `src_subname` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `severity` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `info` varchar(1023) DEFAULT NULL,
  `status_lock` int(11) DEFAULT NULL,
  `sem` int(1) DEFAULT NULL,
  `body` varchar(1023) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `src_ip` varchar(255) DEFAULT NULL,
  `src_organization` varchar(255) DEFAULT NULL,
  `src_category` varchar(255) NOT NULL,
  `src_subcategory` varchar(255) NOT NULL,
  `src_tool` varchar(255) DEFAULT NULL,
  `dest_bunit` varchar(255) NOT NULL,
  `dest_service` varchar(255) NOT NULL,
  `dest_country` varchar(255) NOT NULL,
  `src_system` varchar(1024) DEFAULT NULL,
  `send_mail` smallint(1) DEFAULT NULL,
  `folio_mda` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_gde`
--

CREATE TABLE `grupo_gde` (
  `id` bigint(20) NOT NULL,
  `src_organization` varchar(255) DEFAULT NULL,
  `user` varchar(255) NOT NULL,
  `src_tool` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo_gde`
--

INSERT INTO `grupo_gde` (`id`, `src_organization`, `user`, `src_tool`) VALUES
(9, 'All', 'sfigue', NULL),
(10, NULL, 'sfigue', 'All'),
(15, 'All', 'nprado', NULL),
(16, NULL, 'nprado', 'All'),
(23, 'All', 'supervisor', NULL),
(24, NULL, 'supervisor', 'All'),
(49, 'All', 'operador', NULL),
(50, NULL, 'operador', 'All'),
(51, 'All', 'sfigueroa', NULL),
(52, NULL, 'sfigueroa', 'All'),
(59, 'All', 'mfodic', NULL),
(60, NULL, 'mfodic', 'All'),
(61, 'All', 'maraya', NULL),
(62, NULL, 'maraya', 'All'),
(71, 'All', 'admin', NULL),
(72, NULL, 'admin', 'All'),
(73, 'All', 'syaksic', NULL),
(74, NULL, 'syaksic', 'All'),
(75, 'All', 'adm_redes', NULL),
(76, NULL, 'adm_redes', 'All'),
(77, 'All', 'accenter', NULL),
(78, NULL, 'accenter', 'All'),
(79, 'All', 'bespinoza', NULL),
(80, NULL, 'bespinoza', 'All'),
(81, 'All', 'ccuevas', NULL),
(82, NULL, 'ccuevas', 'All'),
(83, 'All', 'crvalenz', NULL),
(84, NULL, 'crvalenz', 'All'),
(89, 'All', 'dsandoval', NULL),
(90, NULL, 'dsandoval', 'All'),
(91, 'All', 'fduarte', NULL),
(92, NULL, 'fduarte', 'All'),
(95, 'All', 'gcampos', NULL),
(96, NULL, 'gcampos', 'All'),
(99, 'All', 'jjsolis', NULL),
(100, NULL, 'jjsolis', 'All'),
(101, 'All', 'jvera', NULL),
(102, NULL, 'jvera', 'All'),
(109, 'All', 'vsepulveda', NULL),
(110, NULL, 'vsepulveda', 'All'),
(123, 'All', 'llaciart', NULL),
(124, NULL, 'llaciart', 'All'),
(125, 'All', 'gcampos', NULL),
(126, NULL, 'gcampos', 'All'),
(127, 'All', 'fbriones', NULL),
(128, NULL, 'fbriones', 'All'),
(129, 'All', 'ext_fantnovoa', NULL),
(130, NULL, 'ext_fantnovoa', 'All'),
(177, 'All', 'csanchez', NULL),
(178, NULL, 'csanchez', 'All'),
(179, 'Oracle Soporte', 'fjjimenez', NULL),
(180, 'Oracle Soporte - Sistemas ARR', 'fjjimenez', NULL),
(181, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'fjjimenez', NULL),
(182, 'Oracle-Soporte', 'fjjimenez', NULL),
(183, 'Oracle-Soporte - Soporte DTE', 'fjjimenez', NULL),
(184, 'Oracle-Soporte - Soporte OMS', 'fjjimenez', NULL),
(185, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'fjjimenez', NULL),
(186, 'Oracle-Soporte - SPR', 'fjjimenez', NULL),
(187, 'Oracle-Soporte_Satif', 'fjjimenez', NULL),
(188, 'Oracle_Mantenciones-Oracle_Soporte', 'fjjimenez', NULL),
(189, NULL, 'fjjimenez', 'All'),
(190, 'Oracle Soporte', 'oreyes', NULL),
(191, 'Oracle Soporte - Sistemas ARR', 'oreyes', NULL),
(192, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'oreyes', NULL),
(193, 'Oracle-Soporte', 'oreyes', NULL),
(194, 'Oracle-Soporte - Soporte DTE', 'oreyes', NULL),
(195, 'Oracle-Soporte - Soporte OMS', 'oreyes', NULL),
(196, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'oreyes', NULL),
(197, 'Oracle-Soporte - SPR', 'oreyes', NULL),
(198, 'Oracle-Soporte_Satif', 'oreyes', NULL),
(199, 'Oracle_Mantenciones-Oracle_Soporte', 'oreyes', NULL),
(200, NULL, 'oreyes', 'All'),
(201, 'Oracle Soporte', 'mmmogollon', NULL),
(202, 'Oracle Soporte - Sistemas ARR', 'mmmogollon', NULL),
(203, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'mmmogollon', NULL),
(204, 'Oracle-Soporte', 'mmmogollon', NULL),
(205, 'Oracle-Soporte - Soporte DTE', 'mmmogollon', NULL),
(206, 'Oracle-Soporte - Soporte OMS', 'mmmogollon', NULL),
(207, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'mmmogollon', NULL),
(208, 'Oracle-Soporte - SPR', 'mmmogollon', NULL),
(209, 'Oracle-Soporte_Satif', 'mmmogollon', NULL),
(210, 'Oracle_Mantenciones-Oracle_Soporte', 'mmmogollon', NULL),
(211, NULL, 'mmmogollon', 'All'),
(212, 'Oracle Soporte', 'jalexduarte', NULL),
(213, 'Oracle Soporte - Sistemas ARR', 'jalexduarte', NULL),
(214, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'jalexduarte', NULL),
(215, 'Oracle-Soporte', 'jalexduarte', NULL),
(216, 'Oracle-Soporte - Soporte DTE', 'jalexduarte', NULL),
(217, 'Oracle-Soporte - Soporte OMS', 'jalexduarte', NULL),
(218, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'jalexduarte', NULL),
(219, 'Oracle-Soporte - SPR', 'jalexduarte', NULL),
(220, 'Oracle-Soporte_Satif', 'jalexduarte', NULL),
(221, 'Oracle_Mantenciones-Oracle_Soporte', 'jalexduarte', NULL),
(222, NULL, 'jalexduarte', 'All'),
(223, 'Oracle Soporte', 'dleiva', NULL),
(224, 'Oracle Soporte - Sistemas ARR', 'dleiva', NULL),
(225, 'ORACLE SOPORTE_SoporteProduccionSistemasTxD', 'dleiva', NULL),
(226, 'Oracle-Soporte', 'dleiva', NULL),
(227, 'Oracle-Soporte - Soporte DTE', 'dleiva', NULL),
(228, 'Oracle-Soporte - Soporte OMS', 'dleiva', NULL),
(229, 'Oracle-Soporte - Soporte Produccion Sistemas TxD ', 'dleiva', NULL),
(230, 'Oracle-Soporte - SPR', 'dleiva', NULL),
(231, 'Oracle-Soporte_Satif', 'dleiva', NULL),
(232, 'Oracle_Mantenciones-Oracle_Soporte', 'dleiva', NULL),
(233, NULL, 'dleiva', 'All');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia`
--

CREATE TABLE `historia` (
  `HIS_ID` int(11) NOT NULL,
  `EST_INI` char(20) NOT NULL,
  `EST_FIN` char(20) NOT NULL,
  `GEV_ID` int(11) UNSIGNED NOT NULL,
  `USU_USER` varchar(20) NOT NULL,
  `HIS_TS` datetime NOT NULL,
  `HIS_INISTATE` datetime DEFAULT NULL,
  `HIS_TEVNOM` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_nodos`
--

CREATE TABLE `historial_nodos` (
  `HISTORIAL_ID` int(11) NOT NULL,
  `HISTORIAL_TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `USU_USER` varchar(20) NOT NULL,
  `DIS_NOM` varchar(200) NOT NULL,
  `HISTORIAL_DET` text NOT NULL,
  `HISTORIAL_ACC` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia_gde`
--

CREATE TABLE `historia_gde` (
  `id` int(11) NOT NULL,
  `start_status` char(20) NOT NULL,
  `end_status` char(20) NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  `user` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `access_time` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lectura`
--

CREATE TABLE `lectura` (
  `CLI_NOM` varchar(30) DEFAULT NULL,
  `GEV_ID` int(11) UNSIGNED DEFAULT NULL,
  `DIS_NOM` varchar(110) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numbers`
--

CREATE TABLE `numbers` (
  `n` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operador_gde`
--

CREATE TABLE `operador_gde` (
  `user` varchar(50) NOT NULL,
  `user_sup` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name_a` varchar(255) DEFAULT NULL,
  `last_name_b` varchar(50) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `annexed` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operador_gde`
--

INSERT INTO `operador_gde` (`user`, `user_sup`, `name`, `last_name_a`, `last_name_b`, `rut`, `annexed`, `phone`) VALUES
('accenter', NULL, 'Adessa', 'Command', 'Center', NULL, '', ''),
('bespinoza', NULL, 'Bernardo', 'Espinoza', 'Perez', NULL, '4781', '56696842'),
('ccuevas', NULL, 'Christian', 'Cuevas', 'Cuevas', NULL, '5790 - 5791', '223945790'),
('crvalenz', NULL, 'Cristian', 'Valenzuela', 'Tapia', NULL, '', ''),
('csanchez', NULL, 'Camila B', 'Sanchez', 'Gajardo', NULL, '8954', '8954'),
('dleiva', NULL, 'David', 'Leiva', '', NULL, '+56223898742', '+56996405814'),
('dsandoval', NULL, 'David', 'Sandoval', 'castillo', NULL, '4780', '+56993224107'),
('ext_fantnovoa', NULL, 'Felipe', 'Novoa', 'P', NULL, '5791 - 5790', '5790 - 5791'),
('fbriones', NULL, 'Francisco', 'Briones', '', NULL, '5790', '5790'),
('fduarte', NULL, 'Fernando Luis', 'Duarte', 'Morales', NULL, '5790 - 5791', '223945790'),
('fjjimenez', NULL, 'Francisco', 'Jimenez', '', NULL, '+56225878231', '+56971258888'),
('gcampos', NULL, 'Gonzalo', 'Campos', 'Maturana', NULL, '123456', '1234345'),
('jalexduarte', NULL, 'John', 'Duarte', '', NULL, '', ''),
('jjsolis', NULL, 'Juan Jose', 'Solis', 'Vera', NULL, '5790 - 5791', ''),
('jvera', NULL, 'Journney', 'Vera', 'Gonzalez', NULL, '5790 - 5791', '223945790'),
('llaciart', NULL, 'Luis', 'Laciart', '', NULL, '', ''),
('maraya', NULL, 'Mauricio', 'Araya', 'Guerra', NULL, '5790 - 5791', '223945790'),
('mfodic', NULL, 'Mauricio', 'Fodic', '', NULL, '', ''),
('mmmogollon', NULL, 'Maigle', 'Mogollon', '', NULL, '', ''),
('operador', NULL, 'operador', 'operador', 'operador', NULL, '123', '123'),
('oreyes', NULL, 'Oscar', 'Reyes', 'Sepulveda', NULL, '+56223998081', '+56994429391'),
('sfigueroa', NULL, 'Seba', 'Figueroa', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE `parametros` (
  `id` int(11) NOT NULL,
  `parametro` varchar(56) NOT NULL,
  `valor` varchar(1024) NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respaldo_nodos`
--

CREATE TABLE `respaldo_nodos` (
  `RESPALDO_ID` int(11) NOT NULL,
  `RESPALDO_FECHA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `RESPALDO_FECHA_USO` timestamp NULL DEFAULT NULL,
  `RESPALDO_RUTA` varchar(100) NOT NULL,
  `RESPALDO_ESTADO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_gde`
--

CREATE TABLE `servicios_gde` (
  `id` bigint(20) NOT NULL,
  `dest_service` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicios_gde`
--

INSERT INTO `servicios_gde` (`id`, `dest_service`) VALUES
(1, 'AR_Autoriza 7'),
(2, 'AR_B2C_CallCenter'),
(3, 'AR_Brief On Line'),
(4, 'AR_CL_CO_PE_B2B_Falabella'),
(5, 'AR_CPI'),
(6, 'AR_DomainController'),
(7, 'AR_MC_B2B'),
(8, 'AR_MC_B2B_SODIMAC'),
(9, 'AR_MC_B2C_Falabella'),
(10, 'AR_MC_B2C_Sodimac'),
(11, 'AR_MC_CMR_Omnichannel'),
(12, 'AR_MC_Credito Argentina'),
(13, 'AR_MC_Cyber_Financial'),
(14, 'AR_MC_Novios'),
(15, 'AR_MC_SAB'),
(16, 'AR_MC_SEGUROS SFWEB'),
(17, 'AR_MC_SRX'),
(18, 'AR_MC_VU_CMR'),
(19, 'AR_PE_MC_B2B_Sodimac'),
(20, 'AR_PE_UR_TESORERIA'),
(21, 'AR_POS_12'),
(22, 'AR_SPF'),
(23, 'AR_Venta en Linea'),
(24, 'B2B_MC_Falabella_Paises'),
(25, 'BR_B2C Rediseno Sodimac'),
(26, 'BR_CES_Sodimac'),
(27, 'BR_Sodimac ODS'),
(28, 'CL_A2A'),
(29, 'CL_ACL_CMR'),
(30, 'CL_ACL_Tiendas por Dpto.'),
(31, 'CL_Adessa_Sharepoint'),
(32, 'CL_ADM Contratos-ON BASE'),
(33, 'CL_Administrador_Campanas'),
(34, 'CL_Admin_Seguros_Sodimac'),
(35, 'CL_Aplicacion DAD'),
(36, 'CL_AppDynamics'),
(37, 'CL_AppDynamics_Banco_Falabella'),
(38, 'CL_Arquitectura_SPR'),
(39, 'CL_Autorizacion Transbank'),
(40, 'CL_B2B_Movil'),
(41, 'CL_B2C_CallCenter'),
(42, 'CL_B2C_Servidor POS'),
(43, 'CL_BANCO RED ATM MALL PLAZA'),
(44, 'CL_BANCO_Falabella Brief On Line'),
(45, 'CL_Brief On Line'),
(46, 'CL_BSC Corporativo'),
(47, 'CL_Calculo Incentivos'),
(48, 'CL_Capacita'),
(49, 'CL_CARCENTER_Sodimac'),
(50, 'CL_Cartola de Puntos'),
(51, 'CL_CCI'),
(52, 'CL_Circular 40'),
(53, 'CL_Circulo de Especialistas'),
(54, 'CL_Club 15'),
(55, 'CL_CMR-Autoconsulta'),
(56, 'CL_CMR-BOTON DE PAGO SII'),
(57, 'CL_CMR-BOTON DE PAGO WPP'),
(58, 'CL_CMR_BTM'),
(59, 'CL_CMR_INTERNET'),
(60, 'CL_CMR_Life'),
(61, 'CL_CMR_MiniSitio_Autoenrolamiento'),
(62, 'CL_CMR_WebPay '),
(63, 'CL_ComisionesCMR'),
(64, 'CL_ComisionesCMR_TMP'),
(65, 'CL_Conciliacion_Bancaria'),
(66, 'CL_Contacto Viajes'),
(67, 'CL_Control_Trafico'),
(68, 'CL_Core_Bancario GTD'),
(69, 'CL_Core_Bancario_QA'),
(70, 'CL_Cuadratura de Procesos ACL'),
(71, 'CL_DataWarehouse'),
(72, 'CL_Despacho_a_Domicilio'),
(73, 'CL_EECC'),
(74, 'CL_EPM_Hyperion'),
(75, 'CL_EXCHANGE_2003'),
(76, 'CL_FACE'),
(77, 'CL_Falabella Empresas'),
(78, 'CL_Falabella_RTL_Journal'),
(79, 'CL_FINANCIAL-TRF'),
(80, 'CL_FireEye '),
(81, 'CL_Firmador Seguros'),
(82, 'CL_FOSO_MIDDLEWARE'),
(83, 'CL_Gestion filas dimensionado'),
(84, 'CL_Gestion_Contratos_Legales_Seguros_Falabella'),
(85, 'CL_Grabaciones_Seguros_Falabella'),
(86, 'CL_Herramientas TI RRHH'),
(87, 'CL_Hyperion'),
(88, 'CL_IBanking'),
(89, 'CL_Intranet_Sodimac'),
(90, 'CL_Juntos_Aprendemos'),
(91, 'CL_Lavado de Activos'),
(92, 'CL_Master Card'),
(93, 'CL_Maximo'),
(94, 'CL_MC_ACL_Seguros'),
(95, 'CL_MC_Active_Directory'),
(96, 'CL_MC_Arquitectura_Falanet'),
(97, 'CL_MC_ASL+'),
(98, 'CL_MC_Autentificacion_Robusta'),
(99, 'CL_MC_Autoriza 7'),
(100, 'CL_MC_B2B_Falabella Migracion'),
(101, 'CL_MC_B2B_FAL_Paises_y_Tottus_Chile'),
(102, 'CL_MC_B2B_SODIMAC'),
(103, 'CL_MC_B2B_SODIMAC Migracion'),
(104, 'CL_MC_B2B_TOTTUS CL-PE'),
(105, 'CL_MC_B2B_TOTTUS Migracion'),
(106, 'CL_MC_B2C Falabella'),
(107, 'CL_MC_B2C_Sodimac'),
(108, 'CL_MC_B2C_Tottus'),
(109, 'CL_MC_BF_OmniChannel'),
(110, 'CL_MC_Bus_de_Datos'),
(111, 'CL_MC_Call Center'),
(112, 'CL_MC_CallCenter_Seguros'),
(113, 'CL_MC_CMR-FRAUDE'),
(114, 'CL_MC_CMR-PAC'),
(115, 'CL_MC_CMR-TELCO'),
(116, 'CL_MC_CMR_BOTON DE PAGO'),
(117, 'CL_MC_CMR_COBRANZA'),
(118, 'CL_MC_CMR_CYBER'),
(119, 'CL_MC_CMR_Falabella Movil Conectividad'),
(120, 'CL_MC_CMR_Falabella Movil Conectividad_temp'),
(121, 'CL_MC_CMR_Falabella_Movil_SRX'),
(122, 'CL_MC_CMR_FALABELLA_MOVIL_WEB'),
(123, 'CL_MC_CMR_Mis_Gastos'),
(124, 'CL_MC_CMR_OmniChannel'),
(125, 'CL_MC_CMS'),
(126, 'CL_MC_ContactCenter_GmoMann'),
(127, 'CL_MC_Control-M'),
(128, 'CL_MC_Core_Bancario'),
(129, 'CL_MC_Cyber8.1_BCO_CMR'),
(130, 'CL_MC_CYBER_BANCO'),
(131, 'CL_MC_DAD-HA'),
(132, 'CL_MC_DEMANDA_Y_REPOSICION'),
(133, 'CL_MC_DIM'),
(134, 'CL_MC_Documentacion_Sistemas - Evrie'),
(135, 'CL_MC_DomainController'),
(136, 'CL_MC_Domain_Controller_CORP'),
(137, 'CL_MC_DTE'),
(138, 'CL_MC_Emision Instantanea'),
(139, 'CL_MC_EXCHANGE_2010'),
(140, 'CL_MC_Falabella Connect_2.2'),
(141, 'CL_MC_Falabella_Switch_Transbank_POS'),
(142, 'CL_MC_FINANCIAL'),
(143, 'CL_MC_Firma_Electronica'),
(144, 'CL_MC_GESTION DE CARTERA'),
(145, 'CL_MC_Life_Ray'),
(146, 'CL_MC_Middleware Corporativo'),
(147, 'CL_MC_Motores de Riesgo'),
(148, 'CL_MC_MPP'),
(149, 'CL_MC_NControl-M'),
(150, 'CL_MC_Novios'),
(151, 'CL_MC_ODBMS'),
(152, 'CL_MC_OMS'),
(153, 'CL_MC_Open2'),
(154, 'CL_MC_Pay True'),
(155, 'CL_MC_PIM_Sodimac'),
(156, 'CL_MC_PMM'),
(157, 'CL_MC_PMM_Migracion '),
(158, 'CL_MC_POS_Virtual'),
(159, 'CL_MC_Power_Pivot'),
(160, 'CL_MC_PRESENCIAL '),
(161, 'CL_MC_Promociones de Sodimac'),
(162, 'CL_MC_QuickPay'),
(163, 'CL_MC_QuickPay 2.0'),
(164, 'CL_MC_RIO'),
(165, 'CL_MC_SAB'),
(166, 'CL_MC_SAB TOTTUS CHILE'),
(167, 'CL_MC_SATIF'),
(168, 'CL_MC_SEGURO CONTACTO UNIFICADO'),
(169, 'CL_MC_SEGUROS B2C'),
(170, 'CL_MC_SEGUROS CONTACTO CORPORATIVO'),
(171, 'CL_MC_SEGUROS SFWEB'),
(172, 'CL_MC_SEGUROS SIGCO-JASPER-SINIESTRO'),
(173, 'CL_MC_Seguros_Venta_Transparente'),
(174, 'CL_MC_SIEBEL'),
(175, 'CL_MC_Siebel Sodimac'),
(176, 'CL_MC_Siebel_8.1'),
(177, 'CL_MC_SLI'),
(178, 'CL_MC_Sodimac_Switch_Transbank_POS'),
(179, 'CL_MC_SPR'),
(180, 'CL_MC_SRX'),
(181, 'CL_MC_SSEE2'),
(182, 'CL_MC_SVN bancofalabella.com'),
(183, 'CL_MC_SVN falabella.com'),
(184, 'CL_MC_Tasas y Comisiones'),
(185, 'CL_MC_TRL Migracion'),
(186, 'CL_MC_Viajes'),
(187, 'CL_MC_VIAJES PET'),
(188, 'CL_MC_VU_Banco'),
(189, 'CL_MC_VU_CMR'),
(190, 'CL_MC_WEB_CMR'),
(191, 'CL_MC_WMOS_BC'),
(192, 'CL_MC_WMOS_SLE'),
(193, 'CL_MC_WMOS_Tottus'),
(194, 'CL_MICROSTRATEGY'),
(195, 'CL_MONITOREO'),
(196, 'CL_Monitoreo Splunk'),
(197, 'CL_MonitorPlus'),
(198, 'CL_Motores de Riesgo CMR TEMP'),
(199, 'CL_Motores de Riesgo_CMR'),
(200, 'CL_MOTOR_DE_CUPONES'),
(201, 'CL_NETAPP-INGENIERIA'),
(202, 'CL_NOM Store Retail'),
(203, 'CL_ODBMS_URUGUAY'),
(204, 'CL_Panel Web_Contact Center'),
(205, 'CL_Panelweb_ContactCenter'),
(206, 'CL_Panel_Control_CMR'),
(207, 'CL_PANEL_DE_GESTION'),
(208, 'CL_PE_Shopping Precios'),
(209, 'CL_Plataforma_WIFI'),
(210, 'CL_Portal SOAP'),
(211, 'CL_Portal_Pagos_Sodimac'),
(212, 'CL_Productividad Laboral'),
(213, 'CL_Provisiones'),
(214, 'CL_Proyectos Guide Selling'),
(215, 'CL_Qlikeview'),
(216, 'CL_Recaudaciones_Lexicom'),
(217, 'CL_Reloj_Control'),
(218, 'CL_Retail Ideas'),
(219, 'CL_RTE_JAVA'),
(220, 'CL_SAB(TRADIS)'),
(221, 'CL_SANDBOX'),
(222, 'CL_SCA'),
(223, 'CL_Seguridad Perimetral'),
(224, 'CL_Seguros_Falabella_Workflow_Factura'),
(225, 'CL_Seguros_Portal Movil'),
(226, 'CL_SEGUROS_Portal _APP'),
(227, 'CL_Sinergia'),
(228, 'CL_SIR Falabella'),
(229, 'CL_SIR Sodimac'),
(230, 'CL_Sistema_Flejes_Chile'),
(231, 'CL_Sistema_Integral_Reparto'),
(232, 'CL_Sodimac Query'),
(233, 'CL_SPF'),
(234, 'CL_Tablets para Jefes y Gerentes'),
(235, 'CL_TEF_CMR'),
(236, 'CL_Tiendas Tradis'),
(237, 'CL_Tiendas_Juan_Valdez'),
(238, 'CL_Transferencia_de_Archivos'),
(239, 'CL_TRL'),
(240, 'CL_UNIGIS'),
(241, 'CL_USSD FALABELLA MOVIL'),
(242, 'CL_UXPOS'),
(243, 'CL_V2S_Sodimac'),
(244, 'CL_Venta en Linea'),
(245, 'CL_Ventas_Web'),
(246, 'CL_VISA'),
(247, 'CL_WebLogic'),
(248, 'CL_WS Canje de Tottus'),
(249, 'CL_XPC'),
(250, 'CORP_DTE'),
(251, 'CORP_MISION CRITICA'),
(252, 'CO_B2C_CallCenter'),
(253, 'CO_Biometria_Contingencia'),
(254, 'CO_Brief On Line'),
(255, 'CO_Contacto Corp BF'),
(256, 'CO_Contact_Center'),
(257, 'CO_CPI'),
(258, 'CO_CT Verde'),
(259, 'CO_DomainController'),
(260, 'CO_IBANKING'),
(261, 'CO_Identificacion_Biometrica_BF'),
(262, 'CO_MC_B2B_SODIMAC'),
(263, 'CO_MC_B2C_Falabella'),
(264, 'CO_MC_B2C_Sodimac'),
(265, 'CO_MC_BF_Omnichannel'),
(266, 'CO_MC_Core_Bancario-FlexCube'),
(267, 'CO_MC_CYBER'),
(268, 'CO_MC_Novios'),
(269, 'CO_MC_OMS'),
(270, 'CO_MC_SAB'),
(271, 'CO_MC_SRX'),
(272, 'CO_MC_VU_BF'),
(273, 'CO_Portal Bancario Empresas'),
(274, 'CO_SPF'),
(275, 'PE_AplicaciA3n diseno 20/20'),
(276, 'PE_Aplicativo Movil Tnda'),
(277, 'PE_ASL+Tottus'),
(278, 'PE_B2C Tottus ATG11'),
(279, 'PE_B2C_Call Center'),
(280, 'PE_Brief On Line'),
(281, 'PE_Contacto_Tottus_Peru'),
(282, 'PE_Contact_Center'),
(283, 'PE_CPI'),
(284, 'PE_DomainController'),
(285, 'PE_Extranet Pago CTS-CTA Sueldo'),
(286, 'PE_Falabella_Cupones de pago'),
(287, 'PE_Garantia_Extendida'),
(288, 'PE_IBANKING'),
(289, 'PE_Lavado de Activos'),
(290, 'PE_MAE_Ofivent'),
(291, 'PE_MC_Autoriza 7'),
(292, 'PE_MC_B2B_SODIMAC'),
(293, 'PE_MC_B2C_Falabella'),
(294, 'PE_MC_B2C_Sodimac'),
(295, 'PE_MC_B2C_Tottus'),
(296, 'PE_MC_BF_OmniChannel'),
(297, 'PE_MC_Cyber'),
(298, 'PE_MC_DTE'),
(299, 'PE_MC_Novios'),
(300, 'PE_MC_PMM'),
(301, 'PE_MC_PMM_Migracion'),
(302, 'PE_MC_SAB'),
(303, 'PE_MC_SAT'),
(304, 'PE_MC_SATIF'),
(305, 'PE_MC_SEGUROS SFWEB'),
(306, 'PE_MC_SRX'),
(307, 'PE_MC_VU_BF'),
(308, 'PE_MC_Wmos Sodimac'),
(309, 'PE_MC_Wmos Tottus'),
(310, 'PE_ODBMS_Sodimac'),
(311, 'PE_OPEN2'),
(312, 'PE_Reportes'),
(313, 'PE_SAT'),
(314, 'PE_SIGIC'),
(315, 'PE_Sistema_Flejes'),
(316, 'PE_Sistema_Seguimiento_Auditoria'),
(317, 'PE_Sodimac_Proveedores'),
(318, 'PE_SPF'),
(319, 'PE_SSEE'),
(320, 'PE_UXPOS'),
(321, 'PE_Venta en Linea'),
(322, 'PE_Web_Open_Plaza'),
(323, 'UR_B2C Rediseno Sodimac '),
(324, 'UR_Sitio Sodimac'),
(325, 'UR_Sodimac-Integracion'),
(326, 'No catalogado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sla`
--

CREATE TABLE `sla` (
  `CLI_ID` int(11) NOT NULL,
  `DIS_CRIT` varchar(50) DEFAULT NULL,
  `SLA_MED` time DEFAULT '00:03:00',
  `SLA_ALT` time DEFAULT '00:07:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisor`
--

CREATE TABLE `supervisor` (
  `USU_USER` varchar(20) NOT NULL,
  `SUP_NOM` char(255) NOT NULL,
  `SUP_APP` char(255) NOT NULL,
  `SUP_APM` char(255) DEFAULT NULL,
  `SUP_RUT` decimal(8,0) NOT NULL,
  `SUP_ANE` decimal(8,0) DEFAULT NULL,
  `SUP_TEL` decimal(8,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisor_gde`
--

CREATE TABLE `supervisor_gde` (
  `user` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name_a` varchar(255) DEFAULT NULL,
  `last_name_b` varchar(255) DEFAULT NULL,
  `annexed` varchar(255) DEFAULT NULL,
  `rut` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `supervisor_gde`
--

INSERT INTO `supervisor_gde` (`user`, `name`, `last_name_a`, `last_name_b`, `annexed`, `rut`, `phone`) VALUES
('sfigue', 'Sebastian', 'Figueroa', 'Mateo', '4784', NULL, '223874784'),
('nprado', 'Nelson', 'Prado', 'Villalobos', '8954', NULL, '123456'),
('syaksic', 'Sergio', 'Yaksic', 'besoain', '8714', NULL, '123456'),
('adm_redes', 'Administrador', 'Soporte', 'Redes', '2927', NULL, '74325295');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `TICKET_ID` varchar(50) NOT NULL,
  `TICKET_EST` varchar(50) NOT NULL,
  `TICKET_LAST_EST` varchar(50) DEFAULT NULL,
  `TICKET_CONT` int(11) NOT NULL DEFAULT '0',
  `TICKET_TSINI` datetime NOT NULL,
  `TICKET_TSFIN` datetime DEFAULT NULL,
  `TICKET_SEG_TSINI` datetime NOT NULL,
  `TICKET_SEG_TSFIN` datetime DEFAULT NULL,
  `TICKET_EMAIL` varchar(1024) NOT NULL,
  `TICKET_RESUMEN` varchar(50) NOT NULL,
  `TICKET_COD_SERV` varchar(100) NOT NULL,
  `TICKET_CLIID` int(11) NOT NULL,
  `TICKET_CREADOR` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_comentario`
--

CREATE TABLE `ticket_comentario` (
  `TICKET_COM_ID` int(11) UNSIGNED NOT NULL,
  `TICKET_ID` varchar(20) NOT NULL,
  `TICKET_COM_TS` datetime NOT NULL,
  `TICKET_COM_COM` varchar(21844) NOT NULL,
  `TICKET_COM_ACC` char(50) DEFAULT NULL,
  `TICKET_USU_USER` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_estado`
--

CREATE TABLE `ticket_estado` (
  `TCKESTADO_ID` int(11) NOT NULL,
  `TCKESTADO_INI` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_gev`
--

CREATE TABLE `ticket_gev` (
  `TCKGEV_ID` int(11) NOT NULL,
  `TICKET_ID` varchar(30) NOT NULL,
  `GEV_ID` int(11) NOT NULL,
  `TIPO_TICKET` varchar(10) NOT NULL,
  `TCKGEV_EST` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_seguimiento`
--

CREATE TABLE `ticket_seguimiento` (
  `TICKET_SEG_ID` int(11) NOT NULL,
  `TICKET_DIS_CRIT` int(11) NOT NULL,
  `TICKET_CLI_CRIT` int(11) NOT NULL,
  `TICKET_TESP` int(11) NOT NULL,
  `TICKET_CORSEG` varchar(2048) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_evento`
--

CREATE TABLE `tipo_evento` (
  `TEV_NOM` char(20) NOT NULL,
  `TEV_CRIT` int(11) NOT NULL,
  `TEV_DESC` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_evento_gde`
--

CREATE TABLE `tipo_evento_gde` (
  `type` varchar(50) DEFAULT NULL,
  `severity` varchar(50) DEFAULT NULL,
  `body` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_gde`
--

CREATE TABLE `usuario_gde` (
  `user` varchar(20) NOT NULL,
  `user_category` int(11) DEFAULT NULL,
  `user_priority` varchar(50) DEFAULT NULL,
  `user_mail` varchar(1024) DEFAULT NULL,
  `user_group` varchar(50) DEFAULT NULL,
  `user_nick` varchar(50) DEFAULT NULL,
  `user_pass` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_gde`
--

INSERT INTO `usuario_gde` (`user`, `user_category`, `user_priority`, `user_mail`, `user_group`, `user_nick`, `user_pass`) VALUES
('accenter', 1, NULL, 'acc@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('admin', 2, '', '', '', '', '21232f297a57a5a743894a0e4a801fc3'),
('adm_redes', 2, NULL, 'admredes@qw.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('bespinoza', 1, NULL, 'ext_baespinoza@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('ccuevas', 1, NULL, 'ext_chcuevas@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('crvalenz', 1, NULL, 'gestiondeeventos@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('csanchez', 4, NULL, 'cbsanchezg@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('dleiva', 4, NULL, 'dleiva@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('dsandoval', 1, NULL, 'dasandoval@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('ext_fantnovoa', 1, NULL, 'ext_fantnovoa@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('fbriones', 1, NULL, 'fabriones@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('fduarte', 1, NULL, 'ext_flduarte@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('fjjimenez', 1, NULL, 'fjjimenez@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('gcampos', 1, NULL, 'ext_gacamposm@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('jalexduarte', 1, NULL, 'ext_jalexduarte@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('jjsolis', 1, NULL, 'ext_jjsolis@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('jvera', 1, NULL, 'ext_jovera@Falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('llaciart', 4, NULL, 'llaciart@kudaw.com', NULL, NULL, '93b8b0664be84d8f113f9e911f727849'),
('maraya', 1, NULL, 'ext_mearaya@Falabella.cl', NULL, NULL, '6b061dcc72ca126d77362688f3e3adb9'),
('mfodic', 1, NULL, 'mfodic@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('mmmogollon', 1, NULL, 'mmmogollon@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('nprado', 2, NULL, 'nprado@falabella.cl', NULL, NULL, 'd8578edf8458ce06fbc5bb76a58c5ca4'),
('operador', 1, NULL, 'operador@falabella.cl', NULL, NULL, '06d4f07c943a4da1c8bfe591abbc3579'),
('oreyes', 1, NULL, 'oreyes@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('sfigue', 2, NULL, 'santfigueroa@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('sfigueroa', 1, NULL, 'santfigueroa@falabella.cl', NULL, NULL, '81dc9bdb52d04dc20036dbd8313ed055'),
('syaksic', 2, NULL, 'ext_sayaksic@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e'),
('vsepulveda', 3, NULL, 'vsepulveda@falabella.cl', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arquitectura_dispositivo`
--
ALTER TABLE `arquitectura_dispositivo`
  ADD PRIMARY KEY (`DIS_NOM`),
  ADD KEY `DISARQ_INDEX` (`DIS_NOM`);

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  ADD PRIMARY KEY (`AUD_ID`),
  ADD KEY `AUD_TS` (`AUD_TS`);

--
-- Indices de la tabla `auditoria_gde`
--
ALTER TABLE `auditoria_gde`
  ADD PRIMARY KEY (`id`),
  ADD KEY `AUD_TS` (`start_time`);

--
-- Indices de la tabla `categoria_gde`
--
ALTER TABLE `categoria_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`CLI_ID`);

--
-- Indices de la tabla `cliente_gde`
--
ALTER TABLE `cliente_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentario_gde`
--
ALTER TABLE `comentario_gde`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `user` (`user`),
  ADD KEY `status` (`status`);

--
-- Indices de la tabla `comentario_seguimiento`
--
ALTER TABLE `comentario_seguimiento`
  ADD PRIMARY KEY (`COMSC_ID`),
  ADD KEY `correo_comentario_index` (`CORSC_ID`),
  ADD KEY `COMSC_TS` (`COMSC_TS`);

--
-- Indices de la tabla `comentario_seguimiento_gde`
--
ALTER TABLE `comentario_seguimiento_gde`
  ADD PRIMARY KEY (`id`),
  ADD KEY `correo_comentario_index` (`seg_id`),
  ADD KEY `COMSC_TS` (`start_time`);

--
-- Indices de la tabla `comentario_seguimiento_interno`
--
ALTER TABLE `comentario_seguimiento_interno`
  ADD PRIMARY KEY (`COMSC2_ID`);

--
-- Indices de la tabla `contacto_dispositivo`
--
ALTER TABLE `contacto_dispositivo`
  ADD PRIMARY KEY (`DIS_NOM`),
  ADD KEY `DISCNT_INDEX` (`DIS_NOM`);

--
-- Indices de la tabla `contacto_dispositivo_gde`
--
ALTER TABLE `contacto_dispositivo_gde`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `DISCNT_INDEX` (`src_organization`);

--
-- Indices de la tabla `correo_nodo`
--
ALTER TABLE `correo_nodo`
  ADD PRIMARY KEY (`CORSC_ID`,`GEV_ID`),
  ADD KEY `correo_nodo_index` (`CORSC_ID`,`DIS_NOM`),
  ADD KEY `GEV_ID` (`GEV_ID`);

--
-- Indices de la tabla `correo_nodo_gde`
--
ALTER TABLE `correo_nodo_gde`
  ADD PRIMARY KEY (`id`,`group_id`),
  ADD KEY `correo_nodo_index` (`id`),
  ADD KEY `GEV_ID` (`group_id`);

--
-- Indices de la tabla `correo_respaldo`
--
ALTER TABLE `correo_respaldo`
  ADD PRIMARY KEY (`CORSC_ID`);

--
-- Indices de la tabla `correo_seguimiento`
--
ALTER TABLE `correo_seguimiento`
  ADD PRIMARY KEY (`CORSC_ID`);

--
-- Indices de la tabla `correo_seguimiento_gde`
--
ALTER TABLE `correo_seguimiento_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dispositivo`
--
ALTER TABLE `dispositivo`
  ADD PRIMARY KEY (`DIS_NOM`),
  ADD KEY `CLIENTE_DISPOSITIVO_INDEX` (`CLI_ID`,`DIS_NOM`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EST_NOM`);

--
-- Indices de la tabla `estado_gde`
--
ALTER TABLE `estado_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evento_gde`
--
ALTER TABLE `evento_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evento_seguimiento`
--
ALTER TABLE `evento_seguimiento`
  ADD PRIMARY KEY (`EVENTO_SEG_ID`);

--
-- Indices de la tabla `geografia_dispositivo`
--
ALTER TABLE `geografia_dispositivo`
  ADD PRIMARY KEY (`DIS_NOM`),
  ADD KEY `DISGEO_INDEX` (`DIS_NOM`),
  ADD KEY `GEOSUC_INDEX` (`GEOD_SUCURSAL`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`USU_USER`,`CLI_ID`),
  ADD KEY `FK_RELATIONSHIP_23` (`CLI_ID`);

--
-- Indices de la tabla `grupo_evento_gde`
--
ALTER TABLE `grupo_evento_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupo_gde`
--
ALTER TABLE `grupo_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`HIS_ID`),
  ADD KEY `de_historia` (`HIS_TS`),
  ADD KEY `FK_ESTADO_FINAL` (`EST_FIN`),
  ADD KEY `FK_ESTADO_INICIAL` (`EST_INI`),
  ADD KEY `FK_RELATIONSHIP_18` (`GEV_ID`),
  ADD KEY `FK_RELATIONSHIP_20` (`USU_USER`);

--
-- Indices de la tabla `historial_nodos`
--
ALTER TABLE `historial_nodos`
  ADD PRIMARY KEY (`HISTORIAL_ID`);

--
-- Indices de la tabla `historia_gde`
--
ALTER TABLE `historia_gde`
  ADD PRIMARY KEY (`id`),
  ADD KEY `de_historia` (`start_time`),
  ADD KEY `FK_ESTADO_FINAL` (`end_status`),
  ADD KEY `FK_ESTADO_INICIAL` (`start_status`),
  ADD KEY `FK_RELATIONSHIP_18` (`group_id`),
  ADD KEY `FK_RELATIONSHIP_20` (`user`);

--
-- Indices de la tabla `numbers`
--
ALTER TABLE `numbers`
  ADD PRIMARY KEY (`n`);

--
-- Indices de la tabla `operador_gde`
--
ALTER TABLE `operador_gde`
  ADD PRIMARY KEY (`user`),
  ADD KEY `user_sup` (`user_sup`);

--
-- Indices de la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respaldo_nodos`
--
ALTER TABLE `respaldo_nodos`
  ADD PRIMARY KEY (`RESPALDO_ID`);

--
-- Indices de la tabla `servicios_gde`
--
ALTER TABLE `servicios_gde`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `supervisor`
--
ALTER TABLE `supervisor`
  ADD PRIMARY KEY (`USU_USER`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`TICKET_ID`),
  ADD KEY `index_ticket_tsini` (`TICKET_TSINI`),
  ADD KEY `TICKET_CREADOR` (`TICKET_EMAIL`(767));

--
-- Indices de la tabla `ticket_comentario`
--
ALTER TABLE `ticket_comentario`
  ADD PRIMARY KEY (`TICKET_COM_ID`);

--
-- Indices de la tabla `ticket_estado`
--
ALTER TABLE `ticket_estado`
  ADD PRIMARY KEY (`TCKESTADO_ID`);

--
-- Indices de la tabla `ticket_gev`
--
ALTER TABLE `ticket_gev`
  ADD PRIMARY KEY (`TCKGEV_ID`);

--
-- Indices de la tabla `ticket_seguimiento`
--
ALTER TABLE `ticket_seguimiento`
  ADD PRIMARY KEY (`TICKET_SEG_ID`);

--
-- Indices de la tabla `tipo_evento`
--
ALTER TABLE `tipo_evento`
  ADD PRIMARY KEY (`TEV_NOM`);

--
-- Indices de la tabla `usuario_gde`
--
ALTER TABLE `usuario_gde`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  MODIFY `AUD_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auditoria_gde`
--
ALTER TABLE `auditoria_gde`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1061;
--
-- AUTO_INCREMENT de la tabla `categoria_gde`
--
ALTER TABLE `categoria_gde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1024;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `CLI_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliente_gde`
--
ALTER TABLE `cliente_gde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comentario_gde`
--
ALTER TABLE `comentario_gde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=349145;
--
-- AUTO_INCREMENT de la tabla `comentario_seguimiento`
--
ALTER TABLE `comentario_seguimiento`
  MODIFY `COMSC_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comentario_seguimiento_gde`
--
ALTER TABLE `comentario_seguimiento_gde`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `comentario_seguimiento_interno`
--
ALTER TABLE `comentario_seguimiento_interno`
  MODIFY `COMSC2_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contacto_dispositivo_gde`
--
ALTER TABLE `contacto_dispositivo_gde`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT de la tabla `correo_seguimiento`
--
ALTER TABLE `correo_seguimiento`
  MODIFY `CORSC_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `correo_seguimiento_gde`
--
ALTER TABLE `correo_seguimiento_gde`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8204;
--
-- AUTO_INCREMENT de la tabla `estado_gde`
--
ALTER TABLE `estado_gde`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `evento_gde`
--
ALTER TABLE `evento_gde`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1380029;
--
-- AUTO_INCREMENT de la tabla `evento_seguimiento`
--
ALTER TABLE `evento_seguimiento`
  MODIFY `EVENTO_SEG_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `grupo_evento_gde`
--
ALTER TABLE `grupo_evento_gde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294775;
--
-- AUTO_INCREMENT de la tabla `grupo_gde`
--
ALTER TABLE `grupo_gde`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;
--
-- AUTO_INCREMENT de la tabla `historia`
--
ALTER TABLE `historia`
  MODIFY `HIS_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `historial_nodos`
--
ALTER TABLE `historial_nodos`
  MODIFY `HISTORIAL_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `historia_gde`
--
ALTER TABLE `historia_gde`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231211;
--
-- AUTO_INCREMENT de la tabla `parametros`
--
ALTER TABLE `parametros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `respaldo_nodos`
--
ALTER TABLE `respaldo_nodos`
  MODIFY `RESPALDO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicios_gde`
--
ALTER TABLE `servicios_gde`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;
--
-- AUTO_INCREMENT de la tabla `ticket_comentario`
--
ALTER TABLE `ticket_comentario`
  MODIFY `TICKET_COM_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ticket_estado`
--
ALTER TABLE `ticket_estado`
  MODIFY `TCKESTADO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ticket_gev`
--
ALTER TABLE `ticket_gev`
  MODIFY `TCKGEV_ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
