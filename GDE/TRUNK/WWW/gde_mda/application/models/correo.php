<?php
class Correo extends CI_Model {

/*	--------------------------------------------------
	:: Semaforo de seguimiento
	-------------------------------------------------- */
	/**
	* Cantidad de correos que se deben enviar (seguimiento), en base a cliente consultado
	*
	* @input	IdCliente (String o Array) ID del o los clientes a consultar
	* @return	Int cantidad de correos con seguimiento vencido
	*/
	public function cantidadSeguimientosActivos($IdCliente){
		$listaCliente = $this->transformClientList($IdCliente);
		if( $listaCliente == ''){ return 0;}
		$sql	=	"select count(distinct `CORSC_ID`) as cantidadSeguimientosActivos from correo_seguimiento join correo_nodo USING (`CORSC_ID`) where TIME_TO_SEC(TIMEDIFF(`CORSC_TSSEG_FIN`,NOW())) <= 5 and correo_seguimiento.CORSC_EST=1 and `CLI_ID` IN (".$listaCliente.")";
		$result	= $this->db->query($sql, array($listaCliente)); 
		return $result->row()->cantidadSeguimientosActivos;
	}

/*	--------------------------------------------------
	:: Seguimientos activos e inactivos
	-------------------------------------------------- */
	/**
	* Listado de correos de seguimientos ACTIVOS para los clientes consultados
	*
	* @input	IdCliente (String o Array) ID del o los clientes a consultar
	* @return	Int cantidad de correos con seguimiento vencido
	*/
	public function listarSeguimientosActivos(){
		//$listaCliente = $IdCliente;
		$estadoSeguimiento = 1;
		
		$this->db->select('correo_seguimiento_gde.id');
		$this->db->select('IF(`seg_start_time`=\'0000-00-00 00:00:00\', start_time, seg_start_time) as HORA', FALSE);
		$this->db->select('GROUP_CONCAT(DISTINCT correo_nodo_gde.type SEPARATOR \'<br />\') as DIS_NOMS', FALSE);
		$this->db->select('TIME_TO_SEC(TIMEDIFF(seg_end_time,NOW())) as SEGUIMIENTO', FALSE);
		$this->db->select('correo_seguimiento_gde.type');
		$this->db->from('correo_seguimiento_gde');
		$this->db->join('correo_nodo_gde', 'correo_seguimiento_gde.id = correo_nodo_gde.id');
		$this->db->where('correo_seguimiento_gde.status', $estadoSeguimiento);
		//$this->db->where_in('CLI_ID', $listaCliente);
		$this->db->group_by('correo_seguimiento_gde.id');
		$result = $this->db->get();
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Se ha generado un problema al consultar la BD');
		}
		else{
			$this->load->library('table');
			$tmpl = array (
				'table_open' => '<table id="tablaSeguimientosActivos" border="1" cellpadding="2" cellspacing="1" class="mytable">',
			);
			
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( array('Identificador', 'Hora', 'Tipo de Alerta', 'Minutos Restantes','Fuente', 'Cancelar Seguimiento') );
			$this->table->set_template($tmpl);
			
			$queryStatus = array('return' => TRUE, 'result' => $this->table->generate($result));
		}
		return $queryStatus;
	}
	
	/**
	* Listado de correos de seguimientos VENCIDOS para los clientes consultados
	*
	* @input	IdCliente (String o Array) ID del o los clientes a consultar
	* @return	Int cantidad de correos con seguimiento vencido
	*/
	public function listarSeguimientosVencidos($IdCliente){
		$this->load->library('table');
		
		$listaCliente = $IdCliente;
		$estadoSeguimiento = 0;
		
		$this->db->select('correo_seguimiento.CORSC_ID');
		$this->db->select('correo_seguimiento.CORSC_TSINI');
		$this->db->select('correo_seguimiento.CORSC_TSFIN');
		$this->db->select('GROUP_CONCAT(DISTINCT DIS_NOM SEPARATOR \'<br />\') as DIS_NOMS', FALSE);
		$this->db->select('correo_seguimiento.CORSC_LSTCOM');
		$this->db->from('correo_seguimiento');
		$this->db->join('correo_nodo', 'correo_seguimiento.CORSC_ID = correo_nodo.CORSC_ID');
		$this->db->where('correo_seguimiento.CORSC_EST', $estadoSeguimiento);
		$this->db->where('correo_seguimiento.CORSC_TSFIN > DATE_SUB(CURDATE(),INTERVAL 30 DAY)',NULL,FALSE);
		$this->db->where_in('CLI_ID', $listaCliente);
		$this->db->group_by('correo_seguimiento.CORSC_ID');
		$result = $this->db->get();
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Se ha generado un problema al consultar la BD');
		}
		else{
			$this->load->library('table');
			$tmpl = array (
				'table_open'  => '<table id="tablaSeguimientosCancelados" border="1" cellpadding="2" cellspacing="1" class="mytable">',
			);
			
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( array('Identificador','Hora Inicio Seg.', 'Hora Cancelado', 'Nodos', 'Motivo') );
			$this->table->set_template($tmpl);
			
			$queryStatus = array('return' => TRUE, 'result' => $this->table->generate($result));
		}
		return $queryStatus;
	}
	
	/**
	* Cancela un seguimiento en particular
	*
	* @input	idSeguimiento Int ID de seguimiento
	* @return	Array Status de la cancelacion de seguimiento
	*/
	public function cancelarSeguimientoActivo($idSeguimiento, $comentarioCancelacion){
		$queryStatus = array();
		$nuevoEstadoSeguimiento = 0;
		
		$data = array(
			'CORSC_EST'		=> $nuevoEstadoSeguimiento,
			'CORSC_LSTCOM'	=> $comentarioCancelacion,
			'CORSC_TSFIN'	=> date('Y-m-d H:i:s')
		);
		$this->db->where('CORSC_ID', $idSeguimiento);
		$this->db->update('correo_seguimiento', $data); 
		
		$data = array(
			'CORSC_ID'	=> $idSeguimiento,
			'COMSC_COM'	=> $comentarioCancelacion,
			'COMSC_TS'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('comentario_seguimiento', $data);
		
		$this->db->select('GEV_ID');
		$this->db->from('correo_nodo');
		$this->db->where('CORSC_ID', $idSeguimiento);
		$result = $this->db->get();
		
		foreach($result->result() as $row){
			$this->db->insert('comentario', array('GEV_ID' => $row->GEV_ID, 'COM_COM' => 'Se cancela el seguimiento por el siguiente motivo: '.$comentarioCancelacion, 'EST_NOM' => 'cancelseg', 'COM_TS' => date("Y-m-d H:i:s"), 'USU_USER' => $this->session->userdata('username')));
		}
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Error al intentar cancelar seguimiento');
		}
		else{
			$queryStatus = array('return' => TRUE);
		}
		return $queryStatus;
	}
	
/*	--------------------------------------------------
	:: Comentarios de Seguimiento
	-------------------------------------------------- */
	/**
	* Registra un comentario asociado a un ID de seguimiento
	*
	* @input	idSeguimiento Int ID de seguimiento
	* @return	Array Status de la insercion del comentario 
	*/
	public function registrarComentarioSeguimiento($idSeguimiento, $comentarioSeguimiento){
		$queryStatus = array();
		
		/*$data = array(
			'info'	=> $comentarioSeguimiento
		);
		$this->db->where('seg_id', $idSeguimiento);
		$this->db->update('comentario_seguimiento_gde', $data);*/
		
		$data = array(
			'seg_id'	 => $idSeguimiento,
			'info'	     => $comentarioSeguimiento,
			'start_time' => date('Y-m-d H:i:s')
		);
		$this->db->insert('comentario_seguimiento_gde', $data);
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Error al intentar registrar comentario');
		}
		else{
			$queryStatus = array('return' => TRUE);
		}
		return $queryStatus;
	}
	
	
	/**
	* Edita un comentario asociado a un ID de seguimiento
	*
	* @input	idSeguimiento Int ID de seguimiento
	* @return	Array Status de la insercion del comentario 
	*/
	public function editarComentario($idcom, $comentarioSeguimiento){
		$queryStatus = array();
		
		$data = array(
			'COMSC_COM'	=> $comentarioSeguimiento,
		);
		$this->db->where('COMSC_ID', $idcom);
		$this->db->update('comentario_seguimiento', $data);
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Error al intentar registrar comentario');
		}
		else{
			$queryStatus = array('return' => TRUE);
		}
		return $queryStatus;
	}
	
		/**
	* Edita un comentario asociado a un ID de seguimiento
	*
	* @input	idSeguimiento Int ID de seguimiento
	* @return	Array Status de la insercion del comentario 
	*/
	public function editarComentario2($idcom, $comentarioSeguimiento){
		$queryStatus = array();
		
		$data = array(
			'COMSC_COM'	=> $comentarioSeguimiento,
		);
		$this->db->where('COMSC2_ID', $idcom);
		$this->db->update('comentario_seguimiento_interno', $data);
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Error al intentar registrar comentario');
		}
		else{
			$queryStatus = array('return' => TRUE);
		}
		return $queryStatus;
	}
	
	/**
	* Registra un comentario asociado a un ID de seguimiento
	*
	* @input	idSeguimiento Int ID de seguimiento
	* @return	Array Status de la insercion del comentario 
	*/
	public function registrarComentarioSeguimiento2($idSeguimiento, $comentarioSeguimiento){
		$queryStatus = array();
		
		$data = array(
			'CORSC_ID'	=> $idSeguimiento,
			'COMSC_COM'	=> $comentarioSeguimiento,
			'COMSC_TS'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('comentario_seguimiento_interno', $data);
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Error al intentar registrar comentario');
		}
		else{
			$queryStatus = array('return' => TRUE);
		}
		return $queryStatus;
	}
	
	
	/**
	* Listado de comentarios asociados a un seguimiento en particular
	*
	* @input		idSeguimiento Int ID de seguimiento
	* @return	HTML(string) Tabla debidamente formada con los comentarios asociados al ID de seguimiento 
	*/
	public function listarComentariosSeguimiento($idSeguimiento){
		$this->load->library('table');
		$queryStatus = array();
		
		$sql = "SELECT `id`,`start_time`,`info` FROM `comentario_seguimiento_gde` WHERE `seg_id` = ?";
		$result	= $this->db->query($sql, array($idSeguimiento)); 
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Se ha generado un problema');
		}
		else{
			$this->table->set_heading('id','Hora registrada', 'Comentario de Seguimiento');
			$queryStatus = array('return' => TRUE, 'result' => $this->table->generate($result));
		}
		return $queryStatus;
	}
	
	
	/**
	* Listado de comentarios asociados a un seguimiento en particular
	*
	* @input		idSeguimiento Int ID de seguimiento
	* @return	HTML(string) Tabla debidamente formada con los comentarios asociados al ID de seguimiento 
	*/
	public function listarComentariosSeguimiento2($idSeguimiento){
		$this->load->library('table');
		$queryStatus = array();
		
		$sql = "SELECT `COMSC2_ID`,`COMSC_TS`,`COMSC_COM` FROM `comentario_seguimiento_interno` WHERE `CORSC_ID` = ?";
		$result	= $this->db->query($sql, array($idSeguimiento)); 
		
		if( $this->db->_error_number() > 0){
			log_message('error', 'Se ha presentado un error en la BD, codigo ' . $this->db->_error_number());
			log_message('error', $this->db->_error_message());
			$queryStatus = array('return' => FALSE, 'error' => 'Se ha generado un problema');
		}
		else{
			$this->table->set_heading('id', 'Hora registrada', 'Comentario de Seguimiento');
			$queryStatus = array('return' => TRUE, 'result' => $this->table->generate($result));
		}
		return $queryStatus;
	}
	
	
	public function listarComentariosSeguimiento_new($idSeguimiento){
		$this->load->library('table');
		$queryStatus = array();
		
		$sql = "SELECT DATE_FORMAT(`COMSC_TS`,'%d/%m/%Y %H:%i:%s') as hora,`COMSC_COM` FROM `comentario_seguimiento` WHERE `CORSC_ID` = ?";
		$result	= $this->db->query($sql, array($idSeguimiento)); 
		
		$tmpl = array (
			'table_open'			=> '<table style="font-size: x-small; font-family: arial, sans-serif;">',
			'cell_start'			=> '<td style="padding:8px; font-family: arial, sans-serif;">',
			'cell_alt_start'		=> '<td style="padding:8px; font-family: arial, sans-serif;">',
		);
		$this->table->set_heading('', '');
		$this->table->set_template($tmpl);
		
		return $this->table->generate($result);
	}

/*	--------------------------------------------------
	:: Funciones auxiliares
	-------------------------------------------------- */
	
	/**
	* Transforma ids de clientes a un String
	*
	* @input		IdCliente (String o Array) ID del o los clientes a consultar
	* @return	String ID del o los clientes
	*/
	private function transformClientList($IdsClientes){
		$listaCliente = '';
		
		if(is_null($IdsClientes)){ // Si se recibe valor nulo
			$listaCliente = '';
		}
		elseif(is_numeric($IdsClientes)){ // Si se recibe un numero
			$listaCliente = strval($IdsClientes);
		}
		
		elseif(gettype($IdsClientes) == 'array'){
			$listaCliente = implode(',', $IdsClientes); //Si se recibe un array
		}
		else{
			$listaCliente = $IdCliente;
		}
		
		return $listaCliente;
	}
	
	/*	--------------------------------------------------
	:: Envio de Correos
	-------------------------------------------------- */
	
	/**
	* Correos que se envian.
	*
	* @input		postArray (String o Array) ID del o los clientes a consultar
	* @return	Int cantidad de correos con seguimiento vencido
	*/
	public function registrarCorreos($postArray){
		$nodos = explode(";",$postArray['table3']);
		for($i=0;$i<count($nodos)-1;$i++){
			$filas[$i] = $nodos[$i];
		}
		$data = array(
			'CORSC_TSINI'		=> date('Y-m-d H:i:s'),
			'CORSC_TSSEG_FIN'	=> date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'CORSC_EST'			=> 1,
			'CORSC_TIPO'		=> $postArray['tipomail'],
			'CORSC_LSTCOM'		=> $postArray['obs']
		);
		
		$this->db->insert('correo_seguimiento', $data);
		
		$this->db->select_max('CORSC_ID');
		$this->db->from('correo_seguimiento');
		$result1 = $this->db->get();
		
		$this->db->select('CLI_ID');
		$this->db->from('cliente');
		$this->db->where('CLI_NOM', $postArray['clientehid']);
		$result2 = $this->db->get();
		
		foreach($result1->result() as $row){
			$res1["CORSC_ID"] = $row->CORSC_ID;
		}
		foreach($result2->result() as $row){
			$res2["CLI_ID"] = $row->CLI_ID;
		}
		
		$i = 0;
		foreach ($filas as $row) {
			$cel = explode(",",$row);
			if($i == 0){
				$gevid = $cel[0];
				$i++;
			}
			else{
				$gevid = $gevid.','.$cel[0];
			}
		}
		foreach ($filas as $row) {
			$cel = explode(",",$row);
			$data2 = array(
				'CORSC_ID'	=> $res1["CORSC_ID"],
				'CLI_ID'		=> $res2["CLI_ID"],
				'DIS_NOM'	=> $cel[3],
				'GEV_ID'		=> $cel[0]
			);
			$this->db->insert('correo_nodo', $data2);
			
			$data3 = array(
				'GEV_ID'		=> $cel[0],
				'COM_COM'	=> 'Se envia correo a '.$postArray['para'].' con copia a '.$postArray['copia'].' ++ Con asunto: '.$postArray['asunto'].' ++ Asociado a los ids: '.$gevid,
				'EST_NOM'	=> 'correo',
				'COM_TS'		=> date("Y-m-d H:i:s"),
				'USU_USER'	=> $this->session->userdata('username')
			);
			$this->db->insert('comentario', $data3);
		}
		$respaldocorreo = array(
			'CORRES_FROM_NAME'	=> $postArray['desde_nom'],
			'CORRES_FROM_MAIL'	=> $postArray['desde'],
			'CORSC_ID'				=> $res1["CORSC_ID"],
			'CORRES_TO'				=> $postArray['para'],
			'CORRES_CC'				=> $postArray['copia'],
			'CORRES_ASU'			=> $postArray['asunto'],
			'CORRES_CLI'			=> $postArray['cliente'],
			'CORRES_NUMIN'			=> $postArray['numin'],
			'CORRES_ESTACT'		=> $postArray['estactividad'],
			'CORRES_DESCEV'		=> $postArray['desevento'],
			'CORRES_ACCREAL'		=> $postArray['accrealiz'],
			'CORRES_OBS'			=> $postArray['obs'],
			'CORRES_TSINI'			=> $postArray['fecini'],
			'CORRES_TSFIN'			=> $postArray['fecter'],
			'CORRES_TIPAL'			=> $postArray['tipoevento'],
			'CORRES_ALEQ'			=> $postArray['alaeq'],
			'CORRES_CODSERV'		=> $postArray['codservicio'],
			'CORRES_SUCUR'			=> $postArray['sucursales'],
			'CORRES_NOM'			=> $postArray['nombre'],
			'CORRES_GLOSA'			=> $postArray['firma'],
			'CORRES_EMAIL'			=> $postArray['email'],
			'CORRES_TEL'			=> $postArray['fono']
			);
		$this->db->insert('correo_respaldo', $respaldocorreo);
	}
	
	public function registrarCorreos_new($postArray){
		$res1["id"] = '';

		$data = array(
			'start_time'	 => date('Y-m-d H:i:s'),
			'seg_start_time' => '0000-00-00 00:00:00',
			'seg_end_time'	 => date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'status'		 => 1,
			'count'			 => 0,
			'type'			 => $postArray['tipomail'],
			'info'			 => $postArray['obs'],
			'group_id'		 =>  $postArray['group_id']
		);
		$this->db->insert('correo_seguimiento_gde', $data);

		$this->db->select_max('id');
		$this->db->from('correo_seguimiento_gde');
		$result1 = $this->db->get();
		
		/*foreach($result1->result() as $row){
			$res1["id"] = $row->id;
			$data2 = array(
				'seg_id'	 => $row->id,
				'info'		 => $postArray['obs'],
				'start_time' => date('Y-m-d H:i:s')
			);
		}
		$this->db->insert('comentario_seguimiento_gde', $data2);

		$data3 = array(
			'id'     	=> $res1["id"],
			'type'      => $postArray['mail_tipo_aler'],
			'group_id'	=> $postArray['group_id']
		);
		$this->db->insert('correo_nodo_gde', $data3);*/
		
		$data4 = array(
			'group_id'   => $postArray['group_id'],
			'info'	     => 'Se envia correo manual a '.$postArray['para'].' con copia a '.$postArray['copia'].' ++ Con asunto: '.$postArray['asunto'].' ++ Con la siguiente observacion:'.$postArray['obs'],
			'type'	     => 'correo',
			'start_time' => date("Y-m-d H:i:s"),
			'user'	     => $this->session->userdata('username')
		);
		$this->db->insert('comentario_gde', $data4);
		
		//HUGO
		$this->db->where('id',$postArray['group_id']);
		$this->db->update('grupo_evento_gde', array('send_mail' => 0));
	}
	
	public function registrarCorreos2($postArray){
		$data = array(
			'CORSC_TSINI'			=> date('Y-m-d H:i:s'),
			'CORSC_TSSEG_FIN'		=> date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'CORSC_EST'				=> 1,
			'CORSC_TIPO'			=> $postArray['tipomail'],
			'CORSC_LSTCOM'			=> $postArray['obs']
		);
		
		$this->db->insert('correo_seguimiento', $data);
		
		$this->db->select_max('CORSC_ID');
		$this->db->from('correo_seguimiento');
		$result1 = $this->db->get();
	
		$this->db->select('TICKET_CLIID');
		$this->db->from('ticket');
		$this->db->where('TICKET_ID', $postArray['numin']);
		$result2 = $this->db->get();
		
		foreach($result1->result() as $row){
			$res["CORSC_ID"] = $row->CORSC_ID;
		}
		
		foreach($result2->result() as $row){
			$res["CLI_ID"] = $row->TICKET_CLIID;
		}
		
		foreach ($postArray['nodos'] as $nodo) {
			$cel = explode(",",$nodo);
			$data2 = array(
				'CORSC_ID'	=> $res["CORSC_ID"],
				'CLI_ID'		=> $res["CLI_ID"],
				'DIS_NOM'	=> $cel[0],
				'GEV_ID'		=> $cel[1]
			);
			$this->db->insert('correo_nodo', $data2);
			
			$data3 = array(
				'GEV_ID'		=> $cel[0],
				'COM_COM'	=> 'Se envia correo a '.$postArray['para'].' con copia a '.$postArray['copia'].' ++ Con asunto: '.$postArray['asunto'],
				'EST_NOM'	=> 'correo',
				'COM_TS'		=> date("Y-m-d H:i:s"),
				'USU_USER'	=> $this->session->userdata('username')
			);
			$this->db->insert('comentario', $data3);
		}
		
		$respaldocorreo = array(
			'CORRES_FROM_NAME'	=> $postArray['desde_nom'],
			'CORRES_FROM_MAIL'	=> $postArray['desde'],
			'CORSC_ID'				=> $res["CORSC_ID"],
			'CORRES_TO'				=> $postArray['para'],
			'CORRES_CC'				=> $postArray['copia'],
			'CORRES_ASU'			=> $postArray['asunto'],
			'CORRES_CLI'			=> $postArray['cliente'],
			'CORRES_NUMIN'			=> $postArray['numin'],
			'CORRES_ESTACT'		=> $postArray['estactividad'],
			'CORRES_DESCEV'		=> $postArray['desevento'],
			'CORRES_ACCREAL'		=> $postArray['accrealiz'],
			'CORRES_OBS'			=> $postArray['obs'],
			'CORRES_TSINI'			=> $postArray['fecini'],
			'CORRES_TSFIN'			=> $postArray['fecter'],
			'CORRES_TIPAL'			=> $postArray['tipoevento'],
			'CORRES_ALEQ'			=> $postArray['alaeq'],
			'CORRES_CODSERV'		=> $postArray['codservicio'],
			'CORRES_SUCUR'			=> $postArray['sucursales'],
			'CORRES_NOM'			=> $postArray['nombre'],
			'CORRES_GLOSA'			=> $postArray['firma'],
			'CORRES_EMAIL'			=> $postArray['email'],
			'CORRES_TEL'			=> $postArray['fono']
		);
		$this->db->insert('correo_respaldo', $respaldocorreo);
	}
	
	public function registrarError($postArray,$Error){
		$data = array(
			'COM_COM'	=> 'No se logra realizar el envio de correo '.$Error,
			'EST_NOM'	=> 'correoerr',
			'COM_TS'		=> date("Y-m-d H:i:s"),
			'USU_USER'	=> $this->session->userdata('username')
		);
		$this->db->insert('comentario', $data);
	}
	
	public function updateCorreos($postArray){
		$filas = array();
		$res2 = array();
		
		$nodos = explode(";",$postArray['table']);
		for($i=0;$i<count($nodos);$i++){
			$filas[$i] = $nodos[$i];
		}
		$seg["CORSC_ID"] = $postArray['segid'];
		
		$this->db->select('CORSC_CNT');
		$this->db->from('correo_seguimiento');
		$this->db->where('CORSC_ID', $seg["CORSC_ID"]);
		$CORSC_CNT = $this->db->get();
		foreach($CORSC_CNT->result() as $row){
			$CORSC_CNT2 = $row->CORSC_CNT;
		}
		$CORSC_CNT2 = (int)$CORSC_CNT2 + 1;
		$data = array(
			'CORSC_TSSEG_FIN'	=> date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'CORSC_TSSEG_INI'	=> date('Y-m-d H:i:s'),
			'CORSC_CNT'			=> $CORSC_CNT2
		);
		
		$this->db->where('CORSC_ID',$seg["CORSC_ID"]);
		$this->db->update('correo_seguimiento', $data);
		
		if ($this->db->affected_rows() > 0)
			log_message('error', 'El proceso UPDATE fue correcto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		else
			log_message('error', 'El proceso UPDATE fue incorrecto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		$this->db->select('CLI_ID');
		$this->db->from('cliente');
		$this->db->where('CLI_NOM', $postArray['clientehid']);
		$result2 = $this->db->get();
		
		foreach($result2->result() as $row){
			$res2["CLI_ID"] = $row->CLI_ID;
		}
		
		$j = 0;
		foreach ($filas as $row) {
			// $j++;
			$cel = explode(",",$row);
			if($j == 0){
				$gevid = $cel[0];
				$j++;
			}
			else{
				$gevid = $gevid.','.$cel[0];
			}
		}
		log_message('error', 'Proceso DELETE:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$delete = $this->db->delete('correo_nodo', array('CORSC_ID' => $postArray['segid']));
		log_message('error', 'Fin Proceso DELETE:'.$delete.' - '.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		log_message('error', 'Proceso INSERTANODOS:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$insertanodos = $this->insertarNodos($filas,$gevid,$postArray['segid'],$res2["CLI_ID"],$postArray);
		log_message('error', 'Fin Proceso INSERTANODOS:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		$respaldocorreo = array(
			'CORRES_TO'			=> $postArray['para'],
			'CORRES_CC'			=> $postArray['copia'],
			'CORRES_ASU'		=> $postArray['asunto'],
			'CORRES_CLI'		=> $postArray['cliente'],
			'CORRES_NUMIN'		=> $postArray['numin'],
			'CORRES_ESTACT'	=> $postArray['estactividad'],
			'CORRES_DESCEV'	=> $postArray['desevento'],
			'CORRES_ACCREAL'	=> $postArray['accrealiz'],
			'CORRES_OBS'		=> $postArray['obs'],
			'CORRES_TSINI'		=> $postArray['fecini'],
			'CORRES_TSFIN'		=> $postArray['fecter'],
			'CORRES_TIPAL'		=> $postArray['tipoevento'],
			'CORRES_ALEQ'		=> $postArray['alaeq'],
			'CORRES_CODSERV'	=> $postArray['codservicio'],
			'CORRES_SUCUR'		=> $postArray['sucursales'],
			'CORRES_NOM'		=> $postArray['nombre'],
			'CORRES_GLOSA'		=> $postArray['firma'],
			'CORRES_EMAIL'		=> $postArray['email'],
			'CORRES_TEL'		=> $postArray['fono']
			);
		$this->db->where('CORSC_ID',$seg["CORSC_ID"]);
		$this->db->update('correo_respaldo', $respaldocorreo);
		
		if ($this->db->affected_rows() > 0)
			log_message('error', 'El proceso UPDATE CORREORESPALDO fue correcto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		else
			log_message('error', 'El proceso UPDATE CORREORESPALDO fue incorrecto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
			
	}
	
	public function updateCorreos_new($postArray){
		$filas = array();
		$res2 = array();
		
		$nodos = explode(";",$postArray['table']);
		for($i=0;$i<count($nodos);$i++){
			$filas[$i] = $nodos[$i];
		}
		$seg["CORSC_ID"] = $postArray['segid'];
		
		$data2 = array(
			'CORSC_ID'	=> $postArray['segid'],
			'COMSC_COM'	=> $postArray['obs'],
			'COMSC_TS'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('comentario_seguimiento', $data2);
		
		$this->db->select('CORSC_CNT');
		$this->db->from('correo_seguimiento');
		$this->db->where('CORSC_ID', $seg["CORSC_ID"]);
		$CORSC_CNT = $this->db->get();
		foreach($CORSC_CNT->result() as $row){
			$CORSC_CNT2 = $row->CORSC_CNT;
		}
		$CORSC_CNT2 = (int)$CORSC_CNT2 + 1;
		$data = array(
			'CORSC_TSSEG_FIN'	=> date("Y-m-d H:i:s", mktime(date("H")+1, date("i"), date("s"), date("m"),date("d"),date("Y"))),
			'CORSC_TSSEG_INI'	=> date('Y-m-d H:i:s'),
			'CORSC_CNT'			=> $CORSC_CNT2
		);
		
		$this->db->where('CORSC_ID',$seg["CORSC_ID"]);
		$this->db->update('correo_seguimiento', $data);
		
		if ($this->db->affected_rows() > 0)
			log_message('error', 'El proceso UPDATE fue correcto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		else
			log_message('error', 'El proceso UPDATE fue incorrecto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		$this->db->select('CLI_ID');
		$this->db->from('cliente');
		$this->db->where('CLI_NOM', $postArray['clientehid']);
		$result2 = $this->db->get();
		
		foreach($result2->result() as $row){
			$res2["CLI_ID"] = $row->CLI_ID;
		}
		
		$j = 0;
		foreach ($filas as $row) {
			// $j++;
			$cel = explode(",",$row);
			if($j == 0){
				$gevid = $cel[0];
				$j++;
			}
			else{
				$gevid = $gevid.','.$cel[0];
			}
		}
		log_message('error', 'Proceso DELETE:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$delete = $this->db->delete('correo_nodo', array('CORSC_ID' => $postArray['segid']));
		log_message('error', 'Fin Proceso DELETE:'.$delete.' - '.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		log_message('error', 'Proceso INSERTANODOS:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$insertanodos = $this->insertarNodos($filas,$gevid,$postArray['segid'],$res2["CLI_ID"],$postArray);
		log_message('error', 'Fin Proceso INSERTANODOS:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		$respaldocorreo = array(
			'CORRES_TO'				=> $postArray['para'],
			'CORRES_CC'				=> $postArray['copia'],
			'CORRES_ASU'			=> $postArray['asunto'],
			'CORRES_CLI'			=> $postArray['cliente'],
			'CORRES_NUMIN'			=> $postArray['numin'],
			'CORRES_OBS'			=> $postArray['obs'],
			'CORRES_TSINI'			=> $postArray['fecini'],
			'CORRES_TSFIN'			=> $postArray['fecter'],
			'CORRES_TIPAL'			=> $postArray['tipoevento'],
			'CORRES_ALEQ'			=> $postArray['alaeq'],
			'CORRES_DESCEV'		=> $postArray['desevento'],
			'CORRES_CODSERV'		=> $postArray['codservicio'],
			'CORRES_SUCUR'			=> $postArray['sucursales'],
			'CORRES_NOMESTAB'		=> $postArray['nombestab'],
			'CORRES_ESTSERV'		=> $postArray['estserv'],
			'CORRES_ESTSERVLAN'	=> $postArray['estservlan'],
			'CORRES_ESTINC'		=> $postArray['estinc'],
			'CORRES_ENLPRINC'		=> $postArray['enlaceprin'],
			'CORRES_ENLRESP'		=> $postArray['enlaceresp'],
			'CORRES_NOM'			=> $postArray['nombre'],
			'CORRES_GLOSA'			=> $postArray['firma'],
			'CORRES_EMAIL'			=> $postArray['email'],
			'CORRES_TEL'			=> $postArray['fono']
			);
		$this->db->where('CORSC_ID',$seg["CORSC_ID"]);
		$this->db->update('correo_respaldo', $respaldocorreo);
		
		if ($this->db->affected_rows() > 0)
			log_message('error', 'El proceso UPDATE CORREORESPALDO fue correcto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		else
			log_message('error', 'El proceso UPDATE CORREORESPALDO fue incorrecto:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
			
	}
	
	public function insertarNodos($filas,$gevid,$segid,$cliid,$postArray)
	{
		foreach ($filas as $row) {
			$cel = explode(",",$row);
			
			$data2 = array(
				'CORSC_ID'	=> $segid,
				'CLI_ID'		=> $cliid,
				'DIS_NOM'	=> $cel[3],
				'GEV_ID'		=> $cel[0]
			);
			$this->db->insert('correo_nodo', $data2);
			
			$data3 = array(
				'GEV_ID'		=> $cel[0],
				'COM_COM'	=> 'Se envia correo a '.$postArray['para'].' con copia a '.$postArray['copia'].' ++ Con asunto: '.$postArray['asunto'].' ++ Asociado a los ids: '.$gevid,
				'EST_NOM'	=> 'correo',
				'COM_TS'		=> date("Y-m-d H:i:s"),
				'USU_USER'	=> $this->session->userdata('username')
			);
			
			$this->db->insert('comentario', $data3);
		}
	}
	
	/**
	* Direccion de correo del operador.
	*
	* @return	String de direccion de correo del operador
	*/
	public function copyEmail(){
		$user = $this->session->userdata('username');
		$this->db->trans_start();
			$this->db->select('USU_EMAIL');
			$this->db->from('usuario');
			$this->db->where('USU_USER', $user);
			$result = $this->db->get();
		$this->db->trans_complete();
		
		$cor = array();
		foreach($result->result() as $row){
			$cor["Correo"] = $row->USU_EMAIL;
		}
		return $cor;
	}
	
	public function copyEmail2(){
		$user = $this->session->userdata('username');
		$result = $this->db->query("SELECT USU_EMAIL, OPE_TEL, CONCAT( OPE_NOM, ' ', OPE_APP ) AS OPE_NOMBRE FROM usuario LEFT OUTER JOIN operador ON usuario.USU_USER = operador.USU_USER WHERE usuario.USU_USER =  '".$user."'");
		
		$res = array();
		foreach( $result->result() as $row ){
			array_push($res, $row->USU_EMAIL);
			array_push($res, $row->OPE_TEL);
			array_push($res, $row->OPE_NOMBRE);
			return $res;
		}
	}
	
	/**
	* Direcciones de correos de clientes.
	*
	* @input	dispArray contiene la lista de los nodos
	* @return	String lista de correos correspondientes a contactos del nodo
	*/
	public function toEmails2($dispArray){
		$this->db->distinct('CNT_CORHABIL');
		$this->db->from('contacto_dispositivo');
		$this->db->where_in('DIS_NOM', $dispArray);
		$result = $this->db->get();
		
		$cor = array();
		foreach($result->result() as $row){
			$cor["Correos"] = $row->CNT_CORHABIL;
		}
		return $cor;
	}
	
	public function toEmails($dispArray){
		$this->db->select('to_mail');
		$this->db->select('cc_mail');
		$this->db->from('contacto_dispositivo_gde');
		$this->db->where_in('src_organization', $dispArray);
		$result = $this->db->get();
		
		$cor = array();
		$cor["para"] = array();
		$cor["copia"] = array();

		foreach($result->result() as $row){
			array_push( $cor["para"], $row->to_mail);
			array_push( $cor["copia"], $row->cc_mail);
		}
		$cor["para"] = implode(",",$cor["para"]);
		$cor["para"] = str_replace(";",",",$cor["para"]);
		$cor["para"] = explode(",",$cor["para"]);
		$cor["para"] = array_unique($cor["para"]);
		$cor["para"] = implode(",",$cor["para"]);

		$cor["copia"] = implode(",",$cor["copia"]);
		$cor["copia"] = str_replace(";",",",$cor["copia"]);
		$cor["copia"] = explode(",",$cor["copia"]);
		$cor["copia"] = array_unique($cor["copia"]);
		$cor["copia"] = implode(",",$cor["copia"]);
		
		return $cor;
	}
	
	/**
	* Dispositivo IP.
	*
	* @input	nodo (String) ID del nodo a consultar
	* @return	array Array de dispositivos y sus IPs
	*/
	public function arrayDispip($nodo){
		$this->db->select('DIS_IPPRINC');
		$this->db->from('dispositivo');
		$this->db->where('DIS_NOM', $nodo);
		$result = $this->db->get();
		
		foreach($result->result() as $row){
			return $row->DIS_IPPRINC;
		}
	}
	
	/**
	* Codigo de servicio.
	*
	* @input	nodo (String o Array) ID del o los clientes a consultar
	* @return	Int cantidad de correos con seguimiento vencido
	*/
	public function codigoServicio($nodo){
		$this->db->select('DIS_CODSERV');
		$this->db->from('dispositivo');
		$this->db->where('DIS_NOM', $nodo);
		$result = $this->db->get();
		
		foreach($result->result() as $row){
			return $row->DIS_CODSERV;
		}
	}
	
	/**
	* .
	*
	* @input	nodo (String o Array) ID del o los clientes a consultar
	* @return	
	*/
	public function getSucursal($nodo){
		$this->db->select('GEOD_SUCURSAL');
		$this->db->from('geografia_dispositivo');
		$this->db->where('DIS_NOM', $nodo);
		$result = $this->db->get();
		
		foreach($result->result() as $row){
			return $row->GEOD_SUCURSAL;
		}
	}
	
	
	/**
	* Se obtienen los nodos asociados a un seguimiento
	*/
	private function _getAsociacionNodosSeguimiento($idSeguimiento){
		$this->db->select('DIS_NOM');
		$this->db->from('correo_nodo');
		$this->db->where('CORSC_ID', $idSeguimiento);
		$result = $this->db->get();
		
		$nodos = array();
		foreach( $result->result() as $row ){
			array_push($nodos, $row->DIS_NOM);
		}
		return $nodos;
	}
	
	/**
	* Se obtienen los eventos asociados a un seguimiento en base los nodos registrados
	*/
	public function getEventosAsociadosSeguimiento($idSeguimiento){
		$dataCorreo = array();
		
		// Se obtienen los eventos registrados en la tabla de seguimiento
		$this->db->select('GEV_ID');
		$this->db->select('CLI_ID');
		$this->db->from('correo_nodo');
		$this->db->where('CORSC_ID', $idSeguimiento);
		$result = $this->db->get();
		
		$eventos  = array();
		$clientes = array();
		foreach( $result->result() as $row ){
			array_push($eventos, $row->GEV_ID);
			array_push($clientes, $row->CLI_ID);
		}
		
		// Se obtienen los nombres de clientes asociados a los seguimientos
		$this->db->select('CLI_NOM');
		$this->db->from('cliente');
		$this->db->where_in('CLI_ID', $clientes);
		
		$dataCorreo['cliente'] = $this->db->get()->row()->CLI_NOM;
		
		// Se obtienen los eventos activos no asociados en la tabla de seguimiento
		$nodos = $this->_getAsociacionNodosSeguimiento($idSeguimiento);
		
		$result = $this->db->query("select grupo_evento.GEV_ID from grupo_evento LEFT OUTER join correo_nodo on correo_nodo.GEV_ID = grupo_evento.GEV_ID LEFT OUTER join correo_seguimiento on correo_nodo.CORSC_ID = correo_seguimiento.CORSC_ID where EST_NOM = 'Activo' and (correo_seguimiento.CORSC_EST IS NULL or correo_seguimiento.CORSC_EST = 0) and grupo_evento.DIS_NOM in ('".implode("','",$nodos)."')");
		
		$eventos_activos = array();
		foreach( $result->result() as $row ){
			array_push($eventos_activos, $row->GEV_ID);
		}
		
		$eventos_relacionados = array_unique(array_merge($eventos, $eventos_activos));
		
		$this->db->select('grupo_evento.GEV_ID');
		$this->db->select('grupo_evento.GEV_TSINI');
		$this->db->select('grupo_evento.TEV_NOM');
		$this->db->select('grupo_evento.DIS_NOM');
		$this->db->select('geografia_dispositivo.GEOD_SUCURSAL');
		$this->db->from('grupo_evento');
		$this->db->join('geografia_dispositivo', 'grupo_evento.DIS_NOM = geografia_dispositivo.DIS_NOM', 'left outer');
		$this->db->where_in('grupo_evento.GEV_ID', $eventos);
		$result = $this->db->get();
		
		$this->db->select('correo_nodo.GEV_ID');
		$this->db->select('grupo_evento.GEV_TSINI');
		$this->db->select('grupo_evento.TEV_NOM');
		$this->db->select('correo_nodo.DIS_NOM');
		$this->db->from('correo_nodo');
		$this->db->join('grupo_evento', 'grupo_evento.GEV_ID = correo_nodo.GEV_ID');
		$this->db->where('correo_nodo.CORSC_ID', $idSeguimiento);
		$result2 = $this->db->get();
		
		$this->load->library('table');
		$tmpl = array (
			'table_open'  => '<table border="1" cellpadding="2" cellspacing="1" style="height: 300px;display:block; overflow-y:scroll;">'
		);
	
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading( array('Identificador', 'Fecha', 'Evento', 'Nodo', 'Sucursal') );
		$this->table->set_template($tmpl);
		
		$dataCorreo['tabla'] = $this->table->generate($result);

		$hidden = array();
		$hidden2 = array();
		foreach($result->result() as $row){
			array_push($hidden, implode(',', array($row->GEV_ID, $row->GEV_TSINI, $row->TEV_NOM, $row->DIS_NOM )));
		}
		
		foreach($result2->result() as $row){
			array_push($hidden2, implode(',', array($row->GEV_ID, $row->GEV_TSINI, $row->TEV_NOM, $row->DIS_NOM )));
		}
		
		$dataCorreo['hidden']	= implode(';', $hidden);
		$dataCorreo['nodos']		= implode(';', $hidden2);
		
		$dataCorreo['para']		= $this->toEmails($nodos);
		$dataCorreo['paracc']	= $this->copyEmail();
		
		return $dataCorreo;
	}
	
	public function getcorreoRespaldo($idSeguimiento){
		$dataCorreo = array();
		
		// Se obtienen los eventos registrados en la tabla de seguimiento
		$this->db->select('GEV_ID');
		$this->db->select('CLI_ID');
		$this->db->from('correo_nodo');
		$this->db->where('CORSC_ID', $idSeguimiento);
		$result = $this->db->get();
		
		$eventos  = array();
		$clientes = array();
		foreach( $result->result() as $row ){
			array_push($eventos, $row->GEV_ID);
			array_push($clientes, $row->CLI_ID);
		}
		
		// Se obtienen los nombres de clientes asociados a los seguimientos
		$this->db->select('CLI_NOM');
		$this->db->from('cliente');
		$this->db->where_in('CLI_ID', $clientes);
		
		$dataCorreo['cliente'] = $this->db->get()->row()->CLI_NOM;
		
		// Se obtienen los eventos activos no asociados en la tabla de seguimiento
		$nodos = $this->_getAsociacionNodosSeguimiento($idSeguimiento);
		
		$result = $this->db->query("select GEV_ID from correo_nodo LEFT OUTER join correo_seguimiento on correo_nodo.CORSC_ID = correo_seguimiento.CORSC_ID where (correo_seguimiento.CORSC_EST IS NULL or correo_seguimiento.CORSC_EST = 0) and correo_nodo.DIS_NOM in ('".implode("','",$nodos)."')");
		
		$eventos_activos = array();
		foreach( $result->result() as $row ){
			array_push($eventos_activos, $row->GEV_ID);
		}
		
		$eventos_relacionados = array_unique(array_merge($eventos, $eventos_activos));
		
		$this->db->select('grupo_evento.GEV_ID');
		$this->db->select('grupo_evento.GEV_TSINI');
		$this->db->select('grupo_evento.TEV_NOM');
		$this->db->select('grupo_evento.DIS_NOM');
		$this->db->select('geografia_dispositivo.GEOD_SUCURSAL');
		$this->db->from('grupo_evento');
		$this->db->join('geografia_dispositivo', 'grupo_evento.DIS_NOM = geografia_dispositivo.DIS_NOM', 'left outer');
		$this->db->where_in('grupo_evento.GEV_ID', $eventos);
		$result = $this->db->get();
		
		$this->db->select('correo_nodo.GEV_ID');
		$this->db->select('grupo_evento.GEV_TSINI');
		$this->db->select('grupo_evento.TEV_NOM');
		$this->db->select('correo_nodo.DIS_NOM');
		$this->db->from('correo_nodo');
		$this->db->join('grupo_evento', 'grupo_evento.GEV_ID = correo_nodo.GEV_ID');
		$this->db->where('correo_nodo.CORSC_ID', $idSeguimiento);
		$result2 = $this->db->get();
		
		$this->load->library('table');
		$tmpl = array (
			'table_open'  => '<table border="1" cellpadding="2" cellspacing="1" style="height: 300px;display:block; overflow-y:scroll;">'
		);
	
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading( array('Identificador', 'Fecha', 'Evento', 'Nodo', 'Sucursal') );
		$this->table->set_template($tmpl);
		
		$dataCorreo['tabla'] = $this->table->generate($result);
		
		$hidden = array();
		$hidden2 = array();
		foreach($result->result() as $row){
			array_push($hidden, implode(',', array($row->GEV_ID, $row->GEV_TSINI, $row->TEV_NOM, $row->DIS_NOM )));
		}
		
		foreach($result2->result() as $row){
			array_push($hidden2, implode(',', array($row->GEV_ID, $row->GEV_TSINI, $row->TEV_NOM, $row->DIS_NOM )));
		}
		$dataCorreo['hidden']	= implode(';', $hidden);
		$dataCorreo['nodos']	= implode(';', $hidden2);
		
		$this->db->select('CORRES_FROM_NAME');
		$this->db->select('CORRES_FROM_MAIL');
		$this->db->select('CORRES_TO');
		$this->db->select('CORRES_CC');
		$this->db->select('CORRES_ASU');
		$this->db->select('CORRES_CLI');
		$this->db->select('CORRES_NUMIN');
		$this->db->select('CORRES_ESTACT');
		$this->db->select('CORRES_DESCEV');
		$this->db->select('CORRES_ACCREAL');
		$this->db->select('CORRES_OBS');
		$this->db->select('CORRES_TSINI');
		$this->db->select('CORRES_TSFIN');
		$this->db->select('CORRES_TIPAL');
		$this->db->select('CORRES_ALEQ');
		$this->db->select('CORRES_CODSERV');
		$this->db->select('CORRES_SUCUR');
		$this->db->select('CORRES_NOMESTAB');
		$this->db->select('CORRES_ESTSERV');
		$this->db->select('CORRES_ESTSERVLAN');
		$this->db->select('CORRES_ESTINC');
		$this->db->select('CORRES_ENLPRINC');
		$this->db->select('CORRES_ENLRESP');
		$this->db->select('CORRES_NOM');
		$this->db->select('CORRES_GLOSA');
		$this->db->select('CORRES_EMAIL');
		$this->db->select('CORRES_TEL');
		$this->db->from('correo_respaldo');
		$this->db->where('CORSC_ID',$idSeguimiento);
		$correorespaldo = $this->db->get();
		
		if($correorespaldo){
			foreach($correorespaldo->result() as $row){
				$dataCorreo['desde_nom']	= $row->CORRES_FROM_NAME;
				$dataCorreo['desde']			= $row->CORRES_FROM_MAIL;
				$dataCorreo['to']				= $row->CORRES_TO;
				$dataCorreo['cc']				= $row->CORRES_CC;
				$dataCorreo['asu']			= $row->CORRES_ASU;
				$dataCorreo['cli']			= $row->CORRES_CLI;
				$dataCorreo['obs']			= $row->CORRES_OBS;
				$dataCorreo['numin']			= $row->CORRES_NUMIN;
				$dataCorreo['estact']		= $row->CORRES_ESTACT;
				$dataCorreo['descev']		= $row->CORRES_DESCEV;
				$dataCorreo['accreal']		= $row->CORRES_ACCREAL;
				$dataCorreo['fecini']		= $row->CORRES_TSINI;
				$dataCorreo['fecter']		= $row->CORRES_TSFIN;
				$dataCorreo['tipoevento']	= $row->CORRES_TIPAL;
				$dataCorreo['alaeq']			= $row->CORRES_ALEQ;
				$dataCorreo['codservicio']	= $row->CORRES_CODSERV;
				$dataCorreo['sucursales']	= $row->CORRES_SUCUR;
				$dataCorreo['nombestab']	= $row->CORRES_NOMESTAB;
				$dataCorreo['estserv']		= $row->CORRES_ESTSERV;
				$dataCorreo['estservlan']		= $row->CORRES_ESTSERVLAN;
				$dataCorreo['estinc']		= $row->CORRES_ESTINC;
				$dataCorreo['enlaceprin']	= $row->CORRES_ENLPRINC;
				$dataCorreo['enlaceresp']	= $row->CORRES_ENLRESP;
				$dataCorreo['nombre']		= $row->CORRES_NOM;
				$dataCorreo['firma']			= $row->CORRES_GLOSA;
				$dataCorreo['email']			= $row->CORRES_EMAIL;
				$dataCorreo['fono']			= $row->CORRES_TEL;
			}
		}
		return $dataCorreo;
	}

/////////////////////////////////////////////////////////////////////////////////////////////////
// ACTUALIZACION POR HGF, INTEGRACIÓN DE ENVIO DE CORREOS AUTOMÁTICOS AL GDE DE FAL 24/10/2016 //
/////////////////////////////////////////////////////////////////////////////////////////////////
	//Actualiza el mail de mesa de ayuda
	public function updateMailSupport($mail){

		$this->db->where('`parametro`','mail_mesa_ayuda');
		$this->db->update('`parametros`',array('valor' => $mail));
		return 0;
		
	}
	//Obtiene el mail de mesa de ayuda
	public function getMailSupport(){
		$this->db->query("valor");
		$this->db->from('parametros');
		$this->db->where('`parametro`','mail_mesa_ayuda');
		$result = $this->db->get();
		return $result->result();
	}
	
	//Lista los correos registrados en la BBDD
	public function getListMails(){
		$this->db->select('*');
		$this->db->from('contacto_dispositivo_gde');
		$result = $this->db->get();
		return $result->result();
	}
	
	public function getCorreoPatron($id){
		$this->db->select('encargado, src_organization, to_mail, cc_mail,critical_mayor,minor,automatic, support_organization');
		$this->db->from('contacto_dispositivo_gde');
		$this->db->where('`Id`',$id);
		$result = $this->db->get();
		return $result->result();
	}

	public function newPatronMail($variables){
		
		//Valida que el nombre de organization es unico
		if($this->validateOnlyOrg($variables['src_organization']))
		{
			$data = array(
					'src_organization'	   => $variables['src_organization'],
					'to_mail'		       => $variables['to_mail'],
					'cc_mail'	           => $variables['cc_mail'],
					'encargado'		       => $variables['encargado'],
					'support_organization' => $variables['support_organization'],
					'critical_mayor'	=> isset($variables['critical_mayor']) ? '1' : '0',
					'minor'		        => isset($variables['minor']) ? '1' : '0',
					'automatic'		    => isset($variables['automatic']) ? '1' : '0'
				);
			
			$this->db->insert('`contacto_dispositivo_gde`', $data);
			return 0;
		}
		else
		{
			return 'fail_only';			
		}
	
	}

	//Actualiza un registro de la tabla contacto_dispositivos_gde
	public function updatePatronMail($variables){
		
		//Valida que el nombre de organization es unico
		// if($this->validateOnlyOrg($variables['src_organization']) || $this->validateSameOrg($variables['src_organization'],$variables['id']) )
		// {
			//Array de datos a actualizar al registro seleccionado
			$data = array(
					'src_organization'     =>  $variables['src_organization'],
					'to_mail'		       =>  $variables['to_mail'],
					'cc_mail'	           =>  $variables['cc_mail'],
					'encargado'		       =>  $variables['encargado'],
					'support_organization' => $variables['support_organization'],
					'critical_mayor'	=> (isset($variables['critical_mayor'])) ? '1' : '0',
					'minor'		        => (isset($variables['minor']         )) ? '1' : '0',
					'automatic'		    => (isset($variables['automatic']     )) ? '1' : '0'
				);
			
			//Ejecución de los métodos de interacción con la BBDD (MySQL)
			$this->db->where('`Id`', $variables['id']);
			$this->db->update('`contacto_dispositivo_gde`', $data);
			return '0';
		// } 
		// else
		// {
			// return 'fail_only';
		// }
	}

	//Elimina un mail seleccionado
	public function deletePatronMail($id){
		$this->db->delete('`contacto_dispositivo_gde`', array('Id' => $id));
		return '0';
	}

	//Private evita insertar dos organizaciones iguales
	private function validateOnlyOrg($org)
	{
		$this->db->select('*');
		$this->db->from('contacto_dispositivo_gde');
		$this->db->where('`src_organization`',$org);
		$result = $this->db->get();		

		if($result->num_rows>0)
			return false;
		else
			return true;
	}

	//Verifica que al actualizar un mail el org siga siendo el mismo
	private function validateSameOrg($org,$id)
	{
		$this->db->select('*');
		$this->db->from('contacto_dispositivo_gde');
		$this->db->where('`Id`',$id);
		$this->db->where('`src_organization`',$org);
		$result = $this->db->get();		

		if($result->num_rows>0)
			return true;
		else
			return false;
	}
}
?>