<?php

class Excel extends CI_Model {
	
	/**
	*Lista los eventos que se le deben desplegar al usuario en particular
	*
	*@param String $usuario Identificador del usuario que consulta por sus eventos
	*
	*@return String json con los eventos a ser desplegados en el plugin datatables, mÃ¡s las opciones necesarias para filtrado del plugin
	*/
	function obtener_eventos($usuario,$opt){
		
		
		// Se genera array con clientes que puede consultar el operador
		$this->db->select('dest_country');
		$this->db->from('grupo_gde');
		$this->db->where('user',$usuario);
		$this->db->where('status',1);
		$resultPais = $this->db->get();
		$arrayPais = array();
		foreach($resultPais->result() as $estado){
			array_push($arrayPais, $estado->dest_country);
		}
		
		// Se genera array con estados
		$resultEstado = $this->db->get('estado_gde');
		$arrayEstado = array();
		foreach($resultEstado->result() as $estado){
			if( $estado->status != '' )
				array_push($arrayEstado, $estado->status);
		}

		// Se genera el array con los correos de seguimiento activos
		$this->db->select('group_id');
		$this->db->from('correo_seguimiento_gde');
		$this->db->where('status',1);
		$resultCorreosSeguimiento = $this->db->get();
		$arrayCorreosSeguimiento = array();
		foreach( $resultCorreosSeguimiento->result() as $correoSeguimiento ){
			$arrayCorreosSeguimiento[ $correoSeguimiento->group_id ] = '<img src="' . base_url() . 'images/seg.png">';
		}
		
		//print_r ($arrayCorreosSeguimiento);
		
		//$this->load->library('Datatables');

		$this->db->select('g.id as ID');
		$this->db->select('g.severity as SEVERITY');
		$this->db->select('correo_seguimiento_gde.group_id as EMAIL');
		$this->db->select('g.status as STATUS');
		$this->db->select('g.id as REMEDY');
		$this->db->select('g.start_time as FECHA');
		$this->db->select('g.type as TYPE');
		$this->db->select('g.src_name as SRC_NAME');
		$this->db->select('g.count as COUNT');
		$this->db->select('g.src_ip as IP');
		$this->db->select('g.src_organization as SRC_ORGANIZATION');
		$this->db->select('g.src_subcategory as TAG');
		$this->db->select('g.src_category as SRC_CATEGORY');
		$this->db->select('g.dest_service as DEST_SERVICE');
		$this->db->select('g.dest_bunit as DEST_BUNIT');
		$this->db->select('g.dest_country AS DEST_COUNTRY');
		$this->db->select('g.src_tool as TOOL');
			
		$this->db->from('grupo_evento_gde g');
			
		$this->db->join('correo_seguimiento_gde','g.id=correo_seguimiento_gde.group_id','LEFT OUTER');
			
			/*$queryEstado = $this->input->post('sSearch_3');
			if($queryEstado == ''){
				$this->db->where_in('g.status',$arrayEstado);
			}*/
			
			$v_estado 		= $opt["estado"];
			$v_pais 		= $opt["pais"];
			$v_negocio 		= $opt["negocio"];
			$v_servicio 	= $opt["servicio"];
			$v_organization = $opt["organization"];
			$v_tag 			= $opt["tag"];
			$v_search 		= $opt["search"];
			
			if($v_estado != ''){
				$arrayEstado = explode("-",$v_estado);
				$this->db->where_in('g.status',$arrayEstado);
			}
			
			if($v_pais != ''){
				$arrayPais = explode("-",$v_pais);
				$this->db->where_in('g.dest_country',$arrayPais);
			}
			
			if($v_negocio != ''){
				$arrayNegocio = explode("-",$v_negocio);
				$this->db->where_in('g.dest_bunit',$arrayNegocio);
			}
			
			if($v_servicio != ''){
				$arrayServicio = explode("-",$v_servicio);
				$this->db->where_in('g.dest_service',$arrayServicio);
			}
			
			if($v_organization != ''){
				$arrayOrg = explode("-",$v_organization);
				$this->db->where_in('g.src_organization',$arrayOrg);
			}
			
			if($v_tag != ''){
				$arrayTag = explode("-",$v_tag);
				$this->db->where_in('g.src_subcategory',$arrayTag);
			}
			
			if($v_search != ''){
				$Custom = '%'.$v_search.'%';
				$value = '(g.src_name LIKE \''.$Custom.'\' OR g.src_ip LIKE \''.$Custom.'\' OR g.dest_country LIKE \''.$Custom.'\' OR g.dest_bunit LIKE \''.$Custom.'\' OR g.dest_service LIKE \''.$Custom.'\' OR g.src_organization LIKE \''.$Custom.'\' OR g.src_subcategory LIKE \''.$Custom.'\')';
				$this->db->where($value);
			}
			
			// Descomentar para filtro por PAIS
			/*$queryPais = $this->input->post('sSearch_14');
			if($queryPais == ''){
				$this->db->where_in('g.dest_country',$arrayPais);
			}*/
			
			// $this->db->edit_column('ID','<input type="checkbox" name="$1" value="$1" class="check" />','ID');
			// //$this->db->edit_column('LOCK_CONT','<img src="'.base_url().'images/s$1.png" />','LOCK_CONT');
			// $this->db->edit_column('EMAIL',$arrayCorreosSeguimiento,'EMAIL');
			// $this->db->edit_column('REMEDY',' ','REMEDY');
			
			$result = $this->db->get(); 
			// $result = (object) array_merge( (array)json_decode($result), array( 'query' => $this->db->last_query() ) );
		
		$resultados =  array();
		$body = "";
		foreach($result->result() as $rs){
			array_push($resultados, $rs);
			$body = $body + $rs->SRC_NAME;
		}			

		// if( $this->db->_error_number() > 0 ){
		// 	log_message('error', 'Error en ejecucion de listar filtros para el perador ' . $usuario);
		// 	log_message('error', $this->db->_error_number());
		// 	log_message('error', $this->db->_error_message());
		// 	log_message('error', $this->db->last_query());
			
		// 	$errorMsg = 'Se ha presentado un problema, favor intentar nuevamente. Si el problema persiste informar a Administrador';
		// 	$result = '{"sEcho":0,"iTotalRecords":1,"iTotalDisplayRecords":1,"aaData":[["---","---","---","---","---","'.$errorMsg.'","---","---","---","---","---","---","---","---","---","---","---","---","---","---",]],"sColumns":"g.id,g.severity,correo_nodo_gde.id,g.status,g.id,g.start_time,g.type,g.src_name,g.count,g.src_ip,LOCK_CONT,g.src_organization,g.dest_service,g.dest_bunit,g.dest_country,g.src_category,g.info"}';
		// 	echo $result;
		// 	return;
		// }
		//echo $result;
		
		// return json_encode($result);
		return $body;
		
	}


}
	
?>