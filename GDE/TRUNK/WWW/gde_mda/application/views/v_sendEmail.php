<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Formulario de Correo</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/gen_validatorv4.js" type="text/javascript"></script>
</head>
<body onload="load()">
	<h3>
		<?php 
			if(isset($message)){
				echo $message;
			}
			$filas1 = explode(";",$_POST["Fila"]);
			for($i=0;$i<count($filas1)-1;$i++){
				$filas[$i] = $filas1[$i];
			}
			
			function sucursal($nod)
			{
				$CI =& get_instance();
				return $CI->getSucursal($nod);
			}
		?>
	</h3>
	
	<div class="row">
		<div class="twelve columns centered">
		<?php echo form_open('c_sendEmail/sendEmail', array('id' => 'form'));?>
			<fieldset>
				<legend>Envío de correo</legend>
				
				<label for="desde">Desde (Alias): </label>
				<input type="text" name="desde_show" value="<?php echo $_POST["alias_nom"]."<".$_POST["alias"].">"?>" readonly="readonly"/>
					<input type="hidden" name="desde" value="<?php echo $_POST["alias"]?>" readonly="readonly" />
					<input type="hidden" name="desde_nom" value="<?php echo $_POST["alias_nom"]?>" readonly="readonly" />
				<label for="para">*Para: </label>
				<textarea name="para" id="para" value="" rows="3" cols="50"><?php echo $_POST["Correos"]?></textarea>
				<label for="copia">*Copia: </label>
				<textarea name="copia" id="copia" value="" rows="3" cols="50"><?php echo $_POST["CorreoUS"]?></textarea>
				<label for="asunto">*Asunto: </label>
				<input type="text" name="asunto" value="Incidentes cliente <?php echo $_POST["Cliente"]?>"/>
				
				<hr>
				
				<label for="numin">N° Incidente: </label>
				<input type="text" name="numin" value="" />
				<label for="cliente">*Cliente: </label>
				<input type="text" name="cliente"  id="cliente" value="<?php echo $_POST["Cliente"]?>" />
				<label for="tipoevento">Tipo de Alarma: </label>
				<textarea name="tipoevento" value="" rows="3" cols="50"><?php 
					$tipal = array();
					foreach ($filas as $row) {
						$cel = explode(",",$row);
						array_push($tipal, $cel[2]);
					}
					$tipal = array_unique($tipal);
					foreach ($tipal as $row) {
						echo $row."\n";
					}?>
				</textarea>
				<label for="alaeq">Alarma de Equipo: </label>
				<textarea name="alaeq" value="" rows="3" cols="50"></textarea>
				<label for="codservicio">Codigo de Servicio: </label>
				<textarea name="codservicio" value="" rows="3" cols="50"></textarea>
				<label for="sucursales">Direccion:</label>
				<textarea name="sucursales" value="" rows="3" cols="50"><?php 
					$sucur = array();
					foreach ($filas as $row) {
						$cel = explode(",",$row);
						array_push($sucur,sucursal($cel[3]));
					}
					$sucur = array_unique($sucur);
					foreach ($sucur as $row) {
						echo $row."\n";
					}?>
				</textarea>
				<label for="estactividad">Estado Actividad: </label>
				<input type="text" name="estactividad" value="" />
				<label for="fecini">Fecha Inicio Incidente: </label>
				<input type="text" name="fecini" value="" />
				<label for="fecter">Fecha Termino Incidente: </label>
				<input type="text" name="fecter" value="" />
				<label for="desevento">Descripción del Evento: </label>
				<textarea name="desevento" value=""  rows="3" cols="50"></textarea>
				<label for="accrealiz">Acciones Realizadas: </label>
				<textarea name="accrealiz" value="" rows="3" cols="50"></textarea>
				<label for="obs">Observaciones: </label>
				<textarea name="obs" value="" rows="3" cols="50"></textarea>
				<input type="text" style="visibility:hidden" name="table" value="<?php echo $_POST["Fila"] ?>" />
				<input type="text" style="visibility:hidden" name="clientehid" value="<?php echo $_POST["Cliente"]?>" />
				<hr>
				
				<table id="datosEmail" border="1" style="width: 100%; background-color: #f2f2f2; border-collapse: collapse;">
					<thead>
						<tr>
							<td style="font-size: 12px">Fecha</td>
							<td style="font-size: 12px">Evento</td>
							<td style="font-size: 12px">Nodo</td>
							<td style="font-size: 12px">Sucursal</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($filas as $row) {
							$cel = explode(",",$row);
							echo "<tr><td style=\"font-size: 12px\">";
								echo $cel[1];
							echo "</td><td style=\"font-size: 12px\">";
								echo $cel[2];
							echo "</td><td style=\"font-size: 12px\">";
								echo $cel[3];
							echo "</td><td style=\"font-size: 12px\">";
								echo sucursal($cel[3]);
							echo "</td></tr>";
							}
						?>
					</tbody>
				</table>
				<hr />
				<label ><b>Firma</b></label>
				<label for="nombre">Nombre: </label>
				<input type="text" name="nombre" value="" />
				<label for="firma">Glosa Firma: </label>
				<textarea name="firma" value="" rows="3" cols="50"></textarea>
				<label for="email">E-mail: </label>
				<input type="text" name="email" value="" />
				<label for="fono">Telefono: </label>
				<input type="text" name="fono" value="" />
				
				<hr>
				<input type="submit" value="Enviar" id="envioForm" name="my_submit"/>
				<input type="button" id="btncancelar" value="Cancelar" />
			</fieldset>
		<?php echo form_close();?>
		
		</div>
	</div>
	<script type="text/javascript">
	
	
	var frmvalidator = new Validator("form");
	frmvalidator.addValidation("para","req","Favor llenar campo PARA ");
	frmvalidator.addValidation("copia","req","Favor llenar campo COPIA");
	frmvalidator.addValidation("cliente","req","Favor llenar campo CLIENTE");
	
	function load(){
		filas = $('#form input[name="table"]').val();
		console.log(filas);
		$.ajax({
			type: "POST",
			url: "../c_sendEmail/test",
			data: {filas: filas},
			dataType: 'json',
			success: function(data) {
				console.log(data);
				data.filas = Array.prototype.slice.call(data.filas);
				var codigos = new Array();
				for( var i=0; i<data.filas.length; i++ ){
					if( $.inArray(data.filas[i], codigos) < 0 ){
						codigos.push(data.filas[i]);
					}
				}
				cod = codigos.join("\n");
				var nodos = new Array();
				for( var i=0; i<data.nodos.length; i++ ){
					if( $.inArray(data.nodos[i], nodos) < 0 ){
						nodos.push(data.nodos[i]);
					}
				}
				nodo = nodos.join("\n");
				firma = "Operador Centro Monitoreo Servicios Clientes\nSubgerencia de Monitoreo y Disponibilidad de Servicios ENTEL S.A.";
				$('#form input[name="numin"]').val();
				$('#form textarea[name="codservicio"]').val(cod);
				$('#form textarea[name="firma"]').val(firma);
				$('#form textarea[name="alaeq"]').val(nodo);
				$('#form input[name="nombre"]').val(data.firma[2]);
				$('#form input[name="email"]').val(data.firma[0]);
				$('#form input[name="fono"]').val(data.firma[1]);
			}
		});
	}
	
	$('#btncancelar').click(function(event){
		window.close();
	});
	
	$('#envioForm').click(function(event){
		$('#envioForm').attr('disabled','disabled');
		$('#form').submit();
	});
	
	$('#para').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	$('#copia').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	$('#cliente').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	</script>
</body>
</html>