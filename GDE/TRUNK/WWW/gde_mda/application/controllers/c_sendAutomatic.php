<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_sendAutomatic extends CI_Controller {
	
	public function index(){}
	
	private function getGroupEvents()
	{	
		// Query cruce de datos
		$sql = "SELECT REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE ( categoria_gde.encargado,'á','a'), 'é','e'),'í','i'),'ó','o'),'ú','u'),'ñ','n') as encargado_categoria,REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE ( contacto_dispositivo_gde.encargado,'á','a'), 'é','e'),'í','i'),'ó','o'),'ú','u'),'ñ','n') as encargado_correo, categoria_gde.detalle,categoria_gde.pais, grupo_evento_gde.id,grupo_evento_gde.dest_country,grupo_evento_gde.type,grupo_evento_gde.severity,grupo_evento_gde.folio_mda,grupo_evento_gde.src_organization,grupo_evento_gde.status,grupo_evento_gde.send_mail,contacto_dispositivo_gde.automatic FROM `grupo_evento_gde` LEFT JOIN contacto_dispositivo_gde ON grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization LEFT JOIN categoria_gde ON (categoria_gde.pais = ( CASE grupo_evento_gde.dest_country
    WHEN 'Chile' THEN 'CL'
    WHEN 'Uruguay' THEN 'UY'
    WHEN 'Peru' THEN 'PE'
    WHEN 'Mexico' THEN 'ME'
    WHEN 'Colombia' THEN 'CO'
    WHEN 'Brasil' THEN 'BR'
    WHEN 'Brazil' THEN 'BR'
    WHEN 'Argentina' THEN 'AR'
    ELSE 'N/A' END ) AND 
	( grupo_evento_gde.type = categoria_gde.detalle OR categoria_gde.detalle = CONCAT(grupo_evento_gde.type,'.',grupo_evento_gde.severity)) AND categoria_gde.encargado = contacto_dispositivo_gde.encargado ) WHERE grupo_evento_gde.start_time >= NOW() - INTERVAL 7 MINUTE";
		$result = $this->db->query($sql);
		return $result;
	}
	
	public function automatic(){
		
		//Obtener la lista de grupos de eventos que se crearon en los ultimos 2 minutos
		$gruposEventos = $this->getGroupEvents();

		//Envío de tickets 
		foreach($gruposEventos->result() as $evento)
		{
			// echo $evento->id . " | ";
			// echo $evento->src_organization . " | ";
			// echo $evento->severity . " | ";
			// echo $evento->automatic . " | ";
			// echo $evento->encargado_categoria . " | ";
			// echo $evento->encargado_correo . " | ";
			// echo $evento->folio_mda . " | ";
			// echo $evento->send_mail . " | ";
			// echo "----";
			
			if (($evento->severity == "CRITICAL" || $evento->severity == "MAJOR") && $evento->folio_mda == "0" && $evento->status != "Cerrado" && $evento->encargado_categoria && $evento->encargado_correo && (strcasecmp($evento->encargado_correo, $evento->encargado_categoria) == 0)){
				
				// echo "TICKET";
				
				// Genera ticket.
				system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticTicket createTicket ".$evento->id." >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &");
				
			} else {
				// No genera ticket. Valida si tiene correo automatico.
				if(($evento->severity == "CRITICAL" || $evento->severity == "MAJOR") && $evento->status != "Cerrado" && $evento->automatic == "1" && $evento->send_mail === NULL && $evento->folio_mda == "0"){
				// if($evento->automatic == "1" && $evento->send_mail!="0" && $evento->folio_mda == "0"){
					
					// echo "CORREO";
					
					// Envia correo
					system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticEmail sendEmailbyID ".$evento->id." >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &");
				}
				// else {
					// echo "NO APLICA";
				// }
			}
			
			// echo "----\n";
			
		}
	}
	
	private function getGroupEventsTEST()
	{	
		// Query cruce de datos
		$sql = "SELECT REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE ( categoria_gde.encargado,'á','a'), 'é','e'),'í','i'),'ó','o'),'ú','u'),'ñ','n') as encargado_categoria,REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE ( contacto_dispositivo_gde.encargado,'á','a'), 'é','e'),'í','i'),'ó','o'),'ú','u'),'ñ','n') as encargado_correo, categoria_gde.detalle,categoria_gde.pais, grupo_evento_gde.id,grupo_evento_gde.dest_country,grupo_evento_gde.type,grupo_evento_gde.severity,grupo_evento_gde.folio_mda,grupo_evento_gde.src_organization,grupo_evento_gde.send_mail,contacto_dispositivo_gde.automatic FROM `grupo_evento_gde` LEFT JOIN contacto_dispositivo_gde ON grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization LEFT JOIN categoria_gde ON (categoria_gde.pais = ( CASE grupo_evento_gde.dest_country
    WHEN 'Chile' THEN 'CL'
    WHEN 'Uruguay' THEN 'UY'
    WHEN 'Peru' THEN 'PE'
    WHEN 'Mexico' THEN 'ME'
    WHEN 'Colombia' THEN 'CO'
    WHEN 'Brasil' THEN 'BR'
    WHEN 'Brazil' THEN 'BR'
    WHEN 'Argentina' THEN 'AR'
    ELSE 'N/A' END ) AND 
	( grupo_evento_gde.type = categoria_gde.detalle OR grupo_evento_gde.type = CONCAT(categoria_gde.detalle, '.Major') OR 	grupo_evento_gde.type = CONCAT(categoria_gde.detalle, '.Critical')) AND categoria_gde.encargado = contacto_dispositivo_gde.encargado ) WHERE grupo_evento_gde.start_time >= NOW() - INTERVAL 600 MINUTE";
		$result = $this->db->query($sql);
		return $result;
	}
	
	public function automaticTEST(){
		
		//Obtener la lista de grupos de eventos que se crearon en los ultimos 2 minutos
		$gruposEventos = $this->getGroupEventsTEST();

		//Envío de tickets 
		foreach($gruposEventos->result() as $evento)
		{
			// echo $evento->id . " | ";
			// echo $evento->src_organization . " | ";
			// echo $evento->severity . " | ";
			// echo $evento->automatic . " | ";
			// echo $evento->encargado_categoria . " | ";
			// echo $evento->encargado_correo . " | ";
			// echo $evento->folio_mda . " | ";
			// echo $evento->send_mail . " | ";
			// echo "----";
			
			if (($evento->severity == "CRITICAL" || $evento->severity == "MAJOR") && $evento->folio_mda == "0" && $evento->encargado_categoria && $evento->encargado_correo && (strcasecmp($evento->encargado_correo, $evento->encargado_categoria) == 0)){
				
				echo $evento->id . " | ";
				echo $evento->src_organization . " | ";
				echo $evento->severity . " | ";
				echo $evento->automatic . " | ";
				echo $evento->encargado_categoria . " | ";
				echo $evento->encargado_correo . " | ";
				echo $evento->folio_mda . " | ";
				echo $evento->send_mail . " | ";
				echo "----";
				echo "TICKET";
				
				// Genera ticket.
				system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticTicket createTicket ".$evento->id." >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &");
				
			}
			
			// echo "----\n";
			
		}
	}
	
	private function getSyncGroupEventsA()
	{
		// Buscar los que se hayan "Cerrado" en el periodo correspondiente y que tengan asociado un ticket.
		
		$sql = 'SELECT grupo_evento_gde.folio_mda, grupo_evento_gde.id, grupo_evento_gde.status FROM `historia_gde` LEFT JOIN grupo_evento_gde ON (historia_gde.group_id = grupo_evento_gde.id) WHERE historia_gde.end_status = "Cerrado" AND historia_gde.start_time >= NOW() - INTERVAL 17 MINUTE AND grupo_evento_gde.folio_mda != "0" GROUP BY group_id';
		
		$result = $this->db->query($sql);
		return $result;
	}
	
	private function getSyncGroupEventsB()
	{
		// Buscar los que estan en estado "Activo" y "En proceso" y que tengan asociado un ticket.
		
		$sql = 'SELECT folio_mda,id,status FROM grupo_evento_gde WHERE status IN ("Activo","En Proceso") AND folio_mda != "0"';
		
		$result = $this->db->query($sql);
		return $result;
	}
	
	public function syncTicket(){
		
		$gruposEventosA = $this->getSyncGroupEventsA();
		$gruposEventosB = $this->getSyncGroupEventsB();
		
		$comentario = "Ticket normalizado por sincronizacion con MDA";
		$usuario    = "gestor";
		
		foreach($gruposEventosA->result() as $evento)
		{
			echo "cerrado ultimo minutos | ";
			echo $evento->folio_mda . " | ";
			echo $evento->id . " | ";
			echo $evento->status . " | ";
			echo "\n";
			
			if ($evento->status == "Activo"){
				$estado = "Asignado";
			} elseif ($evento->status == "En Proceso"){
				$estado = "Trabajando";
			} else {
				$estado = "Resuelto - Nivel 2";
			}
			
			system('/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticTicket updateTicket '.escapeshellcmd($evento->folio_mda).' '.escapeshellcmd($evento->id).' "'.escapeshellcmd($estado).'" "'.escapeshellcmd($comentario).'" "'.escapeshellcmd($usuario).'" >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &');
		}
		
		foreach($gruposEventosB->result() as $evento)
		{
			echo "abierto | ";
			echo $evento->folio_mda . " | ";
			echo $evento->id . " | ";
			echo $evento->status . " | ";
			echo "\n";
			
			if ($evento->status == "Activo"){
				$estado = "Asignado";
			} elseif ($evento->status == "En Proceso"){
				$estado = "Trabajando";
			} else {
				$estado = "Resuelto - Nivel 2";
			}
			
			system('/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticTicket updateTicket '.escapeshellcmd($evento->folio_mda).' '.escapeshellcmd($evento->id).' "'.escapeshellcmd($estado).'" "'.escapeshellcmd($comentario).'" "'.escapeshellcmd($usuario).'" >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &');
		}
	}
}


/* End of file C_sendAutomatic.php */
/* Location: ./application/controllers/C_sendAutomatic.php */
