<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_nodos_incompletos extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->model('nodos');
	}
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		// $clientes = $this->listarClientes();
		// $clientes = implode(" OR CLI_ID=",$clientes);
		
		$data = array();
		$data['titulo_pagina'] = 'GDE -> Resumen estadistico';
		$data['supervisor'] = $this->session->userdata('username');
		$data['url1'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/nodos_sin_codigo";
		$data['url2'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/nodos_sin_contacto";
		$data['url3'] = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/nodos_sin_criticidad";
		
		$this->load->view('analista/v_nodos_incompletos', $data);
	}
	
	function listarClientes(){
		$results = $this->nodos->listarClientes($this->session->userdata('username'));
		$clientes = array();
		foreach( $results as $cliente ){
			array_push( $clientes, $cliente[0] );
		}
		return $clientes;
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 4){
			return true;
		}
		else{
			return false;
		}
	}
}
