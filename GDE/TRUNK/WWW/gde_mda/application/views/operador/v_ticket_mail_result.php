<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Resultado envio de correo</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<p><?php echo $messageResult; ?></p>

<script type="text/javascript">
	$(document).ready(function(){
		window.opener.AfterSendEmail();
	});
</script>
</body>
</html>