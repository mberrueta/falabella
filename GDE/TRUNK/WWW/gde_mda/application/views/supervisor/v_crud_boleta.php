<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Mantenedor de Correos de Seguimiento</h3>
				<!--<a href='<?php echo site_url('c_seguimientoBoleta/obtenerNodoEstados')?>'>PRUEBA</a>-->
			</header>
			<div style='height:20px;'></div> 
			<div>
				<?php echo $output; ?>
			</div>
		</article>
	</section>
	
	<div style="clear: both";>
</body>
</html>