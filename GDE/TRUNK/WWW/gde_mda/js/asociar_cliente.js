$( function() {
	
	$(document).ready(function(){

/*	--------------------------------------------------
	:: Seleccion de operadores
	-------------------------------------------------- */
		
		$(".usuarios").live('change', function(){
			var id       = $(this).attr('id');
			var option = $(this).val();
			
			console.log("id: "+id+", option: "+option);
			if(id == "selectOperador"){
				$("#selectBackup").removeAttr('disabled');
				$("#selectBackup option").removeAttr('disabled');
				$("#selectBackup option[value='"+ option + "']").attr('disabled', true );
				$("#selectCT1 option[value='"+ option + "']").attr('disabled', true );
			}
			else if(id == "selectBackup"){
				$("#selectOperador option").removeAttr('disabled');
				$("#selectOperador option[value='"+ option + "']").attr('disabled', true );
				$("#selectCT1 option[value='"+ option + "']").attr('disabled', true );
			}
		});

		//TODO: Al momento de enviar el FORM, consultar si estan correctas las opciones seleccionadas

/*	--------------------------------------------------
	:: Datos del cliente
	-------------------------------------------------- */
		$("#cNombre").live('change',function(){
			var readUrl    = '/supervisor/c_crear_cliente/valida';
			var nombre    = $("#cNombre").val();
			var pathname = window.location.pathname.split( '/' );
			var hostname = window.location.hostname;
			var protocol    = window.location.protocol;
			
				$.ajax({
					url: $('#url_js').val() + readUrl + '/' + nombre,
					dataType: 'json',
					success: function(response) {
						console.log(response);
						if (response == false){
							$('#nameInfo').html('').append('<img src="'+protocol+"//"+hostname+"/"+pathname[1]+'/images/icn_alert_success.png">');
						}
						else{
							$('#nameInfo').html('').append('<img id="ayuda" title="El nombre de CLIENTE que esta intentando registrar ya existe" src="'+protocol+"//"+hostname+"/"+pathname[1]+'/images/icn_alert_error.png">');
						}
					}
				});
		});
	});
});
