<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_menu_supervisor extends CI_Controller {

	public function index($mensaje='')
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data['mensaje'] = $mensaje;
		$data['titulo_pagina'] = 'GED -&gt; Menu Supervisor';
		$data['iframe'] = 'http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_turnos';
		$this->load->view('supervisor/v_menu_supervisor',$data);

	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 0){
			return true;
		}
		else{
			return false;
		}
	}
}

/* End of file c_menu_supervisor.php */
/* Location: ./application/controllers/supervisor/c_menu_supervisor.php */
