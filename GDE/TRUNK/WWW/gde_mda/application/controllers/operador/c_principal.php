<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_principal extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if(!$this->_logeado()){
			return redirect('c_login/acceso_denegado');
		}
		
		$this->load->model('evento');
		$this->load->helper('mysql_to_excel_helper');
		//$this->load->model('ticket');
		//$this->load->model('webservices');
	}
	
	
	/** Carga informaci�n y vista inicial del operador solicitante
	* @param String username nombre de usuario solicitante
	*
	* @return vista con datos solicitados
	*/
	public function index()
	{
		// $operador = $this->session->userdata('username');
		
		//$this->load->view('operador/v_secundaria',$data);
		$this->load->view('operador/v_principal');
	}
	
	
	function codServicio(){
		$nodos = $this->input->post('nodos');
		$listanodos = implode("','", $nodos);
		$listanodos = "'".$listanodos."'";
		$result = $this->evento->codServicio($listanodos);
		$result = array_unique($result);
		$html = "<option value='Otro'>Otro</option>";
		foreach($result as $cod){
			$html = $html."<option value='".$cod."'>".$cod."</option>";
		}
		echo $html;
	}
	
	function codServ(){
		$nodos = $this->input->post('nodos');
		$listanodos = implode("','", $nodos);
		$listanodos = "'".$listanodos."'";
		$result = $this->evento->codServ($listanodos);
		echo $result;
	}
	
	function validaCodServ(){
		$codserv = $this->input->post('codserv');
		$result = $this->evento->validaCodServ($codserv);
		echo $result;
	}
	
	function Email()
	{
		echo $this->evento->Email();
	}
	
	
	function cliId(){
		$clientes = $this->input->post('clientes');
		$listaclientes = implode("','", $clientes);
		$listaclientes = "'".$listaclientes."'";
		$result = $this->evento->cliId($listaclientes);
		echo $result;
	}
	
	/** Entrega los eventos que debe gestionar el operador, solicitados por plugin DataTables
	* @param username String nombre de usuario del operador que solicita los eventos
	*
	* @return String json con la informacion necesaria a ser procesada por DataTables.
	*                          Se entregan los eventos asignados al operador solicitante
	*/
	public function obtener_eventos(){
		echo $this->evento->obtener_eventos($this->session->userdata('username'));
	}
	
	public function obtener_eventos_vista_servidor(){
		echo $this->evento->obtener_eventos_vista_servidor($this->session->userdata('username'));
	}
	
	public function obtener_eventos_vista_pais_negocio(){
		echo $this->evento->obtener_eventos_vista_pais_negocio($this->session->userdata('username'));
	}
	
	public function obtener_eventos_export(){
		to_excel($this->evento->obtener_eventos_export($this->session->userdata('username')), "archivoexcel");
		//echo json_encode($this->evento->obtener_eventos_export($this->session->userdata('username')));
	}
	
	public function obtener_type(){
		echo $this->session->userdata('tipo');
		//echo json_encode($this->evento->obtener_eventos_export($this->session->userdata('username')));
	}
	
	/**Entrega el detalle de los eventos seleccionados
	* @param idsGrupos Array $_POST ids de los eventos a consultar
	*
	* @return String detalle (mensaje) de los eventos seleccionados
	*/
	public function detalle_eventos(){
		$idsGrupos = $this->input->post('idsGrupos');
		echo $this->evento->detalle_eventos($idsGrupos);
	}
	
	/**Entrega el detalle de los eventos seleccionados
	* @param idsGrupos Array $_POST ids de los eventos a consultar
	*
	* @return String detalle (mensaje) de los eventos seleccionados
	*/
	public function detalle_eventos2(){
		$idsGrupos = $this->input->post('idsGrupos');
		echo $this->evento->detalle_eventos2($idsGrupos);
	}
	
	
	/** Realiza el cambio de ESTADOS de los eventos seleccionados
	* @param aGrupos Array $_POST ids de los eventos a cambiar de estados
	* @param sEstado String $_POST estado al cual se moveran los estados solicitados
	* @param sComentario String $_POST comentario dado por el operador a la accion que se va a realizar
	* @param username String nombre de usuario del operador que realiza la acci�n
	*
	* @return String resultado del cambio de estado de los eventos seleccionados
	*/
	public function cambiar_estado(){
		echo $this->evento->cambiar_estado($this->input->post('aGrupos'), $this->input->post('sEstado'), $this->input->post('sComentario'), $this->session->userdata('username'));
	}
	
	public function insertaTicketManual(){
		$id    = $this->input->post('ID');
		$folio = $this->input->post('folio');
		
		echo $this->evento->insertaTicketManual($id, $folio);
	}
	
	/** Validacion del usuario que solicita controlador
	*/
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 1 || $this->session->userdata('tipo') == 2 || $this->session->userdata('tipo') == 3 || $this->session->userdata('tipo') == 4)){
			return true;
		}
		else{
			return false;
		}
	}
}

/* End of file c_principal.php */
/* Location: ./application/controllers/c_principal.php */