USE `gestor_eventos_rsp`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `CREATE_FILTRO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_FILTRO`()
BEGIN
START TRANSACTION;
    CREATE TEMPORARY TABLE temporal
	   SELECT gestor_eventos_rsp.grupo_evento_gde.status,
            gestor_eventos_rsp.grupo_evento_gde.dest_country,
            gestor_eventos_rsp.grupo_evento_gde.dest_bunit,
            gestor_eventos_rsp.grupo_evento_gde.src_organization,
            gestor_eventos_rsp.grupo_evento_gde.src_subcategory,
            gestor_eventos_rsp.grupo_evento_gde.src_system,
            sum(gestor_eventos_rsp.grupo_evento_gde.count) as ACUM,
            'OK'
	   FROM gestor_eventos_rsp.grupo_evento_gde

	   GROUP BY gestor_eventos_rsp.grupo_evento_gde.status,
            gestor_eventos_rsp.grupo_evento_gde.dest_country,
            gestor_eventos_rsp.grupo_evento_gde.src_system,
            gestor_eventos_rsp.grupo_evento_gde.dest_bunit,
            gestor_eventos_rsp.grupo_evento_gde.src_organization,
            gestor_eventos_rsp.grupo_evento_gde.src_subcategory;
	
  UPDATE estadistica.filtro_base SET ESTADO='OLD';
	
  INSERT INTO estadistica.filtro_base
	
  SELECT * FROM temporal;
  
  DELETE FROM estadistica.filtro_base WHERE ESTADO='OLD';
  
  DROP TEMPORARY TABLE temporal;
COMMIT;
END
