<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Mantenedor de Clientes</h3>
			</header>
			
		<?php
		if($men != ''){
			echo urldecode($men);
		}

		if($final != ''){
			//echo '<p>Administracion de Clientes</p>';
			echo '<table id="clientes" cellspacing="0" cellpadding="0" width="100%">';
			echo '<thead>';
				echo '<tr>';
				echo '<th>Cliente</th>';
				echo '<th>Criticidad</th>';
				echo '<th>Operador</th>';
				echo '<th>Backup</th>';
				echo '<th>Turno de Noche</th>';
				echo '<th>Turno FDS</th>';
				echo '<th> </th>';
				echo '<th> </th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
					foreach($final as $cliente){
					echo '<tr>';
					echo '<td>'.$cliente['Cliente'].'</td>';
					echo '<td>'.$cliente['Criti'].'</td>';
					echo '<td>'.$cliente['Operador'].'</td>';
					echo '<td>'.$cliente['Backup'].'</td>';
					echo '<td>'.$cliente['Noche'].'</td>';
					echo '<td>'.$cliente['FDS'].'</td>';
					echo '<td><a href="'.site_url('supervisor/c_editar_detalle_cliente/index/'.$cliente['Cliente']).'"><img src="'.base_url().'images/edit-icon.png" class="EditBtn"></a></td>';
					echo '<td><a href="'.site_url('supervisor/c_eliminar_cliente/index/'.$cliente['Cliente']).'" onclick="return confirm(\'Confirma la eliminacion de: '.$cliente['Cliente'].'\')"><img src="'.base_url().'images/delete-icon.png" class="DeleteBtn"></a></td>';
					//echo '<td><img src="'.base_url().'images/delete-icon.png" class="DeleteBtn"</td>';
					//echo '<td>'.anchor('supervisor/c_editar_detalle_cliente/index/'.$cliente['Cliente'],'Editar',array('class'=>'edit')).'</td>';
					//echo '<td>'.anchor('supervisor/c_eliminar_cliente/index/'.$cliente['Cliente'],'Eliminar',array('class'=>'delete','onclick'=>"return confirm('Confirma la eliminacion de: ".$cliente['Cliente']."')")).'</td>';
					echo '<tr>';
				}
			echo '</tbody>';
			echo '<tfoot>';
			echo '<tr>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td><a href="'.site_url('supervisor/c_crear_cliente').'"><button id="buttonNuevo"></button></a></td>';
			echo '</tr>';
			echo '</tfoot>';
			echo '</table>';
			echo '<br />';
			//echo anchor('supervisor/c_crear_cliente/','Crear nuevo Cliente',array('class'=>'add'));
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		?>
 			
		</article>
	</section>
	
		<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

</body>
</html>