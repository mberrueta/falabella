var readUrl  = '/supervisor/c_crear_cliente/valida';
var getUrl    = '/supervisor/c_editar_detalle_cliente/obtener_operadores';
var select;

$( function() {

		leerDatos();  // Se pre-selecionan las opciones de operador registradas
		
/*	--------------------------------------------------
	:: Seleccion de operadores
	-------------------------------------------------- */
	
		$(".usuarios").live('change', function(){
			var id       = $(this).attr('id');
			var option = $(this).val();
			
			console.log("id: "+id+", option: "+option);
			if(id == "selectOperador"){
				$("#selectBackup").removeAttr('disabled');
				$("#selectBackup option").removeAttr('disabled');
				$("#selectBackup option[value='"+ option + "']").attr('disabled', true );
				$("#selectCT1 option[value='"+ option + "']").attr('disabled', true );
			}
			else if(id == "selectBackup"){
				$("#selectOperador option").removeAttr('disabled');
				$("#selectOperador option[value='"+ option + "']").attr('disabled', true );
				$("#selectCT1 option[value='"+ option + "']").attr('disabled', true );
			}
		});

/*	--------------------------------------------------
	:: Datos del cliente
	-------------------------------------------------- */
	
		$("#cNombre2").live('change',function(){
			var readUrl    = '/supervisor/c_crear_cliente/valida';
			var nombre    = $(this).val();
			var pathname = window.location.pathname.split( '/' );
			var hostname = window.location.hostname;
			var protocol    = window.location.protocol;
			
				$.ajax({
					url: $('#url_js').val() + readUrl + '/' + nombre,
					dataType: 'json',
					success: function(response) {
						console.log(response);
						if (response == false){
							$('#nameInfo').html('').append('<img src="'+protocol+"//"+hostname+"/"+pathname[1]+'/images/icn_alert_success.png">');
						}
						else{
							$('#nameInfo').html('').append('<img id="ayuda" title="El nombre de CLIENTE que esta intentando registrar ya existe" src="'+protocol+"//"+hostname+"/"+pathname[1]+'/images/icn_alert_error.png">');
						}
					}
				});
		});

/*	--------------------------------------------------
	TODO :: Snipet: 
	Catch a Click event.
	Check if the element that was clicked was unselected, then select it
	Or if the element that was clicked was selected, then unselect it
	-------------------------------------------------- */

	$('select option').mousedown(function(){
			select = $(this).attr('selected');
			//console.log($(this).attr('selected'));
	});
	
	$('select option').mouseup(function(){
			//console.log($(this).attr('selected'));
			if($(this).attr('selected') === select){
				$(this).removeAttr('selected');
				//console.log('unselect');
			}
	});
	
});


/*	--------------------------------------------------
	:: Pre-Sleccion de los operadores registrados
	-------------------------------------------------- */

function leerDatos(){

	var nombre = document.getElementById("cNombre2").value;
	$.ajax({
		url: $('#url_js').val() + getUrl + '/' + nombre + '/' + 1,
		dataType: 'json',
		success: function(response) {
				$("#selectOperador option[value="+response.Operador+"]").attr("selected",true);
				$("#selectBackup option[value="+response.Backup+"]").attr("selected",true);
				
				var otros = response.Otros.split('|');
				for(var op in otros){
					if(otros[op] != ""){
						$('#selectCT1 option').filter('[value="'+otros[op]+'"]').attr("selected",true);
					}
				}
				var analistas = response.Analistas.split('|');
				for(var analista in analistas){
					if(analistas[analista] != ""){
						$('#selectAnalistas option').filter('[value="'+analistas[analista]+'"]').attr("selected",true);
					}
				}
		}
	});
}