/**
Gestor de Eventos
Operador.js

Genera todos las eventos asociados de la vista operador
Acciones que se realizan mediante este archivo
- Carga de la tabla de eventos
- Cambio de estados de los eventos
- Carga de los filtros y detalles de eventos
- Consultas de eventos segun filtros seleccionados
- Pruebas a los dispositivos
- Receso de actividades

Autor:
Jonathan Araneda Labarca (jaraneda@kudaw.com)
Kudaw ltda. 2011-2012
 
v2.2 Oct 2012
*/

/*	--------------------------------------------------
	:: Declaracion de variables globales
	-------------------------------------------------- */

var bMenu					= true;				//Para realizar "Fijar" del menú de filtros
var bMoviendo 				= false;
var bTest					= true;				//Verifica si otra prueba se esta realizando o no
var oTable;											//Objeto datatables
var oTable_v2;
var oTable_v3;
var iCDown 					= 0;					//Contador interno de eventos DOWN
var aEventosChecked		    = new Array();		//Eventos seleccionados
var aEventosChecked_servidor  = '';		//Eventos seleccionados
var aEventosChecked_sistema	  = '';		//Eventos seleccionados
var bFlagInicial			= 0;
var oFilaSeleccionada;							//Se mantiene la fila seleccionada

var arrayFilterType				= new Array();
arrayFilterType['estado'] 		= 'base',
arrayFilterType['pais'] 		= 'base',
arrayFilterType['negocio'] 		= 'base',
arrayFilterType['servicio'] 	= 'base',
arrayFilterType['organization']	= 'base';
arrayFilterType['tag']			= 'base';
// arrayFilterType['region'] 		= 'masivo',
// arrayFilterType['fw'] 			= 'masivo',
// arrayFilterType['pe'] 			= 'masivo',
// arrayFilterType['sucursal'] 	= 'masivo';

var arrayActiveFilters			= new Array();

/*	--------------------------------------------------
	:: Declaracion carga iniciar y de asociacion accion-dom
	-------------------------------------------------- */

$(document).ready(function(){
	
	loadTable_Evento();
	getActualFilters(true);
	
/*	--------------------------------------------------
		:: Nuevas solicitudes Adessa
		-------------------------------------------------- */
	
	$("#tabs").tabs({
		 select: function (event, ui) {
			var tabID = ui.index;
			closeNav();

			if (tabID == 0) {
				loadTable_Evento();
			} else if (tabID == 1){
				loadTable_Servidor();
			} else {
				loadTable_Pais_Negocio();
			}
		}
	});
	
	

	
	
/*	--------------------------------------------------
	:: Valores iniciales
	-------------------------------------------------- 	*/
	
	// Recargar la tabla de eventos
	setInterval(function () {
		getActualFilters();
	},30*1000);
	
	
	$("#eventos").selectable({
			filter: "tr",
			distance: 1,
			start: function(event, ui) {
				$(this).find('.ui-selected').removeClass('marcado');
			},
			selecting: function(event, ui) {
				$(ui.selecting).find('td .check:checkbox').attr('checked', true);
				bCEstado = bCambiarEstado();
			},
			unselecting: function(event, ui) {
				$(ui.unselecting).find('td .check:checkbox').attr('checked', false);
				bCEstado = bCambiarEstado();
			},
			selected: function(event, ui){
				$(this).find('.ui-selected').addClass('marcado');
			}
      });
	  
/*	--------------------------------------------------
	:: Dialogos de confirmacion y desicion
	-------------------------------------------------- */
	$('#dialog-confirmarProcesado').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Si': function() {
				cambiarEstado($('#selCambioEstado').val());
				$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#dialog_aticket').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Si': function() {
				$( this ).dialog( 'close' );
			}
		}
	});
	
	
	$('#dialog-ticket').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
			$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#dialog_enproceso').dialog({
		nameUser: '',
		width: 700,
		height: 350,
		autoOpen: false,
		buttons: {
			'No': function() {
			$( this ).dialog( 'close' );
			},
			'Si': function() {
				$('#dialog-confirmarProcesado').dialog('open');
				$( this ).dialog( 'close' );
			}
		}
	});

	$('#dialog_mover').dialog({
			nameUser: '',
			width: 800,
			height: 400,
			autoOpen: false,
			modal: true,
			buttons: {
				'Cancelar': function() {
					$('#mover_sComment').val("");
					$( this ).dialog( 'close' );
				},
				'Mover': function() {
					$('#dialog-confirmarProcesado').dialog('open');
					$( this ).dialog( 'close' );
				}
			}
		});
	
	$('#dialog_boleta').dialog({
		nameUser: '',
		width: 400,
		height: 500,
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
				creaTicket();
				$( this ).dialog( 'close' );
			},
			'Cancelar': function() {
				$( this ).dialog( 'close' );
			}
		}
	});
	
	$('#dialog_otro').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
			$( this ).dialog( 'close' );
			},
			'Si': function() {
				if($('#otro_sComent').val())
				{
					$('#dialog-confirmarProcesado').dialog('open');
					$( this ).dialog( 'close' );
				} else {
					alert("Valor obligatorio");
				}
			}
		}
	});
	
	$('#dialog_bolconf').dialog({
		autoOpen: false,
		buttons: {
			'Cancelar' : function() {
				$(this).dialog('close');
			},
			'Aceptar' : function() {
				var aIdGrupo  = new Array();
				var aClientes = new Array();
				var aNodos    = new Array();
				
				$('.check:checked').each(function(){
					aIdGrupo.push($(this).attr('name'));
					aClientes.push($(this).parent().parent().find("td:eq(10)").text());
					aNodos.push($(this).parent().parent().find("td:eq(11)").text());
				});
				
				$('#boleta_sCodServMan').removeAttr('style');
				$('#boleta_sCodServMan').attr("style","visibility:visible"); 
				
				aClientes = unique(aClientes);
				aNodos    = unique(aNodos);
				
				$('#boleta_sGevs').val(aIdGrupo.join(','));
				$.ajax({
					url: "c_principal/codServicio",
					type: "POST",
					data: {nodos:aNodos},
					success: function(html){
						$('#boleta_sCodServ').html(html);
					}
				});
				$.ajax({
					url: "c_principal/codServ",
					type: "POST",
					data: {nodos:aNodos},
					success: function(html){
						$('#boleta_sCodigos').val(html);
					}
				});
				$.ajax({
					url: "c_principal/Email",
					type: "POST",
					success: function(html){
						$('#boleta_sCorreo').val(html);
					}
				});
				$.ajax({
					url: "c_principal/cliId",
					type: "POST",
					data: {clientes:aClientes},
					success: function(html){
						$('#boleta_sCliente').val(html);
					}
				});
				
				$('#boleta_sFuente').val('Plataforma GDE');
				$('#boleta_sOrigenticket').val('GDE');
				
				$('#boleta_sTipinfo').val('Cometarios GDE\nInformacion desde GDE');
				$('#boleta_sOrigen').val('Monitor');
				
				$('#dialog_boleta').dialog('open');
				$(this).dialog('close');
			}
		}
	});
	
	$('#dialog-procesado').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'Aceptar': function() {
			$( this ).dialog( 'close' );
			}
		}
	});
	
	
});

/*	--------------------------------------------------
	:: Fase 3. Generacion y Actualizacion de filtros
	-------------------------------------------------- */
		
	/**
	* Funcion callback para actualizar datos en filtros de busqueda.
	* 				Actualiza la tabla de filtros segun el tipo solicitado
	*
	* @input:	object	filterType	contiene la informacion de la solicitud del filtro
	*				string	htmlFIlter	string html con resultado de la llamada a filtro
	*/
	function actualizarDivFiltro( filterType, htmlFilter){
		//console.log("Actualizar DIv Filtro");
		var filterData	= $(htmlFilter);
		// Se recorren los filtros para marcar aquellos que se encontraban marcados antes de la actualizacion
		filterData.find('input[type="checkbox"]').each(function(){
			
			// Se deja marcado inicialmente el valor del filtro de estados Activo
			if( bFlagInicial < 2 && ($(this).val()=='Activo' || $(this).val()=='En Proceso') ){
				$(this).prop('checked', true);
				bFlagInicial += 1;
			}
			
			// Se marcan los filtros previamente seleccionados
			if( arrayActiveFilters.indexOf($(this).attr('name')) > -1 ){
				$(this).prop('checked', true);
			}
			
			// Se aplican clases a filtro cliente (coloreo rojo en filtro cliente)
			if( $(this).hasClass('clienteDown') ){
				$(this).parent().parent().attr('style','background-color: #FF0000; color: #000000;');
			}
		});
		$('#filtro_' + filterType.tipoFiltro).html('').append( filterData );
		//bDesabilitarSucursales();
	}

	/**
	* Funcion para realizar consulta ajax y manejar respuestas.
	* 				Si la respuesta es ok se ejecuta la function callback solicitada
	*
	* @input:	string					urlRequest		url de solicitud
	*				object					dataObj			data para enviar vía post para solicitud
	*				callbackFunction		function			funcion callback a ser ejecutada cuando se ejecute correctamente la llamada
	*/
	function peticionASYNC(urlRequest, dataObj, callbackFunction){
		return $.ajax({
			url:		urlRequest,
			type:		'POST',
			data:		dataObj,
			cache:		false,
			success:	function(response){
				var filterResponse = JSON.parse(response);
				if( filterResponse.stt == 'error' ){
					// alert(filterResponse.msg);
					console.log(filterResponse);
				}
				else{
					callbackFunction( dataObj,  filterResponse.msg);
				}
				
				//return false;
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest);
				console.log(textStatus);
				console.log(errorThrown);
				// alert("Se ha detectado un problema en la consulta a servidor, favor intentar nuevamente. Si el problema persiste comunicar a administrador");
			}
		});
	}

	/**
	* Funcion para solicitud de actualizacion de filtros.
	*
	* @input:
	*/
	function getActualFilters(isFirstload){
		//console.log("--->Actulaizar filtros");
		
		var bool = 0;

		arrayActiveFilters = new Array();
		arrayActiveFilters = $('.filtro:checked').map(function(){ return this.name; }).get();

		for(var index in arrayFilterType){
			var dataObj			= {'tipoFiltro':index, 'classFilter':arrayFilterType[index], 'activeFilter':arrayActiveFilters},	// Objeto con el tipo de filtro solicitado
			controllerRequest	= 'c_filtro',										// Controlador a llamar en la solicitud ajax
			functionRequest		= 'filterPetitionRouter';		// Funcion del controlador a llamar en la solicitud ajax
			
			var urlRequest		= controllerRequest +"/"+  functionRequest;

			//console.log(dataObj);
			peticionASYNC(urlRequest, dataObj, actualizarDivFiltro).done(function(data){
				bool += 1;
				if (bool == 6 && isFirstload != true) {
					//console.log("Filtrando");
					filtrar(true);
				}
			});

			
		}
	}

	/**
	* Accion sobre checkbox de filtros para realizar 
	*
	*/
	$('.filtro').live('change',function(){
		//console.log("Filtrando");
		getActualFilters(false);
		
		/*$("#eventos_v2 tbody tr.row_selected").removeClass('row_selected');
		$("#row_header_v1").addClass("ui-tabs-hide");
		$('#titulo_eventos_detalle').css('display','none');*/
	});

	


	// Se realiza la busqueda del campo de texto
	$('.filtro_texto').live('keyup',function(e){
		var id = $("#tabs li.ui-state-active").attr('id');
		
		if(e.keyCode == 13) {
			if(id == 1){
				oTable_v2.fnDraw();
			} else {
				filtrar(false);
			}
		}
	});
	
/*	--------------------------------------------------
	:: Funciones de soporte para el funcionamiento
	-------------------------------------------------- */
	
	function AfterSendEmail(){
		// oTable.fnDraw();
		filtrar(true);
	}
	
	// Funcion que devuelve los valores unicos de un Array
	var unique = function(origArr) {  
		var newArr = [],  
			origLen = origArr.length,  
			found,  
			x, y;  
	  
		for ( x = 0; x < origLen; x++ ) {  
			found = undefined;  
			for ( y = 0; y < newArr.length; y++ ) {  
				if ( origArr[x] === newArr[y] ) {  
				  found = true;  
				  break;  
				}  
			}  
			if ( !found) newArr.push( origArr[x] );  
		}  
	   return newArr;  
	};
	
	// Funcion que entrega un string con los valores de los filtros seleccionados
	function getFiltrosActivos( tipo_filtro ){
		v = new Array();
		
		if(tipo_filtro == "texto"){
			b = $('.filtro_'+tipo_filtro).val();
			v.push(b);
		} else {
			$('.filtro_'+tipo_filtro+':checked').each(function(){
				v.push($(this).val());
			});
		}
		return v.join("@");
	}
	
	// Funcion que llama a fnFilter, con los filtros seleccionados, para filtrar la tabla de eventos
	function filtrar( esFiltroDesdeCheckbox ){
		
		var id = $("#tabs li.ui-state-active").attr('id');
		 
		if(id == 0){
			aEventosChecked = $('.check:checked').map(function(){ return this.value; }).get();
			oTable.fnDraw();
		} else if (id == 1){
			aEventosChecked_servidor = $('.row_selected').find('td:eq(1)').html();
			//console.log(aEventosChecked_servidor);
			oTable_v2.fnDraw();
		} else {
			oTable_v3.fnDraw();
		}
	}
	
	// Funcion que, segun los estados de los nodos seleccionados, genera las opciones para mover de estados los eventos
	function bCambiarEstado(){
		var aCheck = new Array();
		// var aCli = new Array();
		var i = 0;
		$('.check:checked').each(function(){
			i += 1;
			aCheck.push($(this).parent().parent().find("td:eq(3)").text());
			// aCli.push($(this).parent().parent().find("td:eq(7)").text());
		});
		// aCli = unique(aCli);
		aCheck = unique(aCheck); // Estados de los eventos seleccionados
		if(aCheck.length == 1){
			if(aCheck[0] == 'Activo'){
				
				$.ajax({
					url: "c_principal/obtener_type",
					type: "POST",
					success: function(html){
						$('#selCambioEstado option').remove();
						
						if(html == 1){
							$('#selCambioEstado').append('<option value="En Proceso">En Proceso</option>');
						} else {
							$('#selCambioEstado').append('<option value="En Proceso">En Proceso</option><option value="Deshabilitar">Deshabilitar</option>');
							// $('#selCambioEstado').append('<option value="En Proceso">En Proceso</option><option value="Omitido">Omitido</option><option value="Deshabilitar">Deshabilitar</option>');
						}
						
						$('#cant_select').html(i);
						$('#nom_select').html(aCheck[0]);
						$('#subCambioEstado').prop('disabled', false);
						return true;
					}
				});
				
				
				
			}
			// else if(aCheck[0] == 'En Proceso' && aCli.length == 1){
				// $('#selCambioEstado option').remove();
				// $('#selCambioEstado').append('<option value="Boleta">Boleta</option><option value="Cerrado">Cerrado</option>');
				// $('#selCambioEstado').append('<option value="Cerrado">Cerrado</option>');
				// $('#cant_select').html(i);
				// $('#nom_select').html(aCheck[0]);
				// $('#subCambioEstado').prop('disabled', false);
				// return true;
			// }
			else if(aCheck[0] == 'En Proceso' || aCheck[0] == 'Deshabilitar'){
				$('#selCambioEstado option').remove();
				$('#selCambioEstado').append('<option value="Cerrado">Cerrado</option>');
				$('#cant_select').html(i);
				$('#nom_select').html(aCheck[0]);
				$('#subCambioEstado').prop('disabled', false);
				return true;
			}
			// else if(aCheck[0] == 'Omitido'){
				// $.ajax({
					// url: "c_principal/obtener_type",
					// type: "POST",
					// success: function(html){
						// if(html == 2){
							// $('#selCambioEstado').append('<option value="En Proceso">En Proceso</option>');
						// }
					// }
				// });
			// }
			else{
				$('#selCambioEstado option').remove();
				$('#selCambioEstado').append('<option value="">-----</option>');
				$('#cant_select').html(i);
				$('#nom_select').html(aCheck[0]);
				$('#subCambioEstado').prop('disabled', true);
				return false;
			}
		}
		else{
			$('#selCambioEstado option').remove();
			$('#selCambioEstado').append('<option value="">-----</option>');
			$('#cant_select').html(i);
			$('#nom_select').html('');
			$('#subCambioEstado').prop('disabled', true);
			return false;
		}
	}
	
	// Funcion que mueve los eventos al estado seleccionado
	function cambiarEstado(sEstado){
		aGrupos    = new Array();
		aTicket    = new Array();
		
		i = 0;
		$('.check:checked').each(function(){
			aGrupos.push($(this).attr('name'));
			aTicket.push($(this).parent().parent().find("td:eq(4)").html());
			i += 1;
		});
		
		if(sEstado === 'En Proceso'){ // sEstado corresponde al estado al cual se cambian
			comentario = $('#enproc_sComent').val();
		}
		// else if(sEstado === 'Boleta'){ 
			// comentario = "paso a boleta";
		// }
		else{
			comentario = $('#otro_sComent').val();
		}
		
		// console.log(aGrupos.join(','));
		// console.log(sEstado);
		// console.log(comentario);
		// console.log(aTicket);
		
		$.post('c_principal/cambiar_estado', {'aGrupos':aGrupos.join(','), 'sEstado':sEstado, 'sComentario': comentario} , function(data){
			filtrar();

			status  = '';
			mensaje = data.split('|');

			if(mensaje[0] == 0){
				status = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
			}
			else{
				status = '<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>';
			}
			$('#dialog-procesado').html('').append('<p>'+status+''+mensaje[1]+'</p><p>Enviado información a la mesa de ayuda.');
			$('#dialog-procesado').dialog('open');
			
			
			// Integración con MDA Adessa
			$.post('../c_sendAutomaticTicket/update', {'aTicket':aTicket.join(','), 'aGrupos': aGrupos.join(','), 'sEstado':sEstado, 'sComentario': comentario} , function(data){
				console.log(data);
				
			});
			
			oTable.fnDraw();
		});
	}
	
	// Se realiza el efecto de "ocultar" los hijos del arbol
	$('.padre_arbol').live('click', function(e){
		e.preventDefault();
		$(this).next('ul').toggle();
		root = $(this).find('span');
		if( root.text() == "+" ){
			root.text("-");
		}
		else{
			root.text("+");
		}
		root.toggleClass('blue').toggleClass('white');
	});

/*	--------------------------------------------------
	:: Movimiento Header
	-------------------------------------------------- */
	
	$('.mov_contenedor_header').live('click', function(){
		var value = $('#row_header').attr('style').split(':')[1].split('px;')[0];
		if(value == 0){
			$('#row_header').attr('style','margin-top:-120px;');
			$(this).text("Desplegar encabezado");
		}else{
			$('#row_header').attr('style','margin-top:0px;');
			$(this).text("Ocultar encabezado");
		}
	});
	
/*	--------------------------------------------------
	:: Despliegue menu de pruebas y acciones
	-------------------------------------------------- */

	// Despliegue de filtros en header
	$('.select_filtro_menu').live('click', function(){
		var value = $('.contenedor_filtros_superior').css('display');
		if (value == "none") $('.contenedor_filtros_superior').css('display','block');
		else $('.contenedor_filtros_superior').css('display','none');
	});
	
	// Despliegue de menu en header
	$('#sel_menu_header').live('click', function(){
		$('#options_menu_header').slideToggle("fast");
	});
	
	// Despliegue de menu de pruebas
	$('.menu_boton').live('click',function(){
		if(bTest){
			$('.menu').remove();
			if ( $(this).parent().children().next().html() != null){
				return;
			}
			else{
				var pos = $(this).offset();  
				var width = $(this).width();  
				var reposition = { left: (pos.left - 200 + width) + 'px', top: pos.top - 140 + 'px' }; 
			
				var tablaMenu = $('<div class="menu">'
				+'<table>'
				+'	<tr>'
				+'		<td>ping</td>'
				+'	</tr>'
				+'	<tr>'
				+'		<td>traceroute</td>'
				+'	</tr>'
				+'</table>'
				+'</div>');
				
				$(this).parent().append(tablaMenu);
				
				tablaMenu.css({  
					left: reposition.left,  
					top: reposition.top  
				});
				tablaMenu.css('display', 'block');
				return;
			}
		}
	});
	
	// Eliminar menu
	$('.menu').live('mouseleave',function(){
		////console.log(bTest);
		if(bTest){
			$('.menu').remove();
		}
	});
	
	// Realizar accion en opciones de menu
	$('.menu td').live('click',function(){
		if(bTest){
			bTest		= false;
			nodo		= $(this).parentsUntil(".menu_acciones").parentsUntil("tbody").find('td:eq(12)').text();
			cliente	= $(this).parentsUntil(".menu_acciones").parentsUntil("tbody").find('td:eq(10)').text();
			nomnodo	= $(this).parentsUntil(".menu_acciones").parentsUntil("tbody").find('td:eq(11)').text();
			thtml		= $(this).html();
			obj		= $(this);

			$(this).append('<img src="../images/loading_bar.gif">');
			$.post( "c_prueba/cargaIframe", { 'nodo': nodo, 'nomnodo':nomnodo,'cliente': cliente, 'cmd':thtml}, function( data ) {
				bTest = true;
				var w = window.open('', '', 'width=650,height=450');
				w.document.write(data);
				w.document.close();
				obj.html(thtml);
			});
		}
	});

/*	--------------------------------------------------
	:: Marcar casillas de eventos
	-------------------------------------------------- */
	$("#checkall").live('change',function(e){
		$("#div_folio_manual").css("display","none");
		e.preventDefault();
		var chstatus = $(this).is(':checked');
		//console.log(chstatus);
		if(chstatus == true){
			$('#eventos tbody tr td input[type=checkbox]:not(:checked)').attr("checked", "checked"); 
			$('#eventos tbody tr').addClass('marcado');
		}
		else{
			$('#eventos tbody tr td input[type=checkbox]:checked').removeAttr("checked");  
			$('#eventos tbody tr').removeClass('marcado');
		}
		bCambiarEstado();
	});
	
	$('.check').live('change',function(){
		$("#div_folio_manual").css("display","none");
		if($(this).is(':checked')){
			$(this).parent().parent().addClass("marcado");
		}
		else{
			$(this).parent().parent().removeClass("marcado");
		}
		
		bCEstado = bCambiarEstado();
	});
	
/*	--------------------------------------------------
	:: Cambiar estado de eventos
	-------------------------------------------------- */
	$('#subCambioEstado').live('click',function(e){
		e.preventDefault();
		$('#tMover tbody').empty();
		$('#enproc_sComent').val('');

		sEstado  = $('#selCambioEstado').val();
		// console.log(sEstado);

		if(sEstado == 'En Proceso'){
			if($('#dialog_otro').dialog( "isOpen" )===true){ $('#dialog_otro').dialog( "close" ); }
			$('.check:checked').each(function(){
				var fecha     = $(this).parent().parent().find("td:eq(5)").text();
				var tipo      = $(this).parent().parent().find("td:eq(6)").text();
				var nonbre    = $(this).parent().parent().find("td:eq(7)").text();
				var severidad = $(this).parent().parent().find("td:eq(1)").text();
				$('#tMover').append('<tr><td>'+fecha+'</td><td>'+severidad+'</td><td>'+nonbre+'</td><td>'+tipo+'</td><td></tr>');
			});

			fecha = new Date();
			$('#enproc_sFecha').text(String(fecha));
			$('#dialog_mover').dialog('open');
		}
		else if(sEstado == 'Omitido'){
			/*var tick_act = true;
			var tickets_activos = new Array();
			
			$('.check:checked').each(function(){
				// Columna de ticket con TICKET_ID
				tickets_activos.push($(this).parent().parent().find("td:eq(4)").text());
			});
			
			var blockedTile = new Array("ERROR", "", " ");
			
			for(var i=0; i<tickets_activos.length; i++){
				// Columna de ticket con TICKET_ID
				if(blockedTile.indexOf(tickets_activos[i]) == -1){
					tick_act = false;
				}
			}*/
			
			if($('#dialog_otro').dialog( "isOpen" )===true){
				$('#dialog_otro').dialog( "close" ); 
			}
			
			/*if(!tick_act){
				alert("Algunos de los eventos seleccionados ya se encuentran en ticket. Estos no pueden ser omitidos.");
			}*/
			// else{
				if($('#dialog_enproceso').dialog( "isOpen" )===true){ $('#dialog_enproceso').dialog( "close" ); }
				$('#otro_sComent').val('');
				$('#dialog_otro').dialog('open');
			// }
			
		}
		else if(sEstado == 'Cerrado'){
			//var tick_act = true;
			//var tickets_activos = new Array();
			
			/*$('.check:checked').each(function(){
				// Columna de ticket con TICKET_ID
				tickets_activos.push($(this).parent().parent().find("td:eq(4)").text());
			});
			
			var blockedTile = new Array("ERROR", "", " ");
			
			for(var i=0; i<tickets_activos.length; i++){
				// Columna de ticket con TICKET_ID
				if(blockedTile.indexOf(tickets_activos[i]) == -1){
					tick_act = false;
				}
			}*/
			if($('#dialog_otro').dialog( "isOpen" )===true){
				$('#dialog_otro').dialog( "close" ); 
			}
			
			/*if(!tick_act){
				alert("Algunos de los eventos seleccionados ya se encuentran en ticket. Estos serán cerrados automaticamente cuando se cierre el ticket.");
			}*/
			//else{
				if($('#dialog_enproceso').dialog( "isOpen" )===true){ $('#dialog_enproceso').dialog( "close" ); }
				$('#otro_sComent').val('');
				$('#dialog_otro').dialog('open');
			//}
			
		}
		else{
			if($('#dialog_enproceso').dialog( "isOpen" )===true){ $('#dialog_enproceso').dialog( "close" ); }
			$('#otro_sComent').val('');
			$('#dialog_otro').dialog('open');
		}
	});

/*	--------------------------------------------------
	:: Cargar detalle de evento y nodo
	-------------------------------------------------- */
    $("#eventos tbody tr").live('dblclick', function(){
		
		if ( !$(this).hasClass('row_selected') ) { // Se resalta linea seleccionada
			$("#eventos tbody tr.row_selected").removeClass('row_selected');
			$('#eventos tbody tr td input[type=checkbox]:checked').removeAttr("checked"); 
			$(this).addClass('row_selected');
			$(this).find('td:eq(0)').children().prop("checked",true);
		}
		
		var nodo      = $(this).find('td:eq(6)');                            // Se captura objeto nodo
		var ticketID  = $(this).find('td:eq(4)').text();
		var grupoId   = $(this).find('td:eq(0)').children().attr('name');    // Se captura id de grupo evento
		oFilaSeleccionada = grupoId;
		
		// console.log(ticketID);
		if (ticketID == "Sin Ticket"){
			$("#div_folio_manual").css("display","block");
		} else {
			$("#div_folio_manual").css("display","none");
		}
		
		bCEstado = bCambiarEstado();
		
		//TODO: agregar error: ante perdida de coneccion
		$(".boxTitle").html($(nodo).text());
		//$(".boxTitle_gevid").html(grupoId+' - ');
		$.ajax({
		  url: "c_detalle/obtenerHistoria",
		  type: "POST",
		  //data: "nodo="+$(nodo).text()+"&grupoId="+grupoId+"&nombre="+$(nombre).text(),
		  data: "grupoId="+grupoId,
		  cache: false,
		  success: function(html){
			// console.log(html);
			obj = jQuery.parseJSON(html);
			$("#bloqueHistoria").html('').html(obj.hist);
			$("#bloqueSumariza").html('').html(obj.sumz);
			$("#comGruId").val(grupoId);
			$("#comTicketId").val(ticketID);
			$("#bloqueComentar").html('').html(obj.cmnt);

			// Mostrar div lateral
			/*$('#mov_filtro_der').css('display','block');
			var parentObject = $('#eventos_wrapper').parents(':eq(2)');
			parentObject.removeClass('twelve').addClass('eight');*/
			openNav();
			
		  }
		});
    });
	
/*	--------------------------------------------------
	:: Agregar multiples comentarios
	-------------------------------------------------- */
	function enviarMultiplesComentarios(){
		var coment      = $('#comentario').val();
		var grId        = $("#comGruId").val();
		var comTicketId = $("#comTicketId").val();
		var envMDA      = 0;
		
		if(grId != "comGruId" && coment){
			if ($('#comenvMDA').is(":checked")){
				envMDA = 1;
				$("#bloqueComentar").html('').html("Enviando mensaje a Mesa de Ayuda");
			}
			$.ajax({
			  url: "c_detalle/registrarComentario",
			  type: "POST",
			  data: "coment="+coment+"&grupoId="+grId+"&envMDA="+envMDA+"&comTicketId="+comTicketId,
			  cache: false,
			  success: function(html){
				////console.log(html);
				var coment  = $('#comentario').val('');
				$("#bloqueComentar").html('').html(html);
				oTable.fnDraw(false);
			  }
			});
		}
		else if(grId == "comGruId"){
			alert("Se debe seleccionar un evento (doble click en tabla)");
		}
		else{
			alert("Favor ingrese un comentario");
		}
	}
	
	$("#envComentario").live('click', function( e ){
		e.preventDefault();
		enviarMultiplesComentarios();
	});
	
	$("#comentario").live('keyup',function(e){
		if(e.keyCode == 13) {
			enviarMultiplesComentarios();
		}
	});

/*	--------------------------------------------------
	:: Exportar como texto plano
	-------------------------------------------------- */
	
	$('#exportar').live('click', function(e){
		e.preventDefault();
		
		$("#tablexport").html("");
		
		if($('.check:checked').length > 0 ) {
			// var datahtml = "<thead>"+$('#eventos thead').html()+"</thead><tbody>"
			var datahtml ='<thead><tr><th></th><th>SEVERIDAD</th><th>CORREO</th><th>ESTADO</th><th>FOLIO MDA</th><th>FECHA</th><th>TIPO ALERTA</th><th>NOMBRE</th><th>Ac.</th><th>IP</th><th>AREA SOPORTE</th><th>UBICACION DEL ELEMENTO</th><th>DESCRIPCION DEL ELEMENTO</th><th>SERVICIO</th><th>NEGOCIO</th><th>PAIS</th><th>HERRAMIENTA</th></tr></thead><tbody>'
			$('.check:checked').each(function(){
				var padreTR	= $(this).parent().parent();
				datahtml += ("<tr>"+padreTR.html()+"</tr>");
			});
			datahtml += "</tbody>";
			
			$("#tablexport").append($(datahtml));
			$("#tablexport").tableExport({type:'excel',escape:'false'});
			
		} else {
			
			var exportData = new Array();
			
			var v_estado       = getFiltrosActivos('estado');
			var v_pais         = getFiltrosActivos('pais');
			var v_negocio      = getFiltrosActivos('negocio');
			var v_servicio     = getFiltrosActivos('servicio');
			var v_organization = getFiltrosActivos('organization');
			var v_tag          = getFiltrosActivos('tag');
			var v_search       = $('.filtro_texto').val();
			var v_criticidad   = $('.filtro_criticidad').val();
			
			if(v_estado == '') v_estado='Activo';
			
			exportData.push(
				{"name":"v_estado", "value": v_estado},
				{"name":"v_pais", "value": v_pais},
				{"name":"v_negocio", "value": v_negocio},
				{"name":"v_servicio", "value": v_servicio},
				{"name":"v_organization", "value": v_organization},
				{"name":"v_tag", "value": v_tag},
				{"name":"v_search", "value": v_search},
				{"name":"v_criticidad", "value": v_criticidad});
			
			$.ajax({
				url: "c_principal/obtener_eventos_export",
				type: "POST",
				cache: false,
				data: exportData,
				success: function(data){
					var blob = new Blob([data], { type: "application/vnd.ms-excel" });
					var URL = window.URL || window.webkitURL;
					var downloadUrl = URL.createObjectURL(blob);
					document.location = downloadUrl;
					//obj = jQuery.parseJSON(datahtml);
					//console.log(obj);
					//$("#tablexport").append($(obj));
					//$("#tablexport").tableExport({type:'excel',escape:'false'});
				}
			});
		}
	});
	
/*	--------------------------------------------------
	:: Envio de Correos
	-------------------------------------------------- */
	$('#envioSidenavMail').live('click', function(e){
		e.preventDefault();
		$('#se-pre-con').show();
		$.post('../c_sendNewEmail/sendEmail', $('#form').serialize() ,function(data){
			$('#se-pre-con').hide();
			closeNavMail();
			alert(data);
		});
	});

	$('#noenvioSidenavMail').live('click', function(e){
		e.preventDefault();
		closeNavMail();
	});
	

	$('#sendSidenavMail').live('click', function(e){
		e.preventDefault();
		
		var sameclient		= true;
		var estadovalido	= true;
		var segui			= true;
		var estados			= new Array();
		var clientes		= new Array();
		var nodos			= new Array();
		var seguimientos	= new Array();
		var correosJSON;
		var correoUSER;
		var id = '';
		var corr = '';
		var corr2 = '';
		
		// Campos del correo Adessa
		var mail_severidad = '';
		var mail_pais      = '';
		var mail_negocio   = '';
		var mail_servicios = '';
		var mail_area	   = '';
		var mail_tipo_disp = '';
		var mail_ubicacion = '';
		var mail_nombre    = '';
		var mail_ip        = '';
		var mail_fecha     = '';
		var mail_tipo_aler = '';
		
		var mail_mensaje_mda = '';
		
		$('.check:checked').each(function(){
			var padreTR	= $(this).parent().parent();
			
			estados.push(padreTR.find("td:eq(3)").html());
			seguimientos.push(padreTR.find("td:eq(2)").html());
			clientes.push(padreTR.find("td:eq(7)").html());
			nodos.push(padreTR.find("td:eq(6)").html());

			id             = padreTR.find("td:eq(0)").children().attr('name');
     		mail_severidad = padreTR.find("td:eq(1)").html();
			mail_pais      = padreTR.find("td:eq(15)").html();
			mail_negocio   = padreTR.find("td:eq(14)").html();
			mail_servicios = padreTR.find("td:eq(13)").html();
			mail_area	   = padreTR.find("td:eq(10)").html();
			mail_tipo_disp = padreTR.find("td:eq(12)").html();
			mail_ubicacion = padreTR.find("td:eq(11)").html();
			mail_nombre    = padreTR.find("td:eq(7)").html();
			mail_ip        = padreTR.find("td:eq(9)").html();
			mail_fecha     = padreTR.find("td:eq(5)").html();
			mail_tipo_aler = padreTR.find("td:eq(6)").html();

		});
		
		clientes = unique(clientes);
		
		if ( clientes.length > 1 && nodos.length > 1){ sameclient = false; }
		if ( clientes.length == 0 ){ sameclient = false; }
		
		for(var i=0; i<estados.length; i++){
			if ((estados[i] == 'Omitido') || (estados[i] == 'Cerrado')){
				estadovalido = false;
			}
		}
		
		if(sameclient && estadovalido && segui){
			//console.log(mail_area);
			$.post('../c_sendNewEmail/toEmail', {'Area': mail_area} ,function(data){
				correosJSON=JSON.parse(data);
				//console.log(correosJSON);
				corr  = correosJSON.para;
				corr2 = correosJSON.copia;
				$.post('../c_sendNewEmail/getMensaje', {'Id': id} ,function(mensaje){

				var mail_mensaje = JSON.parse(mensaje)
					// Adaptacion FALABELLA
					var nom   = "Gestion de Eventos";
					var alias = "cdge@Falabella.cl";
					
					if(mail_severidad == "CRITICAL"){
						if(mail_servicios.match('No catalogado')) {
							mail_mensaje_mda = "Señores MDA, favor generar folio y derivar ticket a: "+mail_area;
						} else {
							mail_mensaje_mda = "Señores MDA, favor generar folio y contactar telefónicamente a: "+mail_area+", ya que degrada a los servicios: " +mail_servicios.replace("|","");
						}
					} else if (mail_severidad == "MAJOR"){
						mail_mensaje_mda = "Señores MDA, favor generar folio y derivar ticket a: "+mail_area;
					} else {
						mail_mensaje_mda = "Señores "+mail_area+", favor revisar la siguiente notificación.";
					}
					
					/*var da = new Date();
					var fechact = da.toLocaleString();
					$('#form #mail_fechactual').val(fechact);

					// Datos basicos
					$("#form #mail_desde_show").val(nom + " <" + alias +">");
					$("#form #mail_desde").val(alias);
					$("#form #mail_desde_nom").val(nom);
					$("#form #mail_para").val(corr);
					$("#form #mail_copia").val(corr2);
					$("#form #mail_asunto").val("Notificación de Incidente - " + mail_nombre + " - " + mail_tipo_aler);
					
					// Detalles
					$("#form #mail_severidad").val(mail_severidad);
					$("#form #mail_pais").val(mail_pais);
					$("#form #mail_negocio").val(mail_negocio);
					$("#form #mail_servicios").val(mail_servicios);
					$("#form #mail_area").val(mail_area);
					$("#form #mail_tipo_disp").val(mail_tipo_disp);
					$("#form #mail_ubicacion").val(mail_ubicacion);
					$("#form #mail_nombre").val(mail_nombre);
					$("#form #mail_ip").val(mail_ip);
					$("#form #mail_fecha").val(mail_fecha);
					$("#form #mail_mensaje").val(mail_mensaje);
					$("#form #mail_tipo_aler").val(mail_tipo_aler);
					$("#form #mail_group_id").val(id);*/
					
					//openNavMail();
					
					$.post('../c_sendNewEmail/loadView', {'alias_nom':nom, 'alias': alias, 'para': corr, 'copia': corr2, 'group_id': id, 'mail_severidad': mail_severidad, 'mail_pais': mail_pais, 'mail_negocio': mail_negocio, 'mail_servicios': mail_servicios, 'mail_area': mail_area, 'mail_tipo_disp': mail_tipo_disp, 'mail_ubicacion': mail_ubicacion, 'mail_nombre': mail_nombre, 'mail_ip': mail_ip, 'mail_fecha': mail_fecha, 'mail_tipo_aler': mail_tipo_aler, 'mail_mensaje': mail_mensaje,'mail_mensaje_mda': mail_mensaje_mda} ,function(data){
						w = window.open('', '', 'width=600,height=500');
						w.document.write(data);
						w.document.close();
					});
				});
			});
		}
		else if(!estadovalido && !sameclient){
			alert("Favor seleccionar solo un evento<br/>Verificar que estado de los eventos sean Activo o En Proceso");
		}
		else if(!estadovalido){
			alert("Verificar que estado de los eventos sean Activo o En Proceso");
		}
		else if(!sameclient){
			alert("Favor seleccionar solo un evento");
		}
		else if(!segui){
			alert("Evento seleccionado ya se encuentran en seguimiento");
		}
	});
	
	
/*	--------------------------------------------------
	:: Funcionalidades Adessa
	-------------------------------------------------- */
	
	// Limpieza de filtros
	$('#clearFilter').live('click',function(e){
		var value = $(this).attr("name");
		$('.filtro_'+value+':checked').removeAttr("checked");
		getActualFilters(false);
	});
	
	$('#titulo_eventos_detalle_hijos').live('click',function(e){
		$("#eventos_v2 tbody tr.row_selected").removeClass('row_selected');
		$("#row_header_v1").addClass("ui-tabs-hide");
		$('#titulo_eventos_detalle').css('display','none');
	});
	
	function loadTable_Evento(){
		oTable = $('#eventos').dataTable({
			"bJQueryUI": true,
			"bAutoWidth": false,
			"sScrollX": "100%",
			"sScrollY": "68vh",
			"sScrollXInner": "230%",
			"bScrollCollapse": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength" : 50,
			/*"aoColumnDefs": [
			  { "bSearchable": false, "bSortable": false  , "aTargets": [0] }
			],*/
			"aoColumnDefs": [
		  { "sName": "ID"							,	"sWidth": "10px"  , "bSearchable": false, "bSortable": false  , "aTargets": [ 0 ] },
		  { "sName": "SEVERITY"						,	"sWidth": "10px"  , "bSearchable": false, "aTargets": [ 1 ] },
		  { "sName": "EMAIL"						,	"sWidth": "20px"  , "aTargets": [ 2 ] },
		  { "sName": "STATUS"							,	"sWidth": "60px" , "aTargets": [ 3 ] },
		  { "sName": "REMEDY"											,	"sWidth": "80px"  , "bSearchable": false, "aTargets": [ 4 ] }, 
		  { "sName": "FECHA" 											,	"sWidth": "140px", "aTargets": [ 5 ] },
		  { "sName": "TYPE"										,	"sWidth": "100px", "aTargets": [ 6 ] },
		  { "sName": "SRC_NAME"							,	"sWidth": "100px", "aTargets": [ 7 ] },
		  { "sName": "COUNT"							,	"sWidth": "20px" , "bSearchable": false , "aTargets": [ 8 ] },
		  { "sName": "IP"							,	"sWidth": "100px", "aTargets": [ 9 ] },
  		  //{ "sName": "LOCK_CONT"							,	"sWidth": "10px", "aTargets": [ 10 ] },
		  { "sName": "SRC_ORGANIZATION"					,	"sWidth": "120px", "aTargets": [ 10 ] },
		  { "sName": "TAG"					,	"sWidth": "120px", "aTargets": [ 11 ] },
		  { "sName": "SRC_CATEGORY"					,	"sWidth": "120px", "aTargets": [ 12 ] },
		  { "sName": "DEST_SERVICE"					,	"sWidth": "100px", "aTargets": [ 13 ] },
		  { "sName": "DEST_BUNIT"					,	"sWidth": "100px", "aTargets": [ 14 ] },
		  { "sName": "DEST_COUNTRY"							,	"sWidth": "80px", "aTargets": [ 15 ] },
		  //{ "sName": "SRC_CATEGORY"					,	"sWidth": "80px", "aTargets": [ 15 ] },
		  { "sName": "TOOL"					,	"sWidth": "100px", "aTargets": [ 16 ] }
		],
			"aaSorting": [[5,'desc']],
			//"aoSearchCols": [null,null,null,{ "sSearch": "Activo" },null,null,null,null,null,null,null,null,null,null,null,null,null],
			"oLanguage": {
				"sUrl": "../../js/spanish.txt"  // Idioma de la tabla
			},
			"bProcessing": true,
			"bServerSide": true,
			"bDeferRender": true,
			"bDestroy": true,
			"sAjaxSource": "c_principal/obtener_eventos",
			'fnServerData': function(sSource, aoData, fnCallback){
				
				var v_estado       = getFiltrosActivos('estado');
				var v_pais         = getFiltrosActivos('pais');
				var v_negocio      = getFiltrosActivos('negocio');
				var v_servicio     = getFiltrosActivos('servicio');
				var v_organization = getFiltrosActivos('organization');
				var v_tag          = getFiltrosActivos('tag');
				var v_search       = $('.filtro_texto').val();
				var v_criticidad   = $('.filtro_criticidad').val();
				
				if(v_estado == '') v_estado='Activo@En Proceso';
				
				aoData.push(
					{"name":"v_estado", "value": v_estado},
					{"name":"v_pais", "value": v_pais},
					{"name":"v_negocio", "value": v_negocio},
					{"name":"v_servicio", "value": v_servicio},
					{"name":"v_organization", "value": v_organization},
					{"name":"v_tag", "value": v_tag},
					{"name":"v_search", "value": v_search},
					{"name":"v_criticidad", "value": v_criticidad});
					
				//aEventosChecked = $('.check:checked').map(function(){ return this.value; }).get();
				
				$.ajax({
					'dataType'	: 'json',
					'type'		: 'POST',
					'url'		: sSource,
					'data'		: aoData,
					'success'	: fnCallback
				});
			  },
			'fnDrawCallback': function( oSettings ) {	// Funcion que se ejecuta DESPUES de la carga de la tabla
			
				// Clases que se asociacian a la celda del segun el nombre del tipo de evento
				classes = new Array();
				classes['CRITICAL']		= 'background-color:#f67777; font-weight: bold;';
				classes['MAJOR']		= 'background-color:#fdc358; font-weight: bold;';
				classes['MINOR']		= 'background-color:#ffffb1; font-weight: bold;';	
				
				// Se agrega la clase a la fila segun la criticidad del NODO y el tipo de evento
				$('#eventos tbody tr').each(function(){
					oClase = $(this).find("td:eq(1)");
					if(oClase.length){
						clase  = oClase.html().split('/').pop().split('.')[0];	//Se obtiene la imagen que se muestra representando la criticidad del nodo [0:5]
						$(this).addClass('criticidad'+oClase.html());					//Se agrega la clase al TR con la finalidad de RESALTAR la criticidad del nodo
						
						// Solicitud de Adessa
						$(this).attr('style',classes[oClase.html()]);
					}
				});
				
				// Se marcan los eventos previamente seleccionados
				$.each(aEventosChecked, function(index, value){
					$('.check:[name="'+value+'"]').prop("checked",true).parent().parent().addClass('marcado');
				});
				
				// Se marca la fila previamente seleccionada
				/*if(oFilaSeleccionada){
					$(".check[name="+oFilaSeleccionada+"]").parents("tr").addClass('row_selected');
				}*/
			},
			
			"fnInitComplete": function(oSettings, json) {
				
				$.ajax({
					url: "c_principal/obtener_type",
					type: "POST",
					success: function(html){
						// console.log(html);
						$('#eventos_length label #incrustado').html("");
						
						if(html == 3){
							var htmlIncrustado = '<div id="incrustado"style="float: right; margin-top: 0px;"><input type="submit" value="Exportar" name="exportar" id="exportar" /></div>';
						} else {
							var htmlIncrustado = '<div id="incrustado"style="float: right; margin-top: 0px;"><input type="submit" value="Exportar" name="exportar" id="exportar" /><input type="submit" value="Enviar Correo" name="sendSidenavMail" id="sendSidenavMail" /></div>';
						}
						
						$('#eventos_length label').append(htmlIncrustado);
					}
				});

				
				//var htmlIncrustado = '<div style="float: right; margin-top: 0px;"><input type="submit" value="Enviar Correo" name="sendNewMail" id="sendNewMail" /></div>';
				// var htmlIncrustado = '<div id="incrustado"style="float: right; margin-top: 0px;"><input type="submit" value="Exportar" name="exportar" id="exportar" /><input type="submit" value="Enviar Correo" name="sendSidenavMail" id="sendSidenavMail" /></div>';
				
				
				//this.fnAdjustColumnSizing();
				//this.fnDraw();
			}
		});
	}
	
	// Carga tabla auxiliar
	function loadTable_Servidor(){
		oTable_v2 = $("#eventos_v2").dataTable({
			
			"bJQueryUI": true,
			"bAutoWidth": false,					// Ancho de la tabla
			//"sScrollX": "100%",
			//"sScrollXInner": "110%",
			"bScrollCollapse": true,
			"sPaginationType": "full_numbers",	// Se declara que la paginanacion entrege todos los numeros
			"iDisplayLength" : 50,
			"oLanguage": {
				"sUrl": "../../js/spanish.txt"  // Idioma de la tabla
			},
			"bProcessing": true,
			"bServerSide": true,
			"bDeferRender": true,
			"bDestroy": true,
			"sAjaxSource": "c_principal/obtener_eventos_vista_servidor",
			'fnServerData': function(sSource, aoData, fnCallback){
				
				var v_estado       = getFiltrosActivos('estado');
				var v_pais         = getFiltrosActivos('pais');
				var v_negocio      = getFiltrosActivos('negocio');
				var v_servicio     = getFiltrosActivos('servicio');
				var v_organization = getFiltrosActivos('organization');
				var v_tag          = getFiltrosActivos('tag');
				var v_search       = $('.filtro_texto').val();
				var v_criticidad   = $('.filtro_criticidad').val();
				
				aoData.push(
					{"name":"v_estado", "value": v_estado},
					{"name":"v_pais", "value": v_pais},
					{"name":"v_negocio", "value": v_negocio},
					{"name":"v_servicio", "value": v_servicio},
					{"name":"v_organization", "value": v_organization},
					{"name":"v_tag", "value": v_tag},
					{"name":"v_search", "value": v_search},
					{"name":"v_criticidad", "value": v_criticidad});
					
				aEventosChecked = $('.check:checked').map(function(){ return this.value; }).get();
				
				$.ajax({
					'dataType'	: 'json',
					'type'		: 'POST',
					'url'		: sSource,
					'data'		: aoData,
					'success'	: fnCallback
				});
			  },
			'fnDrawCallback': function( oSettings ) {
				classes = new Array();
				classes['CRITICAL']		= 'background-color:#f67777; font-weight: bold;';
				classes['MAJOR']		= 'background-color:#fdc358; font-weight: bold;';
				classes['MINOR']		= 'background-color:#ffffb1; font-weight: bold;';
			
				$('#eventos_v2 tbody tr').each(function(){
					oClase = $(this).find("td:eq(0)");
					selec  = $(this).find("td:eq(1)").html()
					
					if(oClase.length){
						clase  = oClase.html().split('/').pop().split('.')[0];
						$(this).addClass('criticidad'+oClase.html());
						$(this).attr('style',classes[oClase.html()]);
					}
					// Se marcan los eventos previamente seleccionados
					if(aEventosChecked_servidor && aEventosChecked_servidor == selec){
						$(this).addClass('row_selected');
						//console.log(aEventosChecked_servidor);
					};
				});
				
				
			}   
		});
	}
	
	function loadTable_Pais_Negocio(){
		oTable_v3 = $("#eventos_v3").dataTable({
			
			"bJQueryUI": true,
			"bAutoWidth": false,					// Ancho de la tabla
			"sPaginationType": "full_numbers",	// Se declara que la paginanacion entrege todos los numeros
			"iDisplayLength" : 50,
			"oLanguage": {
				"sUrl": "../../js/spanish.txt"  // Idioma de la tabla
			},
			"bProcessing": true,
			"bServerSide": true,
			"bDeferRender": true,
			"bDestroy": true,
			"sAjaxSource": "c_principal/obtener_eventos_vista_pais_negocio",
			'fnServerData': function(sSource, aoData, fnCallback){
				
				var v_estado       = getFiltrosActivos('estado');
				var v_pais         = getFiltrosActivos('pais');
				var v_negocio      = getFiltrosActivos('negocio');
				var v_servicio     = getFiltrosActivos('servicio');
				var v_organization = getFiltrosActivos('organization');
				var v_tag          = getFiltrosActivos('tag');
				var v_search       = $('.filtro_texto').val();
				var v_criticidad   = $('.filtro_criticidad').val();
				
				aoData.push(
					{"name":"v_estado", "value": v_estado},
					{"name":"v_pais", "value": v_pais},
					{"name":"v_negocio", "value": v_negocio},
					{"name":"v_servicio", "value": v_servicio},
					{"name":"v_organization", "value": v_organization},
					{"name":"v_tag", "value": v_tag},
					{"name":"v_search", "value": v_search},
					{"name":"v_criticidad", "value": v_criticidad});
				
				$.ajax({
					'dataType'	: 'json',
					'type'		: 'POST',
					'url'		: sSource,
					'data'		: aoData,
					'success'	: fnCallback
				});
			  },
			'fnDrawCallback': function( oSettings ) {
				classes = new Array();
				classes['CRITICAL']		= 'background-color:#f67777; font-weight: bold;';
				classes['MAJOR']		= 'background-color:#fdc358; font-weight: bold;';
				classes['MINOR']		= 'background-color:#ffffb1; font-weight: bold;';
			
				$('#eventos_v3 tbody tr').each(function(){
					oClase = $(this).find("td:eq(0)");
					if(oClase.length){
						clase  = oClase.html().split('/').pop().split('.')[0];
						$(this).addClass('criticidad'+oClase.html());
						$(this).attr('style',classes[oClase.html()]);
					}
				});
			}   
		});
	}
	
	function loadTable_Detalle(data){
		oTable = $('#eventos').dataTable({
			
			"bJQueryUI": true,
			"bAutoWidth": false,
			"sScrollX": "100%",
			"sScrollXInner": "150%",
			"bScrollCollapse": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength" : 20,
			"aoColumnDefs": [
			  { "bSearchable": false, "bSortable": false  , "aTargets": [0] }
			],
			"aaSorting": [[1,'asc']],
			"bLengthChange": true,
			"oLanguage": {
				"sUrl": "../../js/spanish.txt"
			},
			"bProcessing": true,
			"bServerSide": true,
			"bDeferRender": true,
			"bDestroy": true,
			"sAjaxSource": "c_detalle/obtenerHijos",
			'fnServerData': function(sSource, aoData, fnCallback){
				
				aoDataFinal = aoData.concat(data);
				
				$.ajax({
					'dataType'	: 'json',
					'type'		: 'POST',
					'url'		: sSource,
					'data'		: aoDataFinal,
					'success'	: fnCallback
				});
			},
			'fnDrawCallback': function( oSettings ) {
				classes = new Array();
				classes['CRITICAL']		= 'background-color:#f67777; font-weight: bold;';
				classes['MAJOR']		= 'background-color:#fdc358; font-weight: bold;';
				classes['MINOR']		= 'background-color:#ffffb1; font-weight: bold;';	
				
				$('#eventos tbody tr').each(function(){
					oClase = $(this).find("td:eq(1)");
					if(oClase.length){
						clase  = oClase.html().split('/').pop().split('.')[0];
						$(this).addClass('criticidad'+oClase.html());
						
						// Solicitud de Adessa
						$(this).attr('style',classes[oClase.html()]);
					}
				});
				
				// Se marcan los eventos previamente seleccionados
				$.each(aEventosChecked, function(index, value){
					$('.check:[name="'+value+'"]').prop("checked",true).parent().parent().addClass('marcado');
				});
				
				// Se marca la fila previamente seleccionada
				if(oFilaSeleccionada){
					$(".check[name="+oFilaSeleccionada+"]").parents("tr").addClass('row_selected');
				}
			},
			
			"fnInitComplete": function(oSettings, json) {
				
				$.ajax({
					url: "c_principal/obtener_type",
					type: "POST",
					success: function(html){
						// console.log(html);
						$('#eventos_length label #incrustado').html("");
						var htmlIncrustado = '<div id="incrustado" style="float: right; margin-top: 0px;">';
						
						if(html == 3){
							htmlIncrustado += '<div style="float: right; margin-top: 0px;"><input type="submit" value="Exportar" name="exportar" id="exportar" /></div>';
						} else {
							htmlIncrustado += '<div style="float: right; margin-top: 0px;"><input type="submit" value="Exportar" name="exportar" id="exportar" /><input type="submit" value="Enviar Correo" name="sendSidenavMail" id="sendSidenavMail" /></div>';
						}
						
						htmlIncrustado += '<input type="submit" value="Ocultar detalle" name="titulo_eventos_detalle_hijos" id="titulo_eventos_detalle_hijos" />';
						htmlIncrustado += '</div>';
						
						$('#eventos_length label').append(htmlIncrustado);
						$("#row_header_v1").removeClass("ui-tabs-hide");
						$('#titulo_eventos_detalle').css('display','block');
						
						this.fnAdjustColumnSizing();
						this.fnDraw();
					}
				});
			}
		});
	}
	
	/*	--------------------------------------------------
	:: Cargar detalle de evento y nodo
	-------------------------------------------------- */
	$("#eventos_v2 tbody tr").live('click', function(){
		if ( !$(this).hasClass('row_selected') ) { // Se resalta linea seleccionada
			$("#eventos tbody tr.row_selected").removeClass('row_selected');
			$("#eventos_v2 tbody tr.row_selected").removeClass('row_selected');
			$("#eventos_v3 tbody tr.row_selected").removeClass('row_selected');
			$(this).addClass('row_selected');
		}
		
		var estado   = getFiltrosActivos('estado');
		var nombre   = $(this).find('td:eq(1)');
		var ip       = $(this).find('td:eq(2)');
		var pais     = $(this).find('td:eq(3)');
		var negocio  = $(this).find('td:eq(4)');
		//var servicio = $(this).find('td:eq(5)');
		//var organiza = $(this).find('td:eq(6)');
		//var tag      = $(this).find('td:eq(7)');
		
		
		data = new Array();
		data.push(
			{"name":"estado", "value": estado},
			{"name":"nombre", "value": $(nombre).text()},
			{"name":"ip", "value": $(ip).text()},
			{"name":"pais", "value": $(pais).text()},
			{"name":"negocio", "value": $(negocio).text()});
			//{"name":"servicio", "value": $(servicio).text()},
			//{"name":"organiza", "value": $(organiza).text()},
			//{"name":"tag", "value": $(tag).text()});
		
		loadTable_Detalle(data);
		
	});
	
	$("#eventos_v3 tbody tr").live('click', function(){
		if ( !$(this).hasClass('row_selected') ) { // Se resalta linea seleccionada
			$("#eventos tbody tr.row_selected").removeClass('row_selected');
			$("#eventos_v2 tbody tr.row_selected").removeClass('row_selected');
			$("#eventos_v3 tbody tr.row_selected").removeClass('row_selected');
			$(this).addClass('row_selected');
		}
		
		var estado   = getFiltrosActivos('estado');
		var pais     = $(this).find('td:eq(1)');
		var negocio_l  = $(this).find('td:eq(2)');
		var sistema_l  = $(this).find('td:eq(3)');
		
		var sistema = $(sistema_l).text().replace("\\",'\\\\\\\\');
		var negocio = $(negocio_l).text().replace("\\",'\\\\\\\\');
		
		//var aplica   = $(this).find('td:eq(4)');
		
		data = new Array();
		data.push(
			{"name":"estado", "value": estado},
			{"name":"pais", "value": $(pais).text()},
			{"name":"negocio", "value": negocio},
			{"name":"sistema", "value": sistema}
			//{"name":"sistema", "value": $(sistema).text()}
			//{"name":"aplica", "value": $(aplica).text()}
		);
		
		//console.log(negocio);
		
		loadTable_Detalle(data);
	});
	
	function openNav() {
		document.getElementById("mySidenav").style.width = "33%";
	}

	function closeNav() {
		$("#eventos tbody tr.row_selected").removeClass('row_selected');
		document.getElementById("mySidenav").style.width = "0";
	}
	
	function openNavMail() {
		document.getElementById("mySidenavMail").style.width = "35%";
	}
	
	function closeNavMail() {
		document.getElementById("mySidenavMail").style.width = "0";
	}
	
	// Despliegue de menu en header
	$('#crearTicketManual').live('click', function(){
		
		var grId = $("#comGruId").val();
		$("#bloqueComentar").html('').html("Generado ticket en Mesa de Ayuda.");
		
		$.ajax({
			url: "../c_sendAutomaticTicket/createTicket",
			type: "POST",
			data: "ID="+grId+"&isManual="+true+"&folio="+true,
			success: function(html){
				var coment  = $('#comentario').val('');
				$("#bloqueComentar").html('').html(html);
				oTable.fnDraw(false);
			}
		});
	});
	
	$('#folio_manual').live('click', function(){
		
		var grId = $("#comGruId").val();
		var folioId = $("#texto_folio_manual").val();

		if(!isNaN(parseInt(folioId))){
			$.ajax({
				url: "c_principal/insertaTicketManual",
				type: "POST",
				data: "ID="+grId+"&folio="+folioId,
				success: function(html){
					console.log(html);
					oTable.fnDraw(false);
				}
			});
		} else {
			alert ("Formato Invalido. Debe contener solo numeros.");
			
		}
	});
