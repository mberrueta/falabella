<?php

class Cliente extends CI_Model {

	// METODOS ADESSA
	function insert_clientes($usuario){
		$buffArray = array();
		array_push($buffArray, array('user' => $usuario['nom_usu_ope'], 'src_organization' => 'All', 'src_tool' => NULL));
		array_push($buffArray, array('user' => $usuario['nom_usu_ope'], 'src_organization' => NULL , 'src_tool' => 'All'));
		$this->db->flush_cache();
		$this->db->insert_batch('grupo_gde',$buffArray);
	}
	
	
	function listar_usuarios(){
		$sql = 'SELECT usuario_gde.user, usuario_gde.user_category, IFNULL(operador_gde.name, IFNULL(supervisor_gde.name, analista_gde.name)) as name, IFNULL(operador_gde.last_name_a, IFNULL(supervisor_gde.last_name_a, analista_gde.last_name_a)) as last_name FROM usuario_gde LEFT JOIN operador_gde ON operador_gde.user = usuario_gde.user LEFT JOIN supervisor_gde ON supervisor_gde.user = usuario_gde.user LEFT JOIN analista_gde ON analista_gde.user = usuario_gde.user ORDER BY usuario_gde.user ASC';
		
		$query = $this->db->query($sql);
		
		if( $query->num_rows() > 0 ) {
			return $query;
		} else {
			return array();
		}
	}
	
	function listar_organization(){
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$sql = 'SELECT src_organization FROM contacto_dispositivo_gde ORDER BY src_organization ASC';
		$query = $this->db->query($sql);
		
		if( $query->num_rows() > 0 ) {
			$result['stt'] = TRUE;
			$result['cli'] = $query;
		}
		
		return $result;
	}	
	
	function listar_servicio(){
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$sql = 'SELECT dest_service FROM servicios_gde ORDER BY dest_service ASC';
		$query = $this->db->query($sql);
		
		if( $query->num_rows() > 0 ) {
			$result['stt'] = TRUE;
			$result['cli'] = $query;
		}
		
		return $result;
	}
	
	function listar_asociaciones_organizacion($id_cliente){
		$result        = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('src_organization');
			$this->db->from('grupo_gde');
			$this->db->where('user',$id_cliente);
			$this->db->where('src_organization IS NOT NULL');
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	function listar_asociaciones_herramienta($id_cliente){ 
		$result        = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('src_tool');
			$this->db->from('grupo_gde');
			$this->db->where('user',$id_cliente);
			$this->db->where('src_tool IS NOT NULL');
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}

	// function listar_cliente($supervisor, $es_admin=false){
	function listar_cliente(){

		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$sql = "SELECT DISTINCT(src_organization) FROM grupo_gde ORDER BY src_organization ASC";
		$this->db->trans_start();
		$cli = $this->db->query($sql);
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	function listar_nombre($id_cliente){
		$this->db->trans_start();
			$this->db->select('CLI_NOM, CLI_CRIT');
			$this->db->from('cliente');
			$this->db->where('CLI_ID',$id_cliente);
			$data = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			return $data;
		}
	}
	
	function listar_id_cliente($nombre){
		$this->db->trans_start();
			$this->db->select('CLI_ID');
			$this->db->from('cliente');
			$this->db->where('CLI_NOM',$nombre);
			$data = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			return $data;
		}
	}
	
	function listar_sla($cliente){
		/**
		* Retorna los valores de SLA asociados al nombre de un Cliente
		* @param String cliente Nombre de Cliente consultado
		* @return Array array asociativo que contempla los SLA segun criticidad de nodo
		*                     NaN: asociado a criticidad NULL
		*/
		$this->db->trans_start();
			$this->db->select("IFNULL(  `DIS_CRIT` ,  'NaN' ) as DIS_CRIT", FALSE);	// Se reemplaza el valor NULL por el string NaN
			$this->db->select("MINUTE( SLA_MED ) as SLA_MED", FALSE);
			$this->db->select("MINUTE( SLA_ALT ) as SLA_ALT", FALSE);
			$this->db->from("sla");
			$this->db->join("cliente","sla.CLI_ID=cliente.CLI_ID");
			$this->db->where("CLI_NOM",$cliente);
			$sla = $this->db->get();
		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE){
			$slaAsociado = array();
			foreach ($sla->result() as $row){
				$slaAsociado[ 'crit_'.$row->DIS_CRIT ] = array('medio' => $row->SLA_MED, 'alto' => $row->SLA_ALT); // Se genera array asociativo con valores de SLA
			}
			return $slaAsociado;
		}
	}
	
	function listar_criticidad($cliente){
		$this->db->trans_start();
			$this->db->select('CLI_CRIT');
			$this->db->from('cliente');
			$this->db->where('CLI_NOM',$cliente);
			$cri = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			foreach ($cri->result() as $row){
				$criticidad = $row->CLI_CRIT;
			}
			return $criticidad;
		}
	}
	
	function listar_operador($cliente){
		#$exl = 'SELECT `CLI_NOM` FROM `cliente` WHERE `USU_USER`=ANY(SELECT `USU_USER` FROM `operador` WHERE `SUP_USU_USER`=\'$supervisor\')';
		#$ex2 = 'SELECT `CLI_NOM` FROM `cliente` join `operador` ON `cliente`.`USU_USER`=`operador`.`USU_USER` WHERE `operador`.`SUP_USU_USER`=\'$supervisor\'';
		
		$this->db->trans_start();
			$this->db->select('USU_USER');
			$this->db->from('cliente_operador');
			$this->db->where('CLI_NOM',$cliente);
			$cri = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			foreach ($cri->result() as $row){
				$operador = $row->USU_USER;
			}
			return $operador;
		}
	}
	
	function listar_asociaciones($id_cliente){
		$result         = Array();
		$result['stt'] = FALSE;
		$result['cli']  = NULL;
		
		$this->db->trans_start();
			$this->db->select('user');
			$this->db->from('grupo_gde');
			$this->db->where('src_organization',$id_cliente);
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}

	/** Verificar Utilidad
	*Verifica que los clientes esten correctamente asignados a un Supervisor (operadores asociados a supervisor)
	*
	*@param  {string} Nombre de usuario de Supervisor
	*@param  {array}  Listado de Clientes a validar
	*@return {int}    Estado de la validacion 1 = Corresponden, 0 = No Corresponden, 2 = Problema en Transaccion
	*/
	private function verificar_supervisor_cliente($supervisor, $clientes){
		$this->db->trans_start();
			$this->db->select('CLI_NOM');
			$this->db->from('cliente');
			$this->db->join('operador','cliente.USU_USER=operador.USU_USER');
			$this->db->where('operador.SUP_USU_USER',$supervisor);
			$this->db->where_in('CLI_NOM', $clientes);
			$result = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			if(sizeof($clientes) == $result->num_rows()){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			return 2;
		}
	}
	
	function verificar_cliente($cliente){
		$this->db->trans_start();
			$this->db->where('user',$cliente);
			$q = $this->db->get('usuario_gde');
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			if($q->num_rows == 0){
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	function verificar_operador_backup($id_operador, $id_backup){
		$sql = "SELECT * FROM `grupo` WHERE `USU_USER`=? AND `GRU_TIPO`='T' AND `CLI_ID`= ANY (SELECT `CLI_ID` FROM `grupo` WHERE `USU_USER`=? AND `GRU_TIPO`='B')";
		$this->db->trans_start();
			$query = $this->db->query($sql, array($id_backup,$id_operador)); 
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			if($query->num_rows>0){
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	function verificar_update_operador_backup($id_operador, $id_backup,$id_cliente){
		$sql = "SELECT * FROM `grupo` WHERE `USU_USER`=? AND `GRU_TIPO`='T' AND `CLI_ID`= ANY (SELECT `CLI_ID` FROM `grupo` WHERE `USU_USER`=? AND `GRU_TIPO`='B' AND `CLI_ID`!=?)";
		$this->db->trans_start();
			$query = $this->db->query($sql, array($id_backup,$id_operador,$id_cliente));
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			if($query->num_rows>0){
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	function eliminar_cliente($clientes){
		
			$this->db->trans_start();
			
				#Obtener ID del Cliente
				$this->db->select('CLI_ID');
				$this->db->where('CLI_NOM',$clientes);
				$query = $this->db->get('cliente');
				$ID = $query->row()->CLI_ID;

				#Eliminar dispositivos (nodos)
				$this->db->where_in('CLI_ID', $ID);
				$this->db->delete('dispositivo');
				$this->db->flush_cache();
				
				#Eliminar grupo
				$this->db->where_in('CLI_ID', $ID);
				$this->db->delete('grupo');
				$this->db->flush_cache();
				
				#Eliminar SLA
				$this->db->where_in('CLI_ID', $ID);
				$this->db->delete('sla');
				$this->db->flush_cache();
				
				#Eliminar cliente
				$this->db->where_in('CLI_ID', $ID);
				$this->db->delete('cliente');
				$this->db->flush_cache();
				
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				return FALSE;
			}
			else{
				return TRUE;
			}
	}
	
	
	/** Crea un nuevo cliente en los registros y genera las asociaciones de CLIENTE-OPERADORES
	*
	*/
	function crear_cliente($cliente, $criticidad, $operador, $backup, $otros, $analistas, $sla_gm, $sla_ga, $sla_0m, $sla_0a, $sla_1m, $sla_1a, $sla_2m, $sla_2a, $sla_3m, $sla_3a, $alias, $nom_alias){
	
		$data = array('CLI_NOM' => $cliente ,'CLI_CRIT' => $criticidad, 'CLI_ALIAS' => $alias, 'CLI_NOM_ALIAS' => $nom_alias);
		
		$this->db->trans_start();
			//Se registra nuevo cliente en tabla cliente
			$this->db->insert('cliente', $data);
			$this->db->flush_cache();
			
			//Se obtiene el CLI_ID del cliente recien creado
			$this->db->select('CLI_ID');
			$this->db->where('CLI_NOM',$cliente);
			$result = $this->db->get('cliente');
			$ID = $result->row()->CLI_ID;
			$this->db->flush_cache();
			
			//Se proceden a registrar usuarios que pueden "ver" al cliente, en tabla grupo
			$batch = array();
			
			if(isset($operador) && $operador != NULL){
				array_push($batch, array('USU_USER' => $operador, 'CLI_ID' => $ID, 'GRU_TIPO' => 'T',  'GRU_STT' => 1));
			}
			if(isset($backup) && $backup != NULL){
				array_push($batch, array('USU_USER' => $backup   , 'CLI_ID' => $ID, 'GRU_TIPO' => 'B',  'GRU_STT' => 0));
			}
			
			foreach($otros as $otro_usu){
				if(isset($otro_usu) && $otro_usu != NULL){
					array_push($batch, array('USU_USER' => $otro_usu , 'CLI_ID' => $ID, 'GRU_TIPO' => 'O',  'GRU_STT' => 1));
				}
			}
			
			foreach($analistas as $analista){
				if(isset($analista) && $analista != NULL){
					array_push($batch, array('USU_USER' => $analista , 'CLI_ID' => $ID, 'GRU_TIPO' => 'A',  'GRU_STT' => 1));
				}
			}
			
			$this->db->insert_batch('grupo', $batch);
			$this->db->flush_cache();
			
			//Se registran los SLA generico y de los nodos
			$sla_g = array('CLI_ID' => $ID, 'DIS_CRIT' => NULL, 'SLA_MED' => gmdate("H:i:s",((int)$sla_gm * 60)), 'SLA_ALT' => gmdate("H:i:s",((int)$sla_ga) * 60));
			$sla_0 = array('CLI_ID' => $ID, 'DIS_CRIT' => '0', 'SLA_MED' => gmdate("H:i:s",((int)$sla_0m * 60)), 'SLA_ALT' => gmdate("H:i:s",((int)$sla_0a) * 60));
			$sla_1 = array('CLI_ID' => $ID, 'DIS_CRIT' => '1', 'SLA_MED' => gmdate("H:i:s",((int)$sla_1m * 60)), 'SLA_ALT' => gmdate("H:i:s",((int)$sla_1a) * 60));
			$sla_2 = array('CLI_ID' => $ID, 'DIS_CRIT' => '2', 'SLA_MED' => gmdate("H:i:s",((int)$sla_2m * 60)), 'SLA_ALT' => gmdate("H:i:s",((int)$sla_2a) * 60));
			$sla_3 = array('CLI_ID' => $ID, 'DIS_CRIT' => '3', 'SLA_MED' => gmdate("H:i:s",((int)$sla_3m * 60)), 'SLA_ALT' => gmdate("H:i:s",((int)$sla_3a) * 60));
			
			$this->db->insert('sla',$sla_g);
			$this->db->flush_cache();
			
			$this->db->insert('sla',$sla_0);
			$this->db->flush_cache();
			
			$this->db->insert('sla',$sla_1);
			$this->db->flush_cache();
			
			$this->db->insert('sla',$sla_2);
			$this->db->flush_cache();
			
			$this->db->insert('sla',$sla_3);
			$this->db->flush_cache();
			
		$this->db->trans_complete();
	
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}
		else{
			return TRUE;
		}
	
	}
	
	/** Registra los CAMBIOS de CLIENTE-OPERADORES en la tabla grupo
	*
	*/
	function update_cliente($id_cliente, $operador, $backup){
		
		$this->db->trans_start();
			
			//Se eliminan los registros del cliente desde la tabla grupo
			$this->db->delete('grupo_gde', array('user' => $id_cliente));
			$this->db->flush_cache();
			
			// log_message('error', 'CLIENTE = '. $id_cliente);
			
			
			//Se proceden a registrar usuarios que pueden "ver" al cliente, en tabla grupo
			$batch = array();
			
			if(isset($operador) && $operador != NULL){
				foreach($operador as $otro_usu){
					// log_message('error', 'OPERADORES = '. $otro_usu);
					array_push($batch, array('user' => $id_cliente, 'src_organization' => $otro_usu, 'src_tool' => NULL));
				}
			}
			
			if(isset($backup) && $backup != NULL){
				foreach($backup as $otro_usu){
					// log_message('error', 'SERVICIOS = '. $otro_usu);
					array_push($batch, array('user' => $id_cliente, 'src_organization' => NULL, 'src_tool' => $otro_usu));
				}
			}
			
			if(sizeof($batch) > 0){
				$this->db->insert_batch('grupo_gde', $batch);
				$this->db->flush_cache();
				
				// log_message('error', "\n\n".$this->db->last_query()."\n");
			}
		
		$this->db->trans_complete();
	
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	
	
	
	function listar_cliente_filtro(){
	
		$sql = "SELECT `cliente`.`CLI_NOM`, IFNULL(T.TTL,0) as TOTAL
				FROM `cliente` 
				LEFT JOIN (SELECT `CLI_ID`, sum(GEV_CNT) as TTL FROM `grupo_evento` WHERE `EST_NOM`='Activo' GROUP BY `CLI_ID`) as T
				ON `cliente`.`CLI_ID`=T.CLI_ID
				GROUP BY `cliente`.`CLI_ID`";
		$filtros = $this->db->query($sql);
		
		$html = '<table><tbody>';
		foreach($filtros->result() as $filtro){
			$html .= '<tr><td>';
			$html .= '<input class="filtro filtro_cliente" type="checkbox" name="cliente" value="'.$filtro->CLI_NOM.'"> '.$filtro->CLI_NOM.' ('.$filtro->TOTAL.')';
			$html .= '</td></tr>';
		}
		$html .= '</tbody></table>';
		
		return '<div class="grupo">'.$html.'</div>';
	}
	
	/**************************************************
	*            MODIFICACIONES FASE 1.3
	**************************************************/
	
	/* Lista los clientes asociados a un operador
	*
	*/
	public function listarClientes($operador, $array = false){
		$this->db->select('grupo.CLI_ID');
		$this->db->select('cliente.CLI_NOM');
		$this->db->from('grupo');
		$this->db->join('cliente','grupo.CLI_ID = cliente.CLI_ID');
		$this->db->where('grupo.USU_USER',$operador);
		$this->db->where('grupo.GRU_STT',1);
		$this->db->order_by('CLI_NOM','asc');
		$this->db->distinct();
		$result = $this->db->get();
		
		$listadoClientes = null;
		
		if(!$array){
			$listadoClientes   = "";
			$estilo = "option";
			foreach($result->result() as $row){
				$listadoClientes .= "<".$estilo." value=\"".$row->CLI_ID."\">".$row->CLI_NOM."</".$estilo.">";
			}
		}
		else{
			$listadoClientes   = array();
			foreach($result->result() as $row){
				array_push($listadoClientes, $row->CLI_ID);
			}
		}
		
		return $listadoClientes;
	}
	
	/**************************************************
	*            MODIFICACIONES FASE 3.2
	**************************************************/
	
	/* Obtener correo alias por id cliente
	*
	*/
	public function get_alias($ID_CLI){
		$this->db->select('cliente.CLI_ALIAS');
		$this->db->from('cliente');
		$this->db->where('cliente.CLI_ID',$ID_CLI);
		$result = $this->db->get();
		
		return $result->row()->CLI_ALIAS;
	}
	
	/* Obtener correo alias por id cliente
	*
	*/
	public function get_alias_nom($NOM_CLI){
		$this->db->select('cliente.CLI_ALIAS');
		$this->db->select('cliente.CLI_NOM_ALIAS');
		$this->db->from('cliente');
		$this->db->like('cliente.CLI_NOM',$NOM_CLI);
		$result = $this->db->get();
		$alias['mail'] = $result->row()->CLI_ALIAS;
		$alias['nom'] = $result->row()->CLI_NOM_ALIAS;
		
		return $alias;
	}
}

/* End of file cliente.php */
/* Location: ./application/models/cliente.php */
