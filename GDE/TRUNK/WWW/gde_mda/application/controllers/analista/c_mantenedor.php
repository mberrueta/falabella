<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_mantenedor extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('nodos');
	}
	
	public function index(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		$this->load->view('analista/v_mantenedor');
	}
	
	function listarNodos(){
		$clientes = $_POST['clientes'];
		$result = $this->nodos->listarNodos($clientes, true);
		
		echo json_encode($result);
	}
	
	function listarClientes(){
		$results = $this->nodos->listarClientes($this->session->userdata('username'));
		
		echo json_encode($results);
	}
	
	function validarNodosNuevos(){
		$nombreNodosNuevos 		= $_POST['nombreNodosNuevos'];

		if (count($nombreNodosNuevos) > 0 && $nombreNodosNuevos[0] != ""){
			$results = $this->nodos->validaNombreNodos($nombreNodosNuevos);

		}
		if (isset($results)){
			echo json_encode( $results );	
		}
		else{
			//echo "puede continuar";
		}

	}
	
	function validarNodos(){
		$nodosNuevos			= $_POST['nodosNuevos'];
		$nodosEliminados		= $_POST['nodosEliminados'];
		$nodosModificados		= $_POST['nodosModificados'];
		
		$result1 = array( 'ESTADO' => "OK2", 'DETALLE' => "No se borraron nodos" );
		$result2 = array( 'ESTADO' => "OK2", 'DETALLE' => "No se crearon nuevos nodos" );
		$result3 = array( 'ESTADO' => "OK2", 'DETALLE' => "No se modificaron nodos" );
		
		if( count($nodosEliminados) > 0 && $nodosEliminados[0] != "" ){
			$result1 = $this->nodos->eliminarNodos( $nodosEliminados, $this->session->userdata('username') );
		}
		if( count($nodosNuevos) > 0 && $nodosNuevos[0] != "" ){
			$result2 = $this->nodos->crearNodos( $nodosNuevos, $this->session->userdata('username') );
		}
		if( count($nodosModificados) > 0 && $nodosModificados[0] != "" ){
			$result3 = $this->nodos->modificarNodos( $nodosModificados, $this->session->userdata('username') );
		}
		
		$results = array( "eliminarNodos"=>$result1, "crearNodos"=>$result2, "modificarNodos"=>$result3);
		
		echo json_encode( $results );
	}
	
	function eliminarNodos(){
		$nodosEliminados		= $_POST['nodosEliminados'];
		
		$results = array( 'ESTADO' => "OK2", 'DETALLE' => "No se borraron nodos" );
		
		if( count($nodosEliminados) > 0 && $nodosEliminados[0] != "" ){
			$results = $this->nodos->eliminarNodos( $nodosEliminados, $this->session->userdata('username') );
		}
		
		$results = array( "eliminarNodos"=>$results);
		
		echo json_encode( $results );
	}
	
	function crearNodos(){
		$nodosNuevos		= $_POST['nodosNuevos'];
		
		$results = array( 'ESTADO' => "OK2", 'DETALLE' => "No se crearon nuevos nodos" );
		
		if( count($nodosNuevos) > 0 && $nodosNuevos[0] != "" ){
			$results = $this->nodos->crearNodos( $nodosNuevos, $this->session->userdata('username') );
		}
		
		$results = array( "crearNodos"=>$results);
		
		echo json_encode( $results );
	}
	
	function editarNodos(){
		$nodosModificados		= $_POST['nodosModificados'];
		
		$results = array( 'ESTADO' => "OK2", 'DETALLE' => "No se modificaron nodos" );
		
		if( count($nodosModificados) > 0 && $nodosModificados[0] != "" ){
			$results = $this->nodos->modificarNodos( $nodosModificados, $this->session->userdata('username') );
		}
		
		$results = array( "modificarNodos"=>$results);
		
		echo json_encode( $results );
	}
	
	public function createExtract(){
		$this->load->helper('download');
		$file_name = 'tmp/backup_'.$this->session->userdata('username').'.csv';
		
		
		$clientes = $_POST['clientes'];
		$result = $this->nodos->listarNodos_exp($clientes, true, $file_name);
		$url = explode("/",$result);
		echo json_encode( $url[1] );
	}
	
	public function backup_download($name){
		$this->load->helper('download');
		$data = file_get_contents("tmp/".$name);
		force_download($name, $data);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 4)){
			return true;
		}
		else{
			return false;
		}
	}
	
}


/* End of file c_mantenedor.php */
/* Location: ./application/controllers/c_mantenedor.php */