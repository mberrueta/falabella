#!/usr/bin/python
#
#Purga de bd consulta, Gestor de Eventos
#Author: Jonathan Araneda Labarca, Kudaw
#v0.0 febrero		2012
#v1.0 octubre		2012

import MySQLdb
import datetime
import logging
import os
import sys

#Se obtienen las fechas relativas a purgar
hoy    = datetime.datetime.today().strftime("%Y-%m-%d")
fecha  = datetime.datetime.today() - datetime.timedelta(days = 90)
fecha  = fecha.strftime("%Y-%m-%d %H:%M")

#Se declara instancia de log
logger    = logging.getLogger('purga')
hdlr      = logging.FileHandler('/var/tmp/purga_%s.log' % (hoy))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)

#Se declara variables para coneccion a bd de consulta
db_host = 'localhost'
db_user = 'root'
db_pass = 'gdekudaw'
db_data = 'gestor_eventos_rsp'
db_data_purga = 'purga_eventos'

#Se realiza coneccion a bd de consulta
try:
	logger.info("Inicio proceso de purga")
	# db      = MySQLdb.connect(host=db_host, user=db_user, passwd=db_pass,db=db_data, unix_socket='/opt/lampp/var/mysql/mysql.sock')
	db      = MySQLdb.connect(host=db_host, user=db_user, passwd=db_pass,db=db_data)
	cursor = db.cursor()
	logger.info("Se conecta a BD "+db_data)

	#Se obtiene el GEV_ID pivote con el que se procedera a eliminar en db de consulta
	query = "SELECT MAX(%s.grupo_evento.GEV_ID) FROM %s.grupo_evento WHERE %s.grupo_evento.GEV_TSINI<'%s'" % (db_data, db_data, db_data, fecha)
	cursor.execute(query)
	result = cursor.fetchone()
	gev_id = str(result[0])
	
	# if ( gev_id == None or not isinstance( gev_id, int ) ):
		# logger.error("El Grupo de Eventos ID no se puede ocupar como pivote debido a que se obtuvo: %s " % (gev_id) )
		# sys.exit(0)
	
	logger.info("Se obtiene Grupo de Eventos ID %i" % ( int(gev_id) ) )
	
	#Se registran los eventos necesarios en la BD de 
	pathDirectorio = os.path.dirname(os.path.abspath(__file__)) + "/respaldos_Dump"
	print pathDirectorio
	if not os.path.exists(pathDirectorio):
		os.makedirs(pathDirectorio)

	query = "/usr/bin/mysqldump --user=%s --password=%s --host=%s --skip-add-drop-table %s grupo_evento --where=\"GEV_ID<=%i\" > %s/dump_grupo_evento_%s.sql" % (db_user, db_pass, db_host, db_data, int(gev_id), pathDirectorio, hoy)
	logger.info("Se genera archivo dump de respaldo grupo_evento")
	os.system(query)
	
	query = "/usr/bin/mysqldump --user=%s --password=%s --host=%s --skip-add-drop-table %s evento --where=\"GEV_ID<=%i\" > %s/dump_evento_%s.sql" % (db_user, db_pass, db_host, db_data, int(gev_id), pathDirectorio, hoy)
	logger.info("Se genera archivo dump de respaldo evento")
	os.system(query)
	
	query = "/usr/bin/mysqldump --user=%s --password=%s --host=%s --skip-add-drop-table %s comentario --where=\"GEV_ID<=%i\" > %s/dump_comentario_%s.sql" % (db_user, db_pass, db_host, db_data, int(gev_id), pathDirectorio, hoy)
	logger.info("Se genera archivo dump de respaldo comentario")
	os.system(query)
	
	query = "/usr/bin/mysqldump --user=%s --password=%s --host=%s --skip-add-drop-table %s historia --where=\"GEV_ID<=%i\" > %s/dump_historia_%s.sql" % (db_user, db_pass, db_host, db_data, int(gev_id), pathDirectorio, hoy)
	
	logger.info("Se genera archivo dump de respaldo historia")
	os.system(query)

	#Se eliminan los datos en la bd de consulta
	rows = 1
	while rows > 0:
		query = "DELETE FROM %s.historia WHERE %s.historia.GEV_ID<=%i LIMIT 10000" % (db_data, db_data, int(gev_id))
		rows  = cursor.execute(query)
		logger.info("Se eliminan valores en tabla historia de BD consulta, "+str(rows)+" filas afectadas")
		db.commit()

	rows = 1
	while rows > 0:
		query = "DELETE FROM %s.comentario WHERE %s.comentario.GEV_ID<=%i LIMIT 10000" % (db_data, db_data, int(gev_id))
		rows  = cursor.execute(query)
		logger.info("Se eliminan valores en tabla comentario de BD consulta, "+str(rows)+" filas afectadas")
		db.commit()

	rows = 1
	while rows > 0:
		query = "DELETE FROM %s.evento WHERE %s.evento.GEV_ID<=%i LIMIT 10000" % (db_data, db_data, int(gev_id))
		rows  = cursor.execute(query)
		logger.info("Se eliminan valores en tabla evento de BD consulta, "+str(rows)+" filas afectadas")
		db.commit()

	rows = 1
	while rows > 0:
		query = "DELETE FROM %s.grupo_evento WHERE %s.grupo_evento.GEV_ID<=%i LIMIT 10000" % (db_data, db_data, int(gev_id))
		rows  = cursor.execute(query)
		logger.info("Se eliminan valores en tabla grupo_evento de BD consulta, "+str(rows)+" filas afectadas")
		db.commit()
	
	#Se cierra coneccion a bd de consulta
	db.close()
	logger.info("Se cierra coneccion con BD")
	logger.info("Fin proceso de purga")
	
	#Se cargan los archivos dump generados en el proceso a la Base de Datos de Purga
	# query  = "/opt/lampp/bin/mysql --user=%s --password=%s --host=%s %s < %s/dump_grupo_evento_%s.sql" % (db_user, db_pass, db_host, db_data_purga, pathDirectorio, hoy)
	# logger.info("Se insertan valores en tabla grupo_evento de BD Purga")
	# os.system(query)
	
	# query  = "/opt/lampp/bin/mysql --user=%s --password=%s --host=%s %s < %s/dump_evento_%s.sql" % (db_user, db_pass, db_host, db_data_purga, pathDirectorio, hoy)
	# logger.info("Se insertan valores en tabla grupo_evento de BD Purga")
	# os.system(query)
	
	# query  = "/opt/lampp/bin/mysql --user=%s --password=%s --host=%s %s < %s/dump_historia_%s.sql" % (db_user, db_pass, db_host, db_data_purga, pathDirectorio, hoy)
	# logger.info("Se insertan valores en tabla grupo_evento de BD Purga")
	# os.system(query)
	
	# query  = "/opt/lampp/bin/mysql --user=%s --password=%s --host=%s %s < %s/dump_comentario_%s.sql" % (db_user, db_pass, db_host, db_data_purga, pathDirectorio, hoy)
	# logger.info("Se insertan valores en tabla grupo_evento de BD Purga")
	# os.system(query)
	
except:
	import traceback
	stack =  traceback.format_exc()
	logger.error("Se solicita salida forzada")
	logger.error("Problema: "+ str(stack))
