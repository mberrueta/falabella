<?php $this->load->view("/supervisor/header");?>
<?php $this->load->view("/supervisor/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Bitácora de eventos</h3>
			</header>
			<div style="margin:15px;"> 
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1">Bitácora de eventos</a></li>
						<li><a href="#tabs-2">Eventos por cliente</a></li>
						<li><a href="#tabs-3">Eventos por operador</a></li>
					</ul>
					<div id="tabs-1">
						<div id="filtrosIzq" class="filtros flota">
						
							<div id="fijarIzq">&gt;&gt;</div>
							<div id="grupo_filtros">
								<span style="position: relative; margin-left: 15px; float: left;">Filtros Estado de Eventos</span>
								<div class="grupoIzq" id="filtro_estado"></div>
								
								<span  style="position: relative; margin-left: 15px; float: left;">Filtros Tipo de Eventos</span>
								<div class="grupoIzq" id="filtro_tipoevento"></div>
								
								<span  style="position: relative; margin-left: 15px; float: left;">Filtros Clientes</span>
								<div class="grupoIzq" id="filtro_cliente"></div>
							</div>
							
							<div class="grupo_texto"><label for="texto">Buscar: </label><input class="filtro_texto" name="texto" type="text" value="" /></div>
							<div style="clear:both;"></div>
						</div>
						
						<div id="tabla_eventos" class="flota" border="0" cellspacing="0" cellpadding="0" >
							<table id="eventos" >
								<thead>
									<tr>
										<th>ID</th>
										<th>FECHA</th>
										<th>ESTADO</th>
										<th>EVENTO</th>
										<th>CLIENTE</th>
										<th>NODO</th>
										<th>IP</th>
										<th>OPERADOR</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
						<div id="filtrosDer" class="filtros flota">
							<div id="fijarDer">&lt;&lt;</div>
							<div id="grupoFiltrosDer">	
								<span  style="position: relative; margin-right: 15px; float: right;">Listado de Operadores</span>
								<div class="grupoDer" id="filtro_operadores"></div>

								<span  style="position: relative; margin-right: 15px; float: right;">Rango de Fecha</span>
							</div>
							
							<div class="grupo_texto">
								<label for="fechaInicio">Fch. Inicio:</label>
								<input id="fechaInicio" name="fechaInicio" type="text" value="" class="filtro_fecha" />
								
								<label for="fechaFin">Fch. Fin:</label>
								<input id="fechaFin" name="fechaFin" type="text" value="" class="filtro_fecha" />
							</div>
							
							<div style="clear:both;"></div>
						</div>
						
						<div style="clear:both;"></div>
					</div>
					<div id="tabs-2">
						<?php echo $eventos_cliente; ?>
					</div>
					<div id="tabs-3">
						<?php echo $eventos_operador; ?>
					</div>
				</div>

			
			</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-sliderAccess.js"></script>
	<script src="<?php echo base_url(); ?>js/datatables.fnMultiFilter.js" type="text/javascript"></script>
	<script>
	var bMenu=true;
	var oTable;
	var aFiltrosChecked;  //filtros activos 
	var lookupOperador;
	
	var arrayFilterType				= new Array();
	arrayFilterType['estado'] 		= 'base',
	arrayFilterType['tipoevento'] 	= 'base',
	arrayFilterType['cliente'] 		= 'base';
	
	function getActualFilters(){
		
		for(var index in arrayFilterType){
		
			var dataObj			= {'tipoFiltro':index, 'classFilter':arrayFilterType[index]},	// Objeto con el tipo de filtro solicitado
			controllerRequest	= '../operador/c_filtro',										// Controlador a llamar en la solicitud ajax
			functionRequest		= 'filterPetitionRouter';		// Funcion del controlador a llamar en la solicitud ajax
			
			var urlRequest		= controllerRequest +"/"+  functionRequest;
			peticionASYNC(urlRequest, dataObj, actualizarDivFiltro);
		}
	}
	
	function peticionASYNC(urlRequest, dataObj, callbackFunction){
		$.ajax({
			url:			urlRequest,
			type:		'POST',	
			data:		dataObj,
			cache:		false,
			success:	function(response){
				var filterResponse = JSON.parse(response);
				if( filterResponse.stt == 'error' ){
					alert(filterResponse.msg);
				}
				else{
					callbackFunction( dataObj,  filterResponse.msg);
				}
				return false;
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Se ha detectado un problema en la consulta a servidor, favor intentar nuevamente. Si el problema persiste comunicar a administrador");
			}
		});
	}
	
	function actualizarDivFiltro( filterType, htmlFilter){
		var filterData	= $(htmlFilter);
		// Se recorren los filtros para marcar aquellos que se encontraban marcados antes de la actualizacion
		filterData.find('input[type="checkbox"]').each(function(){
			// Se aplican clases a filtro cliente (coloreo rojo en filtro cliente)
			if( $(this).hasClass('clienteDown') ){
				$(this).parent().parent().attr('style','background-color: #FF0000; color: #000000;');
			}
		});
		$('#filtro_' + filterType.tipoFiltro).html('').append( filterData );
	}
	
	$(document).ready(function(){
	
		$.ajax({ 
			url: 'c_bitacora/lookupOperador', 
			success: function(data) {
				lookupOperador = jQuery.parseJSON( data );
				lookupOperador['null'] = "SIN INFORMACION";
			
				$('#fechaInicio').datetimepicker({
					dateFormat: 'yy-mm-dd',
					onClose: function(dateText, inst) {
						var endDateTextBox = $('#fechaFin');
						if (endDateTextBox.val() != '') {
							var testStartDate = new Date(dateText);
							var testEndDate = new Date(endDateTextBox.val());
							if (testStartDate > testEndDate)
								endDateTextBox.val(dateText);
						}
						else {
							endDateTextBox.val(dateText);
						}
						filtrar(true);
					},
					onSelect: function (selectedDateTime){
						var start = $(this).datetimepicker('getDate');
						$('#fechaFin').datetimepicker('option', 'minDate', new Date(start.getTime()));
					}
				});
				
				$('#fechaFin').datetimepicker({
					dateFormat: 'yy-mm-dd',
					onClose: function(dateText, inst) {
						var startDateTextBox = $('#fechaInicio');
						if (startDateTextBox.val() != '') {
							var testStartDate = new Date(startDateTextBox.val());
							var testEndDate = new Date(dateText);
							if (testStartDate > testEndDate)
								startDateTextBox.val(dateText);
						}
						else {
							startDateTextBox.val(dateText);
						}
						filtrar(true);
					},
					onSelect: function (selectedDateTime){
						var end = $(this).datetimepicker('getDate');
						$('#fechaInicio').datetimepicker('option', 'maxDate', new Date(end.getTime()) );
					}
				});

				oTable = $('#eventos').dataTable({
					"bJQueryUI": true,
					"sScrollY": "480px",
					"sScrollX": "1200px",
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bAutoWidth" :false,
					"sAjaxSource": "c_bitacora/obtener_eventos",
					"iDisplayLength" : 25,
					"aoColumnDefs": [
					  { "sName": "ID"      , "bVisible": true, "aTargets": [ 0 ] },
					  { "sName": "FECHA"    , "aTargets": [ 1 ] },
					  { "sName": "ESTADO"                 , "aTargets": [ 2 ] },
					  { "sName": "TIPOEVENTO"     , "aTargets": [ 3 ] },
					  { "sName": "CLIENTE"                    , "aTargets": [ 4 ] },
					  { "sName": "NODO"     , "aTargets": [ 5 ] },
					  { "sName": "IPNODO"     , "aTargets": [ 6 ] }
					  ,{ 
							"sName": "OPERADOR"  , 
							"fnRender": function ( oObj ) {
								return lookupOperador[ oObj.aData[7] ];
							},
							"aTargets": [ 7 ] 
						}
					],
					'fnServerData': function(sSource, aoData, fnCallback)
					  {
						$.ajax
						({
						  'dataType': 'json',
						  'type'    : 'POST',
						  'url'     : sSource,
						  'data'    : aoData,
						  'success' : fnCallback
						});
					  },
					// 'fnDrawCallback': function( oSettings ) {
					"fnInitComplete": function(oSettings, json) {
						getActualFilters();
						$.post('c_bitacora/obtener_filtros',function(data){
							/*Se agrega el HTML de los filtros*/
							response = jQuery.parseJSON(data);
							$('#filtro_operadores').html('').append(response.fOperador);
						});
					}
				});
			}
		});
	});

/*	--------------------------------------------------
	:: Seccion de filtros
	-------------------------------------------------- */

	/**
	*	Entrega un array concatenado por el signo @ con los valores del filtro solicitado
	*	@param String filtro solicitado. ej: fecha, tipo, etc..
	*	@return String valores a filtrar dependiendo del tipo solictado
	*/
	function getFiltrosActivos( tipo_filtro ){
		v = new Array();
		if(tipo_filtro !== "texto" && tipo_filtro !== "fecha" ){
			$('.filtro_'+tipo_filtro+':checked').each(function(){
				v.push($(this).val());
			});
		}
		else if(tipo_filtro === "fecha"){
			v.push($('#fechaInicio').val());
			v.push($('#fechaFin').val());
		}
		else{
			b = $('.filtro_'+tipo_filtro).val();
			if(b !== ''){v.push(b);}
		}
		return v.join("@");
	}
	
	/**
	*	Dependiendo del tipo de filtro se realiza una llamada a filtro global o filtro por columna
	*	@param Bool si es True se realiza filtro por columna, sino filtro global
	*/
	function filtrar(tipo){
	
		// Se guardan los filtros seleccionados para luego volver a marcarlos,
		// esto debido a que la funcion de redraw generara nuevamente los filtros, eliminando la marca de seleccion
		aFiltrosChecked = new Array();
		$('.filtro').each(function(i){
			if($(this).prop('checked')){
				aFiltrosChecked.push(i);
			}
		});
		
		if(tipo){
			// Se realiza filtro multi columna
			oTable.fnMultiFilter( { "FECHA": getFiltrosActivos('fecha'), "ESTADO": getFiltrosActivos('estado'), "CLIENTE": getFiltrosActivos('cliente'), "TIPOEVENTO": getFiltrosActivos('tipoevento'), "OPERADOR": getFiltrosActivos('operador')} );
		}
		else{
			oTable.fnFilter( getFiltrosActivos('texto') , null, false, true, false, false);
		}
	}
	
	$('.filtro_texto').live('keyup',function(e){
		// if(e.keyCode === 13 || $(this).val() === '') {
		if(e.keyCode === 13) {
			filtrar(false);
		}
	});

	$('.filtro').live('change',function(){
		filtrar(true);
	});	

	/**
	*	Eventos con los que se realizan los movimientos de secciones de filtros
	*/
	
	// Se mueve seccion de filtros izquierda
	$('#fijarIzq').click(function(){
		if(bMenu){ 
			bMenu = false;
			$('#filtrosIzq').animate({left: 0});
			$(this).text('<<');
		}
		else{ 
			bMenu = true;
			$('#filtrosIzq').animate({left: -250});
			$(this).text('>>');
		}
	});
	
	// Se mueve seccion de filtros derecha
	$('#fijarDer').click(function(){
		if(bMenu){ 
			bMenu = false;
			$('#filtrosDer').animate({right: 0});
			$(this).text('>>');
		}
		else{ 
			bMenu = true;
			$('#filtrosDer').animate({right: -250});
			$(this).text('<<');
		}
	});
	
	
	/**
		Evento doble click con el cual se llama al detalle de un evento en particular
		1. Se colorea la fila seleccionada
		2. Se carga una ventana "pop-up" en la cual se despliega el detalla de eventos
	*/
	$('#eventos tbody td').live('dblclick',function(){
		
		// Se colorea la fila seleccionada
		$("td.row_selected", oTable.fnGetNodes()).removeClass('row_selected');
		$(this).parent().find("td").addClass('row_selected');
		
		// Se solicita detalle de grupo de evento y se despliega en nueva ventana
		aData = oTable.fnGetData( this.parentNode );
		$.post('c_bitacora/obtener_historia', {'idEvento': aData[0]} ,function(data){
			w = window.open('', '', 'width=700,height=480');
			w.document.write(data);
			w.document.close();
		});
	});
	
	$(function() {
		$( "#tabs" ).tabs();
	});
	</script>
</body>
</html>