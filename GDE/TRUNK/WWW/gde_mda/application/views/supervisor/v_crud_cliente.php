<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Mantenedor de Perfiles</h3>
				<!--<a href="<?php echo site_url('supervisor/c_crear_cliente')?>"><button id="buttonNuevo" style="position:absolute;margin-top: 6px;right:40px;background-color:none;"></button></a>-->
			</header>
			
		<?php
		if($men != ''){
			echo urldecode($men);
		}

		if($final != ''){
			echo '<table id="clientes" cellspacing="0" cellpadding="0" width="100%">';
			echo '<thead>';
				echo '<tr>';
				echo '<th>Usuario</th>';
				echo '<th>Nombre</th>';
				echo '<th>Area responsable</th>';
				echo '<th>Origen</th>';
				echo '<th> </th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
					foreach($final as $cliente){
					echo '<tr>';
						echo '<td>'.$cliente['Usuario'].'</td>';
						
						echo '<td>';
						if(isset($cliente['Nombre'])){
							echo $cliente['Nombre'];
						}
						echo '</td>';
						
						echo '<td>';
						if(isset($cliente['Organization'])){
							echo $cliente['Organization'];
						}
						echo '</td>';
						
						echo '<td>';
						if(isset($cliente['Herramienta'])){
							echo $cliente['Herramienta'];
						}
						echo '</td>';

						echo '<td><a href="'.site_url('supervisor/c_editar_detalle_cliente/index/'.$cliente['Usuario']).'"><img src="'.base_url().'images/edit-icon.png" class="EditBtn"></a></td>';
					echo '<tr>';
				}
			echo '</tbody>';
			echo '<tfoot>';
			echo '<tr>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '</tr>';
			echo '</tfoot>';
			echo '</table>';
			echo '<br />';
		}
		else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		?>
 			
		</article>
	</section>
	
		<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

</body>
</html>