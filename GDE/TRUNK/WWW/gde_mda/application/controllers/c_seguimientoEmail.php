<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_seguimientoEmail extends CI_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->model('correo');
	}
	
	private function _listadoClientesAsociados(){
		$usuarioActivo = $this->session->userdata('username');
		// $usuarioActivo = 'jaraneda';
		$this->load->model('cliente');
		return $this->cliente->listarClientes($usuarioActivo, true);	// Se obtienen los IDs de clientes para el operador
	}
	
	public function index($message = ''){
		$data = array();
		if($message != ''){
			$data['message'] = $message;
		}
		
		//$listaIdsClientesDeOperador = $this->_listadoClientesAsociados();	// Se obtienen los IDs de clientes para el operador
		
		$tablaSeguimientosActivos = $this->correo->listarSeguimientosActivos();
		if( $tablaSeguimientosActivos['return'] === TRUE ){
			$data['tablaSeguimientosActivos'] = $tablaSeguimientosActivos['result'];
		}
		else{
			$data['tablaSeguimientosActivos'] = $tablaSeguimientosActivos['error'];
		}
		
		$this->load->view('v_seguimientoEmail', $data);
	}
	
	public function listarSeguimientosVencidos(){
		$listaIdsClientesDeOperador = $this->_listadoClientesAsociados();
		$tablaSeguimientosCancelados = $this->correo->listarSeguimientosVencidos($listaIdsClientesDeOperador);
		
		echo json_encode($tablaSeguimientosCancelados);
	}
	
	public function getEventosAsociadosSeguimiento(){
		$idSeguimiento = $this->input->post('seguimiento');
		$dataCorreo = $this->correo->getEventosAsociadosSeguimiento($idSeguimiento);
		
		echo json_encode($dataCorreo);
	}
	
	public function getcorreoRespaldo(){
		$idSeguimiento = $this->input->post('seguimiento');
		$dataCorreo = $this->correo->getcorreoRespaldo($idSeguimiento);
		
		echo json_encode($dataCorreo);
	}
	
	/**
	* Entrega la cantidad de correos de seguimiento que se deben realizar en el instante consultado
	* @return Int cantidad de correos de seguimiento
	*/
	public function cantidadSeguimientosActivos(){
		$listaCliente = $this->_listadoClientesAsociados();
		echo $this->correo->cantidadSeguimientosActivos($listaCliente);
	}
	
	/**
	* Entrega la lista de comentarios asociados a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/
	public function listarComentariosSeguimiento(){
		$idSeguimiento = $this->input->post('seguimiento', TRUE);
		
		$resultadoConsulta = $this->correo->listarComentariosSeguimiento($idSeguimiento);
		if($resultadoConsulta['return'] === TRUE){
			echo $resultadoConsulta['result'];
		}
		else{
			echo $resultadoConsulta['error'];
		}
	}
	
	
	/**
	* Entrega la lista de comentarios asociados a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/
	public function listarComentariosSeguimiento2(){
		$idSeguimiento = $this->input->post('seguimiento', TRUE);
		
		$resultadoConsulta = $this->correo->listarComentariosSeguimiento2($idSeguimiento);
		if($resultadoConsulta['return'] === TRUE){
			echo $resultadoConsulta['result'];
		}
		else{
			echo $resultadoConsulta['error'];
		}
	}
	
	
	/**
	* Registra un nuevo comentario a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/	
	public function registrarComentarioSeguimiento(){
		$idSeguimiento 			= $this->input->post('seguimiento', TRUE);
		$comentarioSeguimiento	= $this->input->post('comentario', TRUE);
		
		$result = $this->correo->registrarComentarioSeguimiento($idSeguimiento, $comentarioSeguimiento);
		
		echo implode(',', $result);
	}
	
	
	/**
	* Edita comentario a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/	
	public function editarComentario(){
		$idcom 			= $this->input->post('idcom', TRUE);
		$comentarioSeguimiento	= $this->input->post('comentario', TRUE);
		
		$result = $this->correo->editarComentario($idcom, $comentarioSeguimiento);
		
		echo implode(',', $result);
	}
	
	/**
	* Edita comentario a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/	
	public function editarComentario2(){
		$idcom 			= $this->input->post('idcom', TRUE);
		$comentarioSeguimiento	= $this->input->post('comentario', TRUE);
		
		$result = $this->correo->editarComentario2($idcom, $comentarioSeguimiento);
		
		echo implode(',', $result);
	}
	
	/**
	* Registra un nuevo comentario a un seguimiento en particular
	* @return String(html) comentarios asociados a un seguimiento
	*/	
	public function registrarComentarioSeguimiento2(){
		$idSeguimiento 			= $this->input->post('seguimiento', TRUE);
		$comentarioSeguimiento	= $this->input->post('comentario', TRUE);
		
		$result = $this->correo->registrarComentarioSeguimiento2($idSeguimiento, $comentarioSeguimiento);
		
		echo implode(',', $result);
	}
	
	public function cancelarSeguimientoActivo(){
	
		$idSeguimiento 			= $this->input->post('seguimiento', TRUE);
		$comentarioCancelacion	= $this->input->post('comentario', TRUE);
	
		$response = $this->correo->cancelarSeguimientoActivo($idSeguimiento, $comentarioCancelacion);
		log_message('error', implode(',', $response) );
		echo json_encode($response);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 1){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function validarPost($postArray){
		$valid = true;
		if( empty($postArray['para']) || empty($postArray['copia']) || empty($postArray['cliente']) ){
			$valid = false;
		}
		return $valid;
	}
	
	public function sendEmail(){
		$this->load->library('email');
		$postArray = $this->input->post();
		
		$this->load->model('correo');
		$postArray['ope'] = $this->correo->copyEmail2();
		
		$postArray['codserv'] = $this->codServicio($postArray['nodos']);
		
		$postArray['nodos'] = $this->arrayDispip($postArray['nodos']);
		
		$valid = $this->validarPost($postArray);
		
		$this->email->from($postArray['desde'], $postArray['desde_nom']);
		$this->email->to($postArray['para']);
		$this->email->cc($postArray['copia']);
		$email = $this->load->view('v_templateEmail', $postArray, TRUE);
		$this->email->subject($postArray['asunto']);
		$this->email->message($email);
		
		log_message('error', 'Inicio proceso envio de correo. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		if($valid){
			$send = $this->email->send();
			if($send){
				log_message('error', 'Envio de correo exitoso. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
				log_message('error', 'Envio de correo exitoso.');
				$this->saveEmail($postArray);
			}
		}
	}
	
	public function sendEmail_new(){
		$this->load->library('email');
		$postArray = $this->input->post();
		
		$this->load->model('correo');
		$postArray['ope']			= $this->correo->copyEmail2();
		$postArray['codserv']	= $this->codServicio($postArray['nodos']);
		$postArray['nodos']		= $this->arrayDispip($postArray['nodos']);
		$postArray['historial']	= $this->correo->listarComentariosSeguimiento_new($postArray['segid']);
		
		$valid = $this->validarPost($postArray);
		
		$this->email->from($postArray['desde'], $postArray['desde_nom']);
		$this->email->to($postArray['para']);
		$this->email->cc($postArray['copia']);
		$email = $this->load->view('v_templateNewEmail', $postArray, TRUE);
		$this->email->subject($postArray['asunto']);
		$this->email->message($email);
		
		log_message('error', 'Inicio proceso envio de correo. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		
		if($valid){
			$send = $this->email->send();
			if($send){
				log_message('error', 'Envio de correo exitoso. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
				log_message('error', 'Envio de correo exitoso.');
				$this->saveEmail_new($postArray);
			}
		}
	}
	
	public function saveEmail($postArray){
		$this->load->model('correo');
		log_message('error', 'Inicio proceso registro en db. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$this->correo->updateCorreos($postArray);
	}
	
	public function saveEmail_new($postArray){
		$this->load->model('correo');
		log_message('error', 'Inicio proceso registro en db. ASUNTO:'.$postArray['asunto'].'- Operador:'.$postArray['ope'][0]);
		$this->correo->updateCorreos_new($postArray);
	}
	
	public function saveError($postArray,$Error){
		$this->load->model('correo');
		$this->correo->registrarError($postArray,$Error);
	}
	
	public function codServicio($postArray){
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		for($i=0;$i<count($nodos);$i++){
			$cel = explode(",",$nodos[$i]);
			$codserv[$i] = $this->correo->codigoServicio($cel[3]);
		}
		return $codserv;
	}
	
	public function arrayDispip($postArray){
		$nodos = explode(";",$postArray);
		$this->load->model('correo');
		for($i=0;$i<count($nodos);$i++){
			$cel = explode(",",$nodos[$i]);
			$dispip = $this->correo->arrayDispip($cel[3]);
			$cel[3] = $cel[3].",".$dispip;
			$nodos[$i] = implode(",",$cel);
		}
		$nodos = implode(";",$nodos);
		return $nodos;
	}
}

/* End of file c_sendEmail.php */
/* Location: ./application/controllers/c_sendEmail.php */
