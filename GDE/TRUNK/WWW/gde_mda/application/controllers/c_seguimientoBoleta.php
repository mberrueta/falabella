<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_seguimientoBoleta extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
		
		$this->load->model('ticket');
	}
	
	public function obtenerNodoEstados() {
		// Se obtienen los TICKETS que no se encuentran cerrados
		$result = $this->ticket->obtenerNodoEstados();
		
		foreach($result->result() as $row){
			/* Condicional para Ticket: Si el TICKET mantiene el mismo estado anterior o es NULO. 
			NOTA: Cuando se crea un ticket el estado anterior es NULO.*/
			
			if( $row->TICKET_EST == $row->TICKET_LAST_EST  || is_null($row->TICKET_LAST_EST) ){
				
				/* TICKET_CONT  = Contador de horas transcurridas.
					TICKET_TESP = Horas para dar aviso de correo*/
					// $tiempo_limite = Tiempo en Horas para dar aviso
					// $row->TICKET_CONT = Tiempo en minutos del acumulador de minutos.
				$tiempo_limite = (int)$row->TICKET_TESP;
				if((int)$row->TICKET_CONT == ($tiempo_limite*60*1)){ 
					
					// Se manda correo y se resetea el contador
					$result = $this->sendEmailTicket($row);
					// Si el correo se envia correctamente se reseta el contador.
					
					if($result){
						$this->ticket->uptadeContadorSeguimiento($row->TICKET_ID,0);
					}
				} else {
					// Contador aumenta en +1
					$valor = $row->TICKET_CONT + 1;
					$this->ticket->uptadeContadorSeguimiento($row->TICKET_ID,$valor);
				}
			} else {
				// Se reseta el contador
				$this->ticket->uptadeContadorSeguimiento($row->TICKET_ID,0);
			}
		}
	}
	
	public function sendEmailTicket($row) {
		
		$this->load->library('email');
		
		// Se obtienen los valores.
		$cliente   = $row->CLI_NOM;
		$dispo     = $row->DIS_NOM;
		$codigo    = $row->TICKET_COD_SERV;
		$escala    = $row->TICKET_TESP;
		$ticket_id = $row->TICKET_ID;
		$fecha     = $row->TICKET_TSINI;
		$para      = $row->TICKET_CORSEG;
		
		// Asunto del correo.
		$asunto = 'INFORMATIVO - '.$cliente.'_'.$dispo.'_'.$codigo.' - ESCALAMIENTO '.$escala.' HORA(s)';
		
		// Consultas a Webservices para obtener valores de Unidad Resolutora y Comentarios de Incidencias en AR.
		$this->load->model('webservices');
		$detalles = $this->webservices->consultaIncidente($ticket_id);
		$detalles = rtrim($detalles, "\n");
		
		if($detalles == 'fail' or $detalles == 'error'){
			log_message('error', 'Envio de Correo Boleta no Gestionada - Error con webservices - timeout');
		} else {
			
			// Consulta Info de Trabajo
			$info_trabajo = $this->webservices->consultaInfoTrabajo($ticket_id);
			$info_trabajo = rtrim($info_trabajo, "\n");
			
			if($info_trabajo == 'fail' or $info_trabajo == 'error'){
				log_message('error', 'Envio de Correo Boleta no Gestionada - Error con webservices - timeout');
			} else {
				
				// Bloque de consulta de incidentes
				$resultado = array();
				$data = explode("\n", $detalles);
				foreach ($data as $valor){
					$values = explode(",", $valor);
					$resultado[$values[0]] = $values[1];
				}
				
				// Bloque de informacion de trabajo
				$info_trabajo_res = array();
				$info_trabajo_res = explode("_fin_", $info_trabajo);
				
				$final = "";
				foreach ($info_trabajo_res as $valor){
					$resultado_info = array();
					$info_values = explode("\n", $valor);
					
					$temp = "";
					foreach ($info_values as $valor_2){
						$info_data = explode(",", $valor_2, 2);
						if(isset($info_data[1])){
							if(@$info_data[0] == 'notas')
							$temp = $temp.@$info_data[1].'\n';
						}
					}
					$final = $final.$temp;
				}
				
				$final = rtrim($final, "\n");
				
				// Se obtienen los nodos asociados
				$nodosAsoc = $this->ticket->obtenerNodosAsociados_Correo($ticket_id);
				$nodosString = implode("\n", $nodosAsoc);
				
				// Se obtienen los codigos de servicios asociados
				$codigosAsoc = $this->ticket->obtenerCodigosServicio($ticket_id);
				$codigosString = implode("\n", $codigosAsoc);
				
				$detalles = array();
				$detalles['cliente']         = $cliente;
				$detalles['nodos']           = nl2br($nodosString);
				$detalles['codigos']         = nl2br($codigosString);
				$detalles['boleta']          = $ticket_id;
				$detalles['fecha_creacion']  = $fecha;
				$detalles['unidad']          = $resultado['grupo_asignado'];
				$detalles['comentarios']     = $final;
				
				// Datos para el correo.
				$this->email->set_crlf( "\n" );
				$this->email->from('gde@entel.cl', 'Gestor de Eventos CMSC');
				$this->email->to($para);
				$this->email->subject($asunto);
				$email = $this->load->view('v_templateSegBoleta', $detalles, TRUE);
				$this->email->message($email);
				
				if ($this->email->send()){
					return TRUE;
				} else{
					$debug_mail = $this->email->my_print_debugger();
					log_message('error', $debug_mail);
					return FALSE;
				}
			}
		}
	}
	
}

/* End of file c_sendEmail.php */
/* Location: ./application/controllers/c_seguimientoBoleta.php */
