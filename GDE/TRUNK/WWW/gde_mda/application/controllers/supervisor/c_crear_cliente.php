<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crear_cliente extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cliente');
		$this->load->model('operador');
		$this->load->library('form_validation');
		$this->load->helper('email');
	}
 
	public function index($mensaje='') {
		$data['men'] = $mensaje;
		
		$data['titulo_pagina'] = 'GDE -> Mantenedor de Perfiles -> Crear Perfil';
		$data['dir_usuario'] = 'admin';
		$data['organizaciones'] = 'admin';
		$data['operadores'] = $this->operador->listar_operadores($this->session->userdata('username'), true);
		$this->load->view('supervisor/v_crear_cliente',$data);
	}
	
	public function valida($nombre) {
		echo json_encode($this->cliente->verificar_cliente(urldecode($nombre)));
	}

	public function crear_cliente($mensaje='') {
		$this->form_validation->set_rules('cNombre','Nombre','trim|required|xss_clean');
		if(($this->form_validation->run()==FALSE)){
			return $this->index('E|El campo Nombre es requerido para poder crear el CLIENTE');
		}
		if(!valid_email($this->input->post('alias'))){
			return $this->index('E|El campo Correo alias no posee formato de email adecuado');
		}
		else{
			
			$cri				= $this->input->post('cCriticidad');
			$nom				= $this->input->post('cNombre');
			$alias			= $this->input->post('alias');
			$nom_alias		= $this->input->post('nom_alias');
			
			//Valores asignados de SLA
			$sla_gm = $this->input->post('slaGenericoMedio');
			$sla_ga = $this->input->post('slaGenericoAlto');
			$sla_0m = $this->input->post('sla0Medio');
			$sla_0a = $this->input->post('sla0Alto');
			$sla_1m = $this->input->post('sla1Medio');
			$sla_1a = $this->input->post('sla1Alto');
			$sla_2m = $this->input->post('sla2Medio');
			$sla_2a = $this->input->post('sla2Alto');
			$sla_3m = $this->input->post('sla3Medio');
			$sla_3a = $this->input->post('sla3Alto');

			if($this->cliente->verificar_cliente($nom)){ 
				return $this->index('E|El Nombre ingresado para CLIENTE ya existe, favor verifique');
			}
			
			if($this->input->post('operador') == null AND $this->input->post('backup') == null AND $this->input->post('otros') == null ){
				return $this->index('E|Debe asignar al menos un operador al Cliente, favor verifique');
			} else{
				$create = $this->cliente->crear_cliente( $nom, $cri, $this->input->post('operador'), $this->input->post('backup'), $this->input->post('otros'), $this->input->post('analistas'), $sla_gm, $sla_ga, $sla_0m, $sla_0a, $sla_1m, $sla_1a, $sla_2m, $sla_2a, $sla_3m, $sla_3a, $alias, $nom_alias );
			}
			
			if ($create){
				$data = 'Cliente agregado exitosamente';
				redirect('supervisor/c_crud_cliente/index/'.$data);
			}
		}
	}
}