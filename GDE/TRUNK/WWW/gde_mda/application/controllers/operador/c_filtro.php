<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_filtro extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		// if(!$this->_logeado()){
			// return redirect('c_login/acceso_denegado');
		// }
		
		$this->load->model('filtro');
	}

	public function index(){
		$this->load->helper('html');
		$data['file_css']	= array('reset','smoothness/jquery-ui-1.8.16.custom','foundation','gde_entel','operador','ie');
		$data['file_js']		= array('jquery','jquery-ui'); //'jquery.dataTables.min','datatables.fnMultiFilter','cambiar_estado','operador',
		$this->load->view('operador/v_test', $data);
	}
	
	public function filterPetitionRouter(){
	
		$classFilter	= $this->input->post('classFilter');
		$tipoFiltro		= $this->input->post('tipoFiltro');
		$tipoActivo     = $this->input->post('activeFilter');
		
		if( $classFilter == 'base' && isset($tipoFiltro)){
			
			$result = $this->actualizarFiltroBaseASYNC($tipoFiltro, $tipoActivo);
			echo $result;
		}
		elseif( $classFilter == 'masivo' && isset($tipoFiltro)){
			
			$result = $this->actualizarFiltroMasivoASYNC($tipoFiltro);
			echo $result;
		}
		else{
			
			$result['stt'] = 'error';
			$result['msg'] = 'Error en valor entregado a consulta';
			echo json_encode($result);
		}
	}
	
	
	private function actualizarFiltroBaseASYNC($tipoFiltro, $tipoActivo){
		
		$this->load->model('usuario');
		$this->load->model('filtro');
		
		$operador	= $this->session->userdata('username');
		$isUser = $this->usuario->getTypeOfUser($operador);
		
		if ($tipoFiltro == "servicio"){
			return json_encode($this->filtro->listarFiltroBaseServicio($operador, $tipoFiltro, ( $isUser['msg'] == 1 ? true : false), $tipoActivo));
		} else {
			return json_encode($this->filtro->listarFiltroBase($operador, $tipoFiltro, ( $isUser['msg'] == 1 ? true : false), $tipoActivo));
		}
		
	}
	
	private function actualizarFiltroMasivoASYNC($tipoFiltro){
		$this->load->model('filtro');
		$operador	= $this->session->userdata('username');
		
		return json_encode($this->filtro->listarFiltroOnDemand($operador,$tipoFiltro));
	}
	
}

/* End of file c_test.php */
/* Location: ./application/controllers/operador/c_test.php */