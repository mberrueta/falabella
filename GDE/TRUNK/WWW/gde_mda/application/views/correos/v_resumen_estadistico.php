<?php $this->load->view("/supervisor/header");?>
<?php $this->load->view("/supervisor/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Resumen estadístico</h3>
			</header>
			<div style="margin:15px;"> 
				<div id="tabs">
					<ul>
						<li><a href="#tabs-0">Visión de supervisor</a></li>
						<li><a href="#tabs-1">Visión por operador</a></li>
						<li><a href="#tabs-2">Visión Global</a></li>
						<li><a href="#tabs-3">Falla Masiva - Geográfico</a></li>
						<li><a href="#tabs-4">Falla Masiva - Cliente por Nodo</a></li>
						<li><a href="#tabs-5">Falla Masiva - Cliente por Sucursal</a></li>
						<li><a href="#tabs-6">Vision Cumplimiento SLA</a></li>
						<li><a href="#tabs-7">Auditoria Tiempos de Respuesta por Tipo de Evento</a></li>
						<li><a href="#tabs-8">Auditoria Tiempos de Respuesta por Operador</a></li>
						<li><a href="#tabs-9">Cantidad de Eventos por Concentrador</a></li>
						<li><a href="#tabs-10">Estado de salud GDE</a></li>
						<li><a href="#tabs-11">Estado GENERAL nodos GDE</a></li>
					</ul>
					<div id="tabs-0">
						<iframe id="iframe-0" src="<?php echo $url2; ?>" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-1">
						<iframe id="iframe-1" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-2">
						<iframe id="iframe-2" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-3">
						<iframe id="iframe-3" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-4">
						<iframe id="iframe-4" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-5">
						<iframe id="iframe-5" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-6">
						<iframe id="iframe-6" src="<?php echo $url8; ?>" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-7">
						<iframe id="iframe-7" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-8">
						<iframe id="iframe-8" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-9">
						<iframe id="iframe-9" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-10">
						<iframe id="iframe-10" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-11">
						<iframe id="iframe-11" src="" frameborder="0" width="100%" height="960px"></iframe>
					</div>
				</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script>
	$(function() {
		var aIFrames = ['<?php echo $url2; ?>','<?php echo $url3; ?>','<?php echo $url4; ?>','<?php echo $url5; ?>','<?php echo $url6; ?>','<?php echo $url7; ?>','<?php echo $url8; ?>','<?php echo $url9; ?>','<?php echo $url10; ?>','<?php echo $url11; ?>','<?php echo $url12; ?>','<?php echo $url13; ?>'];
		$( "#tabs" ).tabs({
			selected: 0,
			spinner: 'Retrieving data...',
			select: function(e, ui){
				var index=ui.index;
				$('iframe').attr('src','');
				$('#iframe-'+index).attr('src',aIFrames[index]);
			}
		});
	});
	</script>
</body>
</html>