<?php

class Auditoria extends CI_Model {

	/* Registra la accion de un operador
	*  @param: String $accion Accion de un usuario destinada a ser registrada en la tabla de auditoria
	*
	*/
	function regAccionAuditoria($accion){
		$data = array(
			'start_time'   => date("Y-m-d H:i:s", mktime(date("H"), date("i"), 0, date("m"),date("d"),date("Y"))),
			'ip'   => $this->session->userdata('ip_address'),
			'user' => $this->session->userdata('username'),
			'info'  => $accion
		);
		
		$this->db->insert('auditoria_gde', $data); 
	}
}

/* End of file auditoria.php */
/* Location: ./application/models/auditoria.php */
