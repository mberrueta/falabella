#!/usr/bin/python
import paramiko
import sys

#Tunneling PRUEBAS Kudaw
#author: jaraneda
#
#Realiza un puente entre nuestro servidor y el servidor que llega a los nodos para ejecutar un comando remoto via ssh
#
#@params: argv[1] = COMANDO (que se ejecutará de forma remota)
#         argv[2] = HOST (el cual actuara de puente)
#         argv[3] = Nombre del Nodo   (a la cual hay que realizar la prueba)
#@return: Salida standard (resultado de ping)

HOST = sys.argv[2]
USER = "dbarrien"
PASS = "12dbarrien3"

ip  = sys.argv[3]
cmd = sys.argv[1]

if (cmd === 'ping'):
	cmd_pivote = cmd+" "+ip+" -n 4"
elif (cmd === 'tracert'):
	cmd_pivote = cmd+" "+ip
else:
	print 'comando incorrecto, favor comunicar a administrador'
	sys.exit(0)

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(HOST, username=USER, password=PASS)

stdin, stdout, stderr = ssh.exec_command(cmd_pivote)
test_results = stdout.readlines()

for test_result in test_results:
	print test_result.replace("\n","")
