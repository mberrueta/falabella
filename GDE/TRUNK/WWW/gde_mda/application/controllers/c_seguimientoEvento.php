<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_seguimientoEvento extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
		
		$this->load->model('evento');
	}
	
	public function obtenerEventosSinGestionar(){
	
		// Se genera array de operadores
		$arrayOperadores = array();
		$this->db->select("CONCAT(OPE_NOM,' ',OPE_APP) AS NOMBRE",FALSE);
		$this->db->select('CLI_ID');
		$this->db->from('grupo');
		$this->db->join('operador','grupo.USU_USER=operador.USU_USER');
		$this->db->where('GRU_STT',1);
		$this->db->where('GRU_TIPO !=','O');
		$resultOperadores = $this->db->get();
		foreach($resultOperadores->result() as $rowOperador){
		
			$arrayOperadores[ $rowOperador->CLI_ID ] = $rowOperador->NOMBRE;
		}
		
		
		// Se obtienen las reglas a ser consultadas
		$this->db->select('GROUP_CONCAT(DISTINCT  EVENTO_CRIT) AS TIPO',FALSE);
		$this->db->select('GROUP_CONCAT(DISTINCT  EVENTO_CLI_CRIT) AS CRIT',FALSE);
		$this->db->select('EVENTO_CORSEG');
		$this->db->select('EVENTO_TESP');
		$this->db->from('evento_seguimiento');
		$this->db->group_by(array('EVENTO_CORSEG', 'EVENTO_TESP'));
		$resultReglas = $this->db->get();
		
		$this->load->library('table');
		
		// Por cada tiempo y destinatario se genera el resultado y se env�a correo 
		if($resultReglas->num_rows() > 0){
			foreach( $resultReglas->result() as $rowRegla ){
			
				$this->db->select('grupo_evento.TEV_NOM');
				$this->db->select('grupo_evento.GEV_TSINI');
				$this->db->select('cliente.CLI_NOM');
				$this->db->select('grupo_evento.CLI_ID');
				$this->db->select('grupo_evento.DIS_NOM');
				$this->db->from('grupo_evento');
				$this->db->join('cliente','grupo_evento.CLI_ID=cliente.CLI_ID');
				$this->db->where('GEV_TSINI > DATE_SUB(NOW(),INTERVAL 40 MINUTE)',NULL,FALSE); // Se busca datos de tan solo los ultimos 40 minutos
				$this->db->where('EST_NOM','Activo');
				$this->db->where_in('TEV_NOM',explode(',' , $rowRegla->TIPO));
				$this->db->where_in('CLI_CRIT',explode(',' , $rowRegla->CRIT));
				$this->db->where('FLOOR(TIME_TO_SEC(TIMEDIFF(NOW(),GEV_TSINI))/60) = ' . $rowRegla->EVENTO_TESP,NULL,FALSE);
				$resultGruposSinGestionar = $this->db->get();
				
				// echo $this->db->last_query();
				// echo '<br/>';
				
				if($resultGruposSinGestionar->num_rows() > 0){
					$tableRows = array();
					foreach( $resultGruposSinGestionar->result() as $rowSinGestionar ){

						array_push($tableRows, array( $rowSinGestionar->TEV_NOM, $rowSinGestionar->GEV_TSINI,$rowSinGestionar->CLI_NOM,$rowSinGestionar->DIS_NOM, $arrayOperadores[ $rowSinGestionar->CLI_ID ]) );
					}
					
					$this->table->set_heading('Evento', 'Hora de llegada', 'Cliente', 'Nodo', 'Operador');
					$this->table->set_template(array(  'table_open' => '<table border="1" cellpadding="4" cellspacing="0">'));
					$tablaConEventos = $this->table->generate($tableRows);
					
					$this->enviarCorreoEventosSinGestionar($rowRegla->EVENTO_CORSEG, $rowRegla->EVENTO_TESP, $tablaConEventos);
				}
			}
		}
		return;
	}
	
	private function enviarCorreoEventosSinGestionar($destinatario, $tiempoSobrepasado, $tablaConEventos){
		
		$this->load->library('email');
		
		$this->email->from('gde@entel.cl', 'Gestor de Eventos CMSC');
		$this->email->to($destinatario);

		$this->email->subject('Correo de alarmas no gestionadas');
		
		$saludo			= '<h4>Estimado,</h4>';
		$mensaje		= '<div>Los siguientes eventos no han sido gestionados en '. $tiempoSobrepasado .' minutos, favor tomar acciones correspondientes.</div>';
		$despedida	= '<h4>Saludos</h4>';
		$bodyHtml		= implode('<br/>', array($saludo,$mensaje,$tablaConEventos,$despedida) );
		// echo $bodyHtml;
		$this->email->message($bodyHtml);
		
		$send = $this->email->send();
	}
}

/* End of file c_seguimientoEvento.php */
/* Location: ./application/controllers/c_seguimientoEvento.php */