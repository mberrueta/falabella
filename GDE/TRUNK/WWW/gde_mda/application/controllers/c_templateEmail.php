<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_templateEmail extends CI_Controller {

	public function loadTemplateEmail(){
	
		$data = array();
		$this->load->view('v_templateEmail', $data);
	}
}

/* End of file c_templateEmail.php */
/* Location: ./application/controllers/c_templateEmail.php */