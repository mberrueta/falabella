<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_prueba extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		$this->load->model('evento');
	}

	
	/**
	*Ejecuta un comando contra el NODO del CLIENTE solicitado
	*
	*@param {string}  POST. Nombre de NODO
	*@param {string}  POST. Cliente asociado al NODO
	*@return {string} Salida estandar de comando ping
	*/
	
	public function prueba_pivote(){
		$nodo    = $this->input->post('nodo');
		$cliente = $this->input->post('cliente');
		$cmd     = $this->input->post('cmd');
		$data['prueba'] = $this->evento->prueba_pivote($cmd, $nodo, $cliente);
		echo $this->load->view('operador/v_popup_prueba',$data,TRUE);
	}
	
	public function cargaIframe(){
		$vars = array();
		$vars['tipo']    = $this->input->post('cmd');
		$vars['cliente'] = $this->input->post('cliente');
		$vars['nodo']    = $this->input->post('nodo');
		$vars['nomnodo'] = $this->input->post('nomnodo');
		
		echo $this->load->view('operador/v_popup_prueba',$vars, TRUE);
	}
	
	public function prueba($tipo, $cliente, $nodo){
		$data = array();
		$data['tipo']    = $tipo;
		$data['cliente'] = $cliente;
		$data['nodo']    = $nodo;
		$data['resultado'] = $this->evento->prueba_pivote($tipo, $nodo, $cliente);
		
		$this->load->view('operador/v_iframe_prueba', $data);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 1 || $this->session->userdata('tipo') == 2)){
			return true;
		}
		else{
			return false;
		}
	}
}

/* End of file c_prueba.php */
/* Location: ./application/controllers/c_prueba.php */