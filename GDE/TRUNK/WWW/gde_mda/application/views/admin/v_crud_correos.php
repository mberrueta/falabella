<?php $this->load->view('/admin/header');?>
<?php $this->load->view('/admin/lateral');?>
	<section id="main" class="column">
		<article class="module width_full" style="width: 97% !important; border-bottom:0;">
			<header>
				<h3><?php echo $titulo_pagina; ?></h3>
			</header>
			<div id="principalMail" style="margin:30px 0; position:relative;">
				<label style="float:left; width:90px;text-aligne:right; margin:0;" for="principalMail">Mesa de ayuda:</label>
				<input type="text" id="principalMail" size="60" style="float:left; margin-bottom:30px;" placeholder="Error: mesadeayuda@cliente.cl" name="principalMail">
				<button style="margin: 0 4px;float: left;" onclick="updateSupportMail(this);"><span class="ui-icon ui-icon-pencil"></span></button>
				<div style="    position: absolute;right: 10px;top: 2px;width: 438px;">
					<label style="float: left;width: 80px;margin: 2px;padding: 0;" for="principalMail">Búsqueda:</label>
					<input type="text" id="search" style="float: left;margin-bottom: 30px;" name="search">
				</div>
				<button id="buttonNuevo" style="position:absolute;right:0;"></button>
			</div>
			<div style="    position: absolute;
							top: 181px;
							bottom: 0px;
							overflow-x: auto;
							right: 0;
							left: 250px; border:1px solid #ddd;">	
			
					<table id="registros" cellspacing="0" cellpadding="0" border="0" width="100%">
						<thead>
							<tr>
								<!-- <th><input type="checkbox" id="checkall" name="checkall" value=""></th>  -->
								<th>Encargado</th>
								<th>Support Organization</th>
								<th>Área Soporte</th>
								<th>Para</th>
								<th>Copia</th>
								<th>Correo MDA (Critical-Mayor)</th>
								<th>Correo MDA (Minor)</th>
								<th>Envio automático</th>
								<th></th><!-- Editar -->
								<th></th><!-- Eliminar -->
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<!-- <td></td> -->
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><!-- <button id="buttonDel"></button> --></td>

							</tr>
						</tfoot>

					</table>
					
				<!-- </div> --></div>
			
		</article>
	</section>


	<!-- Animacion de loading -->
	<div id="ajaxLoading">
		<img src="<?php echo base_url(); ?>images/ajax-loader.gif" alt="Loading Animation" />
		<span>Loading...</span>
	</div>

	<!-- message dialog box -->
	<div id="msgDialog"><p></p></div>
	
	<!-- Dialog Box de confirmacion para eliminacion -->
	<div id="delConfDialog" data-delete="" title="Confirmar">
		<p>¿Seguro que desea eliminar estos datos?</p>
	</div>

	<div id="crear_patron_mail" class="crud">
		<?php echo validation_errors(); ?> 
		<form  action="" method="post" id="datos_nuevo_patron_mail">
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td>Organización:</td>
					<td><input type="text" id="src_organization" size="60" style="float: left;" name="src_organization"  required/></td>
					<td></td>
				</tr>
				<tr>
					<td>Para:</td>
					<td><textarea id="to_mail" name="to_mail" cols="60" rows="6" required ></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>Copia:</td>
					<td><textarea id="cc_mail" name="cc_mail" cols="60" rows="6" required ></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>Encargado:</td>
					<td><input type="text" id="encargado" size="60" name="encargado" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Support Organization:</td>
					<td><input type="text" id="support_organization" size="60" name="support_organization" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Correo MDA (Critical-Mayor):</td>
					<td><input type="checkbox" id="critical_mayor" name="critical_mayor" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Correo MDA (Minor):</td>
					<td><input type="checkbox" id="minor" name="minor" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Envio automático:</td>
					<td><input type="checkbox" id="automatic" name="automatic" style="float: left;"/></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
	
	
		<div id="actualizar_correo">
			<form  action="" method="post" id="datos_actualizar_correo">
			<input type="hidden" id="id" name="id"/>
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<tr>
					<td>Organización:</td>
					<td><input type="text" readonly="readonly" id="src_organization" size="60" style="float: left;" name="src_organization"  required/></td>
					<td></td>
				</tr>
				<tr>
					<td>Para:</td>
					<td><textarea id="to_mail" name="to_mail" cols="60" rows="6" required ></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>Copia:</td>
					<td><textarea id="cc_mail" name="cc_mail" cols="60" rows="6" required ></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>Encargado:</td>
					<td><input type="text" id="encargado" size="60" name="encargado" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Support Organization:</td>
					<td><input type="text" id="support_organization" size="60" name="support_organization" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Correo MDA (Critical-Mayor):</td>
					<td><input type="checkbox" id="critical_mayor" name="critical_mayor" style="float: left;"/></td>
					<td></td>
				</tr>
				<tr>
					<td>Correo MDA (Minor):</td>
					<td><input type="checkbox" id="minor" name="minor" style="float: left;"/></td>
					<td></td>
				</tr> 
				<tr>
					<td>Automático:</td>
					<td><input type="checkbox" id="automatic" name="automatic" style="float: left;"/></td>
					<td></td>
				</tr>
			</table>
		</div>
	<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/crud_correos.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function(){
		});
	</script>
</body>
</html>