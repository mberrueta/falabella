<?php $this->load->view('admin/header'); ?>
<?php $this->load->view('admin/lateral'); ?>

		<section id="main" class="column" style="width:80%; height:600px;">
			<article class="module width_full" style="margin:auto">
				<div class="module_content" style="height:550px;">
					<h4 align="left" >Restauracion de Nodos</h4>
					<div id="estado" align="left" style="float: left;">
						<h3 id="fecha_actual" >Fecha Actual</h3>
						<h5 id="fecha_ultima" >Ultima Restauracion: </h5>
						<h5 id="fecha_respaldo" >Respaldo Utilizado: </h5>
					</div>
					<div id="tabla" style="margin-left: 300px;">
						<table>
						<thead>
							<tr><th>Fecha de Respaldo</th><th>Acciones</th></tr>
						</thead>
						<tbody id="tabla_respaldo">
							
						</tbody>
						</table>
					</div>
				</div>
			</article>
		</section>
		<div style="clear:both;"></div>
	</div>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajax({
				url			: "c_restauracion_nodos/listarRespaldos",
				dataType	: 'json',
				type		: 'GET',
				success	: function (results) {
					var fecha_act = "";
					var d = new Date();
					fecha_act = "Fecha Actual "+d.getDate()+"-"+(d.getMonth()+1).toString()+"-"+d.getFullYear();
					fecha_ult = "Ultima Restauracion: "+results.fechas[1];
					fecha_res = "Respaldo Utilizado: "+results.fechas[0];
					
					var tabla_respaldo = "";
					for( var i=0; i<results.respaldos.length; i++ ){
						tabla_respaldo += "<tr><td >"+results.respaldos[i][0]+"</td><td ><input type=\"image\" src=\"../../images/mantenedor/restore.png\" class=\"restore\" value=\"RESTAURAR\" url=\""+results.respaldos[i][1]+"\"></input><a style=\"margin-left: 20px;\" href=\"<?php echo base_url(); ?>index.php/admin/c_restauracion_nodos/descargarRespaldos/"+results.respaldos[i][1]+"\"><img src=\"../../images/mantenedor/download.png\" ></img></a></td></tr>";
					}
					$('#tabla_respaldo').html(tabla_respaldo);
					
					$('#fecha_actual').html(fecha_act);
					$('#fecha_ultima').html(fecha_ult);
					$('#fecha_respaldo').html(fecha_res);
				},
				error: function (results) {
					console.log(results);
				}
			});
			
		});
		
		$('.restore').live("click",function(e) {
			console.log($(this).attr("url"));
			if(window.confirm("¿Esta seguro que desea realizar esta operacion? \n Los cambios realizados desde la fecha del respaldo seran eliminados.")){
				$.ajax({
					url		: "c_restauracion_nodos/restaurarRespaldos",
					dataType	: 'json',
					type		: 'POST',
					data		: {'ruta':$(this).attr("url")},
					success	: function (results) {
						console.log(results);
						window.alert("Restauracion Exitosa");
					},
					error: function (results) {
						console.log(results);
						window.alert("Error al restaurar. Intente nuevamente");
					}
				});
			}
		});
	</script>
</body>
</html>
