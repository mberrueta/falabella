<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_operador_operador extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		$this->load->model('operador');
		$this->load->library('form_validation');
	}
	
	public function index($mensaje = ''){
		$data['titulo_pagina'] = 'GDE -> clientes Operador a Operador';
		$data['dir_usuario']    = 'supervisor';
		$data['oprs']               = $this->operador->obtenerOperadores();
		$data['mensaje']         = $mensaje;
		
		$this->load->view('supervisor/v_operador_operador', $data);
	}
	
	public function listarClientes(){
		$operador = $this->input->post('usuario');
		$this->load->model('cliente');
		$clientes = $this->cliente->listarClientes($operador);
		
		echo json_encode(array('listCli' => $clientes) );
	}
	
	public function reasignacionClientes(){
		$mensaje = implode(',', $this->input->post());
		return $this->index("S|".$mensaje);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ( $this->session->userdata('tipo') == 0 || $this->session->userdata('tipo') == 2 )){
			return true;
		}
		else{
			return false;
		}
	}
}