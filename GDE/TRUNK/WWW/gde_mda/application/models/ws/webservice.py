#!/usr/bin/python
# -*- coding: utf-8 -*-
import suds
from suds import WebFault
from suds.client import Client
from suds.xsd.doctor import Import, ImportDoctor
import socket
import xml.dom.minidom
import sys
import urllib2

from suds.plugin import MessagePlugin
import re
import datetime

reload(sys)
sys.setdefaultencoding("utf-8")

# Logging Options
import logging
logging.basicConfig(filename='suds.log',format='%(asctime)s %(thread)d %(levelname)s %(message)s',level=logging.ERROR)
logging.getLogger('suds.client').setLevel(logging.ERROR)
logging.getLogger('suds.transport').setLevel(logging.ERROR)
logging.getLogger('suds.wsdl').setLevel(logging.ERROR)

# webserv = sys.argv[1]
id_ticket = sys.argv[1]

#Conexión con el webservice
url     = 'http://cervello.homologacion.accenture.com/falabella-webservicev3-integracao/integracao.asmx?wsdl'
imp     = Import('http://www.w3.org/2001/XMLSchema',location='http://www.w3.org/2001/XMLSchema.xsd')
imp.filter.add('http://tempuri.org/')
client = Client(url, doctor=ImportDoctor(imp))

ticket = client.factory.create('LookupCaseInformationRequest')
ticket.EmailAddress           = "naoenviar1_maria.l.cacciola@accenture.com"
ticket.IncidentNumber         = id_ticket
ticket.CustomerCompany        = 0
ticket.MultiCompanyPeopleLU   = 0
ticket.UseCustomerCorporateID = 0
ticket.CustomerCorporateID    = 0

result = client.service.LookupCaseInformation(ticket)
msgs = '['

if(type(result.WorkLog.diffgram.NewDataSet[0]) is list):
    for msg in result.WorkLog.diffgram.NewDataSet[0]:
        msgs = msgs + "{\"created_at\":\"" + msg.Date_x0020_Created +":00\", \"Details\" : \"" + msg.Details + "\"},"
    msgs = msgs[:-1] + "]"
else:
    msgs = msgs + "{\"created_at\": \"" + result.WorkLog.diffgram.NewDataSet[0].Date_x0020_Created + ":00\", \"Details\" : \"" + result.WorkLog.diffgram.NewDataSet[0].Details + "\"}"
    msgs = msgs + "]"
print(msgs)