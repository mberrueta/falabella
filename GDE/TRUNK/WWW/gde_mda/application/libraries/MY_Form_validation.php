<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Validation Class extended to work with jQuery Validation plugin
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Validation
 * @author		Arnas L. (aka steelaz)
 */
class MY_Form_validation extends CI_Form_validation {

	/**
	 * Constructor
	 */
	public function __construct($rules = array())
	{
		parent::__construct($rules);
	}

	// --------------------------------------------------------------------

	/**
	 * Run the Validator on single field
	 *
	 * @param	string	$field_name
	 * @return	bool
	 */
	public function run_single($field_name)
	{
		// Do we even have any data to process?  Mm?
		if (count($_POST) == 0)
		{
			return FALSE;
		}

		// Does the _field_data array containing the validation rules exist?
		// If not, we look to see if they were assigned via a config file
		if (count($this->_field_data) == 0)
		{
			// No validation rules?  We're done...
			if (count($this->_config_rules) == 0)
			{
				return FALSE;
			}

			// Is there a validation rule for the particular URI being accessed?
			$uri = ($group == '') ? trim($this->CI->uri->ruri_string(), '/') : $group;

			if ($uri != '' AND isset($this->_config_rules[$uri]))
			{
				$this->set_rules($this->_config_rules[$uri]);
			}
			else
			{
				$this->set_rules($this->_config_rules);
			}

			// We're we able to set the rules correctly?
			if (count($this->_field_data) == 0)
			{
				log_message('debug', "Unable to find validation rules");
				return FALSE;
			}
		}

		// Load the language file containing error messages
		$this->CI->lang->load('form_validation');

		// Cycle through the rules for each field, match the
		// corresponding $_POST item and test for errors
		foreach ($this->_field_data as $field => $row)
		{
			if ($row['field'] != $field_name) continue;

			// Fetch the data from the corresponding $_POST array and cache it in the _field_data array.
			// Depending on whether the field name is an array or a string will determine where we get it from.

			if ($row['is_array'] == TRUE)
			{
				$this->_field_data[$field]['postdata'] = $this->_reduce_array($_POST, $row['keys']);
			}
			else
			{
				if (isset($_POST[$field]) AND $_POST[$field] != "")
				{
					$this->_field_data[$field]['postdata'] = $_POST[$field];
				}
			}

			$this->_execute($row, explode('|', $row['rules']), $this->_field_data[$field]['postdata']);
		}

		// Did we end up with any errors?
		$total_errors = count($this->_error_array);

		if ($total_errors > 0)
		{
			$this->_safe_form_data = TRUE;
		}

		// Now we need to re-set the POST data with the new, processed data
		$this->_reset_post_array();

		// No errors, validation passes!
		if ($total_errors == 0)
		{
			return TRUE;
		}

		// Validation fails
		return FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * Generate options for jQuery.validate plugin
	 *
	 * @param	array	$fields		Array of fields and validation rules (CI format)
	 * @param	string	$remote_url	URL where AJAX request will be sent
	 * @return	string
	 */
	public function jquery_options($fields = array(), $remote_url = '')
	{
		$functions = array();
		
		foreach ($fields as $field)
		{
			$rules = explode('|', $field['rules']);

			// Will add built-in required rule
			if (in_array('required', $rules))
			{
				$options['rules'][$field['field']]['required'] = TRUE;
			}
			
			$options['rules'][$field['field']]['remote']['url'] = $remote_url;
			$options['rules'][$field['field']]['remote']['type'] = 'post';
			
			// "matches" is a special case, since we need to check additional field
			foreach ($rules as $rule)
			{
				preg_match_all('/(.*?)\[(.*?)\]/', $rule, $matches);
				
				if (!empty($matches[1][0]) && $matches[1][0] == 'matches' && !empty($matches[2][0]))
				{
					// Generate random key used as identifyer later on to replace it with JS function
					$functions[$matches[2][0]] = 'function() { return jQuery("#'. $matches[2][0] .'").val();}';
					
					$options['rules'][$field['field']]['remote']['data'][$matches[2][0]] = '%'. $matches[2][0] .'%';
				}
			}
			
		}
		$options['onkeyup'] = FALSE;
		$options['errorClass'] = "valida_error";
		$options['validClass'] = "valida_ok";
		$options['errorElement'] = "div";
		
   
		$json =  json_encode($options);
		foreach ($functions as $key => $function)
		{
			$json = str_replace('"%'. $key .'%"', $function, $json);
		}		
		return $json;
	}
	
	public function is_unique($str, $field){
		$field_ar = explode('.', $field);
		$query = $this->CI->db->get_where($field_ar[0], array($field_ar[1] => $str), 1, 0);
		if ($query->num_rows() === 0) {
			return TRUE;
		}
		return FALSE;
	}
	
}
// END Form Validation Class

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */