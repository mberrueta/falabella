<?php

class Nodos extends CI_Model {

	public function listarNodos($clientes, $cli = false){
		
		$this->db->select('cliente.CLI_NOM');
		$this->db->select('dispositivo.DIS_NOM');
		$this->db->select('dispositivo.DIS_CODSERV');
		$this->db->select('dispositivo.DIS_IPPRINC');
		$this->db->select('dispositivo.DIS_CSPRINC');
		$this->db->select('dispositivo.DIS_CODADMIN');
		$this->db->select('dispositivo.DIS_CRIT');
		$this->db->select('dispositivo.DIS_SLA');
		$this->db->select('dispositivo.DIS_ATEN');
		
		$this->db->select('geografia_dispositivo.GEOD_REGION');
		$this->db->select('geografia_dispositivo.GEOD_PROVINCIA');
		$this->db->select('geografia_dispositivo.GEOD_COMUNA');
		$this->db->select('geografia_dispositivo.GEOD_SUCURSAL');
		
		$this->db->select('arquitectura_dispositivo.ARQ_FW');
		$this->db->select('arquitectura_dispositivo.ARQ_PE');
		$this->db->select('arquitectura_dispositivo.ARQ_TUSER');
		$this->db->select('arquitectura_dispositivo.ARQ_TPASS');
		$this->db->select('arquitectura_dispositivo.ARQ_TEPASS');
		$this->db->select('arquitectura_dispositivo.ARQ_SNPM');
		
		$this->db->select('contacto_dispositivo.CNT_CNTHABIL');
		$this->db->select('contacto_dispositivo.CNT_TELHABIL');
		$this->db->select('contacto_dispositivo.CNT_CELHABIL');
		$this->db->select('contacto_dispositivo.CNT_CORHABIL');
		$this->db->select('contacto_dispositivo.CNT_CNTNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_TELNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_CELNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_CORNOHABIL');
		
		$this->db->from('dispositivo');
		$this->db->join('cliente', 'dispositivo.CLI_ID = cliente.CLI_ID', 'left');
		$this->db->join('geografia_dispositivo', 'dispositivo.DIS_NOM = geografia_dispositivo.DIS_NOM', 'left');
		$this->db->join('arquitectura_dispositivo', 'dispositivo.DIS_NOM = arquitectura_dispositivo.DIS_NOM', 'left');
		$this->db->join('contacto_dispositivo', 'dispositivo.DIS_NOM = contacto_dispositivo.DIS_NOM', 'left');
		
		if($cli){
			$this->db->where_in('cliente.CLI_NOM',$clientes);
		}
		else{
			$this->db->limit(1000);
		}
		
		$result = $this->db->get();
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, array( $row->CLI_NOM, $row->DIS_NOM, $row->DIS_CODSERV, $row->DIS_IPPRINC, $row->DIS_CSPRINC, $row->DIS_CODADMIN, $row->GEOD_REGION, $row->GEOD_PROVINCIA, $row->GEOD_COMUNA, $row->GEOD_SUCURSAL, $row->ARQ_FW, $row->ARQ_PE, $row->DIS_CRIT,$row->DIS_SLA, $row->ARQ_TUSER, $row->ARQ_TPASS, $row->ARQ_TEPASS, $row->ARQ_SNPM, $row->CNT_CNTHABIL, $row->CNT_TELHABIL, $row->CNT_CELHABIL, $row->CNT_CORHABIL, $row->CNT_CNTNOHABIL, $row->CNT_TELNOHABIL, $row->CNT_CELNOHABIL, $row->CNT_CORNOHABIL, $row->DIS_ATEN));
			}
		}
		return $queryStatus;
	}
	
	public function listarClientes( $usuario ){
		$this->db->select('cliente.CLI_ID');
		$this->db->select('cliente.CLI_NOM');
		$this->db->from('cliente');
		$this->db->join('grupo','grupo.CLI_ID = cliente.CLI_ID');
		$this->db->where('grupo.USU_USER',$usuario);
		$this->db->group_by("CLI_ID"); 
		$result = $this->db->get();
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, array( $row->CLI_ID, $row->CLI_NOM ));
			}
		}
		return $queryStatus;
	}

	public function validaNombreNodos($nombreNodosNuevos){

		$nombres =  implode("','",$nombreNodosNuevos);


		$consulta = "SELECT `DIS_NOM`
                     FROM  `dispositivo` 
                     WHERE  `DIS_NOM` 
                     IN ('".$nombres."')";

		$result = $this->db->query($consulta);
		$queryStatus = array();

		foreach($result->result() as $row){
			array_push($queryStatus, array($row->DIS_NOM));
		}
		return $queryStatus;
	}
	
	public function crearNodos( $nodosNuevos, $user ){
		$dispositivo = array();
		$geografia_dispositivo = array();
		$arquitectura_dispositivo = array();
		$contacto_dispositivo = array();
		$historial = array();
		
		foreach($nodosNuevos as $nodo){
			$cli_id = $this->db->get_where('cliente', array('CLI_NOM' => $nodo[0]));
			
			array_push($dispositivo, array(
				'CLI_ID'=> $cli_id->row()->CLI_ID,
				'DIS_NOM'=> $nodo[1],
				'DIS_CODSERV'=> $nodo[2],
				'DIS_IPPRINC'=> $nodo[3],
				'DIS_CSPRINC'=> $nodo[4],
				'DIS_CODADMIN'=> $nodo[5],
				'DIS_CRIT'=> $nodo[12],
				'DIS_SLA'=> $nodo[13],
				'DIS_ATEN'=> $nodo[26],
			));
			
			array_push($geografia_dispositivo, array(
				'DIS_NOM'=> $nodo[1],
				'GEOD_REGION'=> $nodo[6],
				'GEOD_PROVINCIA'=> $nodo[7],
				'GEOD_COMUNA'=> $nodo[8],
				'GEOD_SUCURSAL'=> $nodo[9],
			));
			
			array_push($arquitectura_dispositivo, array(
				'DIS_NOM'=> $nodo[1],
				'ARQ_FW'=> $nodo[10],
				'ARQ_PE'=> $nodo[11],
				'ARQ_TUSER'=> $nodo[14],
				'ARQ_TPASS'=> $nodo[15],
				'ARQ_TEPASS'=> $nodo[16],
				'ARQ_SNPM'=> $nodo[17],
			));
			
			array_push($contacto_dispositivo, array(
				'DIS_NOM'=> $nodo[1],
				'CNT_CNTHABIL'=> $nodo[18],
				'CNT_TELHABIL'=> $nodo[19],
				'CNT_CELHABIL'=> $nodo[20],
				'CNT_CORHABIL'=> $nodo[21],
				'CNT_CNTNOHABIL'=> $nodo[22],
				'CNT_TELNOHABIL'=> $nodo[23],
				'CNT_CELNOHABIL'=> $nodo[24],
				'CNT_CORNOHABIL'=> $nodo[25],
			));
			
			array_push($historial, array(
				'USU_USER'			=> $user ,
				'DIS_NOM'			=> $nodo[1] ,
				'HISTORIAL_DET'	=> implode( ",", $nodo ),
				'HISTORIAL_ACC'	=> 'Crear',
			));
		}
		
		$this->db->insert_batch('historial_nodos', $historial); 
		$this->db->insert_batch('dispositivo', $dispositivo); 
		$this->db->insert_batch('geografia_dispositivo', $geografia_dispositivo); 
		$this->db->insert_batch('arquitectura_dispositivo', $arquitectura_dispositivo); 
		$this->db->insert_batch('contacto_dispositivo', $contacto_dispositivo); 
		
		if( $this->db->_error_number() > 0){
			$queryStatus = array( 'ESTADO' => "ERROR", 'ERROR' => $this->db->_error_number() );
		}
		else{
			$queryStatus = array( 'ESTADO' => "OK", 'DETALLE' => "Se crearon ".count($nodosNuevos)." nodos" );
		}
		return $queryStatus;
	}
	
	public function eliminarNodos( $nodosEliminados, $user ){
		foreach($nodosEliminados as $nodo){
			$this->db->delete('geografia_dispositivo',		array('DIS_NOM' => $nodo));
			$this->db->delete('arquitectura_dispositivo',	array('DIS_NOM' => $nodo));
			$this->db->delete('contacto_dispositivo',			array('DIS_NOM' => $nodo));
			$this->db->delete('dispositivo',						array('DIS_NOM' => $nodo));
			
			$data = array(
				'USU_USER'			=> $user ,
				'DIS_NOM'			=> $nodo ,
				'HISTORIAL_DET'	=> 'Nodo Eliminado',
				'HISTORIAL_ACC'	=> 'Eliminar',
			);
			$this->db->insert('historial_nodos', $data); 
		}
		
		
		
		if( $this->db->_error_number() > 0){
			$queryStatus = array( 'ESTADO' => "ERROR", 'ERROR' => $this->db->_error_number() );
		}
		else{
			$queryStatus = array( 'ESTADO' => "OK", 'DETALLE' => "Se eliminaron ".count($nodosEliminados)." nodos" );
		}
		return $queryStatus;
	}
	
	public function modificarNodos ( $nodosModificados, $user ){
		$historial = array();
		
		foreach($nodosModificados as $nodo){
			$this->db->select('CLI_ID');
			$this->db->from('cliente');
			$this->db->where('CLI_NOM',$nodo[0]);
			$cli_id = $this->db->get();
			
			$this->db->where('DIS_NOM', $nodo[1]);
			$this->db->update('dispositivo', array(
				'CLI_ID'=> $cli_id->row()->CLI_ID,
				'DIS_NOM'=> $nodo[1],
				'DIS_CODSERV'=> $nodo[2],
				'DIS_IPPRINC'=> $nodo[3],
				'DIS_CSPRINC'=> $nodo[4],
				'DIS_CODADMIN'=> $nodo[5],
				'DIS_CRIT'=> $nodo[12],
				'DIS_SLA'=> $nodo[13],
				'DIS_ATEN'=> $nodo[26],
			)); 
			
			$this->db->where('DIS_NOM', $nodo[1]);
			$this->db->update('geografia_dispositivo', array(
				'DIS_NOM'=> $nodo[1],
				'GEOD_REGION'=> $nodo[6],
				'GEOD_PROVINCIA'=> $nodo[7],
				'GEOD_COMUNA'=> $nodo[8],
				'GEOD_SUCURSAL'=> $nodo[9],
			));
			
			$this->db->where('DIS_NOM', $nodo[1]);
			$this->db->update('arquitectura_dispositivo', array(
				'DIS_NOM'=> $nodo[1],
				'ARQ_FW'=> $nodo[10],
				'ARQ_PE'=> $nodo[11],
				'ARQ_TUSER'=> $nodo[14],
				'ARQ_TPASS'=> $nodo[15],
				'ARQ_TEPASS'=> $nodo[16],
				'ARQ_SNPM'=> $nodo[17],
			));
			
			$this->db->where('DIS_NOM', $nodo[1]);
			$this->db->update('contacto_dispositivo', array(
				'DIS_NOM'=> $nodo[1],
				'CNT_CNTHABIL'=> $nodo[18],
				'CNT_TELHABIL'=> $nodo[19],
				'CNT_CELHABIL'=> $nodo[20],
				'CNT_CORHABIL'=> $nodo[21],
				'CNT_CNTNOHABIL'=> $nodo[22],
				'CNT_TELNOHABIL'=> $nodo[23],
				'CNT_CELNOHABIL'=> $nodo[24],
				'CNT_CORNOHABIL'=> $nodo[25],
			));
			
			array_push($historial, array(
				'USU_USER'			=> $user ,
				'DIS_NOM'			=> $nodo[1] ,
				'HISTORIAL_DET'	=> implode( ",", $nodo ),
				'HISTORIAL_ACC'	=> 'Modificar',
			));
		}
		
		$this->db->insert_batch('historial_nodos', $historial);
		
		if( $this->db->_error_number() > 0){
			$queryStatus = array( 'ESTADO' => "ERROR", 'ERROR' => $this->db->_error_number() );
		}
		else{
			$queryStatus = array( 'ESTADO' => "OK", 'DETALLE' => "Se modificaron ".count($nodosModificados)." nodos" );
		}
		return $queryStatus;
	}
	
	public function listadoNodos(){
		$this->db->select('DIS_NOM');
		$this->db->from('historial_nodos');
		$this->db->group_by("DIS_NOM"); 
		$this->db->order_by("DIS_NOM", "asc"); 
		$result = $this->db->get();
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, $row->DIS_NOM);
			}
		}
		return $queryStatus;
	}
	
	public function getNodo($nodo){
		$result = $this->db->get_where('historial_nodos', array('DIS_NOM' => $nodo));
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, array( $row->HISTORIAL_TS, $row->USU_USER, $row->HISTORIAL_ACC, $row->HISTORIAL_DET ));
			}
		}
		return $queryStatus;
	}
	
	public function getRespaldos(){
		$this->db->select('RESPALDO_FECHA');
		$this->db->select('RESPALDO_RUTA');
		$this->db->from('respaldo_nodos');
		$this->db->order_by("RESPALDO_FECHA", "desc"); 
		$this->db->limit(15);
		$result = $this->db->get();
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, array( $row->RESPALDO_FECHA, $row->RESPALDO_RUTA ));
			}
		}
		return $queryStatus;
	}
	
	public function getFechas(){
		$result = $this->db->query('SELECT RESPALDO_FECHA, RESPALDO_FECHA_USO FROM  `respaldo_nodos` WHERE  `RESPALDO_FECHA_USO` = ( SELECT MAX( RESPALDO_FECHA_USO ) FROM `respaldo_nodos` )');
		
		$queryStatus = array();
		if( $this->db->_error_number() > 0){}
		else{
			foreach($result->result() as $row){
				array_push($queryStatus, array( $row->RESPALDO_FECHA, $row->RESPALDO_FECHA_USO ));
			}
		}
		return $queryStatus[0];
	}
	
	public function runRestore($ruta){
		$command = '/usr/bin/python /var/www/html/fase_1/application/models/restaurar_nodos.py '.$ruta;
		$output = shell_exec($command);
		return $output;
	}
	
	function listarNodos_exp($clientes, $cli = false, $name){
		
		$this->db->select('cliente.CLI_NOM');
		$this->db->select('dispositivo.DIS_NOM');
		$this->db->select('dispositivo.DIS_CODSERV');
		$this->db->select('dispositivo.DIS_IPPRINC');
		$this->db->select('dispositivo.DIS_CSPRINC');
		$this->db->select('dispositivo.DIS_CODADMIN');
		$this->db->select('geografia_dispositivo.GEOD_REGION');
		$this->db->select('geografia_dispositivo.GEOD_PROVINCIA');
		$this->db->select('geografia_dispositivo.GEOD_COMUNA');
		$this->db->select('geografia_dispositivo.GEOD_SUCURSAL');
		$this->db->select('arquitectura_dispositivo.ARQ_FW');
		$this->db->select('arquitectura_dispositivo.ARQ_PE');		
		$this->db->select('dispositivo.DIS_CRIT');
		$this->db->select('dispositivo.DIS_SLA');
		$this->db->select('arquitectura_dispositivo.ARQ_TUSER');
		$this->db->select('arquitectura_dispositivo.ARQ_TPASS');
		$this->db->select('arquitectura_dispositivo.ARQ_TEPASS');
		$this->db->select('arquitectura_dispositivo.ARQ_SNPM');
		$this->db->select('contacto_dispositivo.CNT_CNTHABIL');
		$this->db->select('contacto_dispositivo.CNT_TELHABIL');
		$this->db->select('contacto_dispositivo.CNT_CELHABIL');
		$this->db->select('contacto_dispositivo.CNT_CORHABIL');
		$this->db->select('contacto_dispositivo.CNT_CNTNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_TELNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_CELNOHABIL');
		$this->db->select('contacto_dispositivo.CNT_CORNOHABIL');
		$this->db->select('dispositivo.DIS_ATEN');
		
		$this->db->from('dispositivo');
		$this->db->join('cliente', 'dispositivo.CLI_ID = cliente.CLI_ID', 'left');
		$this->db->join('geografia_dispositivo', 'dispositivo.DIS_NOM = geografia_dispositivo.DIS_NOM', 'left');
		$this->db->join('arquitectura_dispositivo', 'dispositivo.DIS_NOM = arquitectura_dispositivo.DIS_NOM', 'left');
		$this->db->join('contacto_dispositivo', 'dispositivo.DIS_NOM = contacto_dispositivo.DIS_NOM', 'left');
		
		if($cli){
			$this->db->where_in('cliente.CLI_NOM',$clientes);
		}
		else{
			$this->db->limit(1000);
		}
		$result = $this->db->get();
		$this->load->dbutil();
		$new_report = $this->dbutil->csv_from_result($result);
		
		$this->load->helper('file');
		write_file($name, $new_report);
		
		return $name;
	}
}
?>