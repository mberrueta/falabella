<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_mapa extends CI_Controller {

	public function index()
	{
		$data = array();
		$this->load->model('evento');
		
		$data['aRegionesAfectadas'] = $this->evento->obtenerRegiones();
		$this->load->view('v_mapa', $data);
	}
}

/* End of file c_mapa.php */
/* Location: ./application/controllers/c_mapa.php */