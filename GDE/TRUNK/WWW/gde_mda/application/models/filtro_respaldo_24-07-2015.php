<?php

class Filtro extends CI_Model {
	
	/**
	*TODO
	* comentar
	*/
	protected $_DB2				= null;
	
	/**
	*TODO
	* comentar
	*/
	function __construct() {
		parent::__construct();
		$this->_DB2 = $this->load->database('estadistica', TRUE);
	}
	
	/**
	*Funcionalidad reemplazo de filtros bases ETC (Estado, Tipo de Evento, Cliente)
	*/
	function listarFiltroBase($usuario, $filterNeed, $isOperador = true){
		$ope	= $this->session->userdata('username');
		$columnaFiltro = array(
			'estado' => array(
				'column'	=> "EST_NOM",
				'join'	=> array("SELECT gestor_eventos_rsp.estado.EST_NOM FROM gestor_eventos_rsp.estado")
			),
			'tipoevento' => array(
				'column'	=> "TEV_NOM",
				'where'	=> array('EST_NOM' => 'Activo'),
				'join'	=> array("SELECT gestor_eventos_rsp.tipo_evento.TEV_NOM from gestor_eventos_rsp.tipo_evento")
			),
			'cliente' => array(
				'column'	=> "CLI_NOM",
				'select'	=> array("SUM( CASE WHEN  `TEV_NOM` =  'DOWN' THEN 1 ELSE 0 END ) AS IS_DOWN"),
				'where'	=> array('EST_NOM' => 'Activo'),
				// 'join'	=> array(
					// "SELECT gestor_eventos_rsp.cliente.CLI_NOM FROM gestor_eventos_rsp.cliente LEFT OUTER JOIN gestor_eventos_rsp.grupo ON gestor_eventos_rsp.cliente.CLI_ID=gestor_eventos_rsp.grupo.CLI_ID WHERE gestor_eventos_rsp.grupo.GRU_STT=1 AND gestor_eventos_rsp.grupo.USU_USER LIKE '". ( $isOperador == true ? '%' . $usuario : '%') ."' ORDER BY gestor_eventos_rsp.cliente.CLI_CRIT ASC, gestor_eventos_rsp.cliente.CLI_NOM ASC"
				// ),
				'join'	=> array(
					"SELECT gestor_eventos_rsp.cliente.CLI_NOM FROM gestor_eventos_rsp.cliente LEFT OUTER JOIN gestor_eventos_rsp.grupo ON gestor_eventos_rsp.cliente.CLI_ID=gestor_eventos_rsp.grupo.CLI_ID WHERE gestor_eventos_rsp.grupo.GRU_STT=1 AND gestor_eventos_rsp.grupo.USU_USER LIKE '".$ope."' ORDER BY gestor_eventos_rsp.cliente.CLI_CRIT ASC, gestor_eventos_rsp.cliente.CLI_NOM ASC"
				),
				'conditionalClass'	=> array('IS_DOWN',1,'clienteDown')
			)
		);
		
		$response = array();
		if( !array_key_exists( $filterNeed, $columnaFiltro ) ){
			$response['stt']		= 'error';
			$response['msg']	= 'El filtro solicitado "'.$filterNeed.'" no se encuentra registrado en el sistema, favor informar a administrador.';
			
		}
		else {
			$result = $this->listFiltersETC($usuario, $filterNeed, $columnaFiltro[$filterNeed]['column'] , $columnaFiltro[$filterNeed], $isOperador);
			$response['stt']		= $result['stt'];
			$response['msg']	= $result['msg'];
		}
		
		return $response;
	}
	
	/**
	*TODO
	* comentar
	*/
	private function listFiltersETC( $usuario, $type, $filter, $additional, $isOperador ){
		
		// Create select query
		$this->_DB2->select($filter);
		$this->_DB2->select('sum(ACUM) as TTL', false);
		// If exists customs selects
		if( array_key_exists('select',$additional) ){
			foreach( $additional['select'] as $select ){
				$this->_DB2->select($select, false);
			}
		}
		$this->_DB2->from('filtro_base');
		$this->_DB2->join('gestor_eventos_rsp.cliente',' filtro_base.CLI_ID = gestor_eventos_rsp.cliente.CLI_ID', 'LEFT JOIN');
		if($isOperador){
			$this->_DB2->join('gestor_eventos_rsp.grupo ',' filtro_base.CLI_ID = gestor_eventos_rsp.grupo.CLI_ID', 'LEFT JOIN');
			$this->_DB2->where('USU_USER',$usuario);
			$this->_DB2->where('GRU_STT ',1);
		}
		// If exists customs where
		if( array_key_exists('where',$additional) ){
			$this->_DB2->where($additional['where']);
		}
		$this->_DB2->group_by($filter);
		$this->_DB2->order_by('CLI_CRIT','ASC');
		$this->_DB2->order_by($filter,'ASC');
		$resultsFiltros = $this->_DB2->get();

		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$type);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$type);
		}
		
		// echo  $this->_DB2->last_query();
		
		
		// Se genera tabla HTML con filtros
		$resultsHTML			= array();
		$additionalClasses	= array();
		
		// Si se debe realizar un cruze, es porque el listado de filtros se encuentra completo en otra tabla de la BD
		if( array_key_exists('join',$additional) ){ 
		
			// Se carga el listado de datos para realizar el cruze
			$resultsJoin = null;
			if( sizeof($additional['join']) > 1){
				$resultsJoin = $this->_DB2->query($additional['join'][0], $additional['join'][1]); 
			}
			else{
				$resultsJoin = $this->_DB2->query($additional['join'][0]); 
			}
			
			foreach($resultsJoin->result() as $resultJoin){
				
				if( $resultsFiltros->num_rows() > 0){
					foreach($resultsFiltros->result() as $resultFiltro){
						
						// Si es que se le debe agregar una clase extra al filtro
						
						if( $resultJoin->$filter === $resultFiltro->$filter ){

							$resultsHTML[$resultJoin->$filter] = $resultFiltro->TTL;
							if( array_key_exists('conditionalClass',$additional) ){
							
								if( $resultFiltro->$additional['conditionalClass'][0] == $additional['conditionalClass'][1] ){

									$additionalClasses[$resultJoin->$filter] = $additional['conditionalClass'][2];
								}
							}
							break;
						}
						else{
							$resultsHTML[$resultJoin->$filter] = 0;
						}
					}
				}
				else{
					$resultsHTML[$resultJoin->$filter] = 0;
				}
			}
		}
		else{
			foreach($resultsFiltros->result() as $resultFiltro){
				
				// Si es que se le debe agregar una clase extra al filtro
				if( array_key_exists('conditionalClass',$additional) ){
					
					if( $resultFiltro->$additional['conditionalClass'][0] == $additional['conditionalClass'][1] ){
						
						$additionalClasses[$resultJoin->$filter] = $additional['conditionalClass'][2];
					}
				}
				$resultsHTML[$resultFiltro->$filter] = $resultFiltro->TTL;
			}
		}
		
		$html = $this->generarFiltroHTML($type, $resultsHTML, $additionalClasses);
		return array('stt' => 'ok', 'msg' => $html);
	}

	/**
	* Funcion auxiliar, genera el html necesario para devolver una tabla con los filtros
	*/
	private function generarFiltroHTML($tipo, $arrayFiltro, $additionalClasses){
		
		// echo print_r($additionalClasses);
		$resultClientes = $this->_DB2->get('gestor_eventos_rsp.cliente');
		$arrayClientes = array();
		foreach( $resultClientes->result() as $cliente ){
			$arrayClientes[ $cliente->CLI_NOM ] = $cliente->CLI_ID;
		}
		
		$this->load->library('table');
        $this->load->helper('form');
		
		$data = array();
		
		foreach($arrayFiltro as $k => $v){
			$checkboxData = array(
				'name'        => $tipo . $k,
				'checked'     => false,
				'value'       => $k,
				'class'       => 'filtro filtro_' . $tipo
			);

			if( array_key_exists($k,$additionalClasses)  ){
				$checkboxData['class'] .= " " . $additionalClasses[$k];
			}
			
			if( $tipo=='cliente'  ){
				$checkboxData['value'] = $arrayClientes[$k];
			}
			
			array_push($data, array(form_checkbox($checkboxData), $k, $v)); // se carga fila de tabla con checkbox para filtrar
		}

		$tmpl = array ( // Se elimina encabezado de tabla ya que no es ocupado
			'table_open' 	=> '<table class="filterContainer" border="0" cellpadding="0" cellspacing="0">',
			'thead_open'	=> '',
			'thead_close'	=> '',
			'heading_row_start'		=> '',
			'heading_row_end'		=> '',
			'heading_cell_start'	=> '',
			'heading_cell_end'		=> '',
		);
		
		$this->table->set_heading(array());
		$this->table->set_template($tmpl);
		
		return $this->table->generate($data);
	}
	
	/**
	*TODO
	* comentar
	*/
	public function listarFiltroOnDemand($usuario, $filterNeed, $isOperador = true){
		
		$relationFilterFunction = array(
											'fw' => array(
													'table'		=> 'filtro_masivo_fw',
													'column'	=> 'ARQ_FW',
													'title'			=> 'Firewalls'
											),
											'pe' => array(
													'table'		=> 'filtro_masivo_pe',
													'column'	=> 'ARQ_PE',
													'title'			=> 'PEs'
											),
											'region' => array(
													'table'		=> 'filtro_masivo_region',
													'column'	=> 'GEOD_REGION',
													'title'			=> 'Regional'
											),
											'sucursal' => array(
													'table'		=> 'filtro_masivo_sucursal',
													'column'	=> 'GEOD_SUCURSAL',
													'title'			=> 'Sucursales'
											)
										);
										
		$response = array();
		if( !array_key_exists($filterNeed,$relationFilterFunction) ){
			$response['stt']		= 'error';
			$response['msg']	= 'El filtro solicitado "'.$filterNeed.'" no se encuentra registrado en el sistema, favor informar a administrador.';
			
		}
		else {
			$result = $this->listMasiveFilter($usuario, $relationFilterFunction[$filterNeed]['table'] , $relationFilterFunction[$filterNeed]['column'], $relationFilterFunction[$filterNeed]['title'], $isOperador);
			$response['stt']		= $result['stt'];
			$response['msg']	= $result['msg'];
		}
		
		return $response;
	}

	/**
	*TODO
	* comentar
	*/
	private function listMasiveFilter($usuario, $typeFilterTable,$typeFilterColumn, $typeFilterTitle, $isOperador){
		
		$arrayClientes	= array();
		$sqlCliente		= "select gestor_eventos_rsp.cliente.CLI_ID,gestor_eventos_rsp.cliente.CLI_NOM from gestor_eventos_rsp.cliente join gestor_eventos_rsp.grupo on gestor_eventos_rsp.cliente.CLI_ID=gestor_eventos_rsp.grupo.CLI_ID where gestor_eventos_rsp.grupo.GRU_STT=1 and gestor_eventos_rsp.grupo.USU_USER=?";
		$resultClientes	= $this->_DB2->query($sqlCliente, array($usuario)); 
		
		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$typeFilterTable);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$typeFilterTable);
		}
		
		foreach( $resultClientes->result() as $cliente ){
			$arrayClientes[ $cliente->CLI_ID ] = $cliente->CLI_NOM;
		}
		
		$this->_DB2->from($typeFilterTable);
		if(sizeof($arrayClientes) != 0){
			$this->_DB2->where_in('CLI_ID',array_keys($arrayClientes));
		}
		
		//Si se produce un error en la ejecución de la query a la BD
		if( $this->_DB2->_error_number() > 0 ){
			log_message('error', 'Error en ejecucion de query filtro '.$typeFilterTable);
			log_message('error', $this->_DB2->_error_number());
			log_message('error', $this->_DB2->_error_message());
			log_message('error', $this->_DB2->last_query());
			return array('stt' => 'error', 'msg' => 'Error en ejecucion de query filtro '.$typeFilterTable);
		}
		
		$queryObj		= $this->_DB2->get();
		$html			= null;
		
		if( $typeFilterTable != 'filtro_masivo_sucursal' ){
		
			$html = $this->treeShapeA($queryObj, $typeFilterTable, $typeFilterColumn, $typeFilterTitle, $arrayClientes);
		}
		else{
		
			$html = $this->treeShapeB($queryObj, $typeFilterTable, $arrayClientes);
		}

		return array('stt' => 'ok', 'msg' => $html);
	}

	/**
	*TODO
	* comentar
	*/
	private function treeShapeA($queryObj, $typeFilterTable, $typeFilterColumn, $typeFilterTitle, $arrayClientes){
	
		$this->load->helper('html');
		$list = array();
		
		if( $queryObj->num_rows() > 0 ){
		
			$padresDeSucursales = $this->obtenerPadres( $queryObj, $typeFilterColumn ) ;
			foreach( $padresDeSucursales as $nodoPadre ){
			
				$checkboxData = array(
					'name'	=> substr($typeFilterTable,7) . $nodoPadre
					,'value'	=> $nodoPadre
					,'class'	=> implode(' ',array('masivo',substr($typeFilterTable,7)))
				);
				$nameList = form_checkbox($checkboxData) . '<span class="padre_arbol" ><span class="white radius label">-</span></span>' . nbs(2) . $nodoPadre;
				
				$subList = array();
				foreach( $queryObj->result() as $queryRow  ){
				
					if($queryRow->$typeFilterColumn == $nodoPadre && array_key_exists($queryRow->CLI_ID, $arrayClientes )){
					
						$subList[ $arrayClientes[$queryRow->CLI_ID] ] = '&bull; ' . $arrayClientes[$queryRow->CLI_ID];
					}
					
				}
				
				$list[ $nameList  ] = $subList;
			}
		}
		
		$titleFilter = '<span class="padre_arbol"><span class="white radius label">-</span></span>';
		$titleFilter .= nbs(2);
		$titleFilter .= $typeFilterTitle;

		return ul( array( $titleFilter =>$list ) );
	}
	
	/**
	*TODO
	* comentar
	*/
	private function treeShapeB($queryObj, $typeFilterTable, $arrayClientes){

		$this->load->helper('html');
		$list = array();
		
		if( $queryObj->num_rows() > 0 ){
		
			$padresDeSucursales = $this->obtenerPadres( $queryObj, 'CLI_ID' ) ;
			foreach( $padresDeSucursales as $nodoPadre ){
				
				if(!array_key_exists($nodoPadre, $arrayClientes ))
					continue;
					
				$checkboxData = array(
					'name'	=> 'masivo_cliente' . $arrayClientes[$nodoPadre]
					,'value'	=> $arrayClientes[$nodoPadre]
					,'class'	=> 'masivo masivo_cliente'
				);
				$nameList = form_checkbox($checkboxData) . '<span class="padre_arbol" ><span class="white radius label">-</span></span>' .  $arrayClientes[$nodoPadre] . nbs(2);
				
				$subList = array();
				foreach( $queryObj->result() as $queryRow  ){
				
					if($queryRow->CLI_ID == $nodoPadre){
				
						$input = preg_replace("/[^a-zA-Z0-9]+/", "", $queryRow->GEOD_SUCURSAL);
						$checkboxSubData = array(
							'name'			=> 'sucursal' . $input . $nodoPadre
							,'value'			=> $queryRow->GEOD_SUCURSAL
							,'class'			=> implode(' ',array('masivo',substr($typeFilterTable,7)))
							,'data_region' => $queryRow->GEOD_REGION
						);
						$subList[ $queryRow->GEOD_SUCURSAL ] = form_checkbox($checkboxSubData) . $queryRow->GEOD_SUCURSAL;
					}
				}
				
				$list[ $nameList  ] = $subList;
			}
		}
		
		return '<div id="arbol_masivo" >' .  ul($list) . '</div>';
	}
	
	/**
	*Funcion auxiliar: Entrega las raices de un arbol, segun el campo que se le solicita
	*
	*@param String $queryObj    Objeto query el cual se recorrera para encontrar las raices 
	*@param String   $tipoPadre  Campo de objeto que se debe considerar como raiz
	*
	*@return Array Reices del arbol consultado
	*/
	function obtenerPadres($queryObj, $tipoPadre){
		$listaPadres = array();
		
		foreach($queryObj->result() as $queryRow){
			array_push($listaPadres, $queryRow->$tipoPadre);
		}
		return array_unique($listaPadres);
	}
}
?>