<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_cambiar_turno extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('operador');
		$this->load->library('form_validation');
	}

	public function index($mensaje='')
	{
		$data['mensaje'] = $mensaje;
		$this->load->view('operador/v_cambiar_turno.php',$data);
	}
	
	public function cambiar_turno() {
		$variable = $_POST['estado'];
		$data = $this->operador->cambiar_estado($this->session->userdata('username'),$variable);
		
		//Registro de accion en auditoria
		$solicitud = "";
		if($variable == 0){
			$solicitud = "Solicitud de estado Trabajando";
		}
		elseif($variable == 1){
			$solicitud = "Solicitud de estado Receso";
		}
		else{
			$solicitud = "Solicitud de estado Cambio Turno";
		}
		
		$respuesta = "";
		$buff = explode('|',$data);
		if($buff[0] == 0){
			$respuesta = "Rechazado";
		}
		else{
			$respuesta = "Aceptado";
		}
		
		$this->load->model('auditoria');
		$this->auditoria->regAccionAuditoria($solicitud.", ".$respuesta);
		
		echo json_encode($data);
	}
	
	public function obtener_estado() {
		echo $this->operador->obtener_estado($this->session->userdata('username'));
	}
}
