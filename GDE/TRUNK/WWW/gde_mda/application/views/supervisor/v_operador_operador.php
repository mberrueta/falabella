<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>

		<section id="main" class="column">
			<article class="module width_full">
				<header>
					<h3>Asignación ágil de Cliente entre Operadores</h3>
				</header>
				<table width="100%">
					<tr><td>
					<div style="position: relative; margin: auto;">
						<div style="width: 100%;">
							<?php
								if(isset($mensaje) && $mensaje != ""){
									$men_final = explode('|', $mensaje);
									echo "<span class=\"".$men_final[0]."\">".$men_final[1]."</span>";
								}
							?>
						</div>
						<?php 
							echo form_open('supervisor/c_operador_operador/reasignacionClientes');
							//echo "<fieldset>";
						?>
						<div class="flota box">
							<select id="opPrincipal" name="opPrincipal">
								<?php echo $oprs;?>
							</select>
							<select multiple="multiple" size="20" id="cliPrincipal" name="cliPrincipal[]">
							</select>
						</div>
						<div class="flota" style="vertical-align: middle; padding-top: 50px; width: 30px; text-align: center;">
							<input id="add" type="button" value=">">
							<input id="addAll" type="button" value=">>">
							<input id="remove" type="button" style="margin-top: 10px;" value="<">
							<input id="removeAll" type="button" value="<<">
						</div>
						<div class="flota box">
							<select id="opReemplazo" name="opReemplazo">
								<?php echo $oprs;?>
							</select>
							<select multiple="multiple" size="20" id="cliReemplazo" name="cliReemplazo[]">
							</select>
							<input type="submit" value="Guardar">
						</div>

						<div style="clear:both;"></div>
						<?php
							//echo "</fieldset>";
							echo form_close();
						?>
					</div>
					</td></tr>
				</table>

			</article>
		</section>
		<input type="hidden" id="url_js" value="<?php echo site_url(); ?>">
		<div style="clear:both;"></div>
	</div> 
	<!-- Fin seccion contenedor -->
	<!-- Seccion de recursos-->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/cliente_entre_operadores.js"></script>
</body>
</html>