<?php

class Supervisor extends CI_Model {
	
	function getAll(){
	
		//$this->db->select('*');
		$this->db->select('IFNULL(supervisor_gde.name, "") AS SUP_NOM', false);
		$this->db->select('IFNULL(supervisor_gde.last_name_a, "") AS SUP_APP', false);
		$this->db->select('IFNULL(supervisor_gde.last_name_b, "") AS SUP_APM', false);
		$this->db->select('IFNULL(supervisor_gde.user, "") AS USU_USER', false);
		$this->db->select('IFNULL(usuario_gde.user_mail, "") AS USU_EMAIL', false);
		$this->db->select('IFNULL(supervisor_gde.annexed, "") AS SUP_ANE', false);
		$this->db->select('IFNULL(supervisor_gde.phone, "") AS SUP_TEL', false);
		$this->db->from('supervisor_gde');
		$this->db->join('usuario_gde', 'supervisor_gde.user = usuario_gde.user');
		$query = $this->db->get();
		if( $query->num_rows() > 0 ) {
			return $query->result();
		} 
		else {
			return array();
		}
	}
	
	public function getByuser( $user ) {
		$this->db->select('supervisor.SUP_NOM, supervisor.SUP_APP, supervisor.SUP_APM, supervisor.SUP_ANE, supervisor.SUP_TEL, usuario.USU_USER, usuario.USU_EMAIL');
		$this->db->from('supervisor');
		$this->db->join('usuario', 'supervisor.USU_USER = usuario.USU_USER');
		$this->db->where('usuario.USU_USER', $user);
		$this->db->limit(1);
		$query = $this->db->get();
		if( $query->num_rows() > 0 ) {
			return $query->row();
		} else {
			return array();
		}
	}
	
	public function create($usuario){
		/* Crear el usuario en la BD */
		$this->db->trans_start();
		$user = array(
			'user'  => $usuario['nom_usu_sup'],
			'user_pass'  => md5($usuario['pass_usu_sup']),
			'user_category'  => '2', /* Codigo 0 de supervisor */
			'user_mail' => $usuario['email_usu_sup'] 
		);
		$this->db->insert('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 2;
		}
		else{
			/*Crear el */
			$this->db->trans_start();
			$supervisor = array(
				'user'     => $usuario['nom_usu_sup'],
				'name'      => $usuario['nom_sup'],
				'last_name_a'      => $usuario['app_sup'],
				'last_name_b'      => $usuario['apm_sup'],
				'annexed'      => $usuario['anexo_sup'],
				'phone'      => $usuario['tel_sup'],
				
			);
			$this->db->insert('supervisor_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 3; /*No se pudo guardar el supervisor en la BD*/
			}
			else{
				return 0; /*Se inserto correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function create_V2($usuario){
		echo 
		
		/* Crear el usuario en la BD */
		$this->db->trans_start();
		$user = array(
			'user'  => $usuario['nom_usu_ope'],
			'user_pass'  => md5($usuario['pass_usu_ope']),
			'user_category'  => '3', /* Codigo 0 de supervisor */
			'user_mail' => $usuario['email_usu_ope'] 
		);
		$this->db->insert('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 2;
		}
		else{
			/*Crear el */
			$this->db->trans_start();
			$supervisor = array(
				'user'     => $usuario['nom_usu_ope'],
				'name'      => $usuario['nom_ope'],
				'last_name_a'      => $usuario['app_ope'],
				'last_name_b'      => $usuario['apm_ope'],
				'annexed'      => $usuario['anexo_ope'],
				'phone'      => $usuario['tel_ope'],
				
			);
			$this->db->insert('analista_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 3; /*No se pudo guardar el supervisor en la BD*/
			}
			else{
				return 0; /*Se inserto correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function edit($usuario, $cambia_pass=false){
		/* Actualizar el usuario en la BD */
		$this->db->trans_start();
		if($cambia_pass){
			$user = array(
				'user'  => md5($usuario['edit_pass_usu_sup']),
				'user_mail' => $usuario['edit_email_usu_sup'] 
			);
		}
		else{
			$user = array(
				'user_mail' => $usuario['edit_email_usu_sup'] 
			);
		}
		
		$this->db->where('user', $usuario['edit_nom_usu_sup']);
		$this->db->update('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; //Problemas modificando usuario
		}
		else{
			/*Modificar el supervisor*/
			$this->db->trans_start();
			$supervisor = array(
				//'USU_USER'     => $usuario['edit_nom_usu_sup'],
				'name'      => $usuario['edit_nom_sup'],
				'last_name_a'      => $usuario['edit_app_sup'],
				'last_name_b'      => $usuario['edit_apm_sup'],
				'annexed'      => $usuario['edit_anexo_sup'],
				'phone'      => $usuario['edit_tel_sup']
			);
			$this->db->where('user', $usuario['edit_nom_usu_sup']);
			$this->db->update('supervisor_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 2; /*No se pudo modificar el supervisor en la BD*/
			}
			else{
				return 0; /*Se modifico correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function edit_V2($usuario, $cambia_pass=false){
		/* Actualizar el usuario en la BD */
		$this->db->trans_start();
		if($cambia_pass){
			$user = array(
				'user_pass'  => md5($usuario['edit_pass_usu_ope']),
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		else{
			$user = array(
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		
		$this->db->where('user', $usuario['edit_nom_usu_ope']);
		$this->db->update('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; //Problemas modificando usuario
		}
		else{
			/*Modificar el supervisor*/
			$this->db->trans_start();
			$supervisor = array(
				//'USU_USER'     => $usuario['edit_nom_usu_sup'],
				'name'      => $usuario['edit_nom_ope'],
				'last_name_a'      => $usuario['edit_app_ope'],
				'last_name_b'      => $usuario['edit_apm_ope'],
				'annexed'      => $usuario['edit_anexo_usu_ope'],
				'phone'      => $usuario['edit_tel_usu_ope']
			);
			$this->db->where('user', $usuario['edit_nom_usu_ope']);
			$this->db->update('supervisor_gde', $supervisor);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 2; /*No se pudo modificar el supervisor en la BD*/
			}
			else{
				return 0; /*Se modifico correctamente el usuario y el supervisor */
			}
		}
		return -1;
	}
	
	public function delete($user){
		//Verificamos si tiene operadores asignados
		$this->db->select('COUNT(*) as ope');
		$this->db->from('operador_gde');
		$this->db->where('user_sup', $user);
		$query_ope = $this->db->get();
		$num_ope = $query_ope->row();
		if($num_ope->ope > 0){
			return 2; /* Tiene operadores asociados. No se puede eliminar*/
		}
		else{
			$tables = array('supervisor_gde', 'usuario_gde');
			$this->db->trans_start();
			$this->db->where('user', $user);
			$this->db->delete($tables);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 1; /*No se pudo eliminar el supervisor en la BD*/
			}
			else{
				return 0; /*Se elimino correctamente el usuario y el supervisor */
			}
		}
		
	}
	
	public function delete_V2($user){
		
		$this->db->trans_start();
		
			$this->db->where('user', $user);
			$this->db->delete('grupo_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('supervisor_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('usuario_gde');
			$this->db->flush_cache();
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; /*No se pudo eliminar el operador en la BD*/
		}
		else{
			return 0; /*Se elimino correctamente el usuario y el operador */
		}
	}
	
	public function resumen_eventos_cliente($supervisor){
		
		// Se obtiene listado de clientes
		$this->db->select('gestor_eventos_rsp.cliente.CLI_ID as id');
		$this->db->select('gestor_eventos_rsp.cliente.CLI_NOM as nombre');
		$this->db->from('gestor_eventos_rsp.cliente');
		$objClientes = $this->db->get();
		$this->db->flush_cache();

		// Se genera array lookup de clientes
		$arrayClientes = array();
		foreach( $objClientes->result() as $row){
			
			$arrayClientes[ $row->id ] = $row->nombre;
		}
		
		// Se obtienen valores de estadisticas para formar tabla
		$objEstadisticas = $this->db->get('estadistica.filtro_base');
		
		// Se genera matriz para construir tabla
		$arrayEstadisticas = array();
		foreach( $objEstadisticas->result() as $row ){
		
			array_push( $arrayEstadisticas, array( $arrayClientes[ $row->CLI_ID ], $row->EST_NOM, $row->TEV_NOM, $row->ACUM ) );
		}
		
		// Se genera tabla
		$this->load->library('table');
		$this->table->set_heading('CLIENTE', 'ESTADO', 'TIPO DE EVENTO', 'CANTIDAD DE EVENTOS');
		$this->table->set_template(array(  'table_open' => '<table id="clientes" border="0" cellspacing="0" cellpadding="0" width="100%" >'));
		return $this->table->generate($arrayEstadisticas);

	}
	
	public function resumen_eventos_operador($supervisor){
	
		// Se obtiene listado de operadores
		$this->db->select("CONCAT(gestor_eventos_rsp.operador.OPE_NOM , ' ' , gestor_eventos_rsp.operador.OPE_APP ) as nombre", FALSE);
		$this->db->select('gestor_eventos_rsp.operador.USU_USER as id');
		$this->db->from('gestor_eventos_rsp.operador');
		$objOperadores = $this->db->get();
		$this->db->flush_cache();
		
		// Se genera array lookup de operadores
		$arrayOperadores = array();
		foreach( $objOperadores->result() as $row){
			
			$arrayOperadores[ $row->id ] = $row->nombre;
		}
		
		// Se obtienen valores de estadisticas para formar tabla
		$this->db->select('gestor_eventos_rsp.grupo.USU_USER');
		$this->db->select('estadistica.filtro_base.EST_NOM');
		$this->db->select('estadistica.filtro_base.TEV_NOM');
		$this->db->select('sum(estadistica.filtro_base.ACUM) as TOTAL');
		$this->db->from('estadistica.filtro_base');
		$this->db->join('gestor_eventos_rsp.grupo', 'estadistica.filtro_base.CLI_ID = gestor_eventos_rsp.grupo.CLI_ID', 'LEFT OUTER');
		$this->db->group_by( array( 'gestor_eventos_rsp.grupo.USU_USER','estadistica.filtro_base.EST_NOM','estadistica.filtro_base.TEV_NOM' ) );
		$objEstadisticas = $this->db->get();
		
		// Se genera matriz para construir tabla
		$arrayEstadisticas = array();
		foreach( $objEstadisticas->result() as $row ){
		
			array_push( $arrayEstadisticas, array( $arrayOperadores[ $row->USU_USER ], $row->EST_NOM, $row->TEV_NOM, $row->TOTAL ) );
		}
		
		// Se genera tabla
		$this->load->library('table');
		$this->table->set_heading('OPERADOR', 'ESTADO', 'TIPO DE EVENTO', 'CANTIDAD DE EVENTOS');
		$this->table->set_template(array(  'table_open' => '<table id="clientes" border="0" cellspacing="0" cellpadding="0" width="100%" >'));
		return $this->table->generate($arrayEstadisticas);
		
	}
	
	/**
	*Entrega los eventos a desplegar en bitacora de seguimiento
	*
	*@param  String Identificador de supervisor que solicita eventos
	*@return json  Eventos a desplegar para plugin Datatables
	*/	
	public function resumen_listado_eventos($supervisor){
		// $fecha = date("Y-m-d H:i:s", mktime(date("H")-4, date("i"), 0, date("m")-1,date("d"),date("Y")));
		$fecha = date("Y-m-d H:i:s", mktime(date("H")-4, date("i"), 0, date("m"),date("d")-7,date("Y")));
		$this->load->library('Datatables');
		
		//Se forma query para obtener listado de eventos
		$this->datatables->select('grupo_evento.GEV_ID as ID');
		$this->datatables->select('grupo_evento.GEV_TSINI as FECHA');
		$this->datatables->select('grupo_evento.EST_NOM as ESTADO');
		$this->datatables->select('grupo_evento.TEV_NOM as TIPOEVENTO');
		$this->datatables->select('cliente.CLI_NOM as CLIENTE');
		$this->datatables->select('grupo_evento.DIS_NOM as NODO');
		$this->datatables->select('grupo_evento.GEV_IP as IPNODO', FALSE);
		$this->datatables->select('IFNULL(historia.USU_USER,grupo.USU_USER) as OPERADOR',FALSE);
		
		$this->datatables->from('grupo_evento');
		
		$this->datatables->join('cliente','grupo_evento.CLI_ID=cliente.CLI_ID','LEFT OUTER');
		$this->datatables->join('historia','grupo_evento.GEV_ID=historia.GEV_ID','LEFT OUTER');
		$this->datatables->join('grupo','grupo_evento.CLI_ID=grupo.CLI_ID AND GRU_TIPO=\'T\' AND grupo.GRU_STT=1','LEFT OUTER');
		
		$this->datatables->where("grupo_evento.GEV_TSINI >", $fecha);

		//Se retorna en formato json eventos segun los filtros solicitados
		return $this->datatables->generate();
	}
	
	/**
	*Despliega la traza de el eventos consultado
	*
	*@param  Integer Identificador (GEV_ID) de evento consultado 
	*@return String  Traza del evento en cuestion
	*/
	public function obtener_historia($idEvento){
		
		$fecha = date("Y-m-d H:i:s", mktime(date("H") + 1, date("i"), 0, date("m"),date("d"),date("Y")));
		
		$this->db->select('T.CLI_NOM');
		$this->db->select('T.OPE_NOM');
		$this->db->select('T.OPE_APP');
		$this->db->select('grupo_evento.DIS_NOM');
		$this->db->select('grupo_evento.GEV_TSINI');
		$this->db->from('grupo_evento');
		$this->db->join('(SELECT operador.OPE_NOM, operador.OPE_APP, cliente.CLI_NOM, cliente.CLI_ID FROM cliente LEFT JOIN grupo ON cliente.CLI_ID=grupo.CLI_ID LEFT JOIN operador ON grupo.USU_USER=operador.USU_USER WHERE grupo.GRU_STT=1 ORDER BY cliente.CLI_ID) as T', 'grupo_evento.CLI_ID=T.CLI_ID', 'LEFT OUTER');
		$this->db->where('grupo_evento.GEV_ID',$idEvento);
		$result = $this->db->get();
		
		$tabla_info  = '<table border="0">';
		#$tabla_info .= '<tr><td>HORA ACTUAL:</td><td>'.$fecha.'</td></tr>';
		$tabla_info .= '<tr><td>CLIENTE:</td><td>'.$result->row()->CLI_NOM.'</td></tr>';
		$tabla_info .= '<tr><td>NODO:</td><td>'.$result->row()->DIS_NOM.'</td></tr>';
		$tabla_info .= '<tr><td>OPERADOR ACTUAL:</td><td>'.$result->row()->OPE_NOM.' '.$result->row()->OPE_APP.'</td></tr>';
		
		$tiempo_llegada = $result->row()->GEV_TSINI;
		$this->db->flush_cache();
		
		$result = $this->db->query('SELECT HIS_TS, USU_USER, EST_INI, CONCAT(\'Movido a\') as ACCION, EST_FIN FROM `historia` WHERE GEV_ID='.$idEvento.' UNION ALL (SELECT COM_TS as HIS_TS, USU_USER,  EST_NOM as EST_INI, if(EST_NOM = \'correo\',\'Envío de Correo\',if(EST_NOM = \'cancelseg\',\'Seguimiento Cancelado\',\'Comentado\')) as ACCION, COM_COM FROM `comentario` WHERE GEV_ID='.$idEvento.')');
		
		$result_tick = $this->db->query('SELECT tc.TICKET_COM_TS,tc.TICKET_USU_USER,tc.TICKET_COM_ACC,tc.TICKET_COM_COM FROM ticket_comentario as tc join ticket_gev as tg on tc.TICKET_ID = tg.TICKET_ID WHERE tg.GEV_ID = '.$idEvento);
		
		$tabla_hist  ='<table BORDER="3" style="max-height:200px; overflow:auto;"><caption>HISTORIA</caption><thead><tr style="background-color: #FFBF00;"><th>FECHA</th><th>OPERADOR</th><th>ESTADO</th><th>ACCION</th><th>DETALLE</th></tr></thead><tbody">';
		
		$tabla_tick = '<table BORDER="3" style="max-height:200px; overflow:auto;"><caption>Acciones en ticket</caption><thead><tr style="background-color: #FFBF00;"><th>FECHA</th><th>OPERADOR</th><th>ACCION</th><th>DETALLE</th></tr></thead><tbody">';
		
		if($result->num_rows() > 0){ //Si existen registros en la tabla Historia para el ID de grupo entregado
			$tabla_info .= '<tr><td>TIEMPO DE RESPUESTA:</td><td>'.$this->time_diff_conv(strtotime($result->row()->HIS_TS)- strtotime($tiempo_llegada)).'</td></tr>';
			$tabla_hist .='<tr><td>'.$tiempo_llegada.'</td><td>---</td><td>---</td><td style="background-color: #D7DF01;">Se alarmó</td><td><b>Activo</b></td></tr>';
			
			foreach($result->result() as $row){
				$tabla_hist .='<tr><td>'.$row->HIS_TS.'</td><td>'.$row->USU_USER.'</td><td>'.$row->EST_INI.'</td>';
				if($row->ACCION == 'Movido a'){
					$tabla_hist .='<td style="background-color: #81DAF5;">'.$row->ACCION.'</td><td><b>'.$row->EST_FIN.'</b></td></tr>';
				}else if($row->ACCION == 'Envío de Correo'){
					$detalle = explode("++",$row->EST_FIN);
					$tabla_hist .='<td >'.$row->ACCION.'</td><td>'.$detalle[0].'<br/>'.$detalle[1].'<br/>'.$detalle[2].'</td></tr>';
				}else if($row->ACCION == 'Seguimiento Cancelado'){
					$tabla_hist .='<td>'.$row->ACCION.'</td><td>'.$row->EST_FIN.'</td></tr>';
				}else {
					$tabla_hist .='<td style="background-color: #01DFA5;">'.$row->ACCION.'</td><td>'.$row->EST_FIN.'</td></tr>';
				}
			}
			$tabla_hist .='</tbody></table>';
		}
		else{
			$tabla_info .= '<tr><td>TIEMPO DE RESPUESTA:</td><td>'.$this->time_diff_conv(strtotime($fecha) - strtotime($tiempo_llegada)).'</td></tr>';
			$tabla_hist .='<tr><td>'.$tiempo_llegada.'</td><td>---</td><td>---</td><td style="background-color: #D7DF01;">Se alarmó</td><td><b>Activo</b></td></tr></tbody></table>';
		}
		
		if($result_tick->num_rows() > 0){ //Si existen registros en la tabla ticket comentario para el ID de grupo entregado
			foreach($result_tick->result() as $row){
				$tabla_tick .='<tr><td>'.$row->TICKET_COM_TS.'</td><td>'.$row->TICKET_USU_USER.'</td><td>'.$row->TICKET_COM_ACC.'</td>'.'</td><td>'.$row->TICKET_COM_COM.'</td></tr></tbody>';
			}
			$tabla_tick .='</table>';
		}
		else{
			$tabla_tick .='<tr><td>'.$tiempo_llegada.'</td><td>---</td><td>---</td><td>Activo</td></tr></tbody></table>';
		}
		
		$tabla_info .= "</table>";
		
		$this->load->model('evento');
		$tabla_sumz = $this->evento->obtenerSumarizados($idEvento);
		$tabla_sumz = str_replace("<tbody>" ,"<caption>Eventos Sumarizados</caption><tbody>",$tabla_sumz);
		
		$tablas = array('tabla_info' => $tabla_info, 'tabla_hist' => $tabla_hist, 'tabla_tick' => $tabla_tick, 'tabla_sumz'=> $tabla_sumz);
		return $tablas;
	}
	
	/**
	*Calcula traduce los segundos a duración en Horas:Minutos:Segundos
	*
	*@param  Integer Segundos a calcular su suración 
	*@return String  Duración con el formato de HH:MM:SS
	*/
	private function time_diff_conv($seconds) {
		$hours = floor($seconds / 3600);
		$minutes = floor($seconds % 3600 / 60);
		$seconds = $seconds % 60;

		return sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
	}

	/**
	*Genera json lookup con la asociacion de operador nombre completo y su nombre de usuario
	*@return String listado en formato JSON con nombre de usuario y su asociacion a Nombre Completo
	*/
	public function lookupOperador(){
		$this->db->select('USU_USER');
		$this->db->select('CONCAT_WS(\' \', operador.OPE_NOM, operador.OPE_APP) as NOMBRE', FALSE);
		$this->db->from('operador');
		$result = $this->db->get();
		
		$lookup = array();
		foreach($result->result() as $row){
			$lookup[ $row->USU_USER ] = $row->NOMBRE;
		}
		
		return json_encode($lookup);
	}
	
}
	
?>