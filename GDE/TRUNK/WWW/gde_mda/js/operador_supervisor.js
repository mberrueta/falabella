var OpBySup       =	'c_operador_supervisor/getOpeSup',
	OpSinSup      =	'c_operador_supervisor/getOpeSinSup', 
	reasignaOPUrl =	'c_operador_supervisor/reasignaOP';

$( function() {
		$( '#msgDialog' ).dialog({
			autoOpen: false,    
			buttons: {
				'Ok': function() {
					$( this ).dialog( 'close' );
				}
			}
		});

	$('#supervisores').change(function(){
		if($('#supervisores :selected').val() != 'default'){ 
		RecargaSinSup();
			$.ajax({
				type: 'POST',
				url: OpBySup,
				data: {supervisor: $('#supervisores :selected').val() },
				dataType: 'json',
				success: function( response ) {
					$('#OpeAsignados').html('');
					var salida = '';
					for( var i in response ) {
						salida += "<option value=\""+response[ i ].user+"\">"+response[ i ].name+" "+response[ i ].last_name_a+"</option>";
					}
					$('#OpeAsignados').html(salida);
				}
			});
		}
		else{
			RecargaSinSup();
			$('#OpeAsignados').html('');
		}
	});
	$('#guarda_aso').bind('click', function(){
		var supervisor = $('#supervisores :selected').val();
		
		var operadores = new Array();
		$('#OpeAsignados option').each(function(){ 
			operadores.push($(this).val());
		});
		
		var operadores_sin = new Array();
		$('#OpSinSup option').each(function(){ 
			operadores_sin.push($(this).val());
		});
		
		var toBeSent = {operadores:operadores , supervisor:supervisor, operadores_sin:operadores_sin};
		$.ajax({
			type: 'POST',
			url: reasignaOPUrl,
			data: toBeSent,
			success: function( response ) {
				if(response == "OK"){
					$( '#msgDialog' ).html('Datos actualizados correctamente');
					$( '#msgDialog' ).dialog('open');
				}
				else{
					$( '#msgDialog' ).html('Error');
					$( '#msgDialog' ).dialog('open');
				}
			}
		});
		
	});

}); //end document ready

function RecargaSinSup(){
	$.ajax({
		type: 'POST',
		url: OpSinSup,
		//data: {supervisor: $('#supervisores :selected').val() },
		dataType: 'json',
		success: function( response ) {
			$('#OpSinSup').html('');
			var salida = '';
			for( var i in response ) {
				salida += "<option value=\""+response[ i ].user+"\">"+response[ i ].name+" "+response[ i ].last_name_a+"</option>";
			}
			$('#OpSinSup').html(salida);
		}
	});
}