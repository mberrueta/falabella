<?php

class WebServices_hugo extends CI_Model {
	
	protected $realPath		= null;
	protected $pythonPath	= null;
	protected $wsFilePath	= null;
	
	function __construct() {
		parent::__construct();
		
		$this->wsFilePath		= realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "ws" . DIRECTORY_SEPARATOR . "webservice.py ";
		$this->pythonPath	= "/usr/bin/python ";
	}
	
	/**
	*Crea el Ticket en Remedy
	*
	*@param String	$codserv	Codigo de servicio de el/los nodo(s) del Ticket
	*@param String	$email		Correo del operador que crea el Ticket
	*@param String	$resumen	Resumen del Ticket
	*@param int		$fuente		Fuente del Ticket
	*@param String	$origen		Origen del Ticket
	*
	*@return String[] Arreglo con los resultados de la creacion del Ticket
	*/
	public function crearTicket($ID){
		
		// Obtiene información del ticket
		$this->db->select('*');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id = '.$ID);
		$result = $this->db->get();
		
		// Datos de eventos del GDE
		$Pais                 = $result->row()->dest_country;
		$Negocio              = $result->row()->dest_bunit;
		$ServicioAfectados    = $result->row()->src_system;
		$AreaResponsable      = $result->row()->src_organization;
		$TipoDispositivo      = $result->row()->src_category;
		$UbicacionDispositivo = $result->row()->src_subcategory;
		$NombreDispositivo    = $result->row()->src_name;
		$IPPuerta             = $result->row()->src_ip;
		$FechaHora            = $result->row()->start_time;
		$Descripcion          = $result->row()->type;
		
		// Mantenedor de categorias
		// $Categoria1           = "";
		// $Categoria2           = "";
		// $Categoria3           = "";
		
		// Mantenedor de correos
		// $GrupoResolutor       = "";
		// $SupportOrganization  = ""
		
		if($result->row()->severity == "CRITICAL"){
			$Urgencia = "1-Critical";
		}
		
		$command = $this->pythonPath . $this->wsFilePath .'createTicket'.' "'.$Pais.'" "'.$Negocio.'" "'.$ServicioAfectados.'" "'.$AreaResponsable.'" "'.$TipoDispositivo.'" "'.$UbicacionDispositivo.'" "'.$NombreDispositivo.'" "'.$IPPuerta.'" "'.$FechaHora.'" "'.$Descripcion.'" "'.$Urgencia.'"';
		
		$output = shell_exec($command);
		return $output;
	}
	
	
	/**
	*Consulta el estado del Ticket OK
	*
	*@param String	$idticket	Identificador unico de cada Ticket
	*
	*@return String[] Arreglo con los resultados de la consulta del Ticket
	*/
	public function consultaIncidente($ID){
		$command = $this->pythonPath . $this->wsFilePath . 'consultaIncidente'." ".$ID." 2>&1";
		exec($command, $output, $status);
		return $output[0];
	}
	
	/**
	*Cierra el Ticket seleccionado
	*
	*@param String	$idticket	Identificador unico de cada Ticket
	*@param Bool	$accion		Identifica mediante que accion fue cerrado el ticket
	*
	*@return String[] Arreglo con los resultados del cierre del Ticket
	*/
	public function actualizaEstadoIncidente($ID, $status, $comentario){
		$command = $this->pythonPath . $this->wsFilePath . 'actualizaEstadoIncidente'.' '.$ID.' "'.$status.'" "'.$comentario.'"';
		$output = shell_exec($command);
		return $output;
	}

	public function syncTicket($ID){
		$command = $this->pythonPath . $this->wsFilePath ." ".$ID." 2>&1";
		exec($command, $output, $status);
		return $output[0];
	}
	

	
}