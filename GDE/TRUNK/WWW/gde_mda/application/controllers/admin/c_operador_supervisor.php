<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_operador_supervisor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('supervisor');
		$this->load->model('operador');
	}
	public function index(){
		
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$supervisores = array();
		/* Obtener la lista de supervisores y armar el array para la vista */
		$lista_sup = $this->supervisor->getAll();
		$supervisores['default'] = '-- Seleccione un supervisor --';
		foreach($lista_sup as $supervisor){
			$supervisores[$supervisor->USU_USER] = $supervisor->SUP_NOM." ".$supervisor->SUP_APP;
		}
		$data['supervisores'] = $supervisores;
		
		/* Obtener la lista de operadores que no estan asignados*/
		$op_sup = $this->operador->OperadoresSinSup();
		$operadores = array();
		foreach($op_sup['cli']->result() as $operador){
			$operadores[$operador->user] = $operador->name." ".$operador->last_name_a;
		}
		$data['OpSinSup'] = $operadores;
		$data['titulo_pagina'] = 'GDE -> Asociación operador supervisor';
		
		
		// $tempo = $this->operador->listar_operadores('frasepulveda');
		// $data['ope_supervisor'] = $tempo['cli']->result();
		
		
		$this->load->view('admin/operador_supervisor',$data);
		
	}
	public function getOpeSinSup(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$lista_op = $this->operador->OperadoresSinSup();
		echo json_encode($lista_op['cli']->result());
	}
	
	
	
	/* Traer los operadores para un supervisor */
	public function getOpeSup(){
	
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
	
		if(isset($_POST['supervisor'])){
			$lista_op = $this->operador->listar_operadores($_POST['supervisor']);
			echo json_encode($lista_op['cli']->result());
		}
	}
	public function reasignaOP(){
	
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if(isset($_POST['supervisor']) && (isset($_POST['operadores']) || isset($_POST['operadores_sin']))){
			if(isset($_POST['operadores'])){
				foreach($_POST['operadores'] as $operador){
					$respuesta = $this->operador->asociaSup($_POST['supervisor'], $operador);
					if($respuesta == FALSE){
						echo "ERROR";
						return;
					}
				}
			}
			if(isset($_POST['operadores_sin'])){
				foreach($_POST['operadores_sin'] as $operador){
					$respuesta = $this->operador->asociaSup(NULL, $operador);
					if($respuesta == FALSE){
						echo "ERROR";
						return;
					}
				}
			}
			echo "OK";
		}
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2 ){
			return true;
		}
		else{
			return false;
		}
	}
}