<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>GDE -> Operador</title>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="Sat, 26 Jul 1997 05:00:00 GMT">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/gde_falabella.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/operador.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ie.css" rel="stylesheet" />
	<!--<link type="text/css" href="<?php echo base_url(); ?>css/gde_new_template.css" rel="stylesheet" /> -->
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/datatables.fnMultiFilter.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/operador.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery.base64.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/tableExport.js" type="text/javascript"></script>
	<!--<script src="<?php echo base_url(); ?>js/cambiar_estado.js" type="text/javascript"></script>-->
</head>
<body>

<!-- Header -->
<div class="row">
	<div class="twelve columns">
		<div class="row" style="margin-top: -10px;">
			</br>
		</div>
		<!--<div class="row" style="margin-left: 1%;">-->
		<div class="row">
			<div class="three columns">
				<div id="title_menu_header" class="menu_header row" style="width:100%">
					<div class="two columns" ></div>
					<div id="usr_menu_header" class="eight columns" >
						<?php echo $this->session->userdata('nombre'); ?>
					</div>
					<?php if ($this->session->userdata('tipo') == 2): ?>
						<div id="sel_menu_header" class="two columns" ></div>
					<?php else: ?>
						<div class="two columns" ></div>
					<?php endif; ?>
				</div>
				
				<div id="options_menu_header" class="menu_header row" style="width:95%">
					<div class="twelve columns">
						<div id="displayMenuAdmin" class="option_menu_header row" ><?php echo anchor('admin/c_menu_admin','Menu de Administración'); ?></div>
					</div>
				</div>
			</div>
			<div class="two columns">
				<div id="title_menu_header" class="menu_header row select_filtro_menu" style="width:130px">
					<div id="usr_menu_header" class="eight columns">Filtros</div>
					<div id="filtro_menu_header" class="two columns" ></div>
				</div>
			</div>
			<div class="four columns">
				<h2 class="section_title">Adessa - Gestor de Eventos</h2>
			</div>
			<div class="two columns">
				<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
			</div>
		</div>
	</div>
</div>

<div class="container" style="margin-top:0px;">
	
	<!-- Filtros horizontales-->
	<div class="row">
		<div class="contenedor_filtros_superior" style="display: none;">
			<div class="row">
				<div class="two columns" style="margin-left: 10px;">
					<span class="filtro_titulo">Estado de Eventos</span>
					<div  class="grupo chico" id="filtro_estado"></div>
				</div>
				<div class="two columns" style="margin-left: 10px;">
					<span class="filtro_titulo">País</span>
					<span style="margin-left: 30%;">
						<a name="pais" id="clearFilter" style="cursor: pointer; color: white; */">
						<img src="<?php echo base_url(); ?>images/clear.png" class="icon_clear"/>
						Limpiar
						</a></span>
					<div  class="grupo chico" id="filtro_pais"></div>
				</div>
				<div class="two columns" style="margin-left: 10px;">
					<span class="filtro_titulo">Negocio</span>
					<span style="margin-left: 20%;"><a name="negocio" id="clearFilter" style="cursor: pointer; color: white; */"><img src="<?php echo base_url(); ?>images/clear.png" class="icon_clear"/>Limpiar</a></span>
					<div  class="grupo chico" id="filtro_negocio"></div>
				</div>
				<div class="two columns" style="margin-left: 10px;">
					<span class="filtro_titulo">Servicio</span>
					<span style="margin-left: 20%;"><a name="servicio" id="clearFilter" style="cursor: pointer; color: white; */"><img src="<?php echo base_url(); ?>images/clear.png" class="icon_clear"/>Limpiar</a></span>
					<div  class="grupo chico" id="filtro_servicio"></div>
				</div>
				<div class="three columns" style="margin-left: 10px;">
					<span class="filtro_titulo">Área Soporte</span>
					<span style="margin-left: 30%;"><a name="organization" id="clearFilter" style="cursor: pointer; color: white; */"><img src="<?php echo base_url(); ?>images/clear.png" class="icon_clear"/>Limpiar</a></span>
					<div  class="grupo chico" id="filtro_organization"></div>
				</div>
				<div class="three columns" style="margin-left: 10px;">
					<span class="filtro_titulo" style="position: absolute;left: 0;">Ubicación del Elemento</span>
					<span style="margin-left: 40%;"><a name="tag" id="clearFilter" style="cursor: pointer; color: white; */"><img src="<?php echo base_url(); ?>images/clear.png" class="icon_clear"/>Limpiar</a></span>
					<div  class="grupo chico" id="filtro_tag"></div>
				</div>
			</div>
			<div class="row">
				<div class="three columns" style="margin-left: 10px; margin-top: 10px;">
					<span class="filtro_titulo" style="margin-top: 0px !important;">Criticidad:</span>
					<select type="text" class="filtro filtro_criticidad" value="" style="height: 17px; float: left; margin-top: 0px !important;" >
						<option value="1">TODAS</option>
						<option value="CRITICAL">CRITICAL</option>
						<option value="MAJOR">MAJOR</option>
						<option value="MINOR">MINOR</option>
					</select>
				</div>
				<div class="three columns" style="margin-left: 10px; margin-top: 10px;">
					<span class="filtro_titulo" style="margin-top: 0px !important;">Buscar palabra:</span>
					<input class="filtro filtro_texto" name="texto" type="text" value="" />
				</div>
			</div>
		</div>
	</div>
</div>


<div id="mySidenav" class="sidenav">
	<div style="width: 100%; margin-bottom: -18px; margin-top: 5px; overflow:hidden;">
		<div style="width: 17%; float: left;">
			<!--<p id="filtro_der_hidden" style="background-color: #0073ae; height: 32px; line-height: 30px; cursor: pointer;">Ocultar</p>-->
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		</div>
		<div style="width: 80%; float: left;">
			<p style="color: #ffffff;">Mostrando informacion para alerta: <br /><span class="boxTitle"></span></p>
		</div>
		
	</div>
	<div class="bloque_informacion" style="position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 25px;
    width: 87% !important;">
		<p style="background-color: #e4e4e4; color: #000000; font-weight: bold;">Comentarios de evento<br /></p>
		<div id="bloqueComentar" class="boxInsideT"></div>
		<div style="clear:both;"></div>
		<div style="margin-top: 5px; margin-left: 5px; height: 30px;">
			<input type="text" name="comentario" id="comentario"/>
			<input type="submit" value="Enviar" id="envComentario" />
		</div>
		<div style="margin-top: -8px; margin-left: 5px; height: 20px; float: left;">
			<input type="checkbox" id="comenvMDA" value="1">&nbsp;&nbsp;Enviar comentario a Mesa de Ayuda
		</div>
		<input type="hidden" value="comGruId" id="comGruId" />
		<input type="hidden" value="comTicketId" id="comTicketId" />
	</div>
	<div class="bloque_informacion" style="    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 330px;
	height: auto !important;
    width: 87% !important;">
		<p style="background-color: #e4e4e4; color: #000000; font-weight: bold;">Sumarización de eventos<br /></p>
		<div id="bloqueSumariza" class="boxInside">
		</div>
	</div>
	<!--<div class="bloque_informacion">
		<p style="background-color: #BA80A6; color: #000000; font-weight: bold;">Historia de Nodo (Últimos 7 dias)<br /></p>
		<div id="bloqueHistoria" class="boxInside">
		</div>
	</div>-->
</div>

<div id="mySidenavMail" class="sidenavMail">
	<div style="width: 100%; margin-bottom: -25px; overflow:hidden;">
		<div style="width: 80%; float: left;">
			<p style="background-color: #BAB1BA; height: 30px; margin-left: 10px;">Envio de Correo Manual</span></p>
		</div>
		<div style="width: 17%; float: left;">
			<a href="javascript:void(0)" class="closebtn" onclick="closeNavMail()">&times;</a>
		</div>
	</div>
	<div class="row">
	<div class="seven columns">
		<?php echo form_open('c_sendNewEmail/sendEmail', array('id' => 'form'));?>
			<fieldset class="fieldset" style="margin-left: 10px;">
				<legend>Datos basicos</legend>
				<label for="desde">Desde (Alias): </label>
				<input type="text" name="desde_show" id="mail_desde_show" value="" readonly="readonly"/>
				<input type="hidden" name="desde" id="mail_desde"  value="" readonly="readonly" />
				<input type="hidden" name="desde_nom" id="mail_desde_nom" value="" readonly="readonly" />
				<label for="para">*Para: </label>
				<textarea name="para" id="mail_para" value="" rows="3" cols="25"></textarea>
				<label for="copia">Copia: </label>
				<textarea name="copia" id="mail_copia" value="" rows="3" cols="50"></textarea>
				<label for="asunto">Asunto: </label>
				<textarea id="mail_asunto" name="asunto" rows="2" cols="20"></textarea>

				<label for="mensaje_mda">Mensaje para Mesa de Ayuda: </label>
				<textarea id="mail_mensaje_mda" name="mensaje_mda" rows="3" cols="3">Señores MDA, favor generar folio y contactar telefonicamente al turno de área responsable, ya que degrada servicio</textarea>
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="severidad">Severidad: </label>
						<input type="text" id="mail_severidad" name="severidad"  id="severidad" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="pais">País: </label>
						<input type="text" id="mail_pais" name="pais" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="negocio">Negocio: </label>
						<input type="text" id="mail_negocio" name="negocio"  value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="servicio">Servicio Afectados: </label>
						<textarea id="mail_servicios" name="servicio" rows="3" cols="50"></textarea>
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="area">Área responsable: </label>
						<input type="text" id="mail_area" name="area" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="tipo_disp">Tipo de Dispositivo: </label>
						<input type="text" id="mail_tipo_disp" name="tipo_disp" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="ubicacion">Ubicación del Dispositivo: </label>
						<input type="text" id="mail_ubicacion"  name="ubicacion" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="nombre">Nombre dispositivo: </label>
						<input type="text" id="mail_nombre" name="nombre" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="ip">IP(/puerta):</label>
						<input type="text" id="mail_ip" name="ip" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="fecha">Fecha/Hora: </label>
						<input type="text" id="mail_fecha" name="fecha" value="" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="mensaje">Descripción de la alerta:</label>
						<textarea id="mail_mensaje" name="mensaje" rows="10" cols="50"></textarea>
					</div>
				</div>
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				
				<label for="obs">Observaciones: </label>
				<textarea name="obs" id="obs" value="" rows="3" cols="50"></textarea>
				<input type="hidden" id="mail_tipo_aler" name="mail_tipo_aler" value="" />
				<input type="hidden" id="mail_group_id" name="group_id" value="" />
				<input type="hidden" id="mail_fechactual" name="fechactual" value="" />
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				<input type="submit" value="Enviar" id="envioSidenavMail"/>
				<input type="button" id="noenvioSidenavMail" value="Cancelar" />
			</fieldset>
			<div id="se-pre-con" class="se-pre-con"></div>
		<?php echo form_close();?>
	</div>
	</div>
</div>

<div id="main">
<div id="tabs" class="row">
	<ul>
		<li id="0"><a href="#row_header_v1" >Vista detalle</a></li>
		<li id="1"><a href="#row_header_v2" >Vista servidor</a></li>
		<li id="2"><a href="#row_header_v3" >Vista País/Negocio/Sistemas</a></li>
	</ul>

<div id="row_header_v1" class="container" style="margin-top:0px;">
	<!-- Cuerpo de gestor-->
	<div class="row">
		<!-- Cuerpo tabla de eventos -->
		<div class="twelve columns">
		
			<!-- Seccion cambiar estado de eventos -->
			<?php if ($this->session->userdata('tipo') == 3): ?>
				<div class="twelve columns"style="margin: 10px 0;"></div>
			<?php else: ?>
				<div class="row">
				<div class="twelve columns" style="margin: 10px 0;">
					<div class="six columns" id="div_folio_manual" style="display: none;">
						<span class="filtro_titulo" style="padding: 8px;">Folio manual:</span>
						<input class="filtro filtro_texto" name="texto" id="texto_folio_manual" type="text" style="padding: 5px; margin-top: 6px;" value="" />
						<input type="submit" value="Enviar" name="folio_enviar" id="folio_manual" style="float: left;">
					</div>
					<div class="six columns">
						<input class="right" type="submit" value="Mover Eventos" id="subCambioEstado" disabled="disabled" style="margin-top: 0;"/>
						<select class="right" name="selCambioEstado" id="selCambioEstado" >
							<option value="">-----</option>
						</select>
						<div class="right" style="margin-top:8px;"><span id="cant_select">0</span> Eventos <span id="nom_select"></span> seleccionados, mover a Estado </div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<!-- Seccion tabla de eventos -->
			<div class="row">
				<div class="twelve columns">
					<table id="eventos" class="cell-border" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><input type="checkbox" name="checkall" id="checkall"></th>
								<th>SEVERIDAD</th>
								<th>CORREO</th>
								<th>ESTADO</th>
								<th>FOLIO MDA</th>
								<th>FECHA</th>
								<th>TIPO ALERTA</th>
								<th>NOMBRE</th>
								<th>Ac.</th>
								<th>IP</th>
								<th>ÁREA SOPORTE</th>
								<th>UBICACIÓN DEL ELEMENTO</th>
								<th>DESCRIPCIÓN DEL ELEMENTO</th>
								<th>SERVICIO</th>
								<th>NEGOCIO</th>
								<th>PAIS</th>
								<th>HERRAMIENTA</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	
<div id="row_header_v2" class="container" style="margin-top:0px;">
	<div class="row">
		<!-- Cuerpo tabla de eventos -->
		<div class="twelve columns">
			<!-- Seccion tabla de eventos -->
			<div class="row">
				<table id="eventos_v2" class="cell-border" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>SEVERIDAD</th>
							<th>NOMBRE</th>
							<th>IP</th>
							<th>PAIS</th>
							<th>NEGOCIO</th>
							<th>CANTIDAD</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
	
<div id="row_header_v3" class="container" style="margin-top:0px;">
	<div class="row">
		<!-- Cuerpo tabla de eventos -->
		<div class="twelve columns">
			<!-- Seccion tabla de eventos -->
			<div class="row">
				<table id="eventos_v3" class="cell-border" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>SEVERIDAD</th>
							<th>PAIS</th>
							<th>NEGOCIO</th>
							<th>SISTEMA</th>
							<th>CANTIDAD</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

</div>
</div>
	<!--SECCION DE DIALOGS-->
	<div id="dialog_mover" title="Cambio de Estado" >
		<form>
			<fieldset>				
				<p>Detalle de Eventos</p>
				<table id="tMover" >
					<thead>
						<tr>
							<td width="200px">Fecha</td>
							<td width="100">Severidad</td>
							<td width="300">Nombre</td>
							<td width="200px">Tipo de Alerta</td>
						</tr>
					</thead>
					<tbody style="font-size:10px"></tbody>
				</table>
				<br/>
				<p>Información adicional</p>
				<table style="width:100%; height: 100%;">
					<tr>
						<td>Fecha actual:</td>
						<td name="enproc_sFecha" id="enproc_sFecha"></td>
					</tr>
					<tr style="height: 15px;">
					</tr>
					<tr>
						<td>*Comentario:</td>
						<td><textarea name="enproc_sComent" id="enproc_sComent" rows="4" cols="50" ></textarea></td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
	
	<div id="dialog_otro">
		<form>
			<fieldset>
				<table>
					<tr>
						<td>*Motivo:</td>
						<td><textarea name="otro_sComent" id="otro_sComent"></textarea></td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
	
	<!--<div id="dialog-confirm">
		<span style="float:left; margin:0 7px 20px 0;">Esta a punto de cambiar a estado:</span><span id="state" style="color: red"></span>
	</div>-->
	
	<div id="dialog-confirmarProcesado">
		<p>¿Está seguro de realizar la siguiente acción?</p>
	</div>
	
	<div id="dialog-procesado" >
		<p></p>
	</div>
	
	<div id="dialog-ticket" title="Creacion de Ticket">
		<p>Ticket creado correctamente</p>
	</div>
	
	<div id="dialog-cturno">
	</div>
	
	<div id="dialog_bolconf" title="Creacion de Ticket en sistema AR" >
		<img src="../../images/info-icon.png" style="float:left; "/>
		<p id="txtcat"></p>
		<p>¿Desea continuar con la acción?</p>
	</div>
	
	<div id="dialog_aticket" title="Paso a vista Tickets" >
		<p>¿Desea pasar a la vista de ticket?</p>
	</div>
	
	<div id="div_tablexport" style="display:none;">
		<table id="tablexport"></table>
	</div>
	
<!--FIN SECCION DE DIALOGS-->
	<script type="text/javascript">
		$('#displayCorreoSeg').click(function(){
			window.open('../c_seguimientoEmail', '', 'width=1300, height=600, location=no');
		});
	</script>
</body>
</html>