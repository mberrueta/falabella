<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Gestor de Eventos</title>
	<!--link rel="shortcut icon" href="<?php echo base_url(); ?>images/entelpcs.ico" /-->
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>css/layout.css" type="text/css" media="screen" /> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/gde_entel.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
	<div id="contenedor" class="width_3_quarter">
		<div id="form_login" class="caja_login" style="width: 350px; margin-top: 10%;" > 
			<h1 id='form_head'>Adessa - Gestor de Eventos</h1><br/>
			<div id="form_input">
			<!--<span style="color:#FF0000; font-weight:bold;">TRABAJANDO, FAVOR NO ACCEDER AL GESTOR</span>-->
			<div class="<?php echo $clase_mensaje; ?>">
				<p><?php echo $mensaje?></p>
			</div>
			<?php
				echo form_open('c_login/autenticar');

				// Show Name Field in View Page
				$data= array(
					'name' => 'username',
					'placeholder' => 'User Name',
					'class' => 'input_box'
				);
				echo form_input($data);

				echo "<div class=\"error_login\">".form_error('username')."</div>";

				// Show Password Field in View Page
				$data= array(
					'name' => 'password',
					'placeholder' => 'Password',
					'class' => 'input_box'
				);
				echo form_password($data);

				echo "<div class=\"error_login\">".form_error('password')."</div>";
				?>
				
			</div>
			<div id="form_button">
				<?php
					// Show Update Field in View Page
					$data = array(
					'type' => 'submit',
					'value'=> 'Entrar',
					'class'=> 'submit_login'
					);
					echo form_submit($data); 
				?>
			</div>

			<?php echo form_close();?>
			<p style="text-align:left; padding: 7px;"></p>
		</div>
		<div class="form-links">
          <!--<a href="http://www.kudaw.com">www.kudaw.com</a>--></br>v3.1 build 20172701
        </div>
	</div>

</body>
</html>