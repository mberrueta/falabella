<html>
<head>
	<title>Asociacion de Operadores</title>
	<base href="<?php echo base_url(); ?>" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.7.3.custom.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/gde.css" type="text/css" media="screen" />
</head>

<?php $this->load->view('supervisor/lateral'); ?>
 
<body>

	<div id="clientes" class="ui-widget">
	
		<?php
		if($men != ''){
			$mensaje = explode('|',$men);
			echo $mensaje[1];
		}
		?>
		
		<?php echo form_open('supervisor/c_crear_cliente/asociar_cliente');
		if($operadores['stt']){
			echo '<table id="operadores">';
			echo '<thead>';
				echo '<tr class="ui-widget-header ">';
				echo '<th>Operador</th>';
				echo '<th>Backup</th>';
				echo '<th>Cambio de Turno 1</th>';
				echo '<th>Cambio de Turno 2</th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			echo '<tr>';
			echo '<td><select id="selectOperador" name="operadores" size="10">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select id="selectBackup" name="backup" size="10">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select id="selectCT1" name="ct1" size="10">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select id="selectCT2" name="ct2" size="10">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '</tr>';
			echo '</tbody>';
			echo '<table>';
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		echo form_hidden('cNombre2', $nombre);
		echo form_hidden('cCriticidad2', $criticidad);

		echo form_submit('asociar','Asociar Cliente');
		echo form_close();
		
		?>
	</div>
 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/asociar_cliente.js"></script>

</body>
</html>