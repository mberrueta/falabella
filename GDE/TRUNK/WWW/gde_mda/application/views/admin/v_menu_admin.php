<?php $this->load->view('admin/header'); ?>
<?php $this->load->view('admin/lateral'); ?>

		<section id="main" class="column" style="width:960px;height:700px; margin-top:5%;">
			<article class="module width_full" style="margin:auto">
				<div class="module_content" >
					
					<span>Esta es la vista de administración. Favor seleccione una de las opciones del menú lateral:</span>
					</br></br>
					</br></br>
					<span>- Mantenedor de Usuarios: Proporciona las herramientas para administrar los usuarios del sistema.-</span>
					</br>
					<span>- Mantenedor de Correos: Define áreas responsables y las lista de distribucion de los correos en el sistema.-</span>
					</br>
					<span>- Mantenedor de Categorias: Define las variables asociadas a integración con la mesa de ayuda.-</span>
					</br>
					<span>- Mantenedor de Perfiles: Define los permisos sobre los eventos que puedan ver los usuarios.-</span>
				</div>		
			</article>
			
		</section>
		<div style="clear:both;"></div>
	</div>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script>
	</script>
</body>
</html>
