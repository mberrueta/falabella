#!/usr/bin/python

#Tunneling PRUEBAS Kudaw
#author: jaraneda
#
#Realiza un puente entre nuestro servidor y el servidor que llega a los nodos para ejecutar un comando remoto via ssh
#
#@params: argv[1] = COMANDO (que se ejecutara de forma remota)
#         argv[2] = HOST (el cual actuara de puente)
#         argv[3] = Nombre del Nodo   (a la cual hay que realizar la prueba)
#@return: Salida standard (resultado de ping

import telnetlib
import sys

#192.168.249.26

result = []
disc   = {}
tlntrs = ''

USER  = 'gde'
PASS  = '12gde3'
'''
HOST  = '192.168.249.8'
NODO  = '10.221.100.70'
CMD   = 'ping'
'''
CMD  = sys.argv[1]
HOST = sys.argv[2]
NODO = sys.argv[3]
tn    = None

try:
    tn = telnetlib.Telnet(HOST)
    welcome = tn.read_until('login: ',5)
    CMDpv = ''
    if "Microsoft" in welcome:
        PRMP = '\gde>'
        BLIN = '\r\n'
        if "ping" in CMD:
            CMDpv = 'ping '+NODO+' -n 4'
        else:
            CMDpv = 'tracert -w 5 '+NODO
    elif "HP-UX" in welcome:
        PRMP = '$ '
        BLIN = '\n'
        if "ping" in CMD:
            CMDpv = 'ping '+NODO+' -n 4'
        else:
            CMDpv = 'traceroute -w 5 '+NODO
    else:
        PRMP = '$ '
        BLIN = '\n'
        if "ping" in CMD:
            CMDpv = 'ping '+NODO+' -c 4'
        else:
            CMDpv = 'traceroute -w 5 '+NODO
    
    tn.write(USER+BLIN)
    tn.read_until('assword: ',5)
    tn.write(PASS+BLIN)
    tn.read_until(PRMP,5)
    tn.write(CMDpv+BLIN)
    tlntrs += tn.read_until(PRMP, timeout=15)
    tn.write('exit'+BLIN)
    tn.close()

except:
    import traceback
    if tn:
        tn.close()
    stack =  traceback.format_exc()
    tlntrs = 'Telnet rechazado por servidor '+HOST+'<br />Problema presentado: '+str(stack)+'<br />Favor comunicar al Administrador.'

print tlntrs