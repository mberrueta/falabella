var readUrl     = 'c_crud_categorias/read',
	createUrl   = 'c_crud_categorias/create_category',
    updateUrl   = 'c_crud_categorias/edit',
    delUrl      = 'c_crud_categorias/delete',
	getDataCategory = 'c_crud_categorias/getCategory';
	// getSupportMail = 'c_crud_categorias/mailSupport';
	// urlUpdateSupportMail = 'c_crud_categorias/updateMailSupport';	

$( function() {
	$( '#msgDialog' ).dialog({
        autoOpen: false,    
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });

	/*Al cargar la vista se deben cargar los datos existentes*/
	
	leerDatos();
	$('#regis').show();
	/* --------------------------------------*/
	/* Inicio de funcion para checkbox */
	$("#checkall").live('change',function(){
		var chstatus = $(this).is(':checked');
		if(chstatus == true){
			$('#registros tbody tr td input[type=checkbox]:not(:checked)').attr("checked", "checked"); 
		}
		else{
			$('#registros tbody tr td input[type=checkbox]:checked').removeAttr("checked");  
		}
	});
	/*Fin de funcion para checkbox*/
	/* --------------------------------------*/
	
	
	/* Inicio para generar username y password automaticamente*/
	$('#nom_sup').keyup(function(e){
		var nombre = $(this).val();
		var apellido = $('#app_sup').val();
		var username = nombre.substring(0,3)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_sup").val(username);
		$("#pass_usu_sup").val(username);
	});
	
	$('#app_sup').keyup(function(e){
		var nombre = $('#nom_sup').val();
		var apellido = $(this).val();
		var username = nombre.substring(0,3)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_sup").val(username);
		$("#pass_usu_sup").val(username);
	});
	/* Fin para generar username y password automaticamente */
	/* --------------------------------------*/
	/* Inicio dialogo confirmar borrar operador*/
	$('#delConfDialog').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
			$('#delConfDialog').attr("data-delete","");
			$( this ).dialog( 'close' );
			},
			'Yes': function() {
				var id = $('#delConfDialog').attr("data-delete");
				$( '#ajaxLoading' ).fadeIn( 'slow' );
				$( this ).dialog( 'close' );
				$.ajax({
					type: 'POST',
					url: delUrl,
					data: {'id':id} ,
					error: function(rhx,var2,var3){
						alert("Error al eliminar categoria");
					},
					success: function( response ) {
						if(response == '0'){
							leerDatos();
						}
						else if(response == '1'){
							$('#msgDialog').html('Ocurrio un error eliminado el supervisor. Intentelo nuevamente.')
							$('#msgDialog').dialog( "option", "title", 'Error').dialog('open');
						}
						else if(response == '2'){
							$('#msgDialog').html('El supervisor tiene operadores asignados. Antes de eliminar debe reasignar los operadores a otro supervisor.')
							$('#msgDialog').dialog( "option", "title", 'No se pudo eliminar').dialog('open');
						}
						$( '#ajaxLoading' ).fadeOut( 'slow' );
						
					}
				});
			}
		}
	});
	/* Fin dialogo confirmar borrar patron_mail*/
	/* --------------------------------------*/
	/* Inicio dialogo para crear patron_mail */
	$('#crear_categoria').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Crear nuevo patrón mail',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$( '#crear_categoria input' ).val( '' );
			// $('#datos_nuevo_patron_mail').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Crear patrón': function() {
				// if($('#datos_nuevo_patron_mail').valid()){
				$( '#ajaxLoading' ).fadeIn( 'slow' )
				var query = $('#datos_nuevo_patron_mail').serializeArray();
				
				$.ajax({
					type: 'POST',
					url: createUrl,
					data: query,
					success: function( response ) {
						if(response == 0){
							leerDatos();
							$('#crear_categoria').dialog( 'close' );
							$( '#ajaxLoading' ).fadeOut( 'slow' );
							//Limpiar los campos de texto
							$( '#crear_supervisor input' ).val( '' );
						}
						else if(response == 'failMail'){
							$( '#ajaxLoading' ).fadeOut( 'slow' );
							$('#msgDialog').html('Formato de mail incorrecto.')
							$('#msgDialog').dialog('open');
						}
						else if(response=='fail_only'){
							$( '#ajaxLoading' ).fadeOut( 'slow' );
							$('#msgDialog').html('Ocurrio un error. organización ya creada')
							$('#msgDialog').dialog('open');
						}
						
						else{
							$( '#ajaxLoading' ).fadeOut( 'slow' );
							$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
							$('#msgDialog').dialog('open');
						}
					}
				});
				
				// }
				// else{
				// 	alert('Los datos estan con error');
				// }
			}
		}
	});
	/* Fin dialogo para crear correo */
	
	/* Inicio dialogo para editar correo */
	$('#actualizar_categoria').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Editar correo',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$('#actualizar_categoria input').val( '' );
			// $('#datos_actualizar_categoria').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Guardar': function() {
				if($('#datos_actualizar_categoria').valid()){
					$( '#ajaxLoading' ).fadeIn( 'slow' )
					var variables = $("#datos_actualizar_categoria").serialize();
					$.ajax({
						type: 'POST',
						url: updateUrl,
						data: variables,
						success: function( response ) {
							if(response == 0){
								leerDatos();
								$('#actualizar_categoria').dialog( 'close' );
								//Limpiar los campos de texto
								$( '#actualizar_categoria input' ).val( '' );
							}
							else if(response == 'failMail'){
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								$('#msgDialog').html('Formato de mail incorrecto.')
								$('#msgDialog').dialog('open');
							}
							else if(response=='fail_only'){
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								$('#msgDialog').html('Ocurrio un error. organización ya creada')
								$('#msgDialog').dialog('open');
							}
							else{
								$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
								$('#msgDialog').dialog('open');
							}
							$( '#ajaxLoading' ).fadeOut( 'slow' );
						}
					});

				}
				else{
					alert('Los datos estan con error');
				}
			}
		}
	});
	/* Fin dialogo para editar correo */

	/* Inicio Binding para nuevo patron_mail */
	$('#buttonNuevo').click(function(){
		$('#crear_categoria').dialog( 'open' );
	});
	
	/* Inicio Binding para editar correo */
    $( '#registros' ).delegate( 'img.EditBtn', 'click', function() {
        
		//Captura del identificador del correo seleccionado
		id = $( this ).parents( 'tr' ).attr( "id" );
       
	   //Muestra el cuadro de carga de datos
	    $('#ajaxLoading' ).fadeIn( 'slow' );
		$('#datos_actualizar_categoria input').val("");
         
		//Inicio del ajax
		$.ajax({
            url: getDataCategory,
            dataType: 'json', 
			data: {'id' : id}, 

			//En caso de algun error se imprimira una alerta indicando que hubo un error, se puede hacer seguimiento via consola
			error: function(xhr, textStatus, errorThrown){
				alert("Error al ejecutar la función.")
			},
            success: function( response ) {
                
				//Seteo de los campos en el formulario de edicion
				$('#actualizar_categoria #pais')      .val( response.pais );
                $('#actualizar_categoria #encargado') .val( response.encargado );
                $('#actualizar_categoria #app')	   .val( response.app );
                $('#actualizar_categoria #det')	   .val( response.detalle );
                $('#actualizar_categoria #id')				 .val( id );
				// $('#actualizar_correo #automatic')		 .attr('checked', (response.automatic && response.automatic == '1' ) ? true : false);
				// $('#actualizar_correo #minor')			 .attr('checked', (response.minor && response.minor == '1' ) ? true : false);
				// $('#actualizar_correo #critical_mayor')	 .attr('checked', (response.critical_mayor && response.critical_mayor == '1' ) ? true : false);
				
				$('#ajaxLoading').fadeOut( 'slow' );
                $('#actualizar_categoria').dialog( 'open' );
            }
        });
        
        return false;
    }); 
	
	
	/* Eliminar supervisor desde tarro de basura*/
    $( '#registros' ).delegate( 'img.DeleteBtn', 'click', function() {
		id = $( this ).parents( 'tr' ).attr( "id" ); 
		var texto = '&iquest;Confirma que desea eliminar el registro ?';
		$('#delConfDialog').html(texto);
		$('#delConfDialog').attr("data-delete",id);
		$('#delConfDialog').dialog('open');
    }); 

	/*Eliminar todos los supervisores seleccionados desde checkbox*/
	$('#buttonDel').click(function(){
		nameUser = '';
		$('#delConfDialog').dialog('option', 'nameUser', nameUser).dialog('open');
	});
	
}); //end document ready

//Función que actualiza el mail de mesa de ayuda
// function updateSupportMail(e){
// 	var mail = $("input#principalMail").val()
	
// 	if(confirm("¿Desea cambiar el mail de mesa de ayuda?"))
// 		$.ajax({
// 			url: urlUpdateSupportMail,
// 			type: 'POST',
// 			data: { 'mail' : mail},
// 			error: function(va1,var2,var3){ alert("Error al actualizar mail mesa de ayuda.")},
// 			success: function(response){

// 				if(response== "0")
// 				{
// 					$( '#ajaxLoading' ).fadeOut( 'slow' );
// 					$('#msgDialog').html("Actualización realizada con éxito.")
// 					$('#msgDialog').dialog('open');
// 				}
// 				else if (response == "1")
// 				{
// 					$( '#ajaxLoading' ).fadeOut( 'slow' );
// 					$('#msgDialog').html("Error al actualizar mail mesa de ayuda.")
// 					$('#msgDialog').dialog('open');
// 				}
				
// 			}
// 		})
// }

$(window).resize(function(){
	header_table();
})

function header_table(){
	$("div[data-header=pais]").width($("th#pais").width())
	$("div[data-header=pais]").height($("th#pais").height())
	$("div[data-header=enc]").width($("th#enc").width())
	$("div[data-header=enc]").height($("th#enc").height())
	$("div[data-header=app]").width($("th#app").width())
	$("div[data-header=app]").height($("th#app").height())
	$("div[data-header=det]").width($("th#det").width())
	$("div[data-header=det]").height($("th#det").height())
	$("div[data-header=editar]").width($("th#editar").width())
	$("div[data-header=editar]").height($("th#editar").height())
	$("div[data-header=eliminar]").width($("th#eliminar").width())
	$("div[data-header=eliminar]").height($("th#eliminar").height())
	$("div[data-header]").css("text-align","center")
	$("div[data-header]").css("padding","3.2px")
	$("div[data-header]").css("border"," 1px solid #ddd")
    $("div[data-header]").css("font-weight"," 900")
    $("div[data-header]").css("color"," white")
    $("div[data-header]").css("background","#269abc")
}

function leerDatos(){
    //Mostrar loading
    $( '#ajaxLoading' ).fadeIn( 'slow' );
    $.ajax({
        url: readUrl,
        dataType: 'json',
		error: function(e){alert(e)},
        success: function( response ) {
			var salida = '';
			for( var i in response ) {
				salida += "<tr id=\""+response[ i ].id+"\"><td>"+ response[ i ].encargado+"</td>";
				salida += "<td>"+ response[ i ].pais+"</td>";
				salida += "<td>"+ response[ i ].app+"</td>";
				salida += "<td>"+ response[ i ].detalle+"</td>";
				// salida += "<td>"+ ( response[ i ].critical_mayor == '1' ? "Permitido" : "Deshabilitado" )+"</td>";
				// salida += "<td>"+ ( response[ i ].minor == '1' ? "Permitido" : "Deshabilitado" ) +"</td>";
				// salida += "<td>"+ ( response[ i ].automatic == '1' ? "Permitido" : "Deshabilitado" ) +"</td>";
				salida += "<td><img src=\"../../images/edit-icon.png\" class=\"EditBtn\"></td>";
				salida += "<td><img src=\"../../images/delete-icon.png\" class=\"DeleteBtn\"  data-id=\""+response[ i ].id+"\"></td>";
				salida += "</tr>";
			}
			//Limpiar filas antiguas
            $( '#registros tbody' ).html( '' );
            //Mostrar los registros
			$( "#registros tbody" ).html(salida);
            //Esconder el loading
            $( '#ajaxLoading' ).fadeOut( 'slow' );
			//Muestra la tabla
			$("#registros").fadeIn()
			//Genera cabecera estática
			header_table();
        }
    });


	$('#search').keyup(function() {
		//Cuadro de búsqueda 
		var $rows = $('#registros tbody tr');	
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

		$rows.show().filter(function() {
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			return !~text.indexOf(val);
		}).hide();
	});
}
