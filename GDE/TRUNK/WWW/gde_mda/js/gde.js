	$(document).ready(function(){
		$('#eventos').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength" : 25
		});
		$('.column').equalHeight();
	});

	function llenarCliente(cliente){
		$('.dataTables_filter input').val(cliente).keyup();
	return;}

	function llenarTipo(tipo){
		$('.dataTables_filter input').val(tipo).keyup();
	return;}

	/*
	* Entrega solo los valores unicos de un array, en un nuevo array
	*/
	
	var unique = function(origArr) {  
		var newArr = [],  
			origLen = origArr.length,  
			found,  
			x, y;  
	  
		for ( x = 0; x < origLen; x++ ) {  
			found = undefined;  
			for ( y = 0; y < newArr.length; y++ ) {  
				if ( origArr[x] === newArr[y] ) {  
				  found = true;  
				  break;  
				}  
			}  
			if ( !found) newArr.push( origArr[x] );  
		}  
	   return newArr;  
	};  	

	/*
	* Generación de vista pop-up para Ticket
	*/
	
	$(function() {
		$('#dialog').dialog({
						bgiframe: true,
						autoOpen: false,
						resizable: false,
						width:600,
						modal: true,
						overlay: {
							backgroundColor: '#000',
							opacity: 0.5
						},
						buttons: {
							'Si': function() {
								var ids4 = "";
								$("input:checked").each(function() {
									ids4 += $(this).attr('id')+"|";
								});
								var path3 = "<?php echo base_url().'index.php/operador/c_eventos_activos/procesar_eventos'; ?>";
								$.post( path3, { 'event': ids4 }, function( ) {
									location.reload();
								});
								return true;
							},

						}
					});                    
					 
		$('#boton_ticket').click( function(){
			var ids3 = "";
			var cliente = "";
			var date = new Date();
			var flag = new Array();
			var i = 0;
			
			$('#t_hora2').val(date);
			$('#t_hora3').val("<?php echo $this->session->userdata('nombre');?>");
			
			 $("input:checked").each(function() {
					ids3 += $(this).attr('id')+"|";
					flag[i] = $(this).parent().parent().find("td:eq(3)").text();
					i = i + 1; 
			   });
			
			
			
			var path2 = "<?php echo base_url().'index.php/operador/c_eventos_activos/consultar_evento'; ?>";
			alert(path2);
			$.post( path2, { 'eventos': ids3 }, function( data ) {
				$('#textfield').val(data); 
			});
			
			$('#t_hora').val(unique(flag)); 
			$('#dialog').dialog('open');                   
		});
		
	});
	
	//<!--BOTON PARA EVENTO OMITIR-->
	var ids2 = "";
	$('#boton_omitir').click(function(){
		alert('bla');
		 $("input:checked").each(function() {
				ids2 += $(this).attr('id')+"|";
				//alert($(this).attr('id'));
           });
		//alert(ids2);
		var path = "index.php/operador/c_eventos_activos/desactivar_evento";
		$.post( path, { 'ids2': ids2 }, function( data ) {
			alert(data);
			location.reload();
		});
	});
	
	/*
	* Generación de pruebas a eventos en base a un nodo seleccionado
	*
	
	//PARA LLENAR INPUT CON NOMBRE DE NODO
	$('table td').click(function(){
		var nodo = $(this).parent().find("td:eq(4)").text();
		if(nodo != "Nombre nodo"){
			$('#nodo').val(nodo);
		}   

	});

	//PARA PRUEBA PING
	$('#button_ping').click(function(){
		var nodo = $('#nodo').val();
		if(nodo != ""){
			$('#button_ping').html('<img width="80" height="10" src="<?php echo base_url(); ?>images/loading_bar.gif">').attr('disabled','disabled');

			var path = "<?php echo base_url().'index.php/operador/c_eventos_activos/ping'; ?>";
			$.post( path, { 'nodo': nodo }, function( data ) {
				$('#button_ping').html('Prueba PING').removeAttr('disabled');
				var w = window.open('', '', 'width=550,height=200,scrollbars');
				w.document.write(data);
				w.document.close();
			});
		}
	});
	*/