var readUrl   = 'c_crud_supervisor/read',
	createUrl = 'c_crud_supervisor/create',
    updateUrl = 'c_crud_supervisor/edit',
    delUrl    = 'c_crud_supervisor/delete',
	getUser   = 'c_crud_supervisor/getbyUser';
	
$( function() {
	$( '#msgDialog' ).dialog({
        autoOpen: false,    
        buttons: {
            'Ok': function() {
                $( this ).dialog( 'close' );
            }
        }
    });

	/*Al cargar la vista se deben cargar los datos existentes*/
	leerDatos();
	$('#registros').show();
	/* --------------------------------------*/
	/* Inicio de funcion para checkbox */
	$("#checkall").live('change',function(){
		var chstatus = $(this).is(':checked');
		if(chstatus == true){
			$('#registros tbody tr td input[type=checkbox]:not(:checked)').attr("checked", "checked"); 
		}
		else{
			$('#registros tbody tr td input[type=checkbox]:checked').removeAttr("checked");  
		}
	});
	/*Fin de funcion para checkbox*/
	/* --------------------------------------*/
	
	
	/* Inicio para generar username y password automaticamente*/
	$('#nom_sup').keyup(function(e){
		var nombre = $(this).val();
		var apellido = $('#app_sup').val();
		var username = nombre.substring(0,3)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_sup").val(username);
		$("#pass_usu_sup").val(username);
	});
	
	$('#app_sup').keyup(function(e){
		var nombre = $('#nom_sup').val();
		var apellido = $(this).val();
		var username = nombre.substring(0,3)+''+apellido;
		username = username.toLowerCase();
		$("#nom_usu_sup").val(username);
		$("#pass_usu_sup").val(username);
	});
	/* Fin para generar username y password automaticamente */
	/* --------------------------------------*/
	/* Inicio dialogo confirmar borrar operador*/
	$('#delConfDialog').dialog({
		nameUser: '',
		autoOpen: false,
		buttons: {
			'No': function() {
			$( this ).dialog( 'close' );
			},
			'Yes': function() {
				var supervisores_eliminar = new Array();
				
				if(nameUser == ''){ //Si nameUser es vacio significa que el dialog se abrio desde los checkbox.
					//Recuperar values de los supervisores con check
					$('#registros tbody tr td input[type=checkbox]:checked').each(function()
					{
						supervisores_eliminar.push($(this).val());
					});
					//alert(supervisores_eliminar);
				}
				else{
					supervisores_eliminar.push(nameUser);
					//alert(supervisores_eliminar);
				}
				var toBeSent = {usuarios: supervisores_eliminar}; 
				$( '#ajaxLoading' ).fadeIn( 'slow' );
				$( this ).dialog( 'close' );
				$.ajax({
					type: 'POST',
					url: delUrl,
					data: toBeSent,
					success: function( response ) {
						if(response == '0'){
							leerDatos();
						}
						else if(response == '1'){
							$('#msgDialog').html('Ocurrio un error eliminado el supervisor. Intentelo nuevamente.')
							$('#msgDialog').dialog( "option", "title", 'Error').dialog('open');
						}
						else if(response == '2'){
							$('#msgDialog').html('El supervisor tiene operadores asignados. Antes de eliminar debe reasignar los operadores a otro supervisor.')
							$('#msgDialog').dialog( "option", "title", 'No se pudo eliminar').dialog('open');
						}
						$( '#ajaxLoading' ).fadeOut( 'slow' );
						
					}
				});
			}
		}
	});
	/* Fin dialogo confirmar borrar supervisor*/
	/* --------------------------------------*/
	/* Inicio dialogo para crear supervisor */
	$('#crear_supervisor').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Crear nuevo supervisor',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$( '#crear_supervisor input' ).val( '' );
			$('#datos_nuevo_supervisor').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Crear Supervisor': function() {
				if($('#datos_nuevo_supervisor').valid()){
					$( '#ajaxLoading' ).fadeIn( 'slow' )
					var query = $('#datos_nuevo_supervisor').serializeArray();
					$.ajax({
						type: 'POST',
						url: createUrl,
						data: query,
						success: function( response ) {
							if(response == 0){
								leerDatos();
								$('#crear_supervisor').dialog( 'close' );
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								//Limpiar los campos de texto
								$( '#crear_supervisor input' ).val( '' );
							}
							else{
								$( '#ajaxLoading' ).fadeOut( 'slow' );
								$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
								$('#msgDialog').dialog('open');
							}
						}
					});
					
				}
				else{
					alert('Los datos estan con error');
				}
			}
		}
	});
	/* Fin dialogo para crear supervisor */
	
	/* Inicio dialogo para editar supervisor */
	$('#actualizar_supervisor').dialog({
		autoOpen: false,
		height: 480,
		width: 640,
		modal: true,
		title: 'Editar supervisor',
		beforeClose: function(event, ui) {
			//Limpiar los campos de texto
			$( '#actualizar_supervisor input' ).val( '' );
			$('#datos_actualizar_supervisor').validate().resetForm();
		},
		buttons: {
			'No': function() {
				$( this ).dialog( 'close' );
			},
			'Guardar': function() {
				if($('#datos_actualizar_supervisor').valid()){
					$( '#ajaxLoading' ).fadeIn( 'slow' )
					var query = $('#datos_actualizar_supervisor').serializeArray();
					var nom_usu = {name: "edit_nom_usu_sup", value: $('#edit_nom_usu_sup').val() };
					query.push(nom_usu);
					$.ajax({
						type: 'POST',
						url: updateUrl,
						data: query,
						success: function( response ) {
							if(response == 0){
								leerDatos();
								$('#actualizar_supervisor').dialog( 'close' );
								//Limpiar los campos de texto
								$( '#actualizar_supervisor input' ).val( '' );
							}
							else{
								$('#msgDialog').html('Ocurrio un error. Vuelva a intentarlo')
								$('#msgDialog').dialog('open');
							}
							$( '#ajaxLoading' ).fadeOut( 'slow' );
						}
					});

				}
				else{
					alert('Los datos estan con error');
				}
			}
		}
	});
	/* Fin dialogo para editar supervisor */

	/* Inicio Binding para nuevo supervisor */
	$('#buttonNuevo').click(function(){
		$('#crear_supervisor').dialog( 'open' );
	});
	
	/* Inicio Binding para editar supervisor */
    $( '#registros' ).delegate( 'img.EditBtn', 'click', function() {
        nameUser = $( this ).parents( 'tr' ).attr( "id" );
        $( '#ajaxLoading' ).fadeIn( 'slow' );
		$('#datos_actualizar_supervisor input').val("");
        
        // $.ajax({
        //     url: getUser +'/' + nameUser,
        //     dataType: 'json',  
        //     success: function( response ) {
        //         $( '#edit_nom_sup' ).val( response.SUP_NOM );
        //         $( '#edit_app_sup' ).val( response.SUP_APP );
		// 		$( '#edit_apm_sup' ).val( response.SUP_APM );
		// 		$( '#edit_nom_usu_sup' ).val( response.USU_USER );
		// 		$( '#edit_email_usu_sup' ).val( response.USU_EMAIL );
		// 		$( '#edit_anexo_sup' ).val( response.SUP_ANE );
		// 		$( '#edit_tel_sup' ).val( response.SUP_TEL );
        //         $( '#ajaxLoading' ).fadeOut( 'slow' );
        //         $( '#actualizar_supervisor' ).dialog( 'open' );
        //     }
        // });
		$.ajax({
    //        url: getUser +'/' + nameUser,
            url: readUrl,
            dataType: 'json',  
            success: function( response ) {
				var cont = 0;
				var i = 0;
				response.forEach(function(e){ 
					if(e.USU_USER == nameUser) 
						i=cont;
					cont++;})

                $( '#edit_nom_sup' ).val( response[i].SUP_NOM );
                $( '#edit_app_sup' ).val( response[i].SUP_APP );
				$( '#edit_apm_sup' ).val( response[i].SUP_APM );
				$( '#edit_nom_usu_sup' ).val( response[i].USU_USER );
				$( '#edit_email_usu_sup' ).val( response[i].USU_EMAIL );
				$( '#edit_anexo_sup' ).val( response[i].SUP_ANE );
				$( '#edit_tel_sup' ).val( response[i].SUP_TEL );
				
                $( '#ajaxLoading' ).fadeOut( 'slow' );
                $( '#actualizar_supervisor' ).dialog( 'open' );
            }
        });
        
        return false;
    }); 
	
	
	/* Eliminar supervisor desde tarro de basura*/
    $( '#registros' ).delegate( 'img.DeleteBtn', 'click', function() {
		nameUser = $( this ).parents( 'tr' ).attr( "id" ); 
		var texto = '&iquest;Confirma que desea eliminar al supervisor '+nameUser+'?';
		$('#delConfDialog').html(texto);
		$('#delConfDialog').dialog('option', 'nameUser', nameUser).dialog('open');
    }); 

	/*Eliminar todos los supervisores seleccionados desde checkbox*/
	$('#buttonDel').click(function(){
		nameUser = '';
		$('#delConfDialog').dialog('option', 'nameUser', nameUser).dialog('open');
	});
	
}); //end document ready

function leerDatos(){
    //Mostrar loading
    $( '#ajaxLoading' ).fadeIn( 'slow' );
    
    $.ajax({
        url: readUrl,
        dataType: 'json',
        success: function( response ) {
			var salida = '';
			for( var i in response ) {
				salida += "<tr id=\""+ response[ i ].USU_USER+"\">";
				//salida += "<td><input type=\"checkbox\" name=\"checkelimina\" value=\""+response[ i ].USU_USER+"\"></td>";
				salida += "<td>"+ response[ i ].SUP_NOM+"</td>";
				salida += "<td>"+ response[ i ].SUP_APP+"</td>";
				salida += "<td>"+ response[ i ].SUP_APM+"</td> <!-- OPE_APM -->";
				salida += "<td>"+ response[ i ].USU_USER+"</td>";
				salida += "<td>"+ response[ i ].USU_EMAIL+"</td>";
				salida += "<td>"+ response[ i ].SUP_ANE+"</td> <!-- OPE_ANE -->";
				salida += "<td>"+ response[ i ].SUP_TEL+"</td> <!-- OPE_TEL -->";
				salida += "<td><img src=\"../../images/edit-icon.png\" class=\"EditBtn\"></td>";
				salida += "<td><img src=\"../../images/delete-icon.png\" class=\"DeleteBtn\"></td>";
				salida += "</tr>";
			}
			//Limpiar filas antiguas
            $( '#registros tbody' ).html( '' );
            //Mostrar los registros
			$( "#registros tbody" ).html(salida);
            //Esconder el loading
            $( '#ajaxLoading' ).fadeOut( 'slow' );
        }
    });
}
