<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_menu_analista extends CI_Controller {

	public function index($mensaje='')
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data['mensaje'] = $mensaje;
		$data['titulo_pagina'] = 'GED -&gt; Menu Analista';
		$data['iframe'] = 'http://localhost/ENTEL/handsontable/';
		$this->load->view('analista/v_menu_analista',$data);
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && ($this->session->userdata('tipo') == 4)){
			return true;
		}
		else{
			return false;
		}
	}
}

/* End of file c_menu_supervisor.php */
/* Location: ./application/controllers/supervisor/c_menu_supervisor.php */
