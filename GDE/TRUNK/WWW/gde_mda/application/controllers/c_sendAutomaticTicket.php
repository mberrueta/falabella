<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/c_sendAutomaticEmail.php");

class C_sendAutomaticTicket extends c_sendAutomaticEmail {
	
	public function index(){}
	
	private function validateTicket($ID)
	{
		$this->db->select('folio_mda');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id = '.$ID);
		$result = $this->db->get();
		return $result;
	}	
	
	private function validateSendMail($ID)
	{
		$sql = "SELECT grupo_evento_gde.send_mail,contacto_dispositivo_gde.automatic FROM `grupo_evento_gde` LEFT JOIN contacto_dispositivo_gde ON grupo_evento_gde.src_organization = contacto_dispositivo_gde.src_organization WHERE grupo_evento_gde.id=".$ID;
		$result = $this->db->query($sql);
		return $result;
	}
	
	// Insert de prueba a BD
	// INSERT INTO `evento_gde` (`severity`, `status`, `start_time`, `type`, `src_name`, `src_subname`, `src_ip`, `src_organization`, `src_category`, `src_subcategory`, `src_tool`, `dest_bunit`, `dest_service`, `dest_country`, `body`, `src_system`) VALUES
// ('CRITICAL', 'Activo', '2017-01-12 09:00:00', 'Disco Indisponible', 'prueba.kudaw.com', 'N/A', 'N/A', 'Kudaw', 'Alerta Prueba', 'N/A', 'Splunk', 'Kudaw', 'N/A', 'Chile', 'Esto es un mensaje de prueba para integracion con Mesa de ayuda', 'No catalogado|')

	public function createTicket($ID = false){
		
		$this->load->model('webservices');
		$this->load->model('evento');
		
		$flagTicket = false;
		
		$isManual = $this->input->post('isManual');
		
		if($isManual){
			$ID = $this->input->post('ID');
			
			$response = $this->validateTicket($ID);
			
			if ($response->row()->folio_mda != 0 && $response->row()->folio_mda != -1){
				$flagTicket = true;
			}
		}
		
		if($flagTicket){
			echo $this->evento->obtenerComentarios($ID);
		} else {
			
			$intentos     = 0;
			$rs           = -1;
			$mensaje      = "";
			
			//Realiza dos intentos mas para obtener el ticket
			while($intentos < 3){
				$retorno = $this->webservices->crearTicket($ID);
				$retorno = rtrim($retorno, "\n");
				$output  = explode(',',$retorno);
				
				if($output[0] == 0){
					if ($output[1]){
						$rs = $output[1];
						break;
					} else {
						$intentos = $intentos + 1;
					}
				} else {
					$intentos = $intentos + 1;
					if ($output[2]) $mensaje  = $output[2];
				}
			}
			
			if ($rs == -1 && $mensaje){
				$coment = 'ERROR en generación automática de folio (Detalle: '.$mensaje.'). Para creación manual haga click <button type="button" class="btn btn-success" id="crearTicketManual" >AQUI</button>';
				$this->evento->registrarComentario($ID, $coment, "gestor");
			} elseif ($rs == -1){
				$coment = 'ERROR en generación automática de folio (Detalle: Problemas con web services). Para creación manual haga click <button type="button" class="btn btn-success" id="crearTicketManual" >AQUI</button>';
				$this->evento->registrarComentario($ID, $coment, "gestor");
			} else {
				$coment = 'Folio automático generado correctamente (Folio N: '.$rs.')';
				$this->evento->registrarComentario($ID, $coment, "gestor");
			}
			
			$this->db->where('id', $ID);
			$this->db->update('grupo_evento_gde', array('folio_mda' => $rs));
			$this->db->flush_cache();
			
			if($isManual) {
				echo $this->evento->obtenerComentarios($ID);
			} else {
				// Envio de correo automatico -- Si aplica.-
				$response = $this->validateSendMail($ID);
				if ($response->row()->automatic == 1 && $response->row()->send_mail === NULL) {
					//echo "entre";
					$this->sendEmailbyID($ID);
				}
			}
		}
	}
	
	/*public function create(){
		
		//Obtener la lista de grupos de eventos que se generará un ticket
		$gruposEventos = $this->getGroupEvents();

		//Envío de tickets 
		foreach($gruposEventos->result() as $ticket)
		{
			$ID = escapeshellcmd($ticket->ID);
			// Version Adessa
			// system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /u01/home/app/splunkge/id.log &");
			
			// Version Desarrollo
			system("/opt/lampp/bin/php /opt/lampp/htdocs/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /opt/lampp/htdocs/gde_test/id.log &");
		}
		
	}*/
	
	public function updateTicket($ID, $grupoId, $status, $comentario, $usuario){
		
		$this->load->model('webservices');
		$this->load->model('evento');
		
		$retorno = $this->webservices->consultaIncidente($ID);
		$retorno = rtrim($retorno, "\n");
		$output  = explode(',',$retorno);
		
		if($output[0] == 'fail' or $output[0] == 'error'){
			// Problema con el Web Services.
			$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: Web Service con problemas.";
			$this->evento->registrarComentario($grupoId, $coment, $usuario);
		} elseif ($output[0] == 0){
			// Estado actual del ticket.-
			$statusActual = $output[1];
			
			// echo $statusActual;
			// echo $status;
			
			// Casos ENPROCESO
			if($status == "Trabajando" && ($statusActual == "Resuelto - Nivel 2" or $statusActual == "Resuelto - Primer Contacto")){
				
				$status = "Asignado";
				
				// Actualizar el ticket.
				$retorno = $this->webservices->actualizaEstadoIncidente($ID, $status, $comentario);
				$retorno = rtrim($retorno, "\n");
				$output  = explode(',',$retorno);
				
				if($output[0] == 'fail' or $output[0] == 'error'){
					// Problema con el Web Services.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: Web Service con problemas.";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} elseif ($output[0] == 0){
					// Ok.
					$coment = "Estado de Folio actualizado en Mesa de Ayuda (Nuevo estado: ".$status.")";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} else {
					// Problema con los datos.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: ".$output[1];
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				}
			}
			
			// Casos CERRADO
			if($status == "Resuelto - Nivel 2" && ($statusActual == "Asignado" or $statusActual == "Trabajando")){
				
				// Actualizar el ticket.
				$retorno = $this->webservices->actualizaEstadoIncidente($ID, $status, $comentario);
				$retorno = rtrim($retorno, "\n");
				$output  = explode(',',$retorno);
				
				if($output[0] == 'fail' or $output[0] == 'error'){
					// Problema con el Web Services.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: Web Service con problemas.";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} elseif ($output[0] == 0){
					// Ok.
					$coment = "Estado de Folio actualizado en Mesa de Ayuda (Nuevo estado: ".$status.")";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} else {
					// Problema con los datos.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: ".$output[1];
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				}
			}
			
			// Caso 3 - Activo/Asignado
			if($status == "Asignado" && ($statusActual == "Asignado" or $statusActual == "Trabajando")){
				$this->load->model('evento');
				$this->evento->cambiar_estado($grupoId, "En Proceso", $comentario, $usuario);
			}
		}
	}
	
	public function update(){
		// Usuario 
		$usuario = $this->session->userdata('username');
		
		//Obtener la lista de tickets a actualizar
		$tickets    = $this->input->post('aTicket');
		$grupoId    = $this->input->post('aGrupos');
		$estado     = $this->input->post('sEstado');
		$comentario = $this->input->post('sComentario');
		
		// Normalizacion de Estado GDE / MDA
		$estadoNorm     = "";
		
		if ($estado == "En Proceso"){
			$estadoNorm = "Trabajando";
		} else {
			$estadoNorm = "Resuelto - Nivel 2";
		}
		
		$arrayGROID = explode(',',$grupoId);
		
		$index = 0;
		foreach(explode(',',$tickets) as $ticket)
		{
			if($ticket != "Sin Ticket" && $ticket != "ERROR"){
			
				if(!$comentario) $comentario = "Sin comentarios asociados";
			
				$grupoId    = escapeshellcmd($arrayGROID[$index]);
				$ID         = escapeshellcmd($ticket);
				$status     = escapeshellcmd($estadoNorm);
				$comentario = escapeshellcmd($comentario);
				$usuario    = escapeshellcmd($usuario);
				
				$comentario = str_replace("!","\!",$comentario);
				
				system('/usr/local/bin/php /u01/home/app/splunkge/www/gde_mda/index.php c_sendAutomaticTicket updateTicket '.$ID.' '.$grupoId.' "'.$status.'" "'.$comentario.'" "'.$usuario.'" >> /u01/home/app/splunkge/www/gde_mda/application/logs/ws/id.log &');
			}
			
			$index++;
		}
		
	}
}


/* End of file C_sendAutomaticTicket.php */
/* Location: ./application/controllers/C_sendAutomaticTicket.php */
