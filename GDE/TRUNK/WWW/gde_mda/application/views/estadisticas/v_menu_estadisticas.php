<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" type="text/css" media="screen" />
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>css/layout.css" type="text/css" media="screen" /> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/gde_entel.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<title>Estadísticas Gestor de Eventos</title>
</head>
<body>

	<div id="contenedor" class="width_full">
	<header id="header">
		<hgroup>
			<h1 class="site_title">
			</h1>
			<h2 class="section_title">
			Gestor de Eventos
			<span><?php echo anchor('estadisticas/c_menu_estadisticas','Menú Estadísticas'); ?></span>
			</h2>
			<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
		</hgroup>
	</header>
	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $this->session->userdata('nombre'); ?></p>
		</div>
	</section>
	
	<section id="main" class="column" style="width: 100%;">
		<article class="module width_full">
			<header>
				<h3>Resumen estadístico</h3>
			</header>
			<div style="margin:15px;"> 
				<div id="tabs">
					<ul>
						<li><a href="#tabs-0">Visión por operador</a></li>
						<li><a href="#tabs-1">Visión Global</a></li>
						<li><a href="#tabs-2">Falla Masiva - Geográfico</a></li>
						<li><a href="#tabs-3">Falla Masiva - Cliente por Nodo</a></li>
						<li><a href="#tabs-4">Falla Masiva - Cliente por Sucursal</a></li>
					</ul>
					<div id="tabs-0">
						<iframe id="iframe-0" src="<?php echo $url3; ?>" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-1">
						<iframe id="iframe-1" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-2">
						<iframe id="iframe-2" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-3">
						<iframe id="iframe-3" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
					<div id="tabs-4">
						<iframe id="iframe-4" src="" frameborder="0" width="100%" height="600px"></iframe>
					</div>
				</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script>
	$(function() {
		var aIFrames = ['<?php echo $url3; ?>','<?php echo $url4; ?>','<?php echo $url5; ?>','<?php echo $url6; ?>','<?php echo $url7; ?>'];
		$( "#tabs" ).tabs({
			selected: 0,
			spinner: 'Retrieving data...',
			select: function(e, ui){
				var index=ui.index;
				$('iframe').attr('src','');
				$('#iframe-'+index).attr('src',aIFrames[index]);
			}
		});
	});
	</script>
</body>
</html>