<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
</head>
<body >
<table border="0" cellspacing="0" cellpadding="0" width="96%" style="border-collapse:separate; border: solid 1px; border-radius: 10px; border-color: silver;">
	<tbody>
		<tr height="80px">
			<td width="1%">
			</td>
			<td colspan="4" >
				<!--<span style="font-size: 50px; font-family: Gisha; font-weight: bold; color: #0068A1 ; margin-left: 30px;">e</span>
				<span style="font-size: 25px; font-family: Arial Black; font-weight: bolder; color: #ef8e01 ; margin-left: -4px; vertical-align: 6px;">)</span>
				<span style="font-size: 35px; font-family: Gisha; font-weight: bold; color: #0068A1 ; margin-left: 4px; vertical-align: 3px;">entel</span>-->
				<span style="font-size: 15px; font-family: Gisha; vertical-align: 7px; font-style: oblique;"><?php echo nl2br($mensaje_mda); ?></span>
			</td>
		</tr>
		<tr >
			<td width="1%"></td>
			<td width="50%" style="padding: 15px; border-collapse:separate; border: solid 1px; border-radius: 10px; border-color: silver;">
				<span style="font-size: 13px; font-family: Gisha; font-weight: bold; margin-left: 10px; font-style: oblique;">Notificación de Incidente:</span>
				<table style="width: 100%; max-width: 100%; margin-top: 20px; margin-bottom: 0; font-size: 11px; font-family: arial, sans-serif;"><tbody >
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;" >N° Folio MDA CLICK: </td><td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;" >
						<!-- N° Incidente -->
							<?php echo nl2br($ticket); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;" >Severidad</td><td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;" >
						<!-- N° Incidente -->
							<?php echo nl2br($severidad); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">País</td><td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Establecimiento -->
							<?php echo nl2br($pais); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Negocio</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Alarma -->
							<?php echo nl2br($negocio); ?>
						</td>
					</tr>
					<tr>
						<td nowrap style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Servicio Afectados</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Alarma de Equipo -->
							<?php echo nl2br($servicio); ?>
						</td>
					</tr>
					<tr>
						<td nowrap style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Área responsable</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($area); ?>
						</td>
					</tr>
					<tr>
						<td nowrap style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Tipo de Dispositivo</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($tipo_disp); ?>
						</td>
					</tr>
					<tr>
						<td nowrap style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Ubicación del Dispositivo</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($ubicacion); ?>
						</td>
					</tr>
					<tr>
						<td nowrap style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Nombre dispositivo</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($nombre); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">IP(/puerta)</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($ip); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Fecha / Hora</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($fecha); ?>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">Descripción</td><td td colspan="2" style="vertical-align: top; padding: 5px; font-family: arial, sans-serif;">
						<!-- Tipo de Falla -->
							<?php echo nl2br($mensaje); ?>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
			<td width="20%">
			</td>
		</tr>
		<tr height="30px">
		</tr>
		<tr >
			<td width="1%">
			</td>
			<td width="45%" style="padding: 15px; border-collapse:separate; border: solid 1px; border-radius: 10px; border-color: silver;">
				<span style="font-size: 13px; font-family: Gisha; font-weight: bold; margin-left: 10px; font-style: oblique;">Ultimo comentario</span>
				<!-- Observaciones -->
					<p style="margin-left:10px; font-size: 11px; font-family: arial, sans-serif;"><?php echo $fechactual.' '.nl2br($obs); ?></p>
			</td>
		</tr>
		<tr height="30px">
		</tr>
		<tr height="80px">
			<td width="1%">
			</td>
			<td colspan="3" width="97%" style="padding-left: 10px; border-collapse:separate; border: solid 1px; border-radius: 10px; border-color: silver;">
				
							<span style="font-size: 14px; font-family: Gisha; font-weight: bold; margin-left: 10px; font-style: oblique;">Gestión de Eventos</span>
			</td>
			<td width="1%">
			</td>
		</tr>
		<tr height="30px">
		</tr>
	</tbody>
</table>
</body>
</html>
