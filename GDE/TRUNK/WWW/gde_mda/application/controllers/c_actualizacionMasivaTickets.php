<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_actualizacionMasivaTickets extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
		
		$this->load->model('evento');
		$this->load->model('webservices');
	}
	
	public function actializacionMasiva(){
	
		# Se obtiene el listado de ids de tickets a actualizar
		$this->db->select('TICKET_ID');
		$this->db->select('TICKET_EST');
		$this->db->from('ticket');
		$this->db->where('TICKET_EST !=', 'CERRADO');
		$this->db->where('MINUTE(TICKET_TSINI) = MINUTE(now())', NULL, FALSE);
		$result = $this->db->get();
		
		$arrayTicketID = array();
		$arrayTicketEstadoInicial = array();
		
		foreach( $result->result() as $ticketID ){
		
			array_push($arrayTicketID, $ticketID->TICKET_ID);
			$arrayTicketEstadoInicial[ $ticketID->TICKET_ID ] = $ticketID->TICKET_EST;
		}

		# Se consulta a WS para actualización de tickets
		if( sizeof($arrayTicketID) > 0){
			$wsResponde = $this->webservices->consultaMasivaListaIncidentes(implode(',', $arrayTicketID));

			echo $wsResponde;
			
			$first		= explode(',' , $wsResponde);
			$second	= preg_split("/[\n:]/" , $first[1]);

			if( $second[0] != "OK"){
			
				log_message('error', 'Se ha presentado un problema en la actualizacion masiva');
				log_message('error', $wsResponde);
			}
			else{
				
				$tres		= preg_split("/[\n]/" , $wsResponde, 2);
				$cuatro	= trim(preg_replace('/\n/', '', $tres[1]));
				$cinco		= explode(';', $cuatro,-1);
				
				$arrayTicketsActualizados = array();
				
				foreach( $cinco as $ticketActualizado){
					
					$seis = explode(',' , $ticketActualizado);

					$this->db->where('TICKET_ID', $seis[0]);
					$this->db->update( 'ticket', array( 'TICKET_EST' => strtoupper($seis[1]), 'TICKET_LAST_EST' =>  strtoupper($arrayTicketEstadoInicial[ $seis[0] ] )) );
					$this->db->flush_cache();
				}
				
				log_message('debug', 'Se ha actualizado el estado de los siguientes Tickets');
				log_message('debug', implode(',', $arrayTicketID));
			}
			
		}
		return;
	}
}

/* End of file c_actualizacionMasivaTickets.php.php */
/* Location: ./application/controllers/c_actualizacionMasivaTickets.php.php */