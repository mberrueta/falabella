<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Crear cliente</h3>
			</header>
			<div style="margin:15px;"> 
			<?php
			if($men != ''){
				$mensaje = explode('|',$men);
				echo $mensaje[1];
			}
			?>
			
			
			<?php echo form_open('supervisor/c_crear_cliente/crear_cliente');?>
			<div id="crear_cliente" style="width: 100%;">
				<table border="0" cellspacing="0" cellpadding="0" style="width: 25%; float: left; margin-right: 5%;">
					<caption>Información básica de Cliente</caption>
					<thead style="background-color: #0073AE; color: white;">
						<tr>
							<th>Atributo</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Nombre:</td>
							<td><input type="text" id="cNombre" size="20" name="cNombre" value="<?php echo set_value('cNombre'); ?>" /></td>
							<td><span id="nameInfo"></span></td>
						</tr>
						<tr>
							<td>Criticidad:</td>
							<td>
								<select name="cCriticidad" style="width: 12em;">
								<?php
									for($i = 0; $i<=9; $i++){
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
								?>
								</select>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>Nombre alias:</td>
							<td>
								<input type="text" id="nom_alias" name="nom_alias" value="" />
							</td>
							<td></td>
						</tr>
						<tr>
							<td>Correo alias:</td>
							<td>
								<input type="text" id="alias" name="alias" value="" />
							</td>
							<td></td>
						</tr>
					</tbody>
				</table>
				
				<table id="SLA" border="0" cellspacing="0" cellpadding="0" style="width: 40%; float: left; margin-right: 5%;">
					<caption>SLA Tiempo de respuesta para eventos Activos</caption>
					<thead style="background-color: #0073AE; color: white;">
						<tr>
							<th>Service Level Agreement</th>
							<th>Tiempo en minutos Nivel 1 (Medio)</th>
							<th>Tiempo en minutos Nivel 2 (Alto)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td style="background-color: yellow;">&nbsp;</td>
							<td style="background-color: red;">&nbsp;</td>
						</tr>
						<tr>
							<td>SLA Generico</td>
							<td><input type="number" min="1" max="59" name="slaGenericoMedio" value="3"/></td>
							<td><input type="number" min="1" max="59" name="slaGenericoAlto" value="7"/></td>
						</tr>
						<tr>
							<td>SLA criticidad 0</td>
							<td><input type="number" min="1" max="59" name="sla0Medio" value="3"/></td>
							<td><input type="number" min="1" max="59" name="sla0Alto" value="7"/></td>
						</tr>
						<tr>
							<td>SLA criticidad 1</td>
							<td><input type="number" min="1" max="59" name="sla1Medio" value="3"/></td>
							<td><input type="number" min="1" max="59" name="sla1Alto" value="7"/></td>
						</tr>
						<tr>
							<td>SLA criticidad 2</td>
							<td><input type="number" min="1" max="59" name="sla2Medio" value="3"/></td>
							<td><input type="number" min="1" max="59" name="sla2Alto" value="7"/></td>
						</tr>
						<tr>
							<td>SLA criticidad 3</td>
							<td><input type="number" min="1" max="59" name="sla3Medio" value="3"/></td>
							<td><input type="number" min="1" max="59" name="sla3Alto" value="7"/></td>
						</tr>
					</tbody>
				</table>
				<div style="padding-bottom: 5%; clear:both;"></div>
			</div>
			
			<?php 
			if($operadores['stt']){
				echo '<table id="operadores" border="0" cellspacing="0" cellpadding="0" width="100%">';
				echo '<thead>';
					echo '<tr>';
						echo '<th>Operador Principal</th>';
						echo '<th>Backup</th>';
						echo '<th>Otros</th>';
						echo '<th>Analistas</th>';
					echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				echo '<tr>';
				echo '<td><select class="usuarios" id="selectOperador" name="operador" size="10">';
					foreach($operadores['cli']->result() as $operador) :
						echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
					endforeach;
				echo '</select></td>';
				echo '<td><select class="usuarios" id="selectBackup" name="backup" size="10" disabled="disabled">';
					foreach($operadores['cli']->result() as $operador) :
						echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
					endforeach;
				echo '</select></td>';
				echo '<td><select class="usuarios" id="selectCT1" name="otros[]" size="10" multiple="multiple" >';
					foreach($operadores_sinsup['cli']->result() as $operador) :
						echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
					endforeach;
				echo '</select></td>';
				echo '<td><select class="usuarios" id="selectAnalistas" name="analistas[]" size="10" multiple="multiple" >';
					foreach($analistas['cli']->result() as $operador) :
						echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
					endforeach;
				echo '</select></td>';
				echo '</tr>';
				echo '</tbody>';
				echo '</table>';
			}else{
				echo '<p>Problema</p>'; #Verificar mensaje
			}
			
			echo form_submit('crear','Nuevo Cliente');
			echo form_close();?>
	 			</div>
		</article>
	</section><input type="hidden" id="url_js" value="<?php echo site_url(); ?>">
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/asociar_cliente.js"></script>

</body>
</html>