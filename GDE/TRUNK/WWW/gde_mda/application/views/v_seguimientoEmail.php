<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>GDE -> Seguimientos de Correo</title>

	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
	
	<style>
		body{
			padding: 20px;
		}
		
		#filaSeguimientosActivos{
			width: 600px !important;
			height: 350px !important;
			overflow: scroll !important;
		}
		
		#tablaSeguimientosActivos tbody tr:hover{
			background-color: #FFFF66;
			cursor: pointer;
		}
		
		#tablaSeguimientosActivos thead th{
			color: white !important;
		}
		
		.selectedRow{
			background-color: #FF6600 !important;
		}
		.cellAlertSeguimiento{
			background-color: #CC0000 !important;
			color: #FFFFFF;
		}
		
		.columna{
			float: left;
			width: 45%;
		}
		
		.cancelSeg img{
			display: none;
		}
		
		table thead, table tfoot{
			background-color: #0073AE;
			border-radius: 10px;
			-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
		}
		
		table thead th, table tfoot td{
			color: white;
		}
		#tabnav {
			list-style-type: none;
			padding: 0;
			margin-top: 10px;
		}
		#tabnav li {
			display: inline;
			padding: 0.5em;
			background-color: #ddd;
			cursor: pointer;
		}
		#tabnav li:hover {
			background-color: #ffd;
		}
		#tabnav .selected {
			background-color: #dfd;
		}

		#tabcontainer > div {
			display: none;
			padding: 1em;
		}
		#tabcontainer > .selected {
			display: block;
		}
	</style>
</head>
<body>
	<!--<div class="secciones">
		<button id="seccionActivos" name="seccionActivos" value="Seguimientos Activos">Seguimientos Activos</button>
		<button id="seccionCancelados" name="seccionCancelados" value="Seguimientos Cancelados">Seguimientos Terminados</button>
	</div>-->
	<div class="contenido">
		<div id="areaSeguimientosActivos">
			<div class="columna">
				<div id="filaSeguimientosActivos" class="fila">
					<?php echo $tablaSeguimientosActivos;?>
				</div>
				<div class="fila">
					<div>
						<ul id="tabnav">
							<li class="selected" id="tab-1-nav" onclick="clickTab(this)">Observaciones a Clientes</li>
							<!--<li id="tab-2-nav" onclick="clickTab(this)">Historial Interno</li>-->
						</ul>
						<div id="tabcontainer">
							<div id="tab-1" class="selected">
								<table id="tablaComentariosSeguimiento" border="0" cellpadding="4" cellspacing="0">
									<thead>
										<tr>
											<th width="170px">Hora registrada</th>
											<th>Comentario de Seguimiento</th>
											
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="2" rowspan="1" > 
												<textarea id="comentarioNuevo" rows="4" cols="50" name="comentarioNuevo"></textarea> 
												<input type="submit" id="setComentarioNuevo" disabled="disabled" value="Comentar" />
											</td>
										</tr>
									</tfoot>
									<tbody>
									</tbody>
								</table>
							</div>
							<!--<div id="tab-2">
								<table id="tablaComentariosSeguimiento2" border="0" cellpadding="4" cellspacing="0">
									<thead>
										<tr>
											<th width="170px">Hora </th>
											<th>Comentario Interno</th>
											<th>Editar</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<td colspan="3" rowspan="1" > 
												<textarea id="comentarioNuevo2" rows="4" cols="50" name="comentarioNuevo2"></textarea> 
												<input type="submit" id="setComentarioNuevo2" disabled="disabled" value="Comentar" />
												<input type="submit" id="editComentario2" value="Editar" style="display: none;" />
											</td>
										</tr>
									</tfoot>
									<tbody>
									</tbody>
								</table>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			
			<div class="columna">
				<div id="contenedorAccionCancelar">

					<form id="contenedorAccionCancelarForm" >
						<fieldset>
							<legend align="right">Confirmación de término de seguimiento</legend>
							<label for="motivoCancelacion">Motivo:</label>
							<textarea id="motivoCancelacion" name="motivoCancelacion"></textarea>
							<input type="submit" id="enviarCancelacion" value="Cancelar Seguimiento" />
						</fieldset>
					</form>
					
				</div>
				<div id="contenedorAccionEnviar">
					<form id="contenedorAccionEnviarForm">
						<fieldset>
							<legend>Envío de correo</legend>
								<input type="hidden" type="text" name="id_seguimiento" value="" readonly="readonly"/>
							<label for="desde">Desde: </label>
							<input type="text" name="desde_show" value="" readonly="readonly"/>
								<input type="hidden" name="desde" value="" readonly="readonly"/>
								<input type="hidden" name="desde_nom" value="" readonly="readonly"/>
							<label for="para">*Para: </label>
							<textarea type="text" name="para" value="" ></textarea>
							<label for="copia">*Copia: </label>
							<textarea type="text" name="copia" value=""></textarea>
							<label for="asunto">Asunto: </label>
							<input type="text" name="asunto" value=""/>
							
							<hr>
							
							<label for="numin">N° Incidente: </label>
							<input type="text" name="numin" value="" />
							<label for="cliente">*Cliente: </label>
							<input type="text" name="cliente" value="" />
							<label for="tipoevento">Tipo de Alarma: </label>
							<textarea name="tipoevento" value="" rows="4" cols="50"></textarea>
							<label for="alaeq">Alarma de Equipo: </label>
							<textarea name="alaeq" value="" rows="4" cols="50"></textarea>
							<label for="codservicio">Codigo de Servicio: </label>
							<textarea name="codservicio" value="" rows="4" cols="50"></textarea>
							<label for="sucursales">Direccion:</label>
							<textarea name="sucursales" value="" rows="4" cols="50"></textarea>
							<label for="estactividad">Estado Actividad: </label>
							<input type="text" name="estactividad" value="" />
							<label for="fecini">Fecha Inicio Incidente: </label>
							<input type="text" name="fecini" value="" />
							<label for="fecter">Fecha Termino Incidente: </label>
							<input type="text" name="fecter" value="" />
							<label for="desevento">Descripción de Evento: </label>
							<textarea name="desevento" value="" rows="4" cols="50"></textarea>
							<label for="accrealiz">Acciones Realizadas: </label>
							<textarea name="accrealiz" value="" rows="4" cols="50"></textarea>
							<label for="obs">Observaciones: </label>
							<textarea name="obs" value="" rows="4" cols="50"></textarea>
							
							<input type="hidden" name="table" value="" />
							<input type="hidden" name="table2" value="" />
							<input type="hidden" name="clientehid" value="" />
							<hr />
							
							<div id="datosEmail">
							</div>
							<hr />
							<label ><b>Firma</b></label>
							<label for="nombre">Nombre: </label>
							<input type="text" name="nombre" value="" />
							<label for="firma">Glosa Firma: </label>
							<textarea name="firma" value="" rows="4" cols="50"></textarea>
							<label for="email">E-mail: </label>
							<input type="text" name="email" value="" />
							<label for="fono">Telefono: </label>
							<input type="text" name="fono" value="" />
							<p id="valida">Datos requeridos incompletos.<br/>Favor completarlos antes de realizar envio</p>
							<input type="button" id="enviarCorreoSeguimiento" value="Enviar" />
						</fieldset>
					</form>
				</div>
				<div id="contenedorAccionEnviar_new">
					<form id="contenedorAccionEnviarForm_new">
						<fieldset style="margin-top: 0px !important;">
							<legend style="margin-left: 200px; margin-bottom: 10px;">Envío de correo</legend>
								<input type="hidden" type="text" name="id_seguimiento" value="" readonly="readonly"/>
							<label for="desde">Desde (Alias): </label>
							<input type="text" name="desde_show" value="" readonly="readonly"/>
								<input type="hidden" name="desde" value="" readonly="readonly" />
								<input type="hidden" name="desde_nom" value="" readonly="readonly" />
							<label for="para">*Para: </label>
							<textarea name="para" id="para" value="" rows="3" cols="25"></textarea>
							<label for="copia">*Copia: </label>
							<textarea name="copia" id="copia" value="" rows="3" cols="50"></textarea>
							<label for="asunto">*Asunto: </label>
							<input type="text" name="asunto" value=""/>
							
							<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
							<legend style="margin-left: 200px; margin-bottom: 10px;">Detalles</legend>
							
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="cliente">*Cliente: </label>
									<input type="text" name="cliente"  id="cliente" value="" />
								</div>
								<div style="width: 245px; float: left;">
									<label for="numin">N° Incidente: </label>
									<input type="text" name="numin" value="" />
								</div>
							</div>
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="tipoevento">Tipo de Alarma: </label>
									<textarea name="tipoevento" value="" rows="3" cols="50"></textarea>
								</div>
								<div style="width: 245px; float: left;">
									<label for="codservicio">Codigo de Servicio: </label>
									<textarea name="codservicio" value="" rows="3" cols="50"></textarea>
								</div>
							</div>
							<div class="centered">
								<div style="width: 98%; float: left; margin-right: 35px;">
									<label for="alaeq">Nombre de Equipo: </label>
									<textarea name="alaeq" value="" rows="3" cols="50"></textarea>
								</div>
							</div>
							<div class="centered">
								<div style="width: 98%; float: left; margin-right: 35px;">
									<label for="sucursales">Direccion del Servicio:</label>
									<textarea name="sucursales" value="" rows="3" cols="50"></textarea>
								</div>
							</div>
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="fecini">Fecha/Hora Inicio: </label>
									<input type="text" name="fecini" value="" />
								</div>
								<div style="width: 245px; float: left;">
									<label for="fecter">Fecha/Hora Termino: </label>
									<input type="text" name="fecter" value="" />
								</div>
							</div>
							
							<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
							<legend style="margin-left: 200px; margin-bottom: 10px;">Resumen</legend>
							
							<label for="nombestab">Nombre del Establecimiento: </label>
							<input type="text" name="nombestab" value="" />
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="estserv">Estado del Servicio WAN: </label>
									<select type="text" name="estserv" value="" style="height: 30px;" >
										<option>Servicio Operativo</option>
										<option>Servicio Degradado</option>
										<option>Servicio Intermitente</option>
										<option>Sin Servicio</option>
									</select>
								</div>
								<div style="width: 245px; float: left;">
									<label for="estservlan">Estado del Servicio LAN: </label>
									<select type="text" name="estservlan" value="" style="height: 30px;" >
										<option>Servicio Operativo</option>
										<option>Servicio Degradado</option>
										<option>Servicio Intermitente</option>
										<option>Sin Servicio</option>
										<option>No Aplica</option>
									</select>
								</div>
							</div>
							<div class="centered">
								
								<div style="width: 530px; float: left; margin-right: 35px;">
									<label for="estinc">Estado Incidente: </label>
									<select type="text" name="estinc" value="" style="height: 30px;" >
										<option>INICIO</option>
										<option>SEGUIMIENTO</option>
										<option>TERMINO</option>
									</select>
								</div>
							</div>
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="enlaceprin">Enlace Pricipal: </label>
									<select type="text" name="enlaceprin" value="" style="height: 30px;" >
										<option>Operativo</option>
										<option>Sin Conexión</option>
										<option>Intermitente</option>
										<option>Degradado</option>
									</select>
								</div>
								<div style="width: 245px; float: left;">
									<label for="enlaceresp">Enlace Respaldo: </label>
									<select type="text" name="enlaceresp" value="" style="height: 30px;" >
										<option>Operativo</option>
										<option>Sin Conexión</option>
										<option>Intermitente</option>
										<option>Degradado</option>
										<option>No Aplica</option>
									</select>
								</div>
							</div>
							<div class="centered">
								<div style="width: 98%; float: left; margin-right: 35px;">
									<label for="desevento">Descripcion de Incidente: </label>
									<textarea name="desevento" value="" rows="3" cols="50"></textarea>
								</div>
							</div>
							
							<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
							<legend style="margin-left: 200px; margin-bottom: 10px;">Observaciones</legend>
							<label for="obs">*Observaciones: </label>
							<textarea name="obs" value="" rows="3" cols="50"></textarea>
							
							<input type="hidden" name="table" value="" />
							<input type="hidden" name="clientehid" value="" />
							
							<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
							<legend style="margin-left: 200px; margin-bottom: 10px;">Firma</legend>
							<label for="nombre">Nombre: </label>
							<input type="text" name="nombre" value="" />
							<label for="firma">Glosa Firma: </label>
							<textarea name="firma" value="" rows="3" cols="50"></textarea>
							<div class="centered">
								<div style="width: 245px; float: left; margin-right: 35px;">
									<label for="email">E-mail: </label>
									<input type="text" name="email" value="" />
								</div>
								<div style="width: 245px; float: left;">
									<label for="fono">Telefono: </label>
									<input type="text" name="fono" value="" />
								</div>
							</div>
							
							<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
							<p id="valida2">Datos requeridos incompletos.<br/>Favor completarlos antes de realizar envio</p>
							<input type="button" id="enviarCorreoSeguimiento_new" value="Enviar" />
						</fieldset>
					</form>
				</div>
			</div>		
		</div>
		<div id="areaSeguimientosCancelados">
			<div class="columna" style="overflow-y: scroll; height: 500px">
				
			</div>
			<div class="columna">
				<table id="tablaComentariosCancelados" border="0" cellpadding="4" cellspacing="0">
					<thead>
						<tr>
							<th>Hora registrada</th>
							<th>Comentario de Seguimiento</th>
						</tr>
					</thead>
					<tbody>	
					</tbody>
				</table>	
			</div>
		</div>
	</div>
	
<script type="text/javascript">

/*	--------------------------------------------------
	:: Variables Globales
	-------------------------------------------------- */
	
	var columnasOcultas			= [0],		// Columnas que se deben ocultar
	columnasCountDown			= [3],		// Columna en la que se realiza el countdown
	columnaCancelar				= [4],
	idSeguimiento				= null,		// ID de seguimiento seleccionado
	cuentaRegresivaTimer		= null;		// Object Timer con el que se ejecuta el countdown
	idcom						= null;		// ID del comentario a editar
	idcom2						= null;		// ID del comentario a editar
	
/*	--------------------------------------------------
	:: Plugins
	-------------------------------------------------- */	
	
	/**
	* Genera una tabla con thead estatico y tbody con scroll
	* TODO: Ancho de encabezado difiere de body en scroll
	*/
	jQuery.fn.tableFix = function() {
		
		var o 		= $(this[0]); 			// It's your element
		var args	= arguments[0] || {};	// It's your object of arguments
		var height	= args.height;			// Height of table body
		var p;								// Parent
		
		var thead = o.find('thead').remove();
		o.wrap('<div class="tableFix" />');
		
		p = o.parent();
		p.prepend('<table></table>');
		p.children(':first').append(thead);
		
		o.attr('style','display:block; overflow-y:scroll; height:'+height+'px;');
		
		o.find('tbody tr:first td').each(function(i){
			var w  = $(this).width();
			var ws = (p.children('table:first').find('thead th').eq(i).width());

			if( ws < w ){
				p.children('table:first').find('thead th').eq(i).width(w);
			}
			else{
				p.children('table:last').find('tbody td').eq(i).width(ws);
			}
		});
	};
		
	jQuery.fn.ocultarColumna = function() {
	
		var o 		= $(this[0]);
		var args	= arguments[0] || {};	// It's your object of arguments
		var fila	= args.fila;
		
		if ( jQuery.isNumeric(fila) ) {
			fila = [ fila ];
		}

		for(var i = 0; i < fila.length; i++){
			var index = 1 + fila[i];
			o.find('th::nth-child(' + index +') ,td:nth-child(' + index +')').hide();
		}
	};
	
	jQuery.fn.ocultarColumna2 = function() {
	
		var o 		= $(this[0]);
		var args	= arguments[0] || {};	// It's your object of arguments
		var fila	= args.fila;
		
		if ( jQuery.isNumeric(fila) ) {
			fila = [ fila ];
		}

		for(var i = 0; i < fila.length; i++){
			var index = 1 + fila[i];
			o.find('td:nth-child(' + index +')').hide();
		}
	};
	
/*	--------------------------------------------------
	:: Acciones iniciales
	-------------------------------------------------- */
	function setClasses(parentid, wantedid) {
        var els = document.getElementById(parentid).children
        for (var i = 0; i < els.length; i++) {
            var e = els[i]
            e.className = (e.id == wantedid) ? "selected" : ""
        }
    }
    function clickTab(nav) {
        var currenttab = nav.id.match(/^tab-([0-9]+)/)[0]
        setClasses("tabcontainer", currenttab)
        setClasses("tabnav", nav.id)
    }
	function min2sec( time ){
		if (time.search(':') == -1){
			return parseInt(time);
		}
		timer	= time.split(':');
		min		= parseInt(timer[0]) * 60;
		sec		= parseInt(timer[1]);
		finalval	= min + sec;
		return finalval;
		
	};
	
	//Formatea segundos a minutos
	function sec2min( time ){
		var mins = 0;
		var secs = 0;
		mins = Math.floor(time/60);
		secs = Math.floor(time - mins*60);

		if(secs < 10) secs = "0" + secs;
		return mins + ":" + secs;
	};
	
	//Realiza el efecto cuenta regresiva (countdown)
	function countDownSeguimiento(){
	
		var value = null;
		$('.count').each(function(index){
		
			if(index != 0){
				value = $(this).html();
				value = min2sec( value );
				var finalValue = parseInt(value) - 1;
				if(finalValue < 0)
					$(this).addClass('cellAlertSeguimiento');
				finalValue = sec2min(finalValue);
				$(this).html( finalValue );
			}
		});
	};
	
	$(document).ready(function(){
		$('#areaSeguimientosCancelados').hide();
		$('#contenedorAccionEnviar').hide();
		$('#contenedorAccionEnviar_new').hide();
		$('#contenedorAccionCancelar').hide();
		$('#valida').hide();
		$('#valida2').hide();
		$('#tablaSeguimientosActivos tbody tr').append('<td class="cancelSeg"><img src="<?php echo base_url(); ?>images/delete-icon_red.png" /></td>');
			
		$('#tablaSeguimientosActivos tr').each(function(index){
			var item = $(this),
			bufferCol = null;
			
			// Se agrega la clase count para realizar el countDown
			for(var i = 0; i < columnasCountDown.length; i++){
				bufferCol = item.children().eq(columnasCountDown[i]);
				bufferCol.addClass('count');
			}
		});
		
		$('#tablaComentariosSeguimiento').width(600);
		$('#tablaComentariosSeguimiento2').width(600);
		
		$('#tablaSeguimientosActivos').ocultarColumna({fila:0});	// Se ocultan las columnas que se deben ocultar
		
		cuentaRegresivaTimer = setInterval(function () {
			countDownSeguimiento();
		},1000);
	});
	

/*	--------------------------------------------------
	:: Secciones
	-------------------------------------------------- */
	
	// Se carga seccion de seguimientos Activos
	$('#seccionActivos').click(function(e){
		e.preventDefault();
	$("#seccionActivos").prop("disabled", true);

		$('#areaSeguimientosCancelados').hide();
		$('#areaSeguimientosActivos').show();
		$('#valida').hide();
		$('#valida2').hide();
	$("#seccionActivos").prop("disabled", false);
		
	});
	
	// Se carga seccion de seguimientos Cancelados
	$('#seccionCancelados').click(function(e){
		e.preventDefault();
	$("#seccionCancelados").prop("disabled", true);
		
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/listarSeguimientosVencidos",
			dataType: 'json',
			success: function(data) {
				if( data.return == true ){
					$('#areaSeguimientosCancelados').find('.columna:first').empty().append(data.result);
					$('#tablaComentariosCancelados tbody').empty();
					$('#areaSeguimientosCancelados').show();
					$('#areaSeguimientosActivos').hide();
				}
				else{
					alert(data.error);
				}
	$("#seccionCancelados").prop("disabled", false);				
				
				return true;
			}
		});
	});
	
/*	--------------------------------------------------
	:: Manipulacion de tabla
	-------------------------------------------------- */
	//Seleccion de fila en tabla con un click
	$('#tablaSeguimientosActivos tbody tr').click(function(){
		var table = $(this);
		
		$('#contenedorAccionEnviar').hide();
		$('#contenedorAccionEnviar_new').hide();
		$('#contenedorAccionCancelar').hide();
		
		idSeguimiento = table.children().first().text();
		//alert(idSeguimiento);
		
		// Se habilita comentario
		$('#setComentarioNuevo').removeAttr('disabled');
		$('#setComentarioNuevo2').removeAttr('disabled');
		$('#setComentarioNuevo').show();
		$('#editComentario').hide();
		
		// Modificacion de clases
		$('#tablaSeguimientosActivos tbody tr').removeClass("selectedRow");
		table.addClass('selectedRow');
		
		$('#tablaSeguimientosActivos td img').parent().addClass('cancelSeg');
		$('.selectedRow').find('td:last').removeClass('cancelSeg');
		
		// Se obtiene tabla con comentarios
		cargarComentarios('tablaComentariosSeguimiento');
		//cargarComentarios2('tablaComentariosSeguimiento2');
	});
	
	//Seleccion de fila en tabla con doble click
	/*$('#tablaSeguimientosActivos tbody tr').live('dblclick', function(){
		var table = $(this);
		
		$('#contenedorAccionEnviar').hide();
		$('#contenedorAccionEnviar_new').hide();
		$('#contenedorAccionCancelar').hide();
		
		idSeguimiento = table.children().first().text();
		
		$('#areaSeguimientosActivos .columna:last').prepend('<span id="loadingMessage">Cargando Correo...</span>');
		
		if(table.find('td:eq(4)').text()=="evento"){
			$.ajax({
				type: "POST",
				url: "c_seguimientoEmail/getcorreoRespaldo",
				data: { seguimiento: idSeguimiento },
				dataType: 'json',
				success: function(data) {
					//Alias
					if( data.desde == null ) data.desde = "gde@entel.cl";
					if( data.desde_nom == null ) data.desde_nom = "Gestor de Eventos CMSC";
					
					$('#contenedorAccionEnviarForm input[name="desde"]').val(data.desde);
					$('#contenedorAccionEnviarForm input[name="desde_nom"]').val(data.desde_nom);
					$('#contenedorAccionEnviarForm input[name="desde_show"]').val(data.desde_nom+"<"+data.desde+">");
					$('#contenedorAccionEnviarForm input[name="id_seguimiento"]').val(idSeguimiento);
					$('#contenedorAccionEnviarForm textarea[name="para"]').val(data.to);
					$('#contenedorAccionEnviarForm textarea[name="copia"]').val(data.cc);
					$('#contenedorAccionEnviarForm input[name="asunto"]').val(data.asu);
					$('#contenedorAccionEnviarForm input[name="numin"]').val(data.numin);
					$('#contenedorAccionEnviarForm input[name="cliente"]').val(data.cli);
					$('#contenedorAccionEnviarForm input[name="clientehid"]').val(data.cliente);
					$('#contenedorAccionEnviarForm input[name="estactividad"]').val(data.estact);
					$('#contenedorAccionEnviarForm textarea[name="desevento"]').val(data.descev);
					$('#contenedorAccionEnviarForm textarea[name="accrealiz"]').val(data.accreal);
					$('#contenedorAccionEnviarForm textarea[name="obs"]').val(data.obs);
					$('#contenedorAccionEnviarForm textarea[name="tipoevento"]').val(data.tipoevento);
					$('#contenedorAccionEnviarForm textarea[name="alaeq"]').val(data.alaeq);
					$('#contenedorAccionEnviarForm textarea[name="codservicio"]').val(data.codservicio);
					$('#contenedorAccionEnviarForm textarea[name="sucursales"]').val(data.sucursales); //######
					$('#contenedorAccionEnviarForm textarea[name="firma"]').val(data.firma);
					$('#contenedorAccionEnviarForm input[name="fecini"]').val(data.fecini);
					$('#contenedorAccionEnviarForm input[name="fecter"]').val(data.fecter);
					$('#contenedorAccionEnviarForm input[name="nombre"]').val(data.nombre);
					$('#contenedorAccionEnviarForm input[name="email"]').val(data.email);
					$('#contenedorAccionEnviarForm input[name="fono"]').val(data.fono);
					
					$('#contenedorAccionEnviarForm input[name="table"]').val(data.hidden);
					$('#contenedorAccionEnviarForm input[name="table2"]').val(data.nodos);
					$('#datosEmail').empty().append(data.tabla);
					
					$('#datosEmail table').ocultarColumna({fila:0});
					
					$('#loadingMessage').remove();
					$('#contenedorAccionEnviar').show();
					$('#contenedorAccionEnviarForm textarea[name="para"]').focus();
				}
			});
		}
		else if(table.find('td:eq(4)').text()=="evento_new"){
			$.ajax({
				type: "POST",
				url: "c_seguimientoEmail/getcorreoRespaldo",
				data: { seguimiento: idSeguimiento },
				dataType: 'json',
				success: function(data) {
					//Alias
					if( data.desde == null ) data.desde = "gde@entel.cl";
					if( data.desde_nom == null ) data.desde_nom = "Gestor de Eventos CMSC";
					
					$('#contenedorAccionEnviarForm_new input[name="desde"]').val(data.desde);
					$('#contenedorAccionEnviarForm_new input[name="desde_nom"]').val(data.desde_nom);
					$('#contenedorAccionEnviarForm_new input[name="desde_show"]').val(data.desde_nom+"<"+data.desde+">");
					$('#contenedorAccionEnviarForm_new input[name="id_seguimiento"]').val(idSeguimiento);
					$('#contenedorAccionEnviarForm_new textarea[name="para"]').val(data.to);
					$('#contenedorAccionEnviarForm_new textarea[name="copia"]').val(data.cc);
					$('#contenedorAccionEnviarForm_new input[name="asunto"]').val(data.asu);
					$('#contenedorAccionEnviarForm_new input[name="numin"]').val(data.numin);
					$('#contenedorAccionEnviarForm_new input[name="cliente"]').val(data.cli);
					// $('#contenedorAccionEnviarForm_new textarea[name="obs"]').val(data.obs);
					$('#contenedorAccionEnviarForm_new textarea[name="tipoevento"]').val(data.tipoevento);
					$('#contenedorAccionEnviarForm_new textarea[name="alaeq"]').val(data.alaeq);
					$('#contenedorAccionEnviarForm_new textarea[name="codservicio"]').val(data.codservicio);
					$('#contenedorAccionEnviarForm_new textarea[name="sucursales"]').val(data.sucursales);
					$('#contenedorAccionEnviarForm_new textarea[name="firma"]').val(data.firma);
					$('#contenedorAccionEnviarForm_new input[name="fecini"]').val(data.fecini);
					$('#contenedorAccionEnviarForm_new input[name="fecter"]').val(data.fecter);
					$('#contenedorAccionEnviarForm_new input[name="nombestab"]').val(data.nombestab);
					$('#contenedorAccionEnviarForm_new textarea[name="desevento"]').val(data.descev);
					
					$('#contenedorAccionEnviarForm_new select[name="estserv"]').val(data.estserv);
					$('#contenedorAccionEnviarForm_new select[name="estservlan"]').val(data.estservlan);
					
					if( data.estinc == "INICIO" ){
						$('#contenedorAccionEnviarForm_new select[name="estinc"]').val("SEGUIMIENTO");
					}
					else {
						$('#contenedorAccionEnviarForm_new select[name="estinc"]').val(data.estinc);
					}
					
					$('#contenedorAccionEnviarForm_new select[name="enlaceprin"]').val(data.enlaceprin);
					$('#contenedorAccionEnviarForm_new select[name="enlaceresp"]').val(data.enlaceresp);
					
					$('#contenedorAccionEnviarForm_new input[name="nombre"]').val(data.nombre);
					$('#contenedorAccionEnviarForm_new input[name="email"]').val(data.email);
					$('#contenedorAccionEnviarForm_new input[name="fono"]').val(data.fono);
					
					$('#contenedorAccionEnviarForm_new input[name="table"]').val(data.hidden);
					$('#contenedorAccionEnviarForm_new input[name="clientehid"]').val(data.cliente);
					
					$('#loadingMessage').remove();
					$('#contenedorAccionEnviar_new').show();
					$('#contenedorAccionEnviarForm_new textarea[name="para"]').focus();
				}
			});
		}
	});*/
	
	//Seleccion de fila en tabla CANCELADOS con doble click
	$('#tablaSeguimientosCancelados tbody tr').live('dblclick', function(){
		var table = $(this);
		idSeguimiento = table.children().first().text();
		
		$('#tablaSeguimientosCancelados tbody tr').removeClass("selectedRow");
		table.addClass('selectedRow');

		cargarComentarios('tablaComentariosCancelados');
	});	

/*	--------------------------------------------------
	:: Acciones sobre seguimiento
	-------------------------------------------------- */
	
	$('.selectedRow td img').live('click', function(){
		$('#contenedorAccionEnviar').hide();
		$('#contenedorAccionCancelar').show();
	});
	
	//
	// Editar comentario en caso de error o correccion
	//
	$('#tablaComentariosSeguimiento td img').live('click', function(){
		$('#setComentarioNuevo').hide();
		$('#editComentario').show();
		console.log($(this).parent().parent().find("td:eq(0)").text());
		idcom = $(this).parent().parent().find("td:eq(0)").text();
		$('#comentarioNuevo').val($(this).parent().parent().find("td:eq(2)").text());
	});
	
		//
	// Editar comentario en caso de error o correccion
	//
	$('#tablaComentariosSeguimiento2 td img').live('click', function(){
		$('#setComentarioNuevo2').hide();
		$('#editComentario2').show();
		console.log($(this).parent().parent().find("td:eq(0)").text());
		idcom2 = $(this).parent().parent().find("td:eq(0)").text();
		$('#comentarioNuevo2').val($(this).parent().parent().find("td:eq(2)").text());
	});
	
	$('#enviarCancelacion').live('click', function(e){
		e.preventDefault();
		
		var motivoCancelacionText = $('#motivoCancelacion').val();
		$("#enviarCancelacion").prop("disabled", true);
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/cancelarSeguimientoActivo",
			data: {seguimiento: idSeguimiento, comentario: motivoCancelacionText},
			dataType: 'json',
			success: function(data) {
				if( data.return == true ){
					
					var tableRow = $("#tablaSeguimientosActivos .selectedRow td:first").filter(function() {
						return $(this).text() == idSeguimiento;
					}).closest("tr");
					tableRow.remove();
					$('#contenedorAccionCancelar').hide();
					$('#tablaComentariosSeguimiento tbody').empty();
					window.opener.AfterSendEmail();
				}
				else{
					alert(data.error);
				}
				$("#enviarCancelacion").prop("disabled", false);
				return true;
			}
		});
	});
	
/*	--------------------------------------------------
	:: Comentarios en seguimiento
	-------------------------------------------------- */
	
	//Funcion, carga comentario de un seguimiento en particular
	function cargarComentarios(tabla){
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/listarComentariosSeguimiento",
			data: {seguimiento: idSeguimiento},
			success: function(data) {
				var tbody = $(data).find('tbody').html();
				$('#'+tabla+' tbody').html(tbody);
				var foot = $('#'+tabla+' tfoot').html();
				$('#'+tabla+'').ocultarColumna2({fila:0});
				/*if(tabla == "tablaComentariosSeguimiento"){
					$('#tablaComentariosSeguimiento tbody tr').append('<td class="edit"><img src="<?php echo base_url(); ?>images/edit-icon.png" /></td>');
				}*/
				$('#'+tabla+' tfoot').html(foot);
				$('#comentarioNuevo').val("");
				$('#setComentarioNuevo').show();
				$('#editComentario').hide();
				idcom = null;
			}
		});
	};
	
	function cargarComentarios2(tabla){
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/listarComentariosSeguimiento2",
			data: {seguimiento: idSeguimiento},
			success: function(data) {
				var tbody = $(data).find('tbody').html();
				$('#'+tabla+' tbody').html(tbody);
				var foot = $('#'+tabla+' tfoot').html();
				$('#'+tabla+'').ocultarColumna2({fila:0});
				if(tabla == "tablaComentariosSeguimiento2"){
					$('#tablaComentariosSeguimiento2 tbody tr').append('<td class="edit"><img src="<?php echo base_url(); ?>images/edit-icon.png" /></td>');
				}
				$('#'+tabla+' tfoot').html(foot);
				$('#comentarioNuevo2').val("");
				$('#setComentarioNuevo2').show();
				$('#editComentario2').hide();
				idcom2 = null;
			}
		});
	};
	//Agrega un comentario a un seguimiento en particular
	//TODO: Revisar agregar comentarios a Bitacora
	//		Validar el comentario
	$('#setComentarioNuevo').live('click', function(event){
		event.preventDefault();
		$("#setComentarioNuevo").hide();
		var comentarioNuevo = $('#comentarioNuevo').val();
		if(comentarioNuevo == "" || comentarioNuevo == null){
			return false;
		}
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/registrarComentarioSeguimiento",
			data: {seguimiento: idSeguimiento, comentario: comentarioNuevo},
			success: function(data) {
				var resultMessage = data.split(',');
				if(resultMessage[0] != 1){
					alert("Se ha presentado el siguiente problema: " + resultMessage[1]);
					$("#setComentarioNuevo").show();
				}
				else{
					cargarComentarios('tablaComentariosSeguimiento');
					$("#setComentarioNuevo").show();
				}
			}
		});
	});
	
	//Agrega un comentario INTERNO a un seguimiento en particular
	//TODO: Revisar agregar comentarios a Bitacora
	//		Validar el comentario
	$('#setComentarioNuevo2').live('click', function(event){
		event.preventDefault();
		$("#setComentarioNuevo2").hide();
		var comentarioNuevo = $('#comentarioNuevo2').val();
		if(comentarioNuevo == "" || comentarioNuevo == null){
			return false;
		}
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/registrarComentarioSeguimiento2",
			data: {seguimiento: idSeguimiento, comentario: comentarioNuevo},
			success: function(data) {
				var resultMessage = data.split(',');
				if(resultMessage[0] != 1){
					alert("Se ha presentado el siguiente problema: " + resultMessage[1]);
					$("#setComentarioNuevo2").show();
				}
				else{
					cargarComentarios2('tablaComentariosSeguimiento2');
					$("#setComentarioNuevo2").show();
				}
			}
		});
	});
	
	
	//Edita un comentario en particular
	$('#editComentario').live('click', function(event){
		event.preventDefault();
		var comentarioNuevo = $('#comentarioNuevo').val();
		if(comentarioNuevo == "" || comentarioNuevo == null){
			return false;
		}
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/editarComentario",
			data: {idcom: idcom, comentario: comentarioNuevo},
			success: function(data) {
				var resultMessage = data.split(',');
				if(resultMessage[0] != 1){
					alert("Se ha presentado el siguiente problema: " + resultMessage[1]);
				}
				else{
					cargarComentarios('tablaComentariosSeguimiento');
				}
			}
		});
	});
	
	//Edita un comentario en particular
	$('#editComentario2').live('click', function(event){
		event.preventDefault();
	$("#editComentario2").prop("disabled", true);
		
		var comentarioNuevo = $('#comentarioNuevo2').val();
		if(comentarioNuevo == "" || comentarioNuevo == null){
			return false;
		}
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/editarComentario2",
			data: {idcom: idcom2, comentario: comentarioNuevo},
			success: function(data) {
				var resultMessage = data.split(',');
				if(resultMessage[0] != 1){
					alert("Se ha presentado el siguiente problema: " + resultMessage[1]);
				}
				else{
					cargarComentarios2('tablaComentariosSeguimiento2');
				}
	$("#editComentario2").prop("disabled", false);
				
			}
		});
	});
	
	$('#enviarCorreoSeguimiento').click(function(event){
		$("#enviarCorreoSeguimiento").prop("disabled", true);
	
		if ($('#contenedorAccionEnviarForm textarea[name="para"]').val() == "" || $('#contenedorAccionEnviarForm textarea[name="copia"]').val() == "" || $('#contenedorAccionEnviarForm input[name="cliente"]').val() == ""){
			$('#valida').show();
		}
		else{
			$('#valida').hide();
			enviaCorreo();
		}
		$("#enviarCorreoSeguimiento").prop("disabled", false);
		
	});
	
	$('#enviarCorreoSeguimiento_new').click(function(event){
		$("#enviarCorreoSeguimiento_new").prop("disabled", true);
	
		if ($('#contenedorAccionEnviarForm_new textarea[name="para"]').val() == "" || $('#contenedorAccionEnviarForm_new textarea[name="copia"]').val() == "" || $('#contenedorAccionEnviarForm_new input[name="cliente"]').val() == "" || $('#contenedorAccionEnviarForm_new textarea[name="obs"]').val() == "" ){
			$('#valida2').show();
		}
		else{
			$('#valida2').hide();
			enviaCorreo_new();
		}
		$("#enviarCorreoSeguimiento_new").prop("disabled", false);
		
	});
	
	function enviaCorreo(){
		$('#enviarCorreoSeguimiento').attr('disabled','disabled');
		
		var desde			= $('#contenedorAccionEnviarForm input[name="desde"]').val();
		var desde_nom		= $('#contenedorAccionEnviarForm input[name="desde_nom"]').val();
		var para				= $('#contenedorAccionEnviarForm textarea[name="para"]').val();
		var copia			= $('#contenedorAccionEnviarForm textarea[name="copia"]').val();
		var asunto			= $('#contenedorAccionEnviarForm input[name="asunto"]').val();
		var table			= $('#contenedorAccionEnviarForm input[name="table"]').val();
		var nodos			= $('#contenedorAccionEnviarForm input[name="table"]').val();
		var numin			= $('#contenedorAccionEnviarForm input[name="numin"]').val();
		var cliente			= $('#contenedorAccionEnviarForm input[name="cliente"]').val();
		var clientehid		= $('#contenedorAccionEnviarForm input[name="clientehid"]').val();
		var estactividad	= $('#contenedorAccionEnviarForm input[name="estactividad"]').val();
		var desevento		= $('#contenedorAccionEnviarForm textarea[name="desevento"]').val();
		var accrealiz		= $('#contenedorAccionEnviarForm textarea[name="accrealiz"]').val();
		var obs				= $('#contenedorAccionEnviarForm textarea[name="obs"]').val();
		var tipoevento		= $('#contenedorAccionEnviarForm textarea[name="tipoevento"]').val();
		var alaeq			= $('#contenedorAccionEnviarForm textarea[name="alaeq"]').val();
		var codservicio	= $('#contenedorAccionEnviarForm textarea[name="codservicio"]').val();
		var sucursales		= $('#contenedorAccionEnviarForm textarea[name="sucursales"]').val();
		var firma			= $('#contenedorAccionEnviarForm textarea[name="firma"]').val();
		var fecini			= $('#contenedorAccionEnviarForm input[name="fecini"]').val();
		var fecter			= $('#contenedorAccionEnviarForm input[name="fecter"]').val();
		
		var nombre			= $('#contenedorAccionEnviarForm input[name="nombre"]').val();
		var email			= $('#contenedorAccionEnviarForm input[name="email"]').val();
		var fono			= $('#contenedorAccionEnviarForm input[name="fono"]').val();
		var segid			= $('#contenedorAccionEnviarForm input[name="id_seguimiento"]').val();
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/sendEmail",
			data: {desde:desde, desde_nom:desde_nom, para:para, clientehid:clientehid, copia:copia, alaeq:alaeq, codservicio:codservicio, sucursales:sucursales, firma:firma, fecini:fecini, fecter:fecter, tipoevento:tipoevento, asunto:asunto, nombre:nombre, email:email, fono:fono, table:table, numin:numin, cliente:cliente, estactividad:estactividad, desevento:desevento, accrealiz:accrealiz, obs:obs, nodos:nodos, segid:segid},
			success: function(data) {
				window.location.reload();
				window.opener.AfterSendEmail();
			}
		});
	};
	
	function enviaCorreo_new(){
		$('#enviarCorreoSeguimiento_new').attr('disabled','disabled');
		
		var d = new Date();
		var fechain = d.toLocaleString();
		
		var desde			= $('#contenedorAccionEnviarForm_new input[name="desde"]').val();
		var desde_nom		= $('#contenedorAccionEnviarForm_new input[name="desde_nom"]').val();
		var para				= $('#contenedorAccionEnviarForm_new textarea[name="para"]').val();
		var copia			= $('#contenedorAccionEnviarForm_new textarea[name="copia"]').val();
		var asunto			= $('#contenedorAccionEnviarForm_new input[name="asunto"]').val();
		var table			= $('#contenedorAccionEnviarForm_new input[name="table"]').val();
		var nodos			= $('#contenedorAccionEnviarForm_new input[name="table"]').val();
		var numin			= $('#contenedorAccionEnviarForm_new input[name="numin"]').val();
		var cliente			= $('#contenedorAccionEnviarForm_new input[name="cliente"]').val();
		var clientehid		= $('#contenedorAccionEnviarForm_new input[name="clientehid"]').val();
		var obs				= $('#contenedorAccionEnviarForm_new textarea[name="obs"]').val();
		var tipoevento		= $('#contenedorAccionEnviarForm_new textarea[name="tipoevento"]').val();
		var alaeq			= $('#contenedorAccionEnviarForm_new textarea[name="alaeq"]').val();
		var codservicio	= $('#contenedorAccionEnviarForm_new textarea[name="codservicio"]').val();
		var sucursales		= $('#contenedorAccionEnviarForm_new textarea[name="sucursales"]').val();
		var firma			= $('#contenedorAccionEnviarForm_new textarea[name="firma"]').val();
		var fecini			= $('#contenedorAccionEnviarForm_new input[name="fecini"]').val();
		var fecter			= $('#contenedorAccionEnviarForm_new input[name="fecter"]').val();
		var nombestab		= $('#contenedorAccionEnviarForm_new input[name="nombestab"]').val();
		var desevento		= $('#contenedorAccionEnviarForm_new textarea[name="desevento"]').val();
		
		var estserv			= $('#contenedorAccionEnviarForm_new select[name="estserv"]').val();
		var estservlan		= $('#contenedorAccionEnviarForm_new select[name="estservlan"]').val();
		var estinc			= $('#contenedorAccionEnviarForm_new select[name="estinc"]').val();
		var enlaceprin		= $('#contenedorAccionEnviarForm_new select[name="enlaceprin"]').val();
		var enlaceresp		= $('#contenedorAccionEnviarForm_new select[name="enlaceresp"]').val();
		
		var nombre			= $('#contenedorAccionEnviarForm_new input[name="nombre"]').val();
		var email			= $('#contenedorAccionEnviarForm_new input[name="email"]').val();
		var fono			= $('#contenedorAccionEnviarForm_new input[name="fono"]').val();
		var segid			= $('#contenedorAccionEnviarForm_new input[name="id_seguimiento"]').val();
		$.ajax({
			type: "POST",
			url: "c_seguimientoEmail/sendEmail_new",
			data: { fechactual:fechain, desde:desde, desde_nom:desde_nom, para:para, clientehid:clientehid, copia:copia, alaeq:alaeq, codservicio:codservicio, sucursales:sucursales, firma:firma, fecini:fecini, fecter:fecter, tipoevento:tipoevento, asunto:asunto, nombre:nombre, email:email, fono:fono, table:table, numin:numin, cliente:cliente, obs:obs, nodos:nodos, segid:segid, nombestab:nombestab, estserv:estserv, estservlan:estservlan, estinc:estinc, enlaceprin:enlaceprin, enlaceresp:enlaceresp, desevento:desevento},
			success: function(data) {
				// console.log(data)
				window.location.reload();
				window.opener.AfterSendEmail();
			}
		});
	};
</script>

</body>
</html>