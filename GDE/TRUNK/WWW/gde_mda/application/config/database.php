<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname']		= '127.0.0.1';
$db['default']['username']		= 'root';
$db['default']['password']		= 'root';
$db['default']['database']		= 'gestor_eventos_rsp';
$db['default']['dbdriver']		= 'mysql';
$db['default']['dbprefix']		= '';
$db['default']['pconnect']		= TRUE;
$db['default']['db_debug']		= FALSE;
$db['default']['cache_on']		= FALSE;
$db['default']['cachedir']		= '';
$db['default']['char_set']		= 'utf8';
$db['default']['dbcollat']		= 'utf8_general_ci';
$db['default']['swap_pre']		= '';
$db['default']['autoinit']		= TRUE;
$db['default']['stricton']		= FALSE;

$db['estadistica']['hostname']	= '127.0.0.1';
$db['estadistica']['username']	= 'root';
$db['estadistica']['password']	= 'root';
$db['estadistica']['database']	= 'estadistica';
$db['estadistica']['dbdriver']	= 'mysql';
$db['estadistica']['dbprefix']	= '';
$db['estadistica']['pconnect']	= TRUE;
$db['estadistica']['db_debug']	= FALSE;
$db['estadistica']['cache_on']	= FALSE;
$db['estadistica']['cachedir']	= '';
$db['estadistica']['char_set']	= 'utf8';
$db['estadistica']['dbcollat']	= 'utf8_general_ci';
$db['estadistica']['swap_pre']	= '';
$db['estadistica']['autoinit']	= TRUE;
$db['estadistica']['stricton']	= FALSE;
