<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>GDE -> Ticket</title>
	<link rel="shortcut icon" href="../images/bancoestado.ico" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="Sat, 26 Jul 1997 05:00:00 GMT">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/gde_entel.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ticket.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/ie.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ticket/jquery-datatables.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ticket/jquery-datatables.fnMultiFilter.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ticket/ticket.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ticket/spin.js"></script>
	<script src="<?php echo base_url(); ?>js/cambiar_estado.js" type="text/javascript"></script>
</head>
<body>
	<div class="container" id="row_header" style="margin-top:0px;">
		<div class="row">
			<div class="twelve columns">
				<div class="row">
					<div class="twelve columns">
						<header id="header">
							<hgroup>
								<h1 class="site_title">
								</h1>
								<h2 class="section_title">
									Gestor de Eventos ENTEL v3.8
									<div id="estado_actual">
										<span id="state-now"></span>
									</div>
								</h2>
								<div class="btn_view_site"><?php echo anchor('c_login/salir','Salir'); ?></div>
							</hgroup>
						</header>
					</div>
				</div>
				<div class="row">
					<section id="secondary_bar">
							<!--<div class="four columns ">
								<div class="user">
									<p><?php echo $this->session->userdata('nombre'); ?> <button id="changeSTT">Cambiar estado</button></p>
								</div>
							</div>-->
							<div class="four columns">
								<div id="title_menu_header" class="menu_header row" style="width:230px">
									<div id="sem_menu_header" class="sem_menu_header_green two columns" >
										0
									</div>
									<div id="usr_menu_header" class="eight columns" >
										<?php echo $this->session->userdata('nombre'); ?>
									</div>
									<div id="sel_menu_header" class="two columns" ></div>
								</div>
								
								<div id="options_menu_header" class="menu_header row" style="width:215px">
									<div class="twelve columns">
										<div id="displayCambiarStt" class="option_menu_header row" >Cambiar Estado</div>
										<div id="displayCorreoSeg" class="option_menu_header row" >Seguimiento Correo</div>
									</div>
								</div>
							</div>
							<div class="eight columns ">
								<dl class="sub-nav" style="margin-top: 5px; margin-left: 15px;">
									<dt>Navegación:</dt>
									<dd><a href="<?php echo base_url(); ?>index.php/operador/c_principal"> Eventos </a></dd>
									<dd><a href="<?php echo base_url(); ?>index.php/operador/c_secundario"> Estadísticas </a></dd>
									<dd class="active"><a href="<?php echo base_url(); ?>index.php/operador/c_ticket"> Ticket </a></dd>
								</dl>
							</div>
					</section>
				</div>
			</div>
		</div>
	
	
	<!-- Cuerpo -->
	<div class="row">
		
		<!-- Filtros -->
		<div class="two columns" style="overflow: hidden;">
			<div class="contenedor_filtros izquierdo" >
				<div id="grupo_filtros">
					<?php
						foreach($filtros as $filtro => $tabla){
							echo '<span class="filtro_titulo">' . $titleFiltros[$filtro][0] . '</span>';
							echo '<div class="grupo" id="filtro_' . $filtro . '">' . $tabla . '</div>';
						}
					?>
				</div>
				<div class="grupo_texto">
					<span class="filtro_titulo">Buscar palabra:</span>
					<input class="filtro filtro_texto" name="texto" type="text" value="" />
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
			
		<!-- Tabla -->
		<div class="ten columns" >
			
			<!-- Cambiar Estado -->
			<div class="row">
				<div class="twelve columns" style="padding-top: 10px;">
						<div class="mov_contenedor_header left">^^</div>
						<input class="right" type="submit" value="Cerrar Ticket" id="subCerrarTicket" disabled="disabled" style="margin-top: 0;"/>
						<div class="right"><span id="cant_select">0</span> Tickets <span id="nom_select"></span> seleccionados </div>
				</div>
			</div>
				
			<!-- Tabla -->
			<div class="row" >
				<div class="twelve columns">
					<div class="row">
						<div class="twelve columns">
							<table id="eventos" cellspacing="0">
								<thead>
									<tr>
										<th><input type="checkbox" name="checkall" id="checkall"></th>
										<th>Estado</th>
										<th>Creación</th>
										<th>ID</th>
										<th>Cliente</th>
										<th>Ac.</th>
										<th>Equipo</th>
										<th>Revisión en</th>
										<th>Opciones</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 10px;">
				<ul class="block-grid three-up">
					<!-- Bloque Datos Ticket -->
					<li>
						<div class="bloque_datos">
							<p style="background-color: #b8b9c8;">Datos Ticket<br /><span class="boxTitle"></span></p>
							<div id="bloqueDatos" class="boxInside" style="margin-top: 5px;">
							</div>
						</div>
					</li>
					<!-- Bloque Nodos Asociados -->
					<li>
						<div class="bloque_datos">
							<p style="background-color: #ffb000;">Nodos Asociados al Ticket<br /><span class="boxTitle"></span></p>
							<div id="bloqueNodosAsociados" class="boxInside">
							</div>
						</div>
					</li>
					<!-- Bloque Eventos Nodo -->
					<li>
						<div class="bloque_datos">
							<p style="background-color: #7fb0db;">Eventos del Nodo<br /><span class="boxTitle"></span></p>
							<div id="bloqueEventosNodos" class="boxInside">
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="columns" >
		</div>
	</div>
</div>
	<div id="dialog_cerrar" title="Cerrar Ticket">
		<input type="radio" name="opc" value="cerrar" checked="checked">Cerrar ticket</input><br/><br/>
		<input type="radio" name="opc" value="noconf">Cliente no conforme</input><br/><br/>
		<input type="radio" name="opc" value="cambiar">Cambiar motivo de estado a observación</input>
	</div>
	<div id="dialog_cerrarconfirm" title="Acción de cierre">
	</div>
	<div id="dialog_cerrarresult" title="Respuesta a Cierre">
	</div>
	<div id="cambiar_estado">
		<form id="datos_cambio_turno">
			<fieldset>
				<table>
					<tbody>
						<tr><td><input type="radio" name="estado" value="0"/></td><td>Trabajando</td></tr>
						<tr><td><input type="radio" name="estado" value="1"/></td><td>Receso</td></tr>
						<!-- <tr><td><input type="radio" name="estado" value="2"/></td><td>Cambio de turno</td></tr> -->
					</tbody>
				</table>
			</fieldset>
		</form>
	</div>
	
<!--FIN SECCION DE DIALOGS-->
	<script type="text/javascript">
		$('#displayCorreoSeg').click(function(){
			window.open('../c_seguimientoEmail', '', 'width=1300, height=600, location=no');
		});
	</script>
</body>
</html>