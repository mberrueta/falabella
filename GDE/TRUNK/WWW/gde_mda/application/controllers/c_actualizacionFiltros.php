<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_actualizacionFiltros extends CI_Controller {
	function __construct(){
		parent::__construct();

	}
	
	public function actualizacionFiltros(){

		$resultFiltrosBases		= $this->actualizarFiltrosBaseSP();
		//$resultAgrupacion		= $this->actualizarAgrupacionSP();
		
		if($resultFiltrosBases){
			log_message('debug', 'resultFiltrosBases: ' . $resultFiltrosBases);
		}
		else{
			log_message('error', 'Problema en la ejecucion de la llamada al procedimiento almacenado CREATE_FILTRO() ');
			log_message('error', $this->db->_error_message());
		}
		
		return;
	}
	
	public function actualizarFiltrosBaseSP(){
		return $this->db->simple_query("CALL CREATE_FILTRO();");
	}
	
	/*public function actualizarAgrupacionSP(){
		return $this->db->simple_query("CALL explode_table('|');");
	}*/

}

/* End of file c_actualizacionfiltros.php.php */
/* Location: ./application/controllers/c_actualizacionfiltros.php.php */
