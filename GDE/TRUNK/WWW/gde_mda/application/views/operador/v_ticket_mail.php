<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Formulario de Correo</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	<style>
	.ajax_loader {
		background: url("spinner_squares_circle.gif") no-repeat center center transparent;
		width:100%;
		height:100%;
	}
	.blue-loader .ajax_loader {
		background: url("ajax-loader_blue.gif") no-repeat center center transparent;
	}
	</style>
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/gen_validatorv4.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/ajax-loader/ajax_loader.js" type="text/javascript"></script>
</head>
<body >
	
	<div class="row">
		<div class="twelve columns centered">
		<?php echo form_open('/operador/c_ticket/sendEmailTicket', array('id' => 'form'));?>
			<fieldset>
				<legend>Correo a Cliente</legend>
				
				<label for="ticket_desde">Desde: </label>
				<input type="text" class="unselectable" name="ticket_desde" id="ticket_desde" value="Correo GDE" readonly="readonly"/>
				<label for="ticket_para">*Para: </label>
				<textarea name="ticket_para" id="ticket_para" value="" rows="3" cols="50"><?php echo $_POST["Correos"]?></textarea>
				<label for="ticket_cc">*Copia: </label>
				<textarea name="ticket_cc" id="ticket_cc" value="" rows="3" cols="50"><?php echo $_POST["copia"]?></textarea>
				<label for="ticket_asunto">*Asunto: </label>
				<input type="text" id="ticket_asunto" name="ticket_asunto" value="<?php echo $_POST["asunto"]?>"/>
				
				<hr>
				
				<label for="ticket_id">N&ordm; Incidente: </label>
				<input type="text" id="ticket_id" name="ticket_id" value="<?php echo $_POST["idTicket"]?>"/>
				<label for="cliente">*Cliente: </label>
				<input type="text" id="cliente" name="cliente" value="<?php echo $_POST["cliente"]?>"/>
				<input type="text" style="visibility:hidden" name="clientehid" value="<?php echo $_POST["cliente"]?>" />
				
				<label for="tipo_alarm">Tipo de Alarma: </label>
				<textarea id="tipo_alarm" name="tipo_alarm" rows="3" cols="50"><?php echo $_POST["tipo"]?></textarea>
				<label for="equipo">Alarma de Equipo: </label>
				<textarea id="equipo" name="equipo" rows="3" cols="50"><?php echo $_POST["equipo"]?></textarea>
				<label for="codigo">Codigo de Servicio: </label>
				<textarea id="codigo" name="codigo" rows="3" cols="50"><?php echo $_POST["codigo"]?></textarea>
				<label for="sucursal">Direccion: </label>
				<textarea id="sucursal" name="sucursal" rows="3" cols="50"></textarea>
				
				<label for="actividad">Estado Actividad: </label>
				<input type="text" id="actividad" name="actividad"/>
				<label for="fecha_inicio">Fecha Inicio Incidente: </label>
				<input type="text" id="fecha_inicio" name="fecha_inicio" value="<?php echo $_POST["fecha_inicio"]?>"/>
				<label for="fecha_termino">Fecha Termino Incidente: </label>
				<input type="text" id="fecha_termino" name="fecha_termino" value=""/>
				
				<label for="desc_evento">Descripción del Evento: </label>
				<textarea name="desc_evento" id="desc_evento" value="" rows="3" cols="50"></textarea>
				<label for="acc_reali">Acciones Realizadas: </label>
				<textarea name="acc_reali" id="acc_reali" value="" rows="3" cols="50"></textarea>
				<label for="observaciones">Observaciones: </label>
				<textarea name="observaciones" id="observaciones" value="" rows="3" cols="50"></textarea>
				<input type="text" style="visibility:hidden" name="clientehid" value="<?php echo $_POST["cliente"]?>" />
				
				<hr>
				
				<label><b>Firma</b></label>
				<br>
				<label for="nombre">Nombre: </label>
				<input type="text" id="nombre" name="nombre" value="<?php echo $_POST["nombre"]?>" />
				<label for="firma">Glosa Firma: </label>
				<textarea name="firma" id="firma" rows="3" cols="50">Operador Centro Monitoreo Servicios Clientes
Subgerencia de Monitoreo y Disponibilidad de Servicios ENTEL S.A.</textarea>
				<label for="email">E-mail: </label>
				<input type="text" id="email" name="email" value="<?php echo $_POST["copia"]?>" />
				<label for="fono">Telefono: </label>
				<input type="text" id="fono" name="fono" value="<?php echo $_POST["telefono"]?>" />
				<hr>
				<label id="msj_error" ></label>
				<input type="button" id="btnEnviar" class="button secondary" value="Enviar" style="margin-left:33%;"/>
				<input type="button" id="btncancelar" class="button secondary" value="Cerrar" />
			</fieldset>
		<?php echo form_close();?>
		
		<script  type="text/javascript">
		
			function validateEmail(field) {
				var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
				return (regex.test(field)) ? true : false;
			}
			
			function validateMultipleEmailsCommaSeparated(text) {
				var result = text.split(",");
				for (var i = 0; i < result.length; i++) {
					var trimmed = result[i].trim();
					if (trimmed != '') {
						if (!validateEmail(trimmed)) {
							return false;
						}
					}
				}
				return true;
			}
			
			var frmvalidator = new Validator("form");
			frmvalidator.addValidation("ticket_para","req","Favor llenar campo PARA ");
			frmvalidator.addValidation("ticket_cc","req","Favor llenar campo COPIA ");
			frmvalidator.addValidation("ticket_asunto","req","Favor llenar campo ASUNTO");
			frmvalidator.addValidation("cliente","req","Favor llenar campo CLIENTE");
			frmvalidator.setAddnlValidationFunction(validateMultipleEmailsCommaSeparated);
		</script>
		
		</div>
	</div>
	<script type="text/javascript">
		
		$('#btncancelar').click(function(){
			window.close();
		});
		
		$('#btnEnviar').click(function(){
			if($('#ticket_para').val() == ''){
				alert("Favor llenar campo PARA ");
				$('#ticket_para').focus();
			}
			else if($('#ticket_cc').val() == ''){
				alert("Favor llenar campo COPIA ");
				$('#ticket_cc').focus();
			}
			else if($('#ticket_asunto').val() == ''){
				alert("Favor llenar campo ASUNTO");
				$('#ticket_asunto').focus();
			}
			else if($('#cliente').val() == ''){
				alert("Favor llenar campo CLIENTE");
				$('#cliente').focus();
			}
			else if(!validateMultipleEmailsCommaSeparated($('#ticket_para').val())){
				alert("Favor verificar los correos");
				$('#ticket_para').focus();
			}
			else if(!validateMultipleEmailsCommaSeparated($('#ticket_cc').val())){
				alert("Favor verificar los correos");
				$('#ticket_cc').focus();
			}
			else {
				$('#btnEnviar').attr('disabled','disabled');
				$.ajax({
					url: "c_ticket/sendEmailTicket",
					type: "POST",
					data: { 'ticket_para' : $('#ticket_para').val(), 'ticket_cc' : $('#ticket_cc').val(), 'ticket_asunto' : $('#ticket_asunto').val(), 'equipo' : $('#equipo').val(), 'ticket_id' : $('#ticket_id').val(), 'cliente' : $('#cliente').val(), 'sucursal' : $('#sucursal').val(), 'clientehid' : $('#clientehid').val(), 'tipo_alarm' : $('#tipo_alarm').val(), 'codigo' : $('#codigo').val(), 'actividad' : $('#actividad').val(), 'fecha_inicio' : $('#fecha_inicio').val(), 'fecha_termino' : $('#fecha_termino').val(), 'desc_evento' : $('#desc_evento').val(), 'acc_reali' : $('#acc_reali').val(), 'observaciones' : $('#observaciones').val(), 'nombre' : $('#nombre').val(), 'firma' : $('#firma').val(), 'email' : $('#email').val(), 'fono' : $('#fono').val() },
					success: function(data){
						console.log(data);
						if (data == 'OK'){
							$('#msj_error').removeAttr('style');
							$('#msj_error').attr('style','visibility:hidden');
							$('#btnEnviar').removeAttr('disabled');
							alert('Correo enviado exitosamente');
						}
						else{
							$('#msj_error').empty();
							$('#msj_error').attr('style','color:red');
							$('#msj_error').append('Problemas al enviar el correo. Intentelo nuevamente');
							$('#btnEnviar').removeAttr('disabled');
						}
					}
				});
			}
			
		});
		
		// function load(){
			
			// $.ajax({
				// type: "POST",
				// url: "c_ticket/getSucursal",
				// data: {nodo: nodo},
				// dataType: 'json',
				// success: function(data) {
					// console.log($('#equipo').val());
					// $('#form textarea[name="sucursal"]').val(data);
				// }
			// });
		// }
	</script>
</body>
</html>