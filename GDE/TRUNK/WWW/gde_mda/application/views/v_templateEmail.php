<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Email Template Gestor de Eventos</title>
</head>
<body style="color: #004f94;">
<table border="0" cellspacing="0" cellpadding="0" width="750px" style="border-collapse:collapse">
	<tbody>
		<tr>
			<td width="320" colspan="2" style="width:239.65pt;border:solid silver 1.0pt;border-top:ridge #2c6f76 9.0pt;padding:0cm 5.4pt 0cm 5.4pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#00264c">Centro de Monitoreo de Servicios Clientes<u></u><u></u></span></p>
			</td>
			<td width="238" style="width:178.15pt;border-top:ridge #2c6f76 9.0pt;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:0cm 5.4pt 0cm 5.4pt">
				<span style="font-size: 50px;font-family: Gisha;font-weight: bold;color: #0068A1;">e</span>
				<span style="font-size: 25px;font-family: Arial Black;font-weight: bolder;color:#ef8e01;margin-left: -4px;vertical-align: 6px;">)</span>
				<span style="font-size: 35px;font-family: Gisha;font-weight: bold;color: #0068A1;margin-left: 4px;vertical-align: 3px;">entel</span>
			</td>
		</tr>
		<tr style="min-height:26.45pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#d0d8e8;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Nº Incidente:<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0cm;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">
					<?php echo $numin;?>
				<u></u><u></u></span></b></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Cliente: <u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $cliente;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Tipo de Alarma:&nbsp;<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php 
						echo nl2br($tipoevento);
					?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:9.1pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:9.1pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Alarma de Equipo<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:9.1pt">
				<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php 
						echo nl2br($alaeq);
					?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr>
			<td width="171" style="width:128.1pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:none;background:#d0d8e8;padding:0cm 5.4pt 0cm 5.4pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Código de Servicio:<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt">
				<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php 
						echo nl2br($codservicio);
					?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		
		<tr>
			<td width="171" style="width:128.1pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:none;background:#d0d8e8;padding:0cm 5.4pt 0cm 5.4pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Direccion<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt">
				<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php 
						echo nl2br($sucursales);
					?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		
		<tr style="min-height:36.65pt">
			<td width="171" style="width:128.1pt;border:none;border-left:solid silver 1.0pt;background:#d0d8e8;padding:0cm 5.4pt 0cm 5.4pt;min-height:36.65pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Estado Actividad:<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" style="width:289.7pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:none;border-right:solid silver 1.0pt;background:#d0d8e8;padding:0cm 5.4pt 0cm 5.4pt;min-height:36.65pt"><p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0cm;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">
				<?php echo $estactividad;?>
			</span></b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><u></u><u></u></span></p></td>
		</tr>
		<tr style="min-height:34.75pt">
			<td width="171" style="width:128.1pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:34.75pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Hora Inicio Incidente:<u></u><u></u></span></b></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e"><u></u>&nbsp;<u></u></span></b></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Hora Término Incidente: <u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:34.75pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $fecini;?>
				<u></u><u></u></span></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><u></u>&nbsp;<u></u></span></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
				<u></u><u></u></span></p>
				
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $fecter;?>
				<u></u><u></u></span></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><u></u>&nbsp;<u></u></span></p><p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:36.45pt">
			<td width="171" style="width:128.1pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:36.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Descripción de Incidente:&nbsp; <u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid #bfbfbf 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:36.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
					<?php echo nl2br($desevento);?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:276.0pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:276.0pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Acciones realizadas: <u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:276.0pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify;line-height:115%"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo nl2br($accrealiz);?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:35.45pt">
			<td width="171" style="width:128.1pt;border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:35.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Observaciones:<u></u><u></u></span></b></p>
			</td>
			<td width="386" colspan="2" valign="top" style="width:289.7pt;border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:35.45pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:0cm;text-align:justify"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo nl2br($obs);?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:35.45pt">
			<td width="557" colspan="3" valign="top" style="width:417.8pt;border-top:none;border-left:solid silver 1.0pt;border-bottom:ridge #2c6f76 7.5pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:35.45pt">
				<p class="MsoNormal" style="margin-bottom:2.0pt"><b><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Atentamente</span></i></b><b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">,<u></u><u></u></span></b></p><p class="MsoNormal" style="margin-bottom:2.0pt"><b><u><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><u></u><span style="text-decoration:none">&nbsp;</span><u></u></span></u></b></p><p class="MsoNormal" style="margin-bottom:2.0pt"><b><i><u><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#1f497d">
					<?php echo $nombre;?>
				</span></u></i></b></p><p class="MsoNormal" style="margin-bottom:2.0pt"><b><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><?php echo nl2br($firma)?><u></u><u></u></span></i></b></p><p class="MsoNormal" style="margin-bottom:2.0pt"><b><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
					E-mail: <u><a href="mailto:<?php echo $email;?>" target="_blank">
						<?php echo $email;?></a></u><u></u><u></u></span></i></b></p><p class="MsoNormal" style="margin-bottom:2.0pt"><b><i><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
					Fono: 
						<?php echo $fono;?>
					</span></i></b><b><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e"><u></u><u></u></span></b></p>
			</td>
		</tr>
		<tr>
			<td width="171" style="width:128.25pt;padding:0cm 0cm 0cm 0cm"></td>
			<td width="149" style="width:111.75pt;padding:0cm 0cm 0cm 0cm"></td>
			<td width="238" style="width:178.5pt;padding:0cm 0cm 0cm 0cm"></td>
		</tr>
	</tbody>
</table>
</body>
</html>