<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Editar</h3>
			</header>
			<div style="margin:15px;"> 
		<?php $hidden = array('nombre' => $nombre);
			echo form_open('supervisor/c_editar_detalle_cliente/editar_asociar_cliente','',$hidden);?>
		<div style="width: 100%;">
			<!--<table width="40%" border="0" cellspacing="2" cellpadding="2">-->
			<table border="0" cellspacing="0" cellpadding="0" style="width: 25%; float: left; margin-right: 5%;">
				<thead style="background-color: #0073AE; color: white;">
					<tr>
						<th>Atributo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Usuario:</td>
						<td>
							<input type="text" id="cNombre2" name="cNombre2" readonly value="<?php echo urldecode($nombre); ?>" />
						</td>
						<td><span id="nameInfo"></span></td>
					</tr>
			</table>
			<div style="padding-bottom: 1%; clear:both;"></div>
		</div>
		<?php
		if($men != ''){
			$mensaje = explode('|',$men);
			echo '<span class="'.$mensaje[0].'">'.$mensaje[1].'</span>';
		}
		
		if($final != ''){
			echo '<h4>Asignación Actual</h4>';
			echo '<table id="clientes" cellspacing="0" cellpadding="0" border="0" width="100%">';
			echo '<thead>';
				echo '<tr">';
				echo '<th>Area Responsable</th>';
				echo '<th>Origen</th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
					echo '<tr>';
					echo '<td>';
					if(isset($final['Organization'])){
						echo $final['Organization'];
					}
					echo '</td>';
					echo '<td>';
					if(isset($final['Herramienta'])){
						echo $final['Herramienta'];
					}
					echo '</td>';
					echo '<tr>';
			echo '<tbody>';
			echo '<table>';
			echo '<br />';
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		
		if($Organization['stt']){
			echo '<h4>Reasignación</h4>';
			echo '<table id="operadores" cellspacing="0" cellpadding="0" border="0" width="100%">';
			echo '<thead>';
				echo '<tr">';
				echo '<th>Area Responsable</th>';
				echo '<th>Origen</th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			echo '<tr>';
			echo '<td><select class="usuarios" id="selectOperador" name="operadores[]" size="10" multiple="multiple">';
				echo '<option value="All">All</option>';
				foreach($Organization['cli']->result() as $row) :
					echo '<option value="'.$row->src_organization.'">'.$row->src_organization.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select class="usuarios" id="selectBackup" name="backup[]" size="4" multiple="multiple">';
				echo '<option value="All">All</option>';
				echo '<option value="Nagios">Nagios</option>';
				echo '<option value="Spectrum">Spectrum</option>';
				echo '<option value="Splunk">Splunk</option>';
			echo '</select></td>';
			echo '</tr>';
			echo '</tbody>';
			echo '</table>';
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		
		echo form_submit('asociar','Actualizar');
		echo form_close();
		
		?><input type="hidden" id="url_js" value="<?php echo site_url(); ?>">
			</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/editar_detalle_cliente.js"></script>->

</body>
</html>