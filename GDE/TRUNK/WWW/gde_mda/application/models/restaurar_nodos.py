#!/usr/bin/python
import MySQLdb
import MySQLdb.cursors
import csv
import logging
import sys
from datetime import datetime

'''
SELECT * FROM `dispositivo`
JOIN `arquitectura_dispositivo` USING (DIS_NOM)
JOIN `contacto_dispositivo`  USING (DIS_NOM)
JOIN `geografia_dispositivo`  USING (DIS_NOM)
'''

#Se declara instancia de log
logger    = logging.getLogger('Restauracion_de_Nodos')
hdlr      = logging.FileHandler('/var/tmp/restauracion_nodos.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)

#MySQL params
host        = "localhost"
user        = "root"
password    = "gdekudaw"
db          = "gestor_eventos_rsp"
myDb        = None

ruta        = sys.argv[1]

try:
	#Declaracion de query INSERT en tablas
	prev_dis  = "INSERT INTO dispositivo (CLI_ID, DIS_NOM, DIS_CODSERV, DIS_IPPRINC, DIS_CSPRINC, DIS_CODADMIN, DIS_CRIT, DIS_SLA, DIS_ATEN) VALUES "
	prev_geo = "INSERT INTO geografia_dispositivo (DIS_NOM, GEOD_REGION, GEOD_PROVINCIA, GEOD_COMUNA, GEOD_SUCURSAL) VALUES "
	prev_arq = "INSERT INTO arquitectura_dispositivo (DIS_NOM, ARQ_FW, ARQ_PE, ARQ_TUSER, ARQ_TPASS, ARQ_TEPASS, ARQ_SNPM) VALUES "
	prev_cnt  = "INSERT INTO contacto_dispositivo (DIS_NOM, CNT_CNTHABIL, CNT_TELHABIL, CNT_CELHABIL, CNT_CORHABIL, CNT_CNTNOHABIL, CNT_TELNOHABIL, CNT_CELNOHABIL, CNT_CORNOHABIL) VALUES "
	
	#Coneccion a BD
	logger.info("Conectando con la Base de Datos")
	myDb = MySQLdb.connect(host, user, password, db, cursorclass=MySQLdb.cursors.DictCursor)
	myCurs = myDb.cursor()

	#Se obtienen los ID de cliente
	myCurs.execute("SELECT CLI_ID, CLI_NOM FROM cliente;")
	myDb.commit()
	
	clientes = {}
	rows = myCurs.fetchall()
	for cliente in rows:
		clientes[cliente['CLI_NOM']] = cliente['CLI_ID'] 
	
	logger.info("Se obtienen IDs de Clientes registrados en Base de Datos")

	#Se lee archivo entregado por ENTEL y se le asocia el ID de Cliente de la BD
	reader   = csv.DictReader(open('/var/www/html/fase_1/backup_nodos/'+ruta, 'rb'), delimiter='\t')
	logger.info("Se lee archivo con detalle de Nodos")
	
	#Se limpian las tablas a tratar
	myCurs.execute("TRUNCATE TABLE dispositivo;")
	myDb.commit()
	myCurs.execute("TRUNCATE TABLE geografia_dispositivo;")
	myDb.commit()
	myCurs.execute("TRUNCATE TABLE arquitectura_dispositivo;")
	myDb.commit()
	myCurs.execute("TRUNCATE TABLE contacto_dispositivo;")
	myDb.commit()

	logger.info("Se limpian las tablas a tratar:\n a) dispositivo\n b) geografia_dispositivo\n c) arquitectura_dispositivo\n d) contacto_dispositivo")
	
	#Se procede con insercion en tabla
	vals_dsp, vals_geo, vals_arq, vals_cnt = [], [], [], []
	flag = 0

	logger.info("Se inicia insercion en tablas")
	
	for row in reader:
		if row['CLIENTE'] in clientes:
			#Se agrega insercion en tabla dispositivo
			tmp_dsp = [ myDb.escape_string(row['NODO']), myDb.escape_string(row['CODIGO DE SERVICIO']), myDb.escape_string(row['IP_PRINCIPAL']), myDb.escape_string(row['C/S_PRINCIPAL']), myDb.escape_string(row['CODIGO ADMIN']), myDb.escape_string(row['CRITICIDAD']), myDb.escape_string(row['SLA']), myDb.escape_string(row['Horario_Habil'])]
			tmp_geo = [myDb.escape_string(row['NODO']), myDb.escape_string(row['REGION']), myDb.escape_string(row['PROVINCIA']), myDb.escape_string(row['COMUNA']), myDb.escape_string(row['SUCURSAL'])]
			tmp_arq  = [myDb.escape_string(row['NODO']), myDb.escape_string(row['FW']), myDb.escape_string(row['PE']), myDb.escape_string(row['TELNET_USER']), myDb.escape_string(row['TELNET_PASS']), myDb.escape_string(row['TELNET_ENAB_PASS']), myDb.escape_string(row['SNMP'])]
			tmp_cnt  = [myDb.escape_string(row['NODO']), myDb.escape_string(row['Contacto_Habil']), myDb.escape_string(row['Telefono_Habil']), myDb.escape_string(row['Celular_Habil']), myDb.escape_string(row['Correo_Habil']), myDb.escape_string(row['Contacto_No_Habil']), myDb.escape_string(row['Telefono_No_Habil']), myDb.escape_string(row['Celular_No_Habil']), myDb.escape_string(row['Correo_No_Habil'])]
			
			vals_dsp.append("("+ str(clientes[row['CLIENTE']]) + ",'" + "','".join(tmp_dsp) +"')")
			vals_geo.append("('"+ "','".join(tmp_geo) +"')")
			vals_arq.append("('"+ "','".join(tmp_arq) +"')")
			vals_cnt.append("('"+ "','".join(tmp_cnt) +"')")

			flag += 1
			if flag >= 2000:
			
				myCurs.execute(prev_dis + ','.join(vals_dsp) + ";")
				myDb.commit()
				
				myCurs.execute(prev_geo + ','.join(vals_geo) + ";")
				myDb.commit()
				
				myCurs.execute(prev_arq + ','.join(vals_arq) + ";")
				myDb.commit()

				myCurs.execute(prev_cnt + ','.join(vals_cnt) + ";")
				myDb.commit()

				vals_dsp, vals_geo, vals_arq, vals_cnt = [], [], [], []
				flag = 0

	#Si quedan lineas por registrar se registran
	if len(vals_dsp) > 0:
		myCurs.execute(prev_dis + ','.join(vals_dsp) + ";")
		myDb.commit()

		myCurs.execute(prev_geo + ','.join(vals_geo) + ";")
		myDb.commit()
		
		myCurs.execute(prev_arq + ','.join(vals_arq) + ";")
		myDb.commit()

		myCurs.execute(prev_cnt + ','.join(vals_cnt) + ";")
		myDb.commit()
	
	fecha = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	myCurs.execute("UPDATE `respaldo_nodos` SET `RESPALDO_FECHA_USO`='"+fecha+"' WHERE `RESPALDO_RUTA`='"+ruta+"';")
	myDb.commit()
	myDb.close() #Cierre coneccion con bd
	
	logger.info("Se termina insercion en tablas")
	logger.info("Se cierra coneccion con Base de Datos")
	print "ok"
	
except:
	import traceback
	if myDb:
		myDb.close()
		
	stack = traceback.format_exc()
	print "error"
	logger.error("Salida forzada")
	logger.error("Problema: "+ str(stack))
