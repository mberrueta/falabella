<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_sendAutomaticTicket_hugo extends CI_Controller {
	
	public function index(){}
	
	private function getGroupEvents()
	{
		$this->db->select('id as ID');
		$this->db->from('grupo_evento_gde');
		$this->db->where('folio_mda = 0');
		$this->db->where('start_time >= NOW() - INTERVAL 240 MINUTE');
		$result = $this->db->get();
		return $result;
	}
	
	private function validateTicket($ID)
	{
		$this->db->select('folio_mda');
		$this->db->from('grupo_evento_gde');
		$this->db->where('id = '.$ID);
		$result = $this->db->get();
		return $result;
	}
	
	// Insert de prueba a BD
	// INSERT INTO `evento_gde` (`severity`, `status`, `start_time`, `type`, `src_name`, `src_subname`, `src_ip`, `src_organization`, `src_category`, `src_subcategory`, `src_tool`, `dest_bunit`, `dest_service`, `dest_country`, `body`, `src_system`) VALUES
	// ('CRITICAL', 'Activo', '2016-12-29 12:40:00', 'PROBLEMA OBJETIVO', 'Viajes Falabella Peru Vuelos', 'N/A', 'N/A', 'Atentus-ViajesFalabella1', 'Objetivo Atentus', 'Viajes Falabella', 'Spectrum', 'Viajes Falabella', 'N/A', 'Peru', 'Esto es un mensaje de prueba para integracion con Mesa de ayuda', 'No catalogado|')

	public function createTicket($ID = false){
		
		$this->load->model('webservices');
		$this->load->model('evento');
		
		$flagTicket = false;
		
		$isManual = $this->input->post('isManual');
		
		if($isManual){
			$ID = $this->input->post('ID');
			
			$response = $this->validateTicket($ID);
			
			if ($response->row()->folio_mda != 0 && $response->row()->folio_mda != -1){
				$flagTicket = true;
			}
		}
		
		if($flagTicket){
			echo $this->evento->obtenerComentarios($ID);
		} else {
			
			$intentos = 0;
			$rs       = -1;
			
			//Realiza dos intentos mas para obtener el ticket
			while($intentos < 3){
				$response = $this->webservices->crearTicket($ID);
				$response = rtrim($response, "\n");
				
				if($response == 'problem' or $response == 'fail' or $response == 'error'){
					$intentos = $intentos + 1;
				} else{
					$rs = $response;
					break;
				}
			}
			
			if ($rs == -1){
				$coment = 'ERROR en generación de ticket automático. Para creación manual haga click <button type="button" class="btn btn-success" id="crearTicketManual" >AQUI</button>';
				$this->evento->registrarComentario($ID, $coment, "gestor");
			} else{
				$coment = 'Ticket automático generado correctamente (Folio: '.$rs.')';
				$this->evento->registrarComentario($ID, $coment, "gestor");
			}
			
			$this->db->where('id', $ID);
			$this->db->update('grupo_evento_gde', array('folio_mda' => $rs));
			$this->db->flush_cache();
			
			if($isManual) {
				echo $this->evento->obtenerComentarios($ID);
			}
		}
	}
	
	public function create(){
		
		//Obtener la lista de grupos de eventos que se generará un ticket
		$gruposEventos = $this->getGroupEvents();

		//Envío de tickets 
		foreach($gruposEventos->result() as $ticket)
		{
			$ID = escapeshellcmd($ticket->ID);
			// Version Adessa
			// system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /u01/home/app/splunkge/id.log &");
			
			// Version Desarrollo
			system("/opt/lampp/bin/php /opt/lampp/htdocs/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /opt/lampp/htdocs/gde_test/id.log &");
		}
		
	}
	
	public function updateTicket($ID, $grupoId, $status, $comentario, $usuario){
		
		$this->load->model('webservices');
		$this->load->model('evento');
		
		// Validar estado anterior
		$detalles     = $this->webservices->consultaIncidente($ID);
		$statusActual = rtrim($detalles, "\n");
		
		if($statusActual == 'fail' or $statusActual == 'error'){
			// Problema con el Web Services.
			$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: Web Service con problemas.";
			$this->evento->registrarComentario($grupoId, $coment, $usuario);
			
		} else {
			
			if($statusActual == $status){
				// Avisar que ya fue actualizado.
				$coment = "El estado del Folio en Mesa de Ayuda ya se encuentra actualizado.";
				$this->evento->registrarComentario($grupoId, $coment, $usuario);
				
			} elseif ($statusActual == "Finalizado"){
				// Avisar que ya esta finalizado.
				$coment = "Folio en Mesa de Ayuda se encuentra en estado Finalizado.";
				$this->evento->registrarComentario($grupoId, $coment, $usuario);
				
			} else {
				// Actualizar el ticket.
				$retorno = $this->webservices->actualizaEstadoIncidente($ID, $status, $comentario);
				$retorno = rtrim($retorno, "\n");
				$output  = explode(',',$retorno);
				
				if($output[0] == 'fail' or $output[0] == 'error'){
					// Problema con el Web Services.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: Web Service con problemas.";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} elseif ($output[0] == 0){
					// Ok.
					$coment = "Estado de Folio actualizado en Mesa de Ayuda (".$status.")";
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				} else {
					// Problema con los datos.
					$coment = "Error en sincronizacion con Mesa de ayuda. Detalle: ".$output[1];
					$this->evento->registrarComentario($grupoId, $coment, $usuario);
				}
			}
		}
		
	}
	
	public function update(){
		// Usuario 
		$usuario = $this->session->userdata('username');
		
		//Obtener la lista de tickets a actualizar
		$tickets    = $this->input->post('aTicket');
		$grupoId    = $this->input->post('aGrupos');
		$estado     = $this->input->post('sEstado');
		$comentario = $this->input->post('sComentario');
		
		// Normalizacion de Estado GDE / MDA
		$estadoNorm     = "";
		
		if ($estado == "En Proceso"){
			$estadoNorm = "Trabajando";
		} else {
			$estadoNorm = "Finalizado";
		}
		
		$arrayGROID = explode(',',$grupoId);
		
		$index = 0;
		foreach(explode(',',$tickets) as $ticket)
		{
			if($ticket != "Sin Ticket" && $ticket != "ERROR"){
			
				$grupoId    = escapeshellcmd($arrayGROID[$index]);
				$ID         = escapeshellcmd($ticket);
				$status     = escapeshellcmd($estadoNorm);
				$comentario = escapeshellcmd($comentario);
				$usuario    = escapeshellcmd($usuario);
				
				// Version Desarrollo
				system('/opt/lampp/bin/php /opt/lampp/htdocs/gde_test/index.php c_sendAutomaticTicket updateTicket '.$ID.' '.$grupoId.' "'.$status.'" "'.$comentario.'" "'.$usuario.'" >> /opt/lampp/htdocs/gde_test/id.log &');
			}
			
			$index++;
		}
		
	}

	private function syncMsg($ID,$json){
		
		#Obtenemos todos los camentarios del grupo
		$this->db->select('*');
		$this->db->from('comentario_gde');
		$this->db->where('grupo_id',$ID);
		$result = $this->db->get();


		return $result;
	}

	public function syncTicket(){
		$this->load->model('webservices_hugo');
		
		//Obtener la lista de grupos de eventos que se generará un ticket
		$gruposEventos = $this->getGroupEvents();
		$json = json_decode($this->webservices_hugo->syncTicket("11377"));	
		
		foreach($json as $msg){
			
			print_r($msg);
			$this->db->select('*');
			$this->db->from('comentario_gde');
			$this->db->where('grupo_id',0);
			$this->db->where('DATE_FORMAT(start_time,\'%d/%m/%Y %H:%i\')',$msg->created_at);
			$this->db->where('info',$msg->Details);
			$result = $this->db->get();

			if(!$result){
				#INSERTAR
				echo("Creado");
				$this->db->insert('comentario_gde', array('group_id' => 0, 'start_time' => $msg->created_at, 'info' => $msg->Details, 'user' => 'MDA'));
			}
		}
		
		//Envío de tickets 
		foreach($gruposEventos->result() as $ticket)
		{
			echo($ticket->ID);
			// echo($this->webservices_hugo->syncTicket($ticket->ID));	
			// $ID = escapeshellcmd($ticket->ID);
			// Version Adessa
			// system("/usr/local/bin/php /u01/home/app/splunkge/www/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /u01/home/app/splunkge/id.log &");
			
			// Version Desarrollo
			// system("/opt/lampp/bin/php /opt/lampp/htdocs/gde_test/index.php c_sendAutomaticTicket createTicket ".$ID." >> /opt/lampp/htdocs/gde_test/id.log &");
		}
	}
}


/* End of file C_sendAutomaticTicket.php */
/* Location: ./application/controllers/C_sendAutomaticTicket.php */
