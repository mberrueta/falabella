<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_eliminar_cliente extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cliente');
		$this->load->model('operador');
	}

	public function index($nombre) {
		if($this->cliente->verificar_cliente(urldecode($nombre)))
		{
			$stt = $this->cliente->eliminar_cliente(urldecode($nombre));
			if($stt){
				redirect('supervisor/c_crud_cliente/index/Cliente removido exitosamente');
			}
			else{
				redirect('supervisor/c_crud_cliente/index/Se ha generado un problema al intentar eliminar el Cliente, favor intente nuevamente<br />Si el problema persiste comunicar al administrador');
			}
		}
		else{
			redirect('supervisor/c_crud_cliente/index/Cliente inexistente');
		}
	}
}

/* End of file c_eliminar_cliente.php */
/* Location: ./application/controllers/supervisor/c_eliminar_cliente.php */
