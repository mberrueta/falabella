#!/usr/bin/python
# -*- coding: utf-8 -*-
import suds
from suds import WebFault
from suds.client import Client
from suds.xsd.doctor import Import, ImportDoctor

import socket
import xml.dom.minidom
import sys
import urllib2

from suds.plugin import MessagePlugin
import re
import datetime

reload(sys)
sys.setdefaultencoding("utf-8")

# Logging Options
import logging
# logging.basicConfig(filename='suds.log',format='%(asctime)s %(thread)d %(levelname)s %(message)s',level=logging.DEBUG)
logging.basicConfig(filename='/u01/home/app/splunkge/www/gde_mda/application/logs/ws/suds.log',format='%(asctime)s %(thread)d %(levelname)s %(message)s',level=logging.ERROR)
logging.getLogger('suds.client').setLevel(logging.ERROR)
logging.getLogger('suds.transport').setLevel(logging.ERROR)
logging.getLogger('suds.wsdl').setLevel(logging.ERROR)

webserv = sys.argv[1]

#Conexión con el webservice
file_xml    = 'file:///u01/home/app/splunkge/www/gde_mda/application/models/ws/files/XMLSchema.xsd'
url     = 'http://falabella-itsm.accenture.com/falabella-webservicev3-integracao/integracao.asmx?wsdl'
imp     = Import('http://www.w3.org/2001/XMLSchema', file_xml)
imp.filter.add('http://tempuri.org/')

client = Client(url, doctor=ImportDoctor(imp), cache=None)

# Creacion de un ticket
if webserv == 'createTicket':
	try:
		ticket = client.factory.create('CreateTicketIntegracaoNagiosRequest')
		ticket.Categoria1           = sys.argv[13]                            # Pais.Monitoreo.gruposresultor
		ticket.Categoria2           = sys.argv[14]                            # aplicacion
		ticket.Categoria3           = sys.argv[15]                            # detalle
		ticket.ProductCategoria1    = 'Infraestructura'                       # Estatico
		ticket.ProductCategoria2    = 'TI - Infraestructura tecnológica'      # Estatico
		ticket.ProductCategoria3    = 'Monitoreo'                             # Estatico
		ticket.ProductName          = 'UPS'                                   # Estatico
		ticket.Pais                 = sys.argv[2]
		ticket.Negocio              = sys.argv[3]
		ticket.ServicioAfectados    = sys.argv[4]
		ticket.AreaResponsable      = sys.argv[5]
		ticket.TipoDispositivo      = sys.argv[6]
		ticket.UbicacionDispositivo = sys.argv[7]
		ticket.NombreDispositivo    = sys.argv[8]
		ticket.IPPuerta             = sys.argv[9]
		ticket.FechaHora            = sys.argv[10]
		ticket.Descripcion          = sys.argv[11]
		ticket.DescripcionAlerta    = sys.argv[11]
		# ticket.SupportOrganization  = 'CHILE - GERENCIA OPERACIONES'   # Mantenedor de correos
		#ticket.GrupoResolutor       = 'Core Telecomunicaciones Adessa' # Mantenedor de correos (Encargado)
		ticket.GrupoResolutor       = sys.argv[16]
		ticket.SupportOrganization  = sys.argv[17]
		ticket.TipoTicket           = 'Incidente'              # Estatico
		ticket.ModoSolicitud        = 'Automático'             # Estatico
		ticket.Impacto              = sys.argv[18]
		ticket.Urgencia             = sys.argv[12]
		
		# logging.getLogger('suds.client').error('error message:' + str(ticket))
		result = client.service.CreateTicketIntegracaoNagios(ticket)
		# logging.getLogger('suds.client').error('error message:' + str(result))
		
		if hasattr(result, 'ResultDescription'):
			print `result.ResultCode`+','+`result.IncidentNumber`+','+`result.ResultDescription`
		else:
			print `result.ResultCode`+','+`result.IncidentNumber`
		
	except socket.error as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except socket.timeout as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except suds.WebFault as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')
	except urllib2.URLError as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')

# Consulta de un ticket
if webserv == 'consultaIncidente':
	
	try:
		ticket = client.factory.create('LookupCaseInformationRequest')
		ticket.EmailAddress            = 'usuariointegracao@cervello'
		ticket.CustomerCompany         = 0
		ticket.IncidentNumber          = sys.argv[2]
		ticket.MultiCompanyPeopleLU    = 0
		ticket.UseCustomerCorporateID  = 0
		ticket.CustomerCorporateID     = 0
		
		# logging.getLogger('suds.client').error('error message:' + str(ticket))
		result = client.service.LookupCaseInformation(ticket)
		# logging.getLogger('suds.client').error('error message:' + str(result))
		
		print `result.ResultCode`+','+`result.Status`
		
	except socket.error as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except socket.timeout as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except suds.WebFault as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')
	except urllib2.URLError as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')
	
# Actualización de un ticket
if webserv == 'actualizaEstadoIncidente':
	try:
		ticket = client.factory.create('UpdateIncidentPriorityRequest')
		ticket.EmailAddress = 'usuariointegracao@cervello'
		ticket.CustomerCompany = None
		ticket.IncidentNumber = sys.argv[2]
		ticket.NewStatus = sys.argv[3]
		ticket.ResolutionProdNameNewStatus = None
		ticket.Resolution = sys.argv[4]
		ticket.AppendResDetails = 0
		ticket.CauseCatTier2 = None
		ticket.CauseCatTier3 = None
		ticket.ResolutionCatTier1 = None
		ticket.ResolutionCatTier2 = None
		ticket.ResolutionCatTier3 = None
		ticket.ResolutionProdCatTier1 = None
		ticket.ResolutionProdCatTier2 = None
		ticket.ResolutionProdCatTier3 = None
		ticket.ResolutionProdName = None
		ticket.ResolutionProdManufacturer = None
		ticket.ResolutionProdModelVersion = None
		ticket.StatusReason = None
		ticket.AssigneeFullName = None
		ticket.AssigneeLoginName = None
		ticket.MultiCompanyPeopleLU = 0
		ticket.UseCustomerCorporateID = 0
		ticket.CustomerCorporateID = None
		
		if sys.argv[3] == 'Resuelto - Nivel 2':
			ticket.Cause = '07.No determinado'
			ticket.ResolutionMethod = 'Resuelto por N2'
		else:
			ticket.Cause = None
			ticket.ResolutionMethod = None
		
		# logging.getLogger('suds.client').error('error message:' + str(ticket))
		result = client.service.UpdateIncidentStatus(ticket)
		# logging.getLogger('suds.client').error('error message:' + str(result))
		
		print `result.ResultCode`+','+`result.ResultDescription`
		
	except socket.error as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except socket.timeout as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('fail')
	except suds.WebFault as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')
	except urllib2.URLError as e: 
		logging.getLogger('suds.client').error('error message:' + str(e))
		print('error')
	