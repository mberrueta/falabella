<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_categorias extends CI_Controller {

	private $form_rules = array();

	// //Valida que los mails son correctos
	// public function mail_validate($mail){

	// 	//expresión regular de formato de mail
	// 	$rex = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

	// 	//Genera el match entre el mail y la expresión regular
	// 	preg_match($rex, $mail, $matches, PREG_OFFSET_CAPTURE);

	// 	//print_r($matches);
	// 	//En caso de existir match retorna true en caso contrario false
	// 	if(isset($matches[0])) 
	// 		return true;
	// 	else 
	// 		return false;
		
	// 	return true;

	// 	//DESARROLLO
	// 	//print_r($matches);
	// }

	public function __construct()
	{
		parent::__construct();
		
		// Set form rules in one place so we can re-use them in regular and ajax validation
		$this->new_mailRuler = array(
				array(
					'field'   => 'src_organization',
					'label'   => 'Organización',
					'rules'   => 'required|min_length[8]'
				),
				array(
					'field'   => 'to_mail',
					'label'   => 'Para',
					'rules'   => 'required|min_length[3]'
				),
				array(
					'field'   => 'cc_mail',
					'label'   => 'Copia',
					'rules'   => 'min_length[3]'
				),
				array(
					'field' => 'encargado',
					'label' => 'Encargado',
					'rules' => 'min_length[3]'
				),
				array(
					'field' => 'critical_mayor',
					'label' => 'Correo MDA (Critical-Mayor)',
					'rules' => ''
				),
				array(
					'field' => 'minor',
					'label' => 'Correo MDA (Minor)',
					'rules' => ''
				)
		);
		
		$this->update_sup = array(
				// array(
				// 	'field'   => 'edit_nom_sup',
				// 	'label'   => 'Nombre supervisor',
				// 	'rules'   => 'required|min_length[3]'
				// ),
				// array(
				// 	'field'   => 'edit_app_sup',
				// 	'label'   => 'Apellido supervisor',
				// 	'rules'   => 'required|min_length[3]'
				// ),
				// array(
				// 	'field' => 'edit_nom_usu_sup',
				// 	'label' => 'Nombre de usuario',
				// 	'rules' => 'required|min_length[6]|is_unique[usuario.USU_USER]'
				// ),
				// array(
				// 	'field' => 'edit_pass_usu_sup',
				// 	'label' => 'Password supervisor',
				// 	'rules' => ''
				// ),
				// array(
				// 	'field' => 'edit_pass_usu_sup2',
				// 	'label' => 'Password supervisor',
				// 	'rules' => 'matches[edit_pass_usu_sup]'
				// ),
				// array(
				// 	'field' => 'edit_email_usu_sup',
				// 	'label' => 'E-mail',
				// 	'rules' => 'required|valid_email'
				// ),
				// array(
				// 	'field' => 'edit_anexo_sup',
				// 	'label' => 'Anexo',
				// 	'rules' => 'required|min_length[4]|max_length[4]|numeric'
				// ),
				// array(
				// 	'field' => 'edit_tel_sup',
				// 	'label' => 'Telefono',
				// 	'rules' => 'required'
				// )
		);

		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model( 'categoria' );
	}

	public function index()
	{	
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data = array();
		$data['titulo_pagina'] = 'Mantenedor de categorias';
		// $json_new_sup = $this->form_validation->jquery_options($this->new_sup, site_url('admin/C_crud_supervisor/valida_new_sup'));
		// $data['validacion_nuevo_sup'] = $json_new_sup;
		// $json_update_sup = $this->form_validation->jquery_options($this->update_sup, site_url('admin/C_crud_supervisor/valida_update_sup'));
		// $data['validacion_actualizar_sup'] = $json_update_sup;
		// $this->load->view('admin/v_crud_supervisor',$data);
		$this->load->view('admin/v_crud_categorias',$data);
	}
	
	public function read()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		 echo json_encode( $this->categoria->getListCategory() );
	}
	
	public function mailSupport()
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		echo json_encode( $this->correo->getMailSupport() );
	}

	public function updateMailSupport(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		if(!empty($_POST))
		{
			$mail = $_POST['mail'];
			echo json_encode( $this->correo->updateMailSupport($mail));
		}
		else{
			echo 1;
		}
	} 

	public function edit(){
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		/* Codigo de estado:
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el supervisor
		*/

		//Valida que los mails cc_mail y to_mail sean validos
		//$to_mail = explode(';',$_POST['to_mail']);
		//$to_mail = preg_split( "/ (,|;) /", $_POST['to_mail'] );
		// $to_mail = preg_split( '/(,|;)/', $_POST['to_mail']);
		// $cc_mail = preg_split( '/(,|;)/', $_POST['cc_mail']);
		// //$cc_mail = explode(';',$_POST['cc_mail']);
		// $cont = true;

		// foreach($to_mail as $mail){
		// 	//print_r($mail);
		// 	if(trim($mail) != "" && !$this->mail_validate(trim($mail))) $cont = false;
		// }
		
		// if($cont) foreach($cc_mail as $mail)
		// 	if(trim($mail) != "" && !$this->mail_validate(trim($mail))) $cont = false;
		
		// if( !empty($_POST) && $cont ) {
			$variables = $_POST;
			$respuesta = $this->categoria->updateCategoria($variables);
			echo $respuesta;
		// }
		// else if(!$cont){
		// 	echo "failMail";
		// }
		// else{
		// 	echo "1";
		// }
	}
	
	public function getCategory()
	{			
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		if( isset($_GET['id']) )
		 	echo  json_encode($this->categoria->getCategory( $_GET['id'])[0]);
	}
	
	public function delete(){
		/* Codigo de estado:
			0 = OK
			1 = Error borrando el usuario
			2 = Tiene operadores asignados
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		if(isset($_POST['id'])){
			$respuesta = "";
			$id = $_POST['id'];
			$respuesta = $respuesta."".$this->categoria->deleteCategoria( $id );
			echo $respuesta;
		}
	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2){
			return true;
		}
		else{
			return false;
		}
	}

	public function create_category(){
		/* Codigo de estado:
		   -1 = Error inesperado
			0 = OK
			1 = No existen datos via post
			2 = Problemas para crear el usuario
			3 = Problemas para crear el supervisor
		*/
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		//Valida que los mails cc_mail y to_mail sean validos
		// $to_mail = explode(';',$_POST['to_mail']);
		// $cc_mail = explode(';',$_POST['cc_mail']);
		// $to_mail = preg_split( '/(,|;)/', $_POST['to_mail']);
		// $cc_mail = preg_split( '/(,|;)/', $_POST['cc_mail']);
		
		// $continue = true;
		
		// foreach($to_mail as $mail)
		// 	if(trim($mail) != "" && !$this->mail_validate(trim($mail))) $cont = false;

		// if($continue) foreach($cc_mail as $mail)
		// 	if(trim($mail) != "" && !$this->mail_validate(trim($mail))) $cont = false;
		
		// //Si pasa la validación de mails continua almacenando
		// if( !empty( $_POST ) && $continue) {
			$variables = $_POST;
			$respuesta = $this->categoria->newCategory($variables);
			echo $respuesta;
		// }
		// else if(!$continue){
		// 	echo "failMail";
		// }
		// else{
		// 	echo "1";
		// }
	}
}