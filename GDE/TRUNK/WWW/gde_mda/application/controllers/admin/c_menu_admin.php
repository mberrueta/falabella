<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_menu_admin extends CI_Controller {

	public function index($mensaje='')
	{
		if(!$this->_logeado()){return redirect('c_login/acceso_denegado');}
		
		$data['mensaje'] = $mensaje;
		// $data['iframe']  = "http://192.168.249.176:8080/en-US/account/insecurelogin?username=admin&password=kudaw2011&return_to=/app/gestor_eventos/vision_global?supervisor=*";
		$data['titulo_pagina'] = "GDE -&gt; Menu Administrador";
		// $this->load->view('supervisor/v_crud_operador',$data);
		$this->load->view('admin/v_menu_admin',$data);


	}
	
	private function _logeado(){
		if($this->session->userdata('is_logged_in') && $this->session->userdata('tipo') == 2){
			return true;
		}
		else{
			return false;
		}
	}
}

/* End of file c_menu_admin.php */
/* Location: ./application/controllers/c_menu_admin.php */
