<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Email Template Gestor de Eventos</title>
</head>
<body style="color: #004f94;">
<table border="0" cellspacing="0" cellpadding="0" width="750px" style="border-collapse:collapse">
	<thead>
		<tr>
			<td width="250" colspan="2" style="border:solid silver 1.0pt;border-top:ridge #2c6f76 9.0pt">
				<p class="MsoNormal" style="text-align:center"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#00264c">Centro de Monitoreo de Servicios Clientes Escalamiento<u></u><u></u></span></p>
			</td>
			<td width="500" style="border-top:ridge #2c6f76 9.0pt;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;text-align: center">
				<span style="font-size: 50px;font-family: Gisha;font-weight: bold;color: #0068A1;">e</span>
				<span style="font-size: 25px;font-family: Arial Black;font-weight: bolder;color:#ef8e01;margin-left: -4px;vertical-align: 6px;">)</span>
				<span style="font-size: 35px;font-family: Gisha;font-weight: bold;color: #0068A1;margin-left: 4px;vertical-align: 3px;">entel</span>
			</td>
		</tr>
	</thead>
	<tbody>
		<tr style="min-height:26.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Cliente: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $cliente;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:50.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Unidad / Enlace: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $nodos;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:50.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Codigo de Servico: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $codigos;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Boleta de Reclamo: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $boleta;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Boleta de Red: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					--------
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Fecha de Inicio: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $fecha_creacion;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:26.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Fecha de Termino: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:26.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
				
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:50.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Unidad Resolutora: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $unidad;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
		<tr style="min-height:50.7pt">
			<td width="250" style="border:solid silver 1.0pt;border-top:none;background:#e0e5f0;padding:0cm 5.4pt 0cm 5.4pt;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:4.0pt;margin-left:7.1pt;line-height:115%"><b><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;color:#0f243e">Comentario en Incidencia AR: <u></u><u></u></span></b></p>
			</td>
			<td width="500" colspan="2" style="border-top:none;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;background:#e0e5f0;padding:0cm 0cm 0cm 0cm;min-height:50.7pt">
				<p class="MsoNormal" style="margin-right:0cm;margin-bottom:2.0pt;margin-left:0.2cm"><span style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;">
					<?php echo $comentarios;?>
				<u></u><u></u></span></p>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>