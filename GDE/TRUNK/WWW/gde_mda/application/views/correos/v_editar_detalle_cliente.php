<?php $this->load->view("/".$dir_usuario."/header");?>
<?php $this->load->view("/".$dir_usuario."/lateral");?>
	<section id="main" class="column">
		<article class="module width_full">
			<header>
				<h3>Editar Cliente <?php echo urldecode($nombre); ?></h3>
			</header>
			<div style="margin:15px;"> 
		<?php $hidden = array('nombre' => $nombre);
			echo form_open('supervisor/c_editar_detalle_cliente/editar_asociar_cliente','',$hidden);?>
		<div style="width: 100%;">
			<!--<table width="40%" border="0" cellspacing="2" cellpadding="2">-->
			<table border="0" cellspacing="0" cellpadding="0" style="width: 25%; float: left; margin-right: 5%;">
				<caption>Información básica de Cliente</caption>
				<thead style="background-color: #0073AE; color: white;">
					<tr>
						<th>Atributo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Nombre cliente:</td>
						<td>
							<input type="text" id="cNombre2" name="cNombre2" value="<?php echo urldecode($nombre); ?>" />
						</td>
						<td><span id="nameInfo"></span></td>
					</tr>
					<tr>
						<td>Criticidad:</td>
						<td><?php
						$options = array('0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9');
						echo form_dropdown('cCriticidad2', $options, $criticidad);?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Nombre alias:</td>
						<td>
							<input type="text" id="nom_alias" name="nom_alias" value="<?php echo $alias['nom']; ?>" />
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Correo alias:</td>
						<td>
							<input type="text" id="alias" name="alias" value="<?php echo $alias['mail']; ?>" />
						</td>
						<td></td>
					</tr>
			</table>
			<table id="SLA" border="0" cellspacing="0" cellpadding="0" style="width: 40%; float: left; margin-right: 5%;">
				<caption>SLA Tiempo de respuesta para eventos Activos</caption>
				<thead style="background-color: #0073AE; color: white;">
					<tr>
						<th>Service Level Agreement</th>
						<th>Tiempo en minutos Nivel 1 (Medio)</th>
						<th>Tiempo en minutos Nivel 2 (Alto)</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>&nbsp;</td>
						<td style="background-color: yellow;">&nbsp;</td>
						<td style="background-color: red;">&nbsp;</td>
					</tr>
					<tr>
						<td>SLA Generico</td>
						<td><input type="number" min="1" max="59" name="slaGenericoMedio" value="<?php echo $slaAsociado['crit_NaN']['medio']; ?>"/></td>
						<td><input type="number" min="1" max="59" name="slaGenericoAlto" value="<?php echo $slaAsociado['crit_NaN']['alto']; ?>"/></td>
					</tr>
					<tr>
						<td>SLA criticidad 0</td>
						<td><input type="number" min="1" max="59" name="sla0Medio" value="<?php echo $slaAsociado['crit_0']['medio']; ?>"/></td>
						<td><input type="number" min="1" max="59" name="sla0Alto" value="<?php echo $slaAsociado['crit_0']['alto']; ?>"/></td>
					</tr>
					<tr>
						<td>SLA criticidad 1</td>
						<td><input type="number" min="1" max="59" name="sla1Medio" value="<?php echo $slaAsociado['crit_1']['medio']; ?>"/></td>
						<td><input type="number" min="1" max="59" name="sla1Alto" value="<?php echo $slaAsociado['crit_1']['alto']; ?>"/></td>
					</tr>
					<tr>
						<td>SLA criticidad 2</td>
						<td><input type="number" min="1" max="59" name="sla2Medio" value="<?php echo $slaAsociado['crit_2']['medio']; ?>"/></td>
						<td><input type="number" min="1" max="59" name="sla2Alto" value="<?php echo $slaAsociado['crit_2']['alto']; ?>"/></td>
					</tr>
					<tr>
						<td>SLA criticidad 3</td>
						<td><input type="number" min="1" max="59" name="sla3Medio" value="<?php echo $slaAsociado['crit_3']['medio']; ?>"/></td>
						<td><input type="number" min="1" max="59" name="sla3Alto" value="<?php echo $slaAsociado['crit_3']['alto']; ?>"/></td>
					</tr>
				</tbody>
			</table>
			<div style="padding-bottom: 5%; clear:both;"></div>
		</div>
		<?php
		if($men != ''){
			$mensaje = explode('|',$men);
			echo '<span class="'.$mensaje[0].'">'.$mensaje[1].'</span>';
		}
		
		if($final != ''){
			echo '<h4>Asignación Actual</h4>';
			echo '<table id="clientes" cellspacing="0" cellpadding="0" border="0" width="100%">';
			echo '<thead>';
				echo '<tr">';
				echo '<th>Operador</th>';
				echo '<th>Backup</th>';
				echo '<th>Otros Operadores</th>';
				echo '<th>Analistas</th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
					echo '<tr>';
					echo '<td>';
					if(isset($final['Operador'])){
						echo $final['Operador'];
					}
					echo '</td>';
					echo '<td>';
					if(isset($final['Backup'])){
						echo $final['Backup'];
					}
					echo '</td>';
					echo '<td>';
					if(isset($final['Otros'])){
						echo $final['Otros'];
					}
					echo '</td>';
					echo '<td>';
					if(isset($final['Analistas'])){
						echo $final['Analistas'];
					}
					echo '</td>';
					echo '<tr>';
			echo '<tbody>';
			echo '<table>';
			echo '<br />';
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		
		if($operadores['stt']){
			echo '<h4>Reasignación</h4>';
			echo '<table id="operadores" cellspacing="0" cellpadding="0" border="0" width="100%">';
			echo '<thead>';
				echo '<tr">';
				echo '<th>Operador</th>';
				echo '<th>Backup</th>';
				echo '<th>Otros Operadores</th>';
				echo '<th>Analistas</th>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			echo '<tr>';
			echo '<td><select class="usuarios" id="selectOperador" name="operadores" size="10" width="100%">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select class="usuarios" id="selectBackup" name="backup" size="10">';
				foreach($operadores['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select class="usuarios" id="selectCT1" name="otros[]" size="10" multiple="multiple">';
				foreach($operadores_sinsup['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '<td><select class="usuarios" id="selectAnalistas" name="analistas[]" size="10" multiple="multiple">';
				foreach($analistas['cli']->result() as $operador) :
					echo '<option value="'.$operador->USU_USER.'">'.$operador->OPE_NOM.' '.$operador->OPE_APP.'</option>';
				endforeach;
			echo '</select></td>';
			echo '</tr>';
			echo '</tbody>';
			echo '</table>';
		}else{
			echo '<p>Problema</p>'; #Verificar mensaje
		}
		
		echo form_submit('asociar','Editar Cliente');
		echo form_close();
		
		?><input type="hidden" id="url_js" value="<?php echo site_url(); ?>">
			</div>
		</article>
	</section>
			<div style="clear:both;"></div>
	</div> <!-- contenedor -->
 
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/editar_detalle_cliente.js"></script>

</body>
</html>