<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_crud_cliente extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cliente');
		$this->load->model('operador');
	}
 
	public function index($mensaje='') {
		$data['men']   = $mensaje;
		$data['final'] = $this->obtenerAsociaciones();
		$data['titulo_pagina'] = 'GDE -> Mantenedor de Perfiles';
		$data['dir_usuario'] = 'admin';
		
		$this->load->view('supervisor/v_crud_cliente',$data);
	}

	private function obtenerAsociaciones() {
		$offset = 0;
		
		$datos = $this->cliente->listar_usuarios();
		
		$data = array();
		$j = 0 + $offset;
		foreach ($datos->result() as $row)
		{
			$ddmenu = array();
			
			$ddmenu['Usuario']			= $row->user;
			$ddmenu['Nombre']			= $row->name." ".$row->last_name;
			$ddmenu['Organization']		= "";
			$ddmenu['Herramienta']			= "";
			
			$data2 = $this->cliente->listar_asociaciones_organizacion($row->user);
			$data3 = $this->cliente->listar_asociaciones_herramienta($row->user);
			
			foreach ($data2['cli']->result() as $row){
				$ddmenu['Organization']  .= $row->src_organization."<br />";
			}
			
			foreach ($data3['cli']->result() as $row){
				$ddmenu['Herramienta']  .= $row->src_tool."<br />";
			}
			
			$data[++$j] = $ddmenu;
		}
		
		return $data;
		
		
		/*$datos = $this->cliente->listar_cliente();
		$data = array();
		
		$j = 0 + $offset;
		foreach ($datos['cli']->result() as $row)
		{
			$Id			= $row->src_organization;

			$ddmenu = array();
			
			$ddmenu['Cliente']	= $Id;
			$ddmenu['Otros']		= "";
			
			$data2 = $this->cliente->listar_asociaciones($Id);
			
			foreach ($data2['cli']->result() as $row)
			{
				$nombre = $this->operador->getByuser($row->user);
				$ddmenu['Otros']  .= $nombre->name." ".$nombre->last_name_a."<br />";
			}
			
			$data[++$j] = $ddmenu;
		}
		return $data;*/
	}
}