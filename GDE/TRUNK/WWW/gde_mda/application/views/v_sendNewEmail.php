<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Formulario de Correo</title>
	<link type="text/css" href="<?php echo base_url(); ?>css/reset.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>css/foundation3.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>js/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>js/gen_validatorv4.js" type="text/javascript"></script>
</head>
<body>
	<h3>
		<?php 
			if(isset($message)){
				echo $message;
			}
		?>
	</h3>
	<div class="row">
		<div class="twelve columns centered">
		<?php echo form_open('c_sendNewEmail/sendEmail', array('id' => 'form'));?>
			<fieldset style="margin-top: 0px !important;">
				<legend style="margin-left: 200px; margin-bottom: 10px;">Envío de correo</legend>
				
				<label for="desde">Desde (Alias): </label>
				<input type="text" name="desde_show" value="<?php echo $_POST["alias_nom"]." <".$_POST["alias"].">"?>" readonly="readonly"/>
				<input type="hidden" name="desde" value="<?php echo $_POST["alias"]?>" readonly="readonly" />
				<input type="hidden" name="desde_nom" value="<?php echo $_POST["alias_nom"]?>" readonly="readonly" />
				<label for="para">*Para: </label>
				<textarea name="para" id="para" value="" rows="3" cols="25"><?php echo $_POST["para"]?></textarea>
				<label for="copia">Copia: </label>
				<textarea name="copia" id="copia" value="" rows="3" cols="50"><?php echo $_POST["copia"]?></textarea>
				<label for="asunto">Asunto: </label>
				<input type="text" name="asunto" value="Notificación de Incidente - <?php echo $_POST["mail_nombre"]?> - <?php echo $_POST["mail_tipo_aler"]?>"/>

				<label for="mensaje_mda">Mensaje para Mesa de Ayuda: </label>
				<textarea name="mensaje_mda" rows="3" cols="3"><?php echo htmlspecialchars($_POST["mail_mensaje_mda"])?></textarea>
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				<legend style="margin-left: 200px; margin-bottom: 10px;">Detalles</legend>
				
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="severidad">Severidad: </label>
						<input type="text" name="severidad" id="severidad" value="<?php echo $_POST["mail_severidad"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="pais">País: </label>
						<input type="text" name="pais" id="pais" value="<?php echo $_POST["mail_pais"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="negocio">Negocio: </label>
						<input type="text" name="negocio" id="negocio" value="<?php echo $_POST["mail_negocio"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="servicio">Servicio Afectados: </label>
						<textarea name="servicio" id="servicio" rows="3" cols="50"><?php echo $_POST["mail_servicios"]?></textarea>
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="area">Área responsable: </label>
						<input type="text" name="area" id="area" value="<?php echo $_POST["mail_area"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="tipo_disp">Tipo de Dispositivo: </label>
						<input type="text" name="tipo_disp" id="tipo_disp" value="<?php echo $_POST["mail_tipo_disp"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="ubicacion">Ubicación del Dispositivo: </label>
						<input type="text" name="ubicacion" value="<?php echo $_POST["mail_ubicacion"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="nombre">Nombre dispositivo: </label>
						<input type="text" name="nombre" value="<?php echo $_POST["mail_nombre"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="ip">IP(/puerta):</label>
						<input type="text" name="ip" value="<?php echo $_POST["mail_ip"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="fecha">Fecha/Hora: </label>
						<input type="text" name="fecha" value="<?php echo $_POST["mail_fecha"]?>" />
					</div>
				</div>
				<div class="centered">
					<div style="width: 98%; float: left; margin-right: 35px;">
						<label for="mensaje">Descripción de la alerta:</label>
						<textarea name="mensaje" rows="10" cols="50"><?php echo htmlspecialchars($_POST["mail_mensaje"])?></textarea>
					</div>
				</div>
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				<legend style="margin-left: 200px; margin-bottom: 10px;">Observaciones</legend>
				<label for="obs">Observaciones: </label>
				<textarea name="obs" id="obs" value="" rows="3" cols="50"></textarea>
				<input type="hidden" name="mail_tipo_aler" value="<?php echo $_POST["mail_tipo_aler"]?>" />
				<input type="hidden" name="group_id" value="<?php echo $_POST["group_id"]?>" />
				<input type="hidden" name="fechactual" value="" />
				
				<!--<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">
				<legend style="margin-left: 200px; margin-bottom: 10px;">Firma</legend>
				<label for="nombre">Nombre: </label>
				<input type="text" name="nombre" value="" />
				<label for="firma">Glosa Firma: </label>
				<textarea name="firma" value="" rows="3" cols="50"></textarea>
				<div class="centered">
					<div style="width: 46%; float: left; margin-right: 35px;">
						<label for="email">E-mail: </label>
						<input type="text" name="email" value="" />
					</div>
					<div style="width: 46%; float: left;">
						<label for="fono">Telefono: </label>
						<input type="text" name="fono" value="" />
					</div>
				</div>
				
				<hr style="border-width: 4px 0 0 !important; margin: 5px 0 12px !important;">-->
				<input type="submit" value="Enviar" id="envioForm" name="my_submit"/>
				<input type="button" id="btncancelar" value="Cancelar" />
			</fieldset>
		<?php echo form_close();?>
		
		</div>
	</div>
	<script type="text/javascript">
	
	
	var frmvalidator = new Validator("form");
	frmvalidator.addValidation("para","req","Favor llenar campo PARA ");
	/*frmvalidator.addValidation("copia","req","Favor llenar campo COPIA");
	frmvalidator.addValidation("cliente","req","Favor llenar campo CLIENTE");
	frmvalidator.addValidation("obs","req","Favor llenar campo OBSERVACIONES");*/
	
	function load(){
		var da = new Date();
		var fechact = da.toLocaleString();
		$('#form input[name="fechactual"]').val(fechact);
	}
	
	$('#btncancelar').click(function(event){
		window.close();
	});
	
	$('#envioForm').click(function(event){
		$('#envioForm').attr('disabled','disabled');
		$('#form').submit();
	});
	
	$('#para').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	$('#copia').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	$('#cliente').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	$('#obs').focus(function() {
		$('#envioForm').removeAttr('disabled');
	});
	</script>
</body>
</html>