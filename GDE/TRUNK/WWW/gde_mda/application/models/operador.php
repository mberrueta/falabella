<?php

class Operador extends CI_Model {
	
	function getAll(){
	
		//$this->db->select('*');
		$this->db->select('IFNULL(operador_gde.name, "") AS OPE_NOM', false);
		$this->db->select('IFNULL(operador_gde.last_name_a, "") AS OPE_APP', false);
		$this->db->select('IFNULL(operador_gde.last_name_b, "") AS OPE_APM', false);
		$this->db->select('IFNULL(operador_gde.user, "") AS USU_USER', false);
		$this->db->select('IFNULL(usuario_gde.user_mail, "") AS USU_EMAIL', false);
		$this->db->select('IFNULL(operador_gde.annexed, "") AS OPE_ANE', false);
		$this->db->select('IFNULL(operador_gde.phone, "") AS OPE_TEL', false);
		$this->db->select('IFNULL(operador_gde.user_sup, "") AS SUP_USU_USER', false);
		$this->db->select('usuario_gde.user_category', false);
		$this->db->from('operador_gde');
		$this->db->join('usuario_gde', 'operador_gde.user = usuario_gde.user');
		if($this->session->userdata('tipo') == 0){
			$this->db->where('operador_gde.user',$this->session->userdata('username'));
		}
		$query = $this->db->get();
		if( $query->num_rows() > 0 ) {
			return $query->result();
		} 
		else {
			return array();
		}
	}
	
	function listar_operadores($supervisor, $es_admin=false){
		#$ex1 = 'SELECT `CLI_ID` FROM `grupo` join `operador` ON `grupo`.`USU_USER`=`operador`.`USU_USER` WHERE `operador`.`SUP_USU_USER`=\'$supervisor\' AND `grupo`.`GRU_TIPO`='T'';
		
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('usuario_gde.user, name, last_name_a');
			$this->db->from('operador_gde');
			$this->db->join('usuario_gde','operador_gde.user=usuario_gde.user');
			if($es_admin == false)
				$this->db->where('operador_gde.user_sup',$supervisor);
			$this->db->where('usuario_gde.user_category !=',4);
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	function listar_organizaciones(){
		
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('src_organization');
			$this->db->from('contacto_dispositivo_gde');
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	function OperadoresSinSup(){
		
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('usuario_gde.user, name, last_name_a');
			$this->db->from('operador_gde');
			$this->db->join('usuario_gde','operador_gde.user=usuario_gde.user');
			$this->db->where('operador_gde.user_sup',null);
			$this->db->where('usuario_gde.user_category !=',4);
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	function listar_analistas(){
		
		$result = Array();
		$result['stt'] = FALSE;
		$result['cli'] = NULL;
		
		$this->db->trans_start();
			$this->db->select('usuario.USU_USER, OPE_NOM, OPE_APP');
			$this->db->from('operador');
			$this->db->join('usuario','operador.USU_USER=usuario.USU_USER');
			// $this->db->where('operador.SUP_USU_USER',null);
			$this->db->where('usuario.USU_TIPO',4);
			$cli = $this->db->get();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === TRUE){
			$result['stt'] = TRUE;
			$result['cli'] = $cli;
		}
		return $result;
	}
	
	public function asociaSup($supervisor, $operador){
		$asocia = array(
			'user_sup' => $supervisor
		);
		$this->db->where('user', $operador);
		$respuesta = $this->db->update('operador_gde', $asocia);
		return $respuesta;
	}
	
	public function getByuser( $user ) {
		$this->db->select('operador_gde.name, operador_gde.last_name_a, operador_gde.annexed, operador_gde.phone, usuario_gde.user, usuario_gde.user_mail');
		$this->db->from('operador_gde');
		$this->db->join('usuario_gde', 'operador_gde.user = usuario_gde.user');
		$this->db->where('usuario_gde.user', $user);
		$this->db->limit(1);
		$query = $this->db->get();
		if( $query->num_rows() > 0 ) {
			return $query->row();
		} else {
			return array();
		}
	}
	
	public function create($usuario){
		/* Crear el usuario en la BD */
	
		$this->db->trans_start();
		$user = array(
			'user'  => $usuario['nom_usu_ope'],
			'user_pass'  => md5($usuario['pass_usu_ope']),
			'user_category'  => $usuario['tipo_usu_ope'], /* Codigo 1 de operador || 4 super operador */
			'user_mail' => $usuario['email_usu_ope'] 
		);
		$this->db->insert('usuario_gde', $user);
		log_message('error', $this->db->last_query());
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 2;
		}
		else{
			/*Crear el operador*/
			$this->db->trans_start();
			$operador = array(
				'user'     => $usuario['nom_usu_ope'],
				//'SUP_USU_USER' => $this->session->userdata('username'),
				'name'      => $usuario['nom_ope'],
				'last_name_a'      => $usuario['app_ope'],
				'last_name_b'      => $usuario['apm_ope'],
				'annexed'      => $usuario['anexo_ope'],
				'phone'      => $usuario['tel_ope'],
				
			);
			
			$operador['user_sup'] = NULL;
			
			$this->db->insert('operador_gde', $operador);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 3; /*No se pudo guardar el operador en la BD*/
			}
			else{
				return 0; /*Se inserto correctamente el usuario y el operador */
			}
		}
		return -1;
	}
	
	public function edit($usuario, $cambia_pass=false){
		/* Actualizar el usuario en la BD */
		$this->db->trans_start();
		if($cambia_pass){
			$user = array(
				'user_pass'  => md5($usuario['edit_pass_usu_ope']),
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		else{
			$user = array(
				'user_mail' => $usuario['edit_email_usu_ope'] 
			);
		}
		
		$this->db->where('user', $usuario['edit_nom_usu_ope']);
		$this->db->update('usuario_gde', $user);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; //Problemas modificando usuario
		}
		else{
			/*Modificar el operador*/
			$this->db->trans_start();
			$operador = array(
				'name'      => $usuario['edit_nom_ope'],
				'last_name_a'      => $usuario['edit_app_ope'],
				'last_name_b'      => $usuario['edit_apm_ope'],
				'annexed'      => $usuario['edit_anexo_usu_ope'],
				'phone'      => $usuario['edit_tel_usu_ope']
			);
			$this->db->where('user', $usuario['edit_nom_usu_ope']);
			$this->db->update('operador_gde', $operador);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return 2; /*No se pudo modificar el operador en la BD*/
			}
			else{
				return 0; /*Se modifico correctamente el usuario y el operador */
			}
		}
		return -1;
	}
	
	public function delete($user){

		$this->db->trans_start();
		
			$this->db->where('user', $user);
			$this->db->delete('grupo_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('operador_gde');
			$this->db->flush_cache();
		
			$this->db->where('user', $user);
			$this->db->delete('usuario_gde');
			$this->db->flush_cache();
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 1; /*No se pudo eliminar el operador en la BD*/
		}
		else{
			return 0; /*Se elimino correctamente el usuario y el operador */
		}
	}

	public function obtener_estado($user){
		/* estado:	0 => trabajando, 
					1 => receso,
					2 => cambio de turno 
					3 => cambio de turno y backup*/

		$data['stt'] = 0;  //estado del operador
		$data['lbl'] = 'OPERADOR TRABAJANDO'; //label del mensaje a desplegar

		$sql    = "SELECT GRU_TIPO FROM grupo WHERE USU_USER='".$user."' AND GRU_STT=1 GROUP BY GRU_TIPO";
		$result = $this->db->query($sql);
		
		if($result->num_rows() == 1 && $result->row()->GRU_TIPO != 'B'){
			$data['lbl'] = 'OPERADOR TRABAJANDO';
			$data['stt'] = 0;
		}
		elseif($result->num_rows() == 2){
		
			$sql_2 = "SELECT operador.OPE_NOM, operador.OPE_APP FROM grupo JOIN operador ON grupo.USU_USER=operador.USU_USER WHERE CLI_ID=ANY(SELECT CLI_ID FROM `grupo` WHERE USU_USER='".$user."' AND GRU_TIPO='B' AND GRU_STT=1) AND GRU_TIPO='T'";
			$result_2 = $this->db->query($sql_2);
			
			$label_bkp = Array();
			foreach($result_2->result() as $row){
				array_push($label_bkp, $row->OPE_NOM." ".$row->OPE_APP);
			}			
			
			if(sizeof(array_unique($label_bkp))){
			$data['lbl'] = 'OPERADOR TRABAJANDO + <span style="color: #FF0000;">BACKUP DE: '.implode(', ', array_unique($label_bkp)).'</span>';
			$data['stt'] = 3;
			}
			else{
				$data['lbl'] = 'OPERADOR TRABAJANDO';
				$data['stt'] = 3;
			}
		}
		else{
			$sql_2 = "SELECT GRU_TIPO FROM grupo WHERE CLI_ID=ANY(SELECT CLI_ID FROM grupo WHERE USU_USER='".$user."' AND GRU_TIPO='T') AND GRU_STT=1 GROUP BY GRU_TIPO";
			$result_2 = $this->db->query($sql_2);
			
			if($result_2->row()->GRU_TIPO == 'B'){
				$data['lbl'] = 'OPERADOR EN RECESO';
				$data['stt'] = 1;
			}
			else{
				$data['lbl'] = 'OPERADOR EN CAMBIO TURNO';
				$data['stt'] = 2;
			}
		}
		return json_encode($data);
		
	}
	
	public function cambiar_estado($user,$variable){
		## $variable: 0 -> trabajando, 1 => receso, 2 => cambio de turno

		## Consulta mis datos por cada cliente
		$this->db->trans_start();
		
			$flag = True;
		
			$this->db->select('CLI_ID, GRU_STT, GRU_TIPO');
			$this->db->from('grupo');
			$this->db->where('USU_USER',$user);
			$datos = $this->db->get();
			$this->db->flush_cache();

			## Si realiza un pedido de TRABAJANDO.
			if($variable == 0){
			## Reviso mi estado y mi rol por cada cliente
				foreach($datos->result() as $row)
				{
					## Si soy operador
					if($row->GRU_TIPO == 'T'){
						if($row->GRU_STT == 0){
							$flag = False;
						
							## Se cambia estado a 1 del operador
							$data = array('GRU_STT' => 1);
							$this->db->where('GRU_TIPO', 'T');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 0 del backup
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'B');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 0 del cambio de turno 1
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'T1');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 0 del cambio de turno 2
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'T2');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
						}
					}
				}
			}
			
			## Si realiza un pedido de RECESO.
			elseif($variable == 1){
				## Validar si algun Backup esta en receso.
				$sql = "SELECT * FROM `grupo` WHERE `GRU_TIPO`='T' AND `GRU_STT`=0 AND `USU_USER` = ANY (SELECT `USU_USER` FROM `grupo` WHERE `GRU_TIPO`='B' AND `CLI_ID` = ANY (SELECT `CLI_ID` FROM `grupo` WHERE `USU_USER`=? AND `GRU_TIPO`='T' AND `GRU_STT`=1))";
				$query = $this->db->query($sql, array($user));
				$this->db->flush_cache();
				if($query->num_rows>0){
				
					$operadores = array();
					foreach($query->result() as $row){
						$operador = $this->db->get_where('operador', array('USU_USER'=>$row->USU_USER) );
						$this->db->flush_cache();
						
						array_push($operadores, $operador->row()->OPE_NOM.' '.$operador->row()->OPE_APP);
					}
					$operadores = array_unique($operadores);
					return "0|El Backup(s) ".implode(', ',$operadores)." se encuentra en receso";
				}
				
				## Reviso mi estado y mi rol por cada cliente
				foreach($datos->result() as $row)
				{
					## Si soy operador
					if($row->GRU_TIPO == 'T'){
						## Si se encuentra activo
						if($row->GRU_STT == 1){
							$flag = False;
						
							## Se cambia estado a 0 del operador
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'T');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 1 del backup
							$data = array('GRU_STT' => 1);
							$this->db->where('GRU_TIPO', 'B');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
						}
					}
					
					## Si soy backup
					if($row->GRU_TIPO == 'B'){
						## Si se encuentra activo
						if($row->GRU_STT == 1){
							$flag = False;
						
							## Se cambia estado a 1 del operador
							$data = array('GRU_STT' => 1);
							$this->db->where('GRU_TIPO', 'T');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 0 del backup
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'B');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
						}
					}
				}
			}
			
			## Si realiza un pedido de cambio de turno
			/*if($variable == 2){
			## Reviso mi estado y mi rol por cada cliente
				foreach($datos->result() as $row)
				{
					## Si soy operador
					if($row->GRU_TIPO == 'T'){
						if($row->GRU_STT == 1){
							$flag = False;
							
							## Se cambia estado a 0 del operador
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'T');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 1 del cambio de turno
							$data = array('GRU_STT' => 1);
							$this->db->where('GRU_TIPO', 'T1');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
						}
					}
					## Si soy operador-backup
					if($row->GRU_TIPO == 'B'){
						if($row->GRU_STT == 1){
							$flag = False;
						
							## Se cambia estado a 0 del operador-backup
							$data = array('GRU_STT' => 0);
							$this->db->where('GRU_TIPO', 'B');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
							
							## Se cambia estado a 1 del cambio de turno
							$data = array('GRU_STT' => 1);
							$this->db->where('GRU_TIPO', 'T1');
							$this->db->where('CLI_ID', $row->CLI_ID);
							$this->db->update('grupo', $data);
							$this->db->flush_cache();
						}
					}
					## Si soy turno de noche
					if($row->GRU_TIPO == 'T1'){
						if($row->GRU_STT == 1){
						
							$dia = date("w");
							## 0 (para domingo) hasta 6 (para sabado)
							## $dia = 6;
							if($dia == 6 || $dia == 0)
							{
								$flag = False;
								
								## Se cambia estado a 0 del operador-turno_noche
								$data = array('GRU_STT' => 0);
								$this->db->where('GRU_TIPO', 'T1');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
								
								## Se cambia estado a 1 a turno fin de semana
								$data = array('GRU_STT' => 1);
								$this->db->where('GRU_TIPO', 'T2');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
							}
							else
							{
								$flag = False;
							
								## Se cambia estado a 0 del operador-turno_noche
								$data = array('GRU_STT' => 0);
								$this->db->where('GRU_TIPO', 'T1');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
								
								## Se cambia estado a 1 a turno fin de semana
								$data = array('GRU_STT' => 1);
								$this->db->where('GRU_TIPO', 'T');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
							}
						}
					}
					## Si soy turno fds
					if($row->GRU_TIPO == 'T2'){
						if($row->GRU_STT == 1){
								$flag = False;
								
								## Se cambia estado a 0 del operador-turno_noche
								$data = array('GRU_STT' => 0);
								$this->db->where('GRU_TIPO', 'T2');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
								
								## Se cambia estado a 1 del cambio de turno
								$data = array('GRU_STT' => 1);
								$this->db->where('GRU_TIPO', 'T');
								$this->db->where('CLI_ID', $row->CLI_ID);
								$this->db->update('grupo', $data);
								$this->db->flush_cache();
						}
					}
				}
			}*/

		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE){
			return "0|Se ha presentado un problema al intentar realizar la acci�n, favor intentar nuevamente.<br />Si el problema persiste comunicar al Administrador.";
		}
		else{
			if($flag){
				return "0|Usted no puede realizar esta accion.";
			}
			else{
				return "1|Se ha realizado su cambio de estado exitosamente.";
			}
		}
	}
	
	/**************************************************
	*            MODIFICACIONES FASE 1.3
	**************************************************/
	
	/* Lista los operadores registrados en el sistema
	*
	*/
	public function obtenerOperadores(){
		$this->db->select('operador.USU_USER');
		$this->db->select('operador.OPE_NOM');
		$this->db->select('operador.OPE_APP');
		$this->db->from('operador');
		$result = $this->db->get();
		
		/*
		$opciones = array();
		foreach($result->result() as $row){
			$opciones[$row->USU_USER] = $row->OPE_NOM." ".$row->OPE_APP;
		}
		return $opciones;
		*/
		
		$html = "";
		foreach($result->result() as $row){
			$html .= "<option value=\"".$row->USU_USER."\">".$row->OPE_NOM." ".$row->OPE_APP."</option>";
		}
		
		return $html;
	}
	
	/* Lista los operadores backup del operador consultado
	*
	*/
	public function obtenerBackups($operador){
		
	}
}
	
?>